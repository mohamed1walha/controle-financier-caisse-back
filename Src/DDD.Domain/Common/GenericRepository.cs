
using DDD.Domain.Interfaces;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;

using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using CommandType = System.Data.CommandType;

namespace DDD.Domain.Common
{
    public abstract class GenericRepository<TContext> : IGenericRepository
        where TContext : DbContext, IDisposable
    {
        protected TContext dbContext;


        public void SetDbContext(DbContext dContext)
        {
            if (dContext != null)
            {
                this.dbContext = (TContext)dContext;
            };
        }


        private List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }

        private T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();
            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                    {
                        pro.SetValue(obj, dr[column.ColumnName].GetType().FullName == "System.DBNull" ? null : dr[column.ColumnName], null);
                        break;
                    }
                    else
                        continue;
                }
            }
            return obj;
        }

        ~GenericRepository()
        {
            Dispose(false);
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this); // so that Dispose(false) isn't called later
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (dbContext != null)
                {
                    dbContext.Dispose();
                }
            }
        }
        protected GenericRepository(TContext context)
        {
            this.dbContext = context;
        }
        protected TContext GetContext()
        {
            return dbContext;
        }

        //public async Task<DataSourceResult> ReadDataFromQueryAsync<T>(string queryString, DataSourceRequest requestModel, DbContext dContext = null, bool useRequestModel = false)
        //{
        //    GC.Collect();
        //    SetDbContext(dContext);
        //    using var connection = new SqlConnection(dbContext.Database.GetDbConnection().ConnectionString);
        //    using var command = new SqlCommand(queryString, connection);
        //    connection.Open();

        //    DataSourceRequest d = new DataSourceRequest();

        //    using (var reader = await command.ExecuteReaderAsync())
        //        if (reader.HasRows)
        //        {

        //            using DataTable tb = new DataTable();
        //            tb.Load(reader);
        //            if (useRequestModel)
        //            {
        //                return await tb.ToDataSourceResultAsync(requestModel);
        //            }
        //            return await tb.ToDataSourceResultAsync(d);

        //        }

        //    return null;
        //}


        //public async Task<List<T>> ReadTypedDataFromQueryAsync<T>(string queryString, DataSourceRequest requestModel, DbContext dContext = null, bool useRequestModel = false)
        //{
        //    var res = await ReadDataFromQueryAsync<T>(queryString, requestModel, dContext, useRequestModel);
        //    return res.Data.Cast<T>().ToList();
        //}

        public async Task<int> ExecuteQuery(string queryString, DbContext dContext = null)
        {

            SetDbContext(dContext);
            using var connection = new SqlConnection(dbContext.Database.GetDbConnection().ConnectionString);
            using var command = new SqlCommand(queryString, connection);
            //DataSourceResult t = new DataSourceResult();

            connection.Open();
            return await command.ExecuteNonQueryAsync();

        }
        public async Task<int> ExecuteQueryByConnectionString(string queryString, string connectionString)
        {
            using var connection = new SqlConnection(connectionString);
            using var command = new SqlCommand(queryString, connection);
            //DataSourceResult t = new DataSourceResult();
            connection.Open();
            return await command.ExecuteNonQueryAsync();
        }
        public async Task<object> ExecuteScalar(string queryString, DbContext dContext = null)
        {

            SetDbContext(dContext);
            using var connection = new SqlConnection(dbContext.Database.GetDbConnection().ConnectionString);
            using var command = new SqlCommand(queryString, connection);
            //DataSourceResult t = new DataSourceResult();

            connection.Open();
            return await command.ExecuteScalarAsync();

        }
        public async Task<object> ExecuteQueryAndReturnValue(string queryString, DbContext dContext = null)
        {
            SetDbContext(dContext);
            using var connection = new SqlConnection(dbContext.Database.GetDbConnection().ConnectionString);
            using var command = new SqlCommand(queryString, connection);
            connection.Open();
            object res = null;
            using (var reader = await command.ExecuteReaderAsync())
            {
                while (reader.Read())
                {
                    res = reader.GetValue(0);
                }


                return res;
            }
        }
        public async Task<List<T>> ReadDataFromQueryAsync<T>(string queryString, DbContext dContext = null)
        {
            GC.Collect();
            SetDbContext(dContext);
            using var connection = new SqlConnection(dbContext.Database.GetDbConnection().ConnectionString);
            using var command = new SqlCommand(queryString, connection);
            connection.Open();
            using (var reader = await command.ExecuteReaderAsync())
                if (reader.HasRows)
                {
                    using DataTable tb = new DataTable();
                    tb.Load(reader);
                    return ConvertDataTable<T>(tb);
                }

            return new List<T>();
        }
        public async Task<List<T>> ReadDataFromQueryByConnectionStringAsync<T>(string queryString, string connectionString)
        {
            GC.Collect();
            using var connection = new SqlConnection(connectionString);
            using var command = new SqlCommand(queryString, connection);
            command.CommandTimeout = 0;
            connection.Open();
            using (var reader = await command.ExecuteReaderAsync())
                if (reader.HasRows)
                {
                    using DataTable tb = new();
                    tb.Load(reader);
                    return ConvertDataTable<T>(tb);
                }

            return new List<T>();
        }
        //public async Task<object> ReadProcedureQueryAsyn(CommandDTO _command, string outPutParamName, DbContext dContext = null)
        //{
        //    GC.Collect();
        //    SetDbContext(dContext);
        //    using var connection = new SqlConnection(dbContext.Database.GetDbConnection().ConnectionString);
        //    connection.Open();
        //    var command = connection.CreateCommand();
        //    command.CommandType = CommandType.StoredProcedure;
        //    command.CommandText = _command.Text;

        //    foreach (var param in _command.parameters)
        //    {
        //        if (param.OutputParam)
        //            command.Parameters.AddWithValue(param.ParamName, param.OutPutParamType).Direction = ParameterDirection.Output;
        //        else command.Parameters.AddWithValue(param.ParamName, param.ParamValue);

        //    }
        //    await command.ExecuteNonQueryAsync();
        //    return command.Parameters[outPutParamName].Value;
        //}

        /// <summary>
        /// Launches the non query.
        /// </summary>
        /// <param name="pSQLCmd">The SQL command.</param>
        /// <param name="pConnectionString">The connection string to use, if null, the default connection string will be used.</param>
        /// <returns></returns>
        //public async Task<int> LaunchNonQuery(string pSQLCmd, string UserName = "Undefined", DbContext dContext = null)
        //{
        //    int result;
        //    using var connection = new SqlConnection(dbContext.Database.GetDbConnection().ConnectionString);
        //    connection.Open();
        //    using (SqlTransaction trans = connection.BeginTransaction())
        //    {
        //        string SetUserNameR = DBAccess.SetUserNameRequest.Replace("@USERNAME", UserName);
        //        try
        //        {
        //            SqlCommand command = new SqlCommand(SetUserNameR, connection, trans);
        //            command.CommandTimeout = 600;
        //            await command.ExecuteNonQueryAsync();
        //            command = new SqlCommand(pSQLCmd, connection, trans);
        //            result = await command.ExecuteNonQueryAsync();
        //            await trans.CommitAsync();
        //        }
        //        catch (Exception ex)
        //        {
        //            await trans.RollbackAsync();
        //            throw ex;
        //        }
        //        finally
        //        {
        //            if (connection.State.Equals(ConnectionState.Open))
        //                await connection.CloseAsync();
        //        }
        //    }

        //    return result;
        //}
    }

}



