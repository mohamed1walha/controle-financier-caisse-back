using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Data;
using System.Linq.Expressions;
using DDD.Domain.Interfaces;

namespace DDD.Domain.Common
{

    public abstract class GenericBaseRepository<TType, TContext> : GenericRepository<TContext>, IGenericBaseRepository<TType> where TType : class
        where TContext : DbContext, IDisposable
    {

        protected GenericBaseRepository(TContext context) : base(context)
        {
            this.dbContext = context;
        }

        public virtual async Task<TType> ExistsByID(object id, DbContext dContext = null)
        {

            SetDbContext(dContext);
            TType res = await GetById(id);

            return res;
        }

        public virtual async Task<List<TType>> GenericGet(
    Expression<Func<TType, bool>> filter = null,
    Func<IQueryable<TType>, IOrderedQueryable<TType>> orderBy = null,
    string includeProperties = "", DbContext dContext = null)
        {
            if (dContext != null)
                SetDbContext(dContext);
            IQueryable<TType> query = this.dbContext.Set<TType>();

            if (filter != null)
            {
                query = query.Where(filter);
            }

            if (includeProperties != null)
            {
                foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    query = query.Include(includeProperty);
                }
            }


            if (orderBy != null)
            {
                return await orderBy(query).ToListAsync();
            }
            else
            {
                return await query.ToListAsync();
            }
        }

        //     public virtual async Task<DataSourceResult> GenericGetDataSourceResult(DataSourceRequest request,
        //Expression<Func<TType, bool>> filter = null,
        //Func<IQueryable<TType>, IOrderedQueryable<TType>> orderBy = null,
        //string includeProperties = "", DbContext dContext = null)
        //     {
        //         if (dContext != null)
        //             SetDbContext(dContext);
        //         IQueryable<TType> query = this.dbContext.Set<TType>();

        //         if (filter != null)
        //         {
        //             query = query.Where(filter);
        //         }

        //         if (includeProperties != null)
        //         {
        //             foreach (var includeProperty in includeProperties.Split
        //             (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
        //             {
        //                 query = query.Include(includeProperty);
        //             }
        //         }


        //         if (orderBy != null)
        //         {
        //             return await orderBy(query).ToDataSourceResultAsync(request);
        //         }
        //         else
        //         {
        //             return await query.ToDataSourceResultAsync(request);
        //         }
        //     }

        public virtual async Task<List<TType>> GetAll(DbContext dContext = null)
        {

            SetDbContext(dContext);
            return await dbContext.Set<TType>().ToListAsync();
        }

        public virtual List<TType> GetAllSync(DbContext dContext = null)
        {

            SetDbContext(dContext);
            return  dbContext.Set<TType>().ToList();
        }

        //public virtual async Task<DataSourceResult> GetAll(DataSourceRequest requestModel, DbContext dContext = null)
        //{

        //    //remove the groups --> not handled with entity framework
        //    //will be handled at the service level
        //    var groups = requestModel.Groups;
        //    requestModel.Groups = null;
        //    var res = await dbContext.Set<TType>().ToDataSourceResultAsync(requestModel);

        //    requestModel.Groups = groups;
        //    return res;
        //}

        public async Task<TType> GetById(object id, DbContext dContext = null)
        {

            SetDbContext(dContext);
            return await dbContext.Set<TType>().FindAsync(id);
        }

        public TType GetByIdSync(object id, DbContext dContext = null)
        {

            SetDbContext(dContext);
            return dbContext.Set<TType>().Find(id);
        }


        public virtual async Task<TType> Edit(TType obj, DbContext dContext = null)
        {

            SetDbContext(dContext);

            var dbEntityEntry = dbContext.Entry<TType>(obj);
            if (dbEntityEntry.State == EntityState.Detached)
            {
                dbContext.Set<TType>().Attach(obj);
            }
            dbEntityEntry.State = EntityState.Modified;
            await dbContext.SaveChangesAsync();

            return obj;
        }

       


        public virtual TType EditSync(TType obj, DbContext dContext = null)
        {

            SetDbContext(dContext);

            var dbEntityEntry = dbContext.Entry<TType>(obj);
            if (dbEntityEntry.State == EntityState.Detached)
            {
                dbContext.Set<TType>().Attach(obj);
            }
            dbEntityEntry.State = EntityState.Modified;
             dbContext.SaveChanges();

            return obj;
        }

        public virtual async Task<TType> Create(TType obj, DbContext dContext = null)
        {

            SetDbContext(dContext);
            GetContext().Entry(obj).State = EntityState.Added;
            try
            {
                await GetContext().SaveChangesAsync();
            }
            catch (Exception e) when (e is DbUpdateException || e is DBConcurrencyException)
            {
                // handle logging
                throw e.InnerException;
            }
            return obj;
        }

        public virtual TType CreateSync(TType obj, DbContext dContext = null)
        {

            SetDbContext(dContext);
            GetContext().Entry(obj).State = EntityState.Added;
            try
            {
                GetContext().SaveChangesAsync();
            }
            catch (Exception e) when (e is DbUpdateException || e is DBConcurrencyException)
            {
                // handle logging
                throw e.InnerException;
            }
            return obj;
        }

        public virtual async Task<List<TType>> CreateMany(List<TType> objs, DbContext dContext = null)
        {

            SetDbContext(dContext);
            foreach (TType obj in objs)
                GetContext().Entry(obj).State = EntityState.Added;
            await GetContext().SaveChangesAsync();
            return objs;
        }


        public virtual async Task<int> Delete(object id, DbContext dContext = null)
        {

            SetDbContext(dContext);
            var entity = await GetById(id);
            if (entity != null)
                GetContext().Set<TType>().Remove(entity);
            return await GetContext().SaveChangesAsync();
        }

        public virtual async Task<int> DeleteMany(List<object> idsToDelete, DbContext dContext = null)
        {

            SetDbContext(dContext);
            TType entity;
            foreach (object id in idsToDelete)
            {
                entity = await GetById(id);
                if (entity != null)
                    GetContext().Set<TType>().Remove(entity);
            }

            var Executed = await GetContext().SaveChangesAsync();
            return Executed;
        }

        public virtual async Task<TType> GenericGetFirstOrDefaultAsync(Expression<Func<TType, bool>> filter = null, Func<IQueryable<TType>, IOrderedQueryable<TType>> orderBy = null, string includeProperties = "", DbContext dContext = null)
        {

            SetDbContext(dContext);
            IQueryable<TType> query = this.dbContext.Set<TType>();

            if (filter != null)
            {
                query = query.Where(filter);
            }

            if (includeProperties != null)
            {
                foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    query = query.Include(includeProperty);
                }
            }


            if (orderBy != null)
            {
                return await orderBy(query).FirstOrDefaultAsync();
            }
            else
            {
                return await query.FirstOrDefaultAsync();
            }
        }


        public virtual TType GenericGetFirstOrDefault(Expression<Func<TType, bool>> filter = null, Func<IQueryable<TType>, IOrderedQueryable<TType>> orderBy = null, string includeProperties = "", DbContext dContext = null)
        {

            SetDbContext(dContext);
            IQueryable<TType> query = this.dbContext.Set<TType>();

            if (filter != null)
            {
                query = query.Where(filter);
            }

            if (includeProperties != null)
            {
                foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    query = query.Include(includeProperty);
                }
            }


            if (orderBy != null)
            {
                return orderBy(query).FirstOrDefault();
            }
            else
            {
                return query.FirstOrDefault();
            }
        }



        public async Task<List<TType>> EditMany(List<TType> obj, DbContext dContext = null)
        {
            SetDbContext(dContext);
            var entity = dbContext.Set<TType>();
            entity.UpdateRange(obj);
            await dbContext.SaveChangesAsync();
            return obj;
        }

    }

}




