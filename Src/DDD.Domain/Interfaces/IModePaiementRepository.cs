using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using Entity;
namespace DDD.Domain.Interfaces
{
    public interface IModePaiementRepository<TEntity> : IGenericBaseRepository<TEntity> where TEntity : Modepaie
    {
        Task<bool> EditAsync(Modepaie res);
        Task<List<ModepaieDTO>> GetAllModePaie();
    }
}
