﻿using DTO;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Domain.Interfaces
{
    public interface ICoffreSortieRepository<TEntity> : IGenericBaseRepository<TEntity> where TEntity : Coffresortie

    {
        Task<List<CoffresortieDTO>> getAllSorties();
        Task<List<CoffresortieDTO>> GetByCoffreId(int id);
    }
}
