﻿using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Domain.Interfaces
{
    public interface IStatusRepository<TEntity> : IGenericBaseRepository<TEntity> where TEntity : Status

    {
        Task<Status> GetByCode(string code);
        Task<List<Status>> checkStatusJoursCaisse(int id);
    }
}
