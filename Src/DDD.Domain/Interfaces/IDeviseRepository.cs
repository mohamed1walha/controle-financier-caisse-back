﻿using DTO;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Domain.Interfaces
{
    public interface IDeviseRepository<TEntity> : IGenericBaseRepository<TEntity> where TEntity : Devise

    {
        Task<List<DeviseDTO>> GetAllByCoffreID(int id);
    }
}
