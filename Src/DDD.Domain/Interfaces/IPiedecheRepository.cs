﻿using DTO;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Domain.Interfaces
{
    public interface IPiedecheRepository<TEntity> : IGenericBaseRepository<TEntity> where TEntity : Piedeche
    {
        Task <List<PiedecheDTO>>GetAllByJoursCaisse(int id);
        Task<List<PiedecheDTO>> GetAllByJoursCaisseComptage(int id);
        bool EditPiedecheWithStatus(Piedeche c, int id);
        //bool EditPiedeche(Piedeche c);
        Task<List<PiedecheDTO>> GetAllByCoffreNull(string code);
        Task<PiedecheDTO> GetByIdWithMontantConvertie(int elt);
        Task<List<PiedecheDTO>> GetAllByJoursCaisseById(int id);
        List<PiedecheDTO> GetAllByJoursCaisseByIdSync(int id);

    }
}
