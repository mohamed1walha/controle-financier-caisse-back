using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
namespace DDD.Domain.Interfaces
{
    public interface IGenericRepository
    {
        public void SetDbContext(DbContext dbContext);
        public Task<List<T>> ReadDataFromQueryAsync<T>(string queryString, DbContext dbContext = null);
        public Task<int> ExecuteQuery(string queryString, DbContext dbContext = null);
       

    }
}


