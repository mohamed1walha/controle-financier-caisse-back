﻿using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Domain.Interfaces
{
    public interface IBilletDetailAliExcepRepository<TEntity> : IGenericBaseRepository<TEntity> where TEntity : BilletDetailAliExcep

    {
    }
}
