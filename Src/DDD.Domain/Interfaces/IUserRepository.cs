﻿using DTO;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Domain.Interfaces
{
    public interface IUserRepository<TEntity> : IGenericBaseRepository<TEntity> where TEntity : User

    {
        Task<UserDTO> GetByUserName(string username);
        Task<List<UserDTO>> GetAllUsers();
        Task<List<UserDTO>> GetAllUsersSimpleData();
        Task<UserDTO> GetByIdWithRole(int id);
        Task<List<UserDTO>> GetAllValidateur();
        Task<List<UserDTO>> GetAllByEtabId(int etabId);
        Task<UserDTO> GetByUserId(int id);
        UserDTO GetByUserIdSync(int id);

    }
}
