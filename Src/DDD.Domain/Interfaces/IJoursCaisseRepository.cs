using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using Entity;


namespace DDD.Domain.Interfaces
{
    public interface IJoursCaisseRepository<TEntity> : IGenericBaseRepository<TEntity> where TEntity : Jourscaisse
    {
        Task<IEnumerable<TEntity>> GetAllAll();
        Task<Jourscaisse> SearchByCode(int numZ, int codeEtab, string codeCaisse);
        Task<List<int>> GetAllZByEtab(int etabId);
        Task<List<string>> GetAllCaisseByEtab(int etabId);
        Task<bool> EditAll(List<Jourscaisse> enveloppesR, int statusReception, int statusEnAttente, int chauffeurId, int emetteurId);
        Task<bool> EditReSyncAll(List<Jourscaisse> enveloppesR, int statusReception, int statusEnAttente, /*int chauffeurId, int emetteurId,*/int motifId, string comments);
        Task <List<JoursCaiseDTO>> GetAllByStatusCompter();
        Task<JoursCaiseDTO> GetFullById(int id);
        Task<List<JoursCaiseDTO>> GetWithModePaie(List<int?> modePaies, List<string> statusAccept,List<string> statusReject);
        Task<List<JoursCaiseDTO>> GetWithModePaieFiltreGetAllByStatusCmptNone(List<int?> modePaies, List<string> statusAccept, List<string> statusReject, DateTime? dateTime1, DateTime? dateTime2);

        Task<List<JoursCaiseDTO>> GetWithModePaieRecep(List<int?> list, List<int?> listStatusAccept, List<int?> listStatusReject);
        Task<List<JoursCaiseDTO>> GetWithModePaieRecepFilter(List<int?> list, List<int?> listStatusAccept, List<int?> listStatusReject, DateTime dateTime1, DateTime dateTime2);
        Task<List<JoursCaiseDTO>> GetWithModePaieRecepFiltreEnveloppeEnAttente(List<int?> list, List<int?> listStatusAccept, List<int?> listStatusReject, DateTime? date1, DateTime? date2);
        Task<List<Jourscaisse>> GetAllWithDateOuvEtab(DateTime? dateTime1, DateTime? dateTimeTo1, int etablissementId);
    }
}
