﻿using DTO;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Domain.Interfaces
{
    public interface ILignePrelevementRepository<TEntity> : IGenericBaseRepository<TEntity> where TEntity : LignePrelevement

    {
        Task<List<LignePrelevementDTO>> GetAllByEnteteId(int id);
        Task<LignePrelevement> EditLigne(LignePrelevement entity);
    }
}
