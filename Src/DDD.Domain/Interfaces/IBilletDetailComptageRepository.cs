﻿using DTO;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Domain.Interfaces
{
    public interface IBilletDetailComptageRepository<TEntity> : IGenericBaseRepository<TEntity> where TEntity : BilletDetailComptage

    {
        Task<List<BilletDetailComptageDTO>> GetAllByPiedechId(int piedechId);

        List<BilletDetailComptageDTO> GetAllByPiedechIdSync(int piedechId);

    }
}
