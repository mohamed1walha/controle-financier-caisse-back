﻿using DTO;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Domain.Interfaces
{
    public interface ICoffreAlimentationRepository<TEntity> : IGenericBaseRepository<TEntity> where TEntity : Coffrealimentation

    {
        Task<List<CoffrealimentationDTO>> GetByCoffreId(int id);
    }
}
