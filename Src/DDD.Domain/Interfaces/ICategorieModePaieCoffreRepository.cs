﻿using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Domain.Interfaces
{
    public interface ICategorieModePaieCoffreRepository<TEntity> : IGenericBaseRepository<TEntity> where TEntity : CategorieModepaieCoffre

    {
    }
}
