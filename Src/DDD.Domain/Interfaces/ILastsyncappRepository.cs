using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity;
namespace DDD.Domain.Interfaces
{
    public interface ILastsyncappRepository<TEntity> : IGenericBaseRepository<TEntity> where TEntity : Lastsyncapp
    {
    }
}
