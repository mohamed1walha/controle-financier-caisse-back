﻿using DTO;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Domain.Interfaces
{
    public interface ICompteDepenseRepository<TEntity> : IGenericBaseRepository<TEntity> where TEntity : CompteDepense

    {
        Task<List<CompteDepenseDTO>> GetAllCompteDepense();
    }
}
