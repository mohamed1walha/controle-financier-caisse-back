﻿using DTO;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Domain.Interfaces
{
    public interface IEntetePrelevementRepository<TEntity> : IGenericBaseRepository<TEntity> where TEntity : EntetePrelevement

    {
        Task<int> EditEntete(EntetePrelevement entity);
        Task<List<EntetePrelevementDTO>> GetAllByEtablissementIdAndUserId(int etablissementId, int userId);
        Task<List<EntetePrelevementDTO>> GetAllByValidateur(int userId);
        Task<List<EntetePrelevementDTO>> GetAllByDemandeur(int userId, bool isAccepted);
        Task<List<EntetePrelevementDTO>> GetAllByEtabIdCaisseIdNumZRef(int etabId, string caisseId, string numZRef);
        Task<List<EntetePrelevementDTO>> GetAllEntete();
        Task<EntetePrelevementDTO> GetByEnteteId(int id);
        //Task<List<EntetePrelevementDTO>> GetAllWithFilter(FilterPrelevementDTO filterPrelevementDTO);
        Task<PagedResults> GetAllEnteteLazy(GridStateDTO grid, FilterPrelevementDTO filters);


    }
}
