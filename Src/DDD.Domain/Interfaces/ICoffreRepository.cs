﻿using DTO;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Domain.Interfaces
{
    public interface ICoffreRepository<TEntity> : IGenericBaseRepository<TEntity> where TEntity : Coffre

    {
        Task<List<CoffreDTO>>GetAllCoffre();
    }
}
