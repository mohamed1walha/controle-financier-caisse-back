﻿using DTO;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Domain.Interfaces
{
    public interface IUserRolesRepository<TEntity> : IGenericBaseRepository<TEntity> where TEntity : UserRoles

    {
        Task<bool> EditAsync(UserRolesDTO entity);
    }
}
