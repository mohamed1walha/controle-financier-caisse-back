﻿using DTO;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Domain.Interfaces
{
    public interface ILigneDepenseRepository<TEntity> : IGenericBaseRepository<TEntity> where TEntity : LigneDepense

    {
        Task<List<LigneDepenseDTO>> GetAllByEnteteId(int id);
        Task<LigneDepense> EditLigne(LigneDepense entity);
        List<LigneDepenseDTO>GetAllByEnteteIdSync(int id);

    }
}
