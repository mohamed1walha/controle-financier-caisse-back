﻿using DTO;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Domain.Interfaces
{
    public interface IEnteteDepenseRepository<TEntity> : IGenericBaseRepository<TEntity> where TEntity : EnteteDepense

    {
        Task<int> EditEntete(EnteteDepense entity);
        Task<List<EnteteDepenseDTO>> GetAllByEtablissementIdAndUserId(int etablissementId, int userId);
        Task<List<EnteteDepenseDTO>> GetAllByValidatorId( int userId);
        Task<List<EnteteDepenseDTO>> GetAllByEtabIdCaisseIdNumZRef(int etabId, string caisseId, int? numZRef);
        Task<List<EnteteDepenseDTO>> GetAllEntete();
        Task<PagedResults> GetAllEnteteLazy(GridStateDTO grid, FilterDepancesDTO filters);
        Task<EnteteDepenseDTO> GetByEnteteId(int id);
    }
}
