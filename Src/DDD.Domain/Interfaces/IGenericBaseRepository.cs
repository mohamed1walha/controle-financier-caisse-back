using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace DDD.Domain.Interfaces
{


    public interface IGenericBaseRepository<TType> : IGenericRepository
        where TType : class
    {

        Task<int> Delete(object id, DbContext dbContext = null);
        Task<TType> Create(TType obj, DbContext dbContext = null);
        TType CreateSync(TType obj, DbContext dbContext = null);

        Task<List<TType>> CreateMany(List<TType> objs, DbContext dContext = null);
        Task<TType> Edit(TType obj, DbContext dbContext = null);
        TType EditSync(TType obj, DbContext dbContext = null);

        Task<List<TType>> GetAll(DbContext dbContext = null);
        List<TType> GetAllSync(DbContext dbContext = null);
        // Task<DataSourceResult> GetAll(DataSourceRequest requestModel, DbContext dbContext = null);
        Task<List<TType>> GenericGet(
    Expression<Func<TType, bool>> filter = null,
    Func<IQueryable<TType>, IOrderedQueryable<TType>> orderBy = null,
    string includeProperties = "", DbContext dbContext = null);
        Task<TType> GenericGetFirstOrDefaultAsync(
  Expression<Func<TType, bool>> filter = null,
  Func<IQueryable<TType>, IOrderedQueryable<TType>> orderBy = null,
  string includeProperties = "", DbContext dbContext = null);

        TType GenericGetFirstOrDefault(
 Expression<Func<TType, bool>> filter = null,
 Func<IQueryable<TType>, IOrderedQueryable<TType>> orderBy = null,
 string includeProperties = "", DbContext dbContext = null);

        Task<TType> GetById(object id, DbContext dbContext = null);
        Task<int> DeleteMany(List<object> idsToDelete, DbContext dbContext = null);
        Task<List<TType>> EditMany(List<TType> obj, DbContext dbContext = null);



        //       Task<DataSourceResult> GenericGetDataSourceResult(DataSourceRequest request,
        //Expression<Func<TType, bool>> filter = null,
        //Func<IQueryable<TType>, IOrderedQueryable<TType>> orderBy = null,
        //string includeProperties = "", DbContext dContext = null);



    }
}

