using AutoMapper;
using DDD.Application.Services;
using DTO;
using DDD.Domain.Interfaces;
using DDD.Domain.Models;
using DTO;
using Entity;

namespace DDD.Application.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
        {
            CreateMap<Customer, CustomerViewModel>();
            CreateMap<Jourscaisse, JoursCaiseDTO>().ReverseMap();
            CreateMap<Piedeche, PiedecheDTO>().ReverseMap();
            CreateMap<Etablissement, EtablissementDTO>().ReverseMap();
            CreateMap<Modepaie, ModepaieDTO>().ReverseMap();
            CreateMap<Depense, DepenseDTO>().ReverseMap();
            CreateMap<Motif, MotifDTO>().ReverseMap();
            CreateMap<Status, StatusDTO>().ReverseMap();
            CreateMap<Parcaisse, ParcaisseDTO>().ReverseMap();
            CreateMap<Emetteur, EmetteurDTO>().ReverseMap();
            CreateMap<Chauffeur, ChauffeurDTO>().ReverseMap();
            CreateMap<Lastsyncapp, LastSyncAppDTO>().ReverseMap();
            CreateMap<Banque, BanqueDTO>().ReverseMap();
            CreateMap<Typefrais, TypeFraisDTO>().ReverseMap();
            CreateMap<UserRoles, UserRolesDTO>().ReverseMap();
            CreateMap<UserModePaie, UserModePaieDTO>().ReverseMap();
            CreateMap<User, UserDTO>().ReverseMap();
            CreateMap<Roles, RolesDTO>().ReverseMap();
            CreateMap<User, LoginDTO>().ReverseMap();
            CreateMap<UserDTO, UpdateUserDTO>().ReverseMap();
            CreateMap<User, UpdateUserDTO>().ReverseMap();
            CreateMap<Coffre, CoffreDTO>().ReverseMap();
            CreateMap<CoffreDTORepo, CoffreDTO>().ReverseMap();
            CreateMap<Coffrealimentation, CoffrealimentationDTO>().ReverseMap();
            CreateMap<CategorieModepaieCoffre, CategorieModePaieCoffreDTO>().ReverseMap();
            CreateMap<Coffresortie, CoffresortieDTO>().ReverseMap();
            CreateMap<Motifresync, MotifresyncDTO>().ReverseMap();
            CreateMap<Devise, DeviseDTO>().ReverseMap();
            CreateMap<BilletDetailSortie, BilletDetailSortieDTO>().ReverseMap();
            CreateMap<BilletDetailComptage, BilletDetailComptageDTO>().ReverseMap();
            CreateMap<BilletDetailAliExcep, BilletDetailAliExcepDTO>().ReverseMap();
            CreateMap<ParamBillet, ParamBilletDTO>().ReverseMap();


            CreateMap<Article, ArticleDTO>().ReverseMap();
            CreateMap<Fournisseur, FournisseurDTO>().ReverseMap();

            CreateMap<CompteDepense, CompteDepenseDTO>().ReverseMap();
            CreateMap<MailDestinataire, MailDestinataireDTO>().ReverseMap();
            CreateMap<ParamMyCash, ParamMyCashDTO>().ReverseMap();

            CreateMap<EnteteDepense, EnteteDepenseDTO>().ReverseMap();
            CreateMap<EntetePrelevement, EntetePrelevementDTO>().ReverseMap();
            CreateMap<LigneDepense, LigneDepenseDTO>().ReverseMap();
            CreateMap<LignePrelevement, LignePrelevementDTO>().ReverseMap();
            CreateMap<JustifsPrelevement, JustifsPrelevementDTO>().ReverseMap();



        }
    }
}
