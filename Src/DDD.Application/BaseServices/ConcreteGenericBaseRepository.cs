﻿using DDD.Domain.Common;
using DDD.Domain.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;

namespace DDD.Application.BaseServices
{
    public class ConcreteGenericBaseRepository<TEntity, DataBaseContext> : GenericBaseRepository<TEntity, DataBaseContext>, IGenericBaseRepository<TEntity>, IDisposable
            where TEntity : class
            where DataBaseContext : DbContext
    {
        public ConcreteGenericBaseRepository(DataBaseContext context) : base(context) { dbContext = context; }


    }
}

    