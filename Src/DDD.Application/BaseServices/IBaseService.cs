﻿using System.Collections.Generic;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;

namespace DDD.Application.BaseServices
{


    public interface IBaseService<TEntityDTO>
    where TEntityDTO : class
    {
        Task<TEntityDTO> GetById(object id, DbContext dbContext = null);
        TEntityDTO GetByIdSync(object id, DbContext dbContext = null);


        Task<List<TEntityDTO>> GetAll(DbContext dbContext = null);
        List<TEntityDTO>GetAllSync(DbContext dbContext = null);
        Task<int> Delete(object id, DbContext dbContext = null);
        Task<TEntityDTO> Edit(TEntityDTO dto, object id, DbContext dbContext = null);
        Task<TEntityDTO> Create(TEntityDTO dto, DbContext dbContext = null);
        Task<List<TEntityDTO>> CreateList(List<TEntityDTO> dto, DbContext dbContext = null);
        List<TEntityDTO> CreateListSync(List<TEntityDTO> dto, DbContext dbContext = null);

        Task<List<TEntityDTO>> UpdateList(List<TEntityDTO> dto, DbContext dbContext = null);
        List<TEntityDTO> UpdateListSync(List<TEntityDTO> dto, DbContext dbContext = null);

        Task<int> DeleteMany(List<object> idsToDelete, DbContext dbContext = null);
        public Task<int> ExecuteQuery(string queryString, DbContext dbContext = null);
        Task<List<TEntityDTO>> ReadDataFromQueryAsync(string queryString, DbContext dbContext = null);
    }
}
