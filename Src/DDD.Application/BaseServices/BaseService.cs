﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System.Linq;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using System.Globalization;

using AutoMapper.Mappers;
using System.Threading.Tasks;
using DDD.Application.utils;
using AutoMapper;

namespace DDD.Application.BaseServices
{
    public abstract class BaseService<TEntityDTO, TEntity, DataBaseContext> : IBaseService<TEntityDTO>
        where TEntityDTO : class
        where TEntity : class
        where DataBaseContext : DbContext
    {
        protected ConcreteGenericBaseRepository<TEntity, DataBaseContext> genericBaseRepository;
        protected IMemoryCache _cache;
        protected IMapper _mapper;

        protected void SetDbContext(DbContext dbContext = null)
        {
            if (dbContext != null)
                genericBaseRepository.SetDbContext(dbContext);
        }
        protected BaseService(DataBaseContext context,IMapper mapper)
        {
            _mapper = mapper;
            genericBaseRepository = new ConcreteGenericBaseRepository<TEntity, DataBaseContext>(context);
        }

        protected BaseService(DataBaseContext context, IMemoryCache cache)
        {
            genericBaseRepository = new ConcreteGenericBaseRepository<TEntity, DataBaseContext>(context);
            _cache = cache;
        }

        public async Task<TEntityDTO> Create(TEntityDTO dto, DbContext dbContext = null)
        {
            SetDbContext(dbContext);
            var entity = _mapper.Map<TEntity>(dto);
            entity = await genericBaseRepository.Create(entity);
            return _mapper.Map<TEntityDTO>(entity);
        }



        public async Task<List<TEntityDTO>> CreateList(List<TEntityDTO> dto, DbContext dbContext = null)
        {
            SetDbContext(dbContext);
            var entity = _mapper.Map<List<TEntity>>(dto);
            foreach (TEntity e in entity)
            {
                await genericBaseRepository.Create(e);
            }

            return _mapper.Map<List<TEntityDTO>>(entity);
        }

        public  List<TEntityDTO> CreateListSync(List<TEntityDTO> dto, DbContext dbContext = null)
        {
            SetDbContext(dbContext);
            var entity = _mapper.Map<List<TEntity>>(dto);
            foreach (TEntity e in entity)
            {
                genericBaseRepository.CreateSync(e);
            }

            return _mapper.Map<List<TEntityDTO>>(entity);
        }

        public async Task<List<TEntityDTO>> UpdateList(List<TEntityDTO> dto, DbContext dbContext = null)
        {
            SetDbContext(dbContext);
            var entity = _mapper.Map<List<TEntity>>(dto);
            foreach (TEntity e in entity)
            {
                await genericBaseRepository.Edit(e);
            }

            return _mapper.Map<List<TEntityDTO>>(entity);
        }

        public List<TEntityDTO> UpdateListSync(List<TEntityDTO> dto, DbContext dbContext = null)
        {
            SetDbContext(dbContext);
            var entity = _mapper.Map<List<TEntity>>(dto);
            foreach (TEntity e in entity)
            {
                 genericBaseRepository.EditSync(e);
            }

            return _mapper.Map<List<TEntityDTO>>(entity);
        }

        public async Task<int> Delete(object id, DbContext dbContext = null)
        {
            SetDbContext(dbContext);
            return await genericBaseRepository.Delete(id);

        }

        public async Task<int> ExecuteQuery(string queryString, DbContext dbContext = null)
        {
            SetDbContext(dbContext);
            return await genericBaseRepository.ExecuteQuery(queryString);
        }

        public async Task<TEntityDTO> Edit(TEntityDTO dto, object id, DbContext dbContext = null)
        {
            SetDbContext(dbContext);
            var entity = await genericBaseRepository.GetById(id);

        
            entity = _mapper.Map<TEntity>(entity);


            entity = await genericBaseRepository.Edit(entity);

            return _mapper.Map<TEntityDTO>(entity);
        }



        public async Task<TEntityDTO> GetById(object id, DbContext dbContext = null)
        {
            SetDbContext(dbContext);
            var entity = await genericBaseRepository.GetById(id);
            return _mapper.Map<TEntityDTO>(entity);

            //return entity.MapTo<TEntityDTO>();
        }

        public TEntityDTO GetByIdSync(object id, DbContext dbContext = null)
        {
            SetDbContext(dbContext);
            var entity = genericBaseRepository.GetByIdSync(id);
            return _mapper.Map<TEntityDTO>(entity);

            //return entity.MapTo<TEntityDTO>();
        }



        public async Task<int> DeleteMany(List<object> idsToDelete, DbContext dbContext = null)
        {
            SetDbContext(dbContext);
            var DeletedCount = await genericBaseRepository.DeleteMany(idsToDelete.Cast<object>().ToList());
            return DeletedCount;
        }



        public async Task<List<TEntityDTO>> GetAll(DbContext dbContext = null)
        {
            SetDbContext(dbContext);
            var result = await genericBaseRepository.GetAll();
            return _mapper.Map<List<TEntityDTO>>(result);
           // return result.MapTo<List<TEntityDTO>>();
        }

        public List<TEntityDTO> GetAllSync(DbContext dbContext = null)
        {
            SetDbContext(dbContext);
            var result = genericBaseRepository.GetAllSync();
            return _mapper.Map<List<TEntityDTO>>(result);
            // return result.MapTo<List<TEntityDTO>>();
        }


        public async Task<List<TEntityDTO>> ReadDataFromQueryAsync(string queryString, DbContext dbContext = null)
        {
            SetDbContext(dbContext);
            var res = await genericBaseRepository.ReadDataFromQueryAsync<TEntity>(queryString);
            return _mapper.Map<List<TEntityDTO>>(res);
        }

        #region EntityWithTranslation

        //public async Task<int> UpdateTranslationDataFromQueryAsync(string tableName, GenericTranslationEntity genericTranslation, DbContext dbContext = null)
        //{
        //    SetDbContext(dbContext);
        //    string query = "UPDATE " + tableName + " SET [FieldTranslation1] = '" + genericTranslation.FieldTranslation1 + "' ,[FieldTranslation2] ='" + genericTranslation.FieldTranslation2 + "' ,[FieldTranslation3] = '" + genericTranslation.FieldTranslation3 + "' ,[FieldTranslation4] = '" + genericTranslation.FieldTranslation4 + "' WHERE Id =" + genericTranslation.Id;
        //    int i = await genericBaseRepository.ExecuteQuery(query);
        //    return i;
        //}




        //public async Task<TEntityDTO> GetByIdFromCach(string key, object id, DbContext dbContext = null)
        //{
        //    if (_cache == null)
        //        //not initialized
        //        return await GetById(id);

        //    Task<TEntityDTO> result;
        //    if (!_cache.TryGetValue(key, out result))
        //        await _cache.Set(key, GetById(id), CacheOptions.GetMemoryCachEntryOptions());
        //    result = _cache.Get(key) as Task<TEntityDTO>;

        //    return await result;
        //}


        //public async Task<List<TEntityDTO>> GetAllFromCache(string key, DbContext dbContext = null)
        //{
        //    if (_cache == null)
        //        //not initialized
        //        return await GetAll(dbContext);

        //    Task<List<TEntityDTO>> result;

        //    if (!_cache.TryGetValue(key, out result))
        //        await _cache.Set(key, GetAll(dbContext), CacheOptions.GetMemoryCachEntryOptions());
        //    result = _cache.Get(key) as Task<List<TEntityDTO>>;

        //    return await result;
        //}





        //Task<GenericTranslationEntity> IBaseService<TEntityDTO>.GetByFieldCodeTranslationDataFromQueryAsync(string tableName, string language, string fieldCode, DbContext dbContext)
        //{
        //    throw new NotImplementedException();
        //}
        //public async Task<int> DeleteWithTranslation(object id, string tableName, string language, DbContext dbContext = null)
        //{
        //    int deletedId = 0;
        //    SetDbContext(dbContext);
        //    deletedId = await genericBaseRepository.Delete(id);
        //    await DeleteTranslationDataFromQueryAsync(tableName, (string)id, language);
        //    return deletedId;

        //}  
        //public async Task<int> DeleteTranslationDataFromQueryAsync(string tableName, string FieldCode, string  language,  DbContext dbContext = null)
        //{
        //    SetDbContext(dbContext);
        //    string query = "delete from " + tableName + " where FieldCode = '" +FieldCode+ "'  and LanguageCode = '" + language+"'";
        //    int i = await genericBaseRepository.ExecuteQuery(query);
        //    return i;
        //}
        #endregion
    }
}
