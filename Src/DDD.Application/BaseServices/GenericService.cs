﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Application.BaseServices
{
    public class GenericService
    {
        protected GenericService()
        {
           
        }
        public async Task<List<T>> ReadDataFromQueryAsync<T>(string conn, string queryString)
        {
            GC.Collect();
            using var connection = new SqlConnection(conn);
            using var command = new SqlCommand(queryString, connection);
            connection.Open();
            using (var reader = await command.ExecuteReaderAsync())
                if (reader.HasRows)
                {
                    using DataTable tb = new DataTable();
                    tb.Load(reader);
                    return ConvertDataTable<T>(tb);
                }

            return new List<T>();
        }
        private List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        private T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();
            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                    {
                        if (pro.Name.Contains("abel"))
                        {

                        }
                        pro.SetValue(obj, dr[column.ColumnName].GetType().FullName == "System.DBNull" ? null : dr[column.ColumnName], null);
                        break;
                    }
                    else
                        continue;
                }
            }
            return obj;
        }
    }
}
