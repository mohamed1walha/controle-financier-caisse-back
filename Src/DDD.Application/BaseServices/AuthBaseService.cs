﻿using AutoMapper;
using DDD.Application.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Application.BaseServices
{
    public abstract class AuthBaseService<TTentityDTO, TEntity, DataBaseContext> : BaseService<TTentityDTO, TEntity, DataBaseContext>, IBaseService<TTentityDTO>
        where TTentityDTO : class
        where TEntity : class
        where DataBaseContext : DbContext,
        new()
    {
        protected ICurrentContextProvider contextProvider;
        protected readonly ContextSession Session;

        protected AuthBaseService(ICurrentContextProvider contextProvider, DataBaseContext dbContext, IMapper _mapper) : base(dbContext, _mapper)
        {
            if (contextProvider != null)
            {
                this.contextProvider = contextProvider;
                //Session = contextProvider.GetCurrentContext();
            }
        }

        protected AuthBaseService(ICurrentContextProvider contextProvider, DataBaseContext dbContext, IMemoryCache cache) : base(dbContext, cache)
        {
            if (contextProvider != null)
            {
                this.contextProvider = contextProvider;
                //Session = contextProvider.GetCurrentContext();
            }
        }


        protected AuthBaseService(DataBaseContext dbContext, IMemoryCache cache) : base(dbContext, cache)
        {
        }
        protected AuthBaseService(DataBaseContext dbContext, DataBaseContext dataBaseContext, IMapper _mapper) : base(dbContext, _mapper)
        {
        }
    }
}
