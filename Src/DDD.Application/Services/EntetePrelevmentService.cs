﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DDD.Application.BaseServices;
using DDD.Application.Interfaces;
using DTO;
using DDD.Domain.Interfaces;
using DDD.Infra.Data.Repository;
using Entity;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using DDD.Application.utils;
using System.Net.Mail;
using System.Text;
using System.IO.Compression;
using DDD.Domain.Models;

namespace DDD.Application.Services
{
    public class EntetePrelevementService<TEntetePrelevementDTO, TEntetePrelevement, DataBaseContext> :
        AuthBaseService<TEntetePrelevementDTO, EntetePrelevement, DataBaseContext>, IEntetePrelevementService<TEntetePrelevementDTO>
       where TEntetePrelevementDTO : EntetePrelevementDTO, new()
       where TEntetePrelevement : EntetePrelevement, new()
       where DataBaseContext : DDDContext, new()
    {
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly IEntetePrelevementRepository<EntetePrelevement> _EntetePrelevementRepository;
        private readonly ILignePrelevementService<LignePrelevementDTO> _lignePrelevementService;
        private readonly IStatusService<StatusDTO> _statusService;
        private readonly IUserService<UserDTO> _userService;

        private readonly ICompteDepenseService<CompteDepenseDTO> _CompteDepenseService;

        private readonly ICompteDepenseRepository<CompteDepense> _compteDepenseRepository;



        public EntetePrelevementService(ICurrentContextProvider contextProvider,
            DataBaseContext dataBaseContext, IMapper mapper,
                                  IEntetePrelevementRepository<EntetePrelevement> EntetePrelevementRepository,
                                  ICompteDepenseRepository<CompteDepense> compteDepenseRepository,
                                  ICompteDepenseService<CompteDepenseDTO> CompteDepenseService,
                                  IConfiguration configuration, IStatusService<StatusDTO> statusService, IUserService<UserDTO> userService,

        ILignePrelevementService<LignePrelevementDTO> lignePrelevementService
                                 ) : base(
           contextProvider, dataBaseContext, mapper)

        {
            _mapper = mapper;
            _EntetePrelevementRepository = EntetePrelevementRepository;
            _configuration = configuration;
            _lignePrelevementService = lignePrelevementService;
            _statusService = statusService;
            _userService= userService;
            _compteDepenseRepository = compteDepenseRepository;
            _CompteDepenseService = CompteDepenseService;

        }

        public async Task<PagedResults> GetAllEnteteLazy(GridStateDTO grid, FilterPrelevementDTO filters)
        {
            return await _EntetePrelevementRepository.GetAllEnteteLazy(grid, filters);
        }

        public async Task<OperationResult> CreateEntetePrelevement(TEntetePrelevementDTO entetePrelevementDTO)
        {
            var entity1 = new CompteDepense();

            var verif = await _EntetePrelevementRepository.GenericGetFirstOrDefaultAsync(x => x.CreateDate.Value.Year == DateTime.Now.Year
            && x.CreateDate.Value.Month == DateTime.Now.Month&&x.EtablissementId==entetePrelevementDTO.EtablissementId);
            if (verif == null || entetePrelevementDTO.TypePrelevement == true)
            {
                var status = await _statusService.GetByCode(STATUS.DEMANDE);
                var statusAcp = await _statusService.GetByCode(STATUS.ACCEPTE);

                string path = _configuration.GetSection("AppSettings")["PathDirectory"];
                if (!File.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                if (entetePrelevementDTO.FileBase64 != null)
                {
                    string file = Path.Combine(path, entetePrelevementDTO.PieceJointe);
                    byte[] fileBytes = Convert.FromBase64String(entetePrelevementDTO.FileBase64);
                    File.WriteAllBytes(file, fileBytes);
                }
                entetePrelevementDTO.CreateDate = DateTime.Now;
                if (entetePrelevementDTO.TypePrelevement == false)
                {
                    var compteDep = await this._compteDepenseRepository.GetById(entetePrelevementDTO.CompteDepenseId);
                    var newSolde = entetePrelevementDTO.MontantTotal + compteDep.Solde;
                    if (compteDep != null && newSolde <= compteDep.MontantLimite)
                    {
                        compteDep.Solde = newSolde;
                         entity1 = _mapper.Map<CompteDepense>(compteDep);
                        entetePrelevementDTO.StatutId = statusAcp.Id;

                    }
                    else
                    {
                        entetePrelevementDTO.StatutId = status.Id;

                    }
                }
                else
                {
                    entetePrelevementDTO.StatutId = status.Id;

                }

                var res = await Create(entetePrelevementDTO);
                if (res != null && entity1?.Id != 0)
                {
                    var update1 = await _compteDepenseRepository.Edit(entity1);

                }

                var entities = _mapper.Map<List<LignePrelevementDTO>>(entetePrelevementDTO.LignePrelevements);
                entities.ForEach(e => e.PrelevementId = res.Id);
                var lignesDTO = await _lignePrelevementService.CreateList(entities);
                var entity = _mapper.Map<List<LignePrelevementDTO>>(lignesDTO);

                res.LignePrelevements = entity;
                if (entetePrelevementDTO.MailTo != null && entetePrelevementDTO.TypePrelevement == true)
                    try
                    {
                        var user = await _userService.GetByUserId((int)entetePrelevementDTO.CreateByUserId);

                        MailMessage mail = new MailMessage();

                        mail.To.Add(entetePrelevementDTO.MailTo);
                        mail.Subject = "Prélèvement exceptionnel";
                        mail.Body = "Prélèvement exceptionnel montant "+ entetePrelevementDTO.MontantTotal +" Dt par "+ user.FirstName +" "+user.LastName;
                        if (!String.IsNullOrEmpty(entetePrelevementDTO.MailCC))
                        {
                            foreach (var elt in entetePrelevementDTO.MailCC.Split(";"))
                            {
                                mail.CC.Add(elt);
                            }
                        }

                        Mail.SendMail(mail);

                    }
                    catch (Exception ex)
                    {
                    }
                return new OperationResult { ErrorOccured=false,Message="",Data=res.Id};
            }
            return new OperationResult { ErrorOccured=true,Message="impossible de create prelevement"};

        }

        public async Task<FileStreamResult> downloadFile(int id)
        {
            var entete = await GetById(id);
            string path = _configuration.GetSection("AppSettings")["PathDirectory"];
            string file = Path.Combine(path, entete.PieceJointe);
            if (!File.Exists(file))
            {
                return null;
            }
            // byte[] filebyte=System.IO.File.ReadAllBytes(file);
            FileStream fileStream = System.IO.File.OpenRead(file);
            var res = new FileStreamResult(fileStream, "image/jpg")
            {
                FileDownloadName = entete.PieceJointe,
            };
            return res;

        }
        public async Task<EntetePrelevementDTO> deleteFile(int id)
        {
            var entete = await GetById(id);
            string path = _configuration.GetSection("AppSettings")["PathDirectory"];
            string file = Path.Combine(path, entete.PieceJointe);
            File.Delete(file);
            entete.PieceJointe = null;
            var entity = _mapper.Map<EntetePrelevement>(entete);
            entity.PieceJointe = null;
            var res = await _EntetePrelevementRepository.EditEntete(entity);
            if (res != 0)
            {
                var resultat = await GetFullEntetePrelevementById(id);
                return resultat;
            }
            return null;

        }

        public async Task<EntetePrelevementDTO> GetFullEntetePrelevementById(int id)
        {
            var entete = await _EntetePrelevementRepository.GetByEnteteId(id);
   
            if (entete != null)
            {

                var lignes = await _lignePrelevementService.GetAllByEnteteId(id);
                entete.LignePrelevements = lignes;
            }
            return entete;

        }

        public async Task<EntetePrelevementDTO> Update(EntetePrelevementDTO entetePrelevement)
        {
            var entete = await GetById(entetePrelevement.Id);
            var statusAcp = await _statusService.GetByCode(STATUS.ACCEPTE);
            var statusRJT = await _statusService.GetByCode(STATUS.REJETE);
            if (entetePrelevement.TypePrelevement == true)
            {
                
                if (entetePrelevement.StatutCode ==statusAcp.Code)
                {
                   
                    entetePrelevement.StatutId = statusAcp.Id;

                }
                else if (entetePrelevement.StatutCode == statusRJT.Code)
                {
                    
                    entetePrelevement.StatutId = statusRJT.Id;

                }
            }else
            {
                if (entetePrelevement.StatutCode == statusAcp.Code)
                {
                    var compteDep = await this._compteDepenseRepository.GetById(entetePrelevement.CompteDepenseId);
                    var newSolde = entetePrelevement.MontantTotal + compteDep.Solde;
                    if (compteDep != null || newSolde > compteDep.MontantLimite)
                    {
                        compteDep.Solde = newSolde;
                        var entity1 = _mapper.Map<CompteDepense>(compteDep);
                        var update1 = await _compteDepenseRepository.Edit(entity1);
                        entetePrelevement.StatutId=statusAcp.Id;
                    }
                }
                else
                {
                    entetePrelevement.StatutId = statusRJT.Id;

                }
            }
           

            if (entete.PieceJointe != entetePrelevement.PieceJointe)
            {
                string path = _configuration.GetSection("AppSettings")["PathDirectory"];
                string fileNew = Path.Combine(path, entetePrelevement.PieceJointe);
                if (entetePrelevement.FileBase64 != null)
                {
                    byte[] fileBytes = Convert.FromBase64String(entetePrelevement.FileBase64);
                    File.WriteAllBytes(fileNew, fileBytes);
                }
                if (entete.PieceJointe != null)
                {
                    string fileOld = Path.Combine(path, entete.PieceJointe);
                    File.Delete(fileOld);
                }
            }
            var ligneList = new List<LignePrelevementDTO>();
            var entity = _mapper.Map<EntetePrelevement>(entetePrelevement);


            var listLigneExit = await _lignePrelevementService.GetAllByEnteteId(entetePrelevement.Id);
            var ligneCreate = new List<LignePrelevementDTO>();
            var ligneDelete = new List<LignePrelevementDTO>();
            var ligneUpdate = new List<LignePrelevementDTO>();

            if (entetePrelevement.LignePrelevements != null && entetePrelevement.LignePrelevements.Count > 0)
            {
                LignePrelevementDTO res = null;
                foreach (var elt in entetePrelevement.LignePrelevements)
                {
                    res = null;
                    var enteteFind = listLigneExit.Find(x => x.Id == elt.Id);
                    elt.PrelevementId = entetePrelevement.Id;
                    if (elt.Id != 0 && enteteFind != null)
                    {
                        ligneUpdate.Add(elt);
                        res = await _lignePrelevementService.Update(elt);
                    }
                    else if (elt.Id != 0 && enteteFind == null)
                    {
                        ligneDelete.Add(elt);
                    }
                    else
                    {
                        res = await _lignePrelevementService.Create(elt);

                    }
                    if (res != null) { ligneList.Add(res); }
                }
            }
            foreach (LignePrelevementDTO elt in listLigneExit)
            {
                var exist = ligneList.Find(x => x.Id == elt.Id);
                if (exist == null)
                {
                    var del = await _lignePrelevementService.Delete(elt.Id);

                }
            }
            var update = await _EntetePrelevementRepository.EditEntete(entity);
            var entetePrelevementDto = await GetFullEntetePrelevementById(update);
          
            return entetePrelevementDto;
        }

        public async Task<List<EntetePrelevementDTO>> GetAllByEtablissementIdAndUserId(int etablissementId, int userId)
        {
            return await _EntetePrelevementRepository.GetAllByEtablissementIdAndUserId(etablissementId, userId);
        }

        public async Task<List<EntetePrelevementDTO>> GetAllByValidateur(int userId)
        {
            return await _EntetePrelevementRepository.GetAllByValidateur(userId);
        }
        public async Task<List<EntetePrelevementDTO>> GetAllByDemandeur(int userId, bool isAccepted)
        {
            return await _EntetePrelevementRepository.GetAllByDemandeur(userId, isAccepted);
        }
        public async Task<List<EntetePrelevementDTO>> GetAllEntete()
        {
            return await _EntetePrelevementRepository.GetAllEntete();
        }

        public async Task<List<EntetePrelevementDTO>> GetAllByEtabIdCaisseIdNumZRef(int etabId, string caisseId, string numZRef)
        {
            return await _EntetePrelevementRepository.GetAllByEtabIdCaisseIdNumZRef(etabId,caisseId,numZRef);
        }
        //public async Task<List<EntetePrelevementDTO>> GetAllWithFilter(FilterPrelevementDTO filterPrelevementDTO)
        //{
        //    return await _EntetePrelevementRepository.GetAllWithFilter(filterPrelevementDTO);
        //}

        public async Task<List<string>> GenerateComptable(List<EntetePrelevementDTO> prelevenetList)
        {

            var resultatGrouppingPvAndEt = prelevenetList
            .GroupBy(item => new { item.EtablissementId, item.DatePrelevement?.Date })
            .Select(group => new EntetePrelevementDTO
            {
                EtablissementId = group.Key.EtablissementId,
                DatePrelevement = group.Key.Date,
                MontantTotal = group.Sum(item => item.MontantTotal),
                Etablissement = group.First().Etablissement
            })
            .ToList();


            List<string> annexes = new List<string>();
            List<string> annexesTitles = new List<string>();


            //annexe 1
            List<GenerationComptableDTO> Annexe1 = new List<GenerationComptableDTO>();
            Annexe1 = this.GenerateAnnexe1(resultatGrouppingPvAndEt, Annexe1);
            List<string> Lignes1 = new List<string>();
            Lignes1 = this.GenerateLignes(Annexe1, Lignes1);
            string AllAnnexe1 = this.GenerateAnnexe(Lignes1);
            annexes.Add(AllAnnexe1);
            annexesTitles.Add("Annexe 1");

            //annexe 2
            List<GenerationComptableDTO> Annexe2 = new List<GenerationComptableDTO>();
            Annexe2 = this.GenerateAnnexe2(resultatGrouppingPvAndEt, Annexe2);
            List<string> Lignes2 = new List<string>();
            Lignes2 = this.GenerateLignes(Annexe2, Lignes2);
            string AllAnnexe2 = this.GenerateAnnexe(Lignes2);
            annexes.Add(AllAnnexe2);
            annexesTitles.Add("Annexe 2");

            //update isCompta

            foreach(var prelevement in prelevenetList)
            {
                prelevement.IsCompta = true;
                var entity = _mapper.Map<EntetePrelevement>(prelevement);
                await this._EntetePrelevementRepository.EditEntete(entity);
            }



            return this.SaveFile(annexesTitles,annexes);
        }

        private List<string> SaveFile(List<string> annexesTitles, List<string> annexes)
        {
            List<string> files = new List<string>();
            string exportDate = DateTime.Now.ToString("dd-MM-yyyy HH-MM-ss");
            string folderPath = @$"C:\Export TRA\Export Annexe {exportDate}"; 

            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
                Console.WriteLine("Folder created successfully.");
            }


            for (int i = 0; i <= annexes.Count-1; i++)
            {
                string fileName = $"{annexesTitles[i]}.tra";

                string filePath = Path.Combine(folderPath, fileName);
                if (!File.Exists(filePath))
                {
                    using (StreamWriter writer = File.CreateText(filePath))
                    {
                        writer.WriteLine(annexes[i]);
                    }

                }

            }

            if (Directory.Exists(folderPath))
            {
                string[] fileNames = Directory.GetFiles(folderPath, "*.tra");
                foreach (string fileName in fileNames)
                {
                    try
                    {
                        string text = File.ReadAllText(fileName);
                        string base64String = ConvertToBase64String(text);
                        files.Add(base64String);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"Erreur lors de la lecture du fichier {fileName}: {ex.Message}");
                    }
                }
                return files;
            }
            else
            {
                Console.WriteLine("Le dossier spécifié n'existe pas.");
            }
            return null;
        }

        static string ConvertToBase64String(string text)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(text);
            return Convert.ToBase64String(bytes);
        }

        private List<string> GenerateLignes(List<GenerationComptableDTO> annexe1, List<string> lignes)
        {
            lignes.Add("!");
            annexe1.ForEach(element =>
            {
                lignes.Add(this.BuildString(element));
            });
            return lignes;
        }

        public string BuildString(GenerationComptableDTO obj)
        {
            string result = "";

            var properties = obj.GetType().GetProperties();

            foreach (var prop in properties)
            {
                if (prop.PropertyType == typeof(ParamsComptableDTO))
                {
                    ParamsComptableDTO param = (ParamsComptableDTO)prop.GetValue(obj);
                    result = result + param.VALUE;
                }
            }
            return result;
        }

        private List<GenerationComptableDTO> GenerateAnnexe1(List<EntetePrelevementDTO> resultatGrouppingPvAndEt, List<GenerationComptableDTO> annexe1)
        {
            resultatGrouppingPvAndEt.ForEach(element =>
            {
                GenerationComptableDTO generationComptableLigne1 = new GenerationComptableDTO();
                GenerationComptableDTO generationComptableLigne2 = new GenerationComptableDTO();

                generationComptableLigne1.JOURNAL.VALUE = this.GenerateValue("CAI", generationComptableLigne1.JOURNAL.VALUE);
                generationComptableLigne2.JOURNAL.VALUE = this.GenerateValue("CAI", generationComptableLigne2.JOURNAL.VALUE);

                generationComptableLigne1.DATECOMPTABLE.VALUE = this.GenerateValue(element.DatePrelevement?.ToString("ddMMyyyy"), generationComptableLigne1.DATECOMPTABLE.VALUE);
                generationComptableLigne2.DATECOMPTABLE.VALUE = this.GenerateValue(element.DatePrelevement?.ToString("ddMMyyyy"), generationComptableLigne2.DATECOMPTABLE.VALUE);


                generationComptableLigne1.TYPE_PIECE.VALUE = this.GenerateValue("OD", generationComptableLigne1.TYPE_PIECE.VALUE);
                generationComptableLigne2.TYPE_PIECE.VALUE = this.GenerateValue("OD", generationComptableLigne2.TYPE_PIECE.VALUE);


                generationComptableLigne1.GENERAL.VALUE = this.GenerateValue("5800000000", generationComptableLigne1.GENERAL.VALUE);
                generationComptableLigne2.GENERAL.VALUE = this.GenerateValue("5400000000", generationComptableLigne2.GENERAL.VALUE);


                generationComptableLigne1.TYPE_CPTE.VALUE = this.GenerateValue("", generationComptableLigne1.TYPE_CPTE.VALUE);
                generationComptableLigne2.TYPE_CPTE.VALUE = this.GenerateValue("", generationComptableLigne2.TYPE_CPTE.VALUE);


                generationComptableLigne1.REFINTERNE.VALUE = this.GenerateValue(string.Concat(element.Etablissement.EtPays, element.DatePrelevement?.ToString("ddMMyyyy")), generationComptableLigne1.REFINTERNE.VALUE);
                generationComptableLigne2.REFINTERNE.VALUE = this.GenerateValue(string.Concat(element.Etablissement.EtPays, element.DatePrelevement?.ToString("ddMMyyyy")), generationComptableLigne2.REFINTERNE.VALUE);

                generationComptableLigne1.LIBELLE.VALUE = this.GenerateValue("RETRAIT ESPECE", generationComptableLigne1.LIBELLE.VALUE);
                generationComptableLigne2.LIBELLE.VALUE = this.GenerateValue("RETRAIT ESPECE", generationComptableLigne2.LIBELLE.VALUE);

                generationComptableLigne1.ECHEANCE.VALUE = this.GenerateValue(element.DatePrelevement?.ToString("ddMMyyyy"), generationComptableLigne1.ECHEANCE.VALUE);
                generationComptableLigne2.ECHEANCE.VALUE = this.GenerateValue(element.DatePrelevement?.ToString("ddMMyyyy"), generationComptableLigne2.ECHEANCE.VALUE);

                generationComptableLigne1.SENS.VALUE = this.GenerateValue("D", generationComptableLigne1.SENS.VALUE);
                generationComptableLigne2.SENS.VALUE = this.GenerateValue("C", generationComptableLigne2.SENS.VALUE);

                string MontantTotal = FormatNumber(element.MontantTotal.ToString());

                generationComptableLigne1.MONTANT1.VALUE = this.GenerateValueInverse(MontantTotal, generationComptableLigne1.MONTANT1.VALUE);
                generationComptableLigne2.MONTANT1.VALUE = this.GenerateValueInverse(MontantTotal, generationComptableLigne2.MONTANT1.VALUE);

                generationComptableLigne1.TYPE_ECRITURE.VALUE = this.GenerateValue("S", generationComptableLigne1.TYPE_ECRITURE.VALUE);
                generationComptableLigne2.TYPE_ECRITURE.VALUE = this.GenerateValue("S", generationComptableLigne2.TYPE_ECRITURE.VALUE);

                generationComptableLigne1.NUMERO.VALUE = this.GenerateValueInverse("1", generationComptableLigne1.NUMERO.VALUE);
                generationComptableLigne2.NUMERO.VALUE = this.GenerateValueInverse("1", generationComptableLigne2.NUMERO.VALUE);

                generationComptableLigne1.DEVISE.VALUE = this.GenerateValue("TND", generationComptableLigne1.DEVISE.VALUE);
                generationComptableLigne2.DEVISE.VALUE = this.GenerateValue("TND", generationComptableLigne2.DEVISE.VALUE);


                generationComptableLigne1.ETABLISSEMENT.VALUE = this.GenerateValue(element.Etablissement.EtPays, generationComptableLigne1.ETABLISSEMENT.VALUE);
                generationComptableLigne2.ETABLISSEMENT.VALUE = this.GenerateValue(element.Etablissement.EtPays, generationComptableLigne2.ETABLISSEMENT.VALUE);

                annexe1.Add(generationComptableLigne1);
                annexe1.Add(generationComptableLigne2);
            });

            return annexe1;
        }

        private List<GenerationComptableDTO> GenerateAnnexe2(List<EntetePrelevementDTO> resultatGrouppingPvAndEt, List<GenerationComptableDTO> annexe2)
        {
            resultatGrouppingPvAndEt.ForEach(async element =>
            {
                var comptedepanse = _CompteDepenseService.GetByEtablissementIdSync(element.EtablissementId);
                GenerationComptableDTO generationComptableLigne1 = new GenerationComptableDTO();
                GenerationComptableDTO generationComptableLigne2 = new GenerationComptableDTO();

                generationComptableLigne1.JOURNAL.VALUE = this.GenerateValue(comptedepanse.JOURNAL, generationComptableLigne1.JOURNAL.VALUE);
                generationComptableLigne2.JOURNAL.VALUE = this.GenerateValue(comptedepanse.JOURNAL, generationComptableLigne2.JOURNAL.VALUE);

                generationComptableLigne1.DATECOMPTABLE.VALUE = this.GenerateValue(element.DatePrelevement?.ToString("ddMMyyyy"), generationComptableLigne1.DATECOMPTABLE.VALUE);
                generationComptableLigne2.DATECOMPTABLE.VALUE = this.GenerateValue(element.DatePrelevement?.ToString("ddMMyyyy"), generationComptableLigne2.DATECOMPTABLE.VALUE);

                
                generationComptableLigne1.TYPE_PIECE.VALUE = this.GenerateValue("OD", generationComptableLigne1.TYPE_PIECE.VALUE);
                generationComptableLigne2.TYPE_PIECE.VALUE = this.GenerateValue("OD", generationComptableLigne2.TYPE_PIECE.VALUE);

                //CompteCaisse dep dynamique
                generationComptableLigne1.GENERAL.VALUE = this.GenerateValue(comptedepanse.CompteCaisseG, generationComptableLigne1.GENERAL.VALUE);
                generationComptableLigne2.GENERAL.VALUE = this.GenerateValue("5800000000", generationComptableLigne2.GENERAL.VALUE);


                generationComptableLigne1.TYPE_CPTE.VALUE = this.GenerateValue("", generationComptableLigne1.TYPE_CPTE.VALUE);
                generationComptableLigne2.TYPE_CPTE.VALUE = this.GenerateValue("", generationComptableLigne2.TYPE_CPTE.VALUE);


                generationComptableLigne1.REFINTERNE.VALUE = this.GenerateValue(string.Concat(element.Etablissement.EtPays, element.DatePrelevement?.ToString("ddMMyyyy")), generationComptableLigne1.REFINTERNE.VALUE);
                generationComptableLigne2.REFINTERNE.VALUE = this.GenerateValue(string.Concat(element.Etablissement.EtPays, element.DatePrelevement?.ToString("ddMMyyyy")), generationComptableLigne2.REFINTERNE.VALUE);

                generationComptableLigne1.LIBELLE.VALUE = this.GenerateValue("ALIMENTATION CAISSE", generationComptableLigne1.LIBELLE.VALUE);
                generationComptableLigne2.LIBELLE.VALUE = this.GenerateValue("ALIMENTATION CAISSE", generationComptableLigne2.LIBELLE.VALUE);

                generationComptableLigne1.ECHEANCE.VALUE = this.GenerateValue(element.DatePrelevement?.ToString("ddMMyyyy"), generationComptableLigne1.ECHEANCE.VALUE);
                generationComptableLigne2.ECHEANCE.VALUE = this.GenerateValue(element.DatePrelevement?.ToString("ddMMyyyy"), generationComptableLigne2.ECHEANCE.VALUE);

                generationComptableLigne1.SENS.VALUE = this.GenerateValue("D", generationComptableLigne1.SENS.VALUE);
                generationComptableLigne2.SENS.VALUE = this.GenerateValue("C", generationComptableLigne2.SENS.VALUE);

                string MontantTotal = FormatNumber(element.MontantTotal.ToString());

                generationComptableLigne1.MONTANT1.VALUE = this.GenerateValueInverse(MontantTotal, generationComptableLigne1.MONTANT1.VALUE);
                generationComptableLigne2.MONTANT1.VALUE = this.GenerateValueInverse(MontantTotal, generationComptableLigne2.MONTANT1.VALUE);

                generationComptableLigne1.TYPE_ECRITURE.VALUE = this.GenerateValue("S", generationComptableLigne1.TYPE_ECRITURE.VALUE);
                generationComptableLigne2.TYPE_ECRITURE.VALUE = this.GenerateValue("S", generationComptableLigne2.TYPE_ECRITURE.VALUE);

                generationComptableLigne1.NUMERO.VALUE = this.GenerateValueInverse("1", generationComptableLigne1.NUMERO.VALUE);
                generationComptableLigne2.NUMERO.VALUE = this.GenerateValueInverse("1", generationComptableLigne2.NUMERO.VALUE);

                generationComptableLigne1.DEVISE.VALUE = this.GenerateValue("TND", generationComptableLigne1.DEVISE.VALUE);
                generationComptableLigne2.DEVISE.VALUE = this.GenerateValue("TND", generationComptableLigne2.DEVISE.VALUE);


                generationComptableLigne1.ETABLISSEMENT.VALUE = this.GenerateValue(element.Etablissement.EtPays, generationComptableLigne1.ETABLISSEMENT.VALUE);
                generationComptableLigne2.ETABLISSEMENT.VALUE = this.GenerateValue(element.Etablissement.EtPays, generationComptableLigne2.ETABLISSEMENT.VALUE);

                annexe2.Add(generationComptableLigne1);
                annexe2.Add(generationComptableLigne2);
            });

            return annexe2;
        }

        private string GenerateValue(string value, string result)
        {
            if (value == null)
            {
                return result;
            }
            else
            {
                return value + result.Substring(value.Length);
            }
        }

        private string GenerateValueInverse(string value, string result)
        {
            if (value == null)
            {
                return result;
            }
            else
            {
                return result.Substring(value.Length) + value;
            }
        }

        private string GenerateAnnexe(List<string> lignes)
        {
            StringBuilder stringBuilder = new StringBuilder();

            foreach (string objet in lignes)
            {
                stringBuilder.Append(objet);
                stringBuilder.Append("\n");
            }

            return stringBuilder.ToString();
        }

        static string FormatNumber(string input)
        {
            // Vérifier si la chaîne contient une virgule
            if (input.Contains(','))
            {
                // Extraire la partie avant et après la virgule
                string[] parts = input.Split(',');

                // Ajouter des zéros à la partie après la virgule si nécessaire
                if (parts.Length > 1 && parts[1].Length < 3)
                {
                    parts[1] = parts[1].PadRight(3, '0');
                }

                // Rejoindre les parties avec un point
                return string.Join(".", parts);
            }
            else
            {
                // Si la chaîne ne contient pas de virgule, ajouter une virgule suivie de trois zéros
                return input + ".000";
            }
        }

        public async Task<PagedResults> GetEntetePrelevements(GridStateDTO grid)
        {
            var results = await this.GetAll();
            if (grid.Page == 0)
                grid.Page = 1;

            if (grid.PageSize == 0)
                grid.PageSize = int.MaxValue;

            var count = results.Count();
            var items = results.Skip((grid.Page - 1) * grid.PageSize).Take(grid.PageSize).ToList();
            var retVal = new PagedResults(items, count, grid.Page, grid.PageSize);
            return retVal;
        }
    }
}
