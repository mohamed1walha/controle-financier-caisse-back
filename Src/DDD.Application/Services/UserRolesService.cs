﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DDD.Application.BaseServices;
using DDD.Application.Interfaces;
using DTO;
using DDD.Domain.Interfaces;
using DDD.Infra.Data.Repository;
using Entity;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;


namespace DDD.Application.Services
{
    public class UserRolesService<TUserRolesDTO, TUserRoles, DataBaseContext> :
        AuthBaseService<TUserRolesDTO, UserRoles, DataBaseContext>, IUserRolesService<TUserRolesDTO>
       where TUserRolesDTO : UserRolesDTO, new()
       where TUserRoles : UserRoles, new()
       where DataBaseContext : DDDContext, new()
    {
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly IUserRolesRepository<UserRoles> _userRolesRepository;

        public UserRolesService(ICurrentContextProvider contextProvider,
            DataBaseContext dataBaseContext, IMapper mapper,
                                  IUserRolesRepository<UserRoles> UserRolesRepository,
                                  IConfiguration configuration
                                 ) : base(
           contextProvider, dataBaseContext, mapper)

        {
            _mapper = mapper;
            _userRolesRepository = UserRolesRepository;
            _configuration = configuration;


        }

        public async Task<bool> EditAsync(UserRolesDTO entity)
        {
            return await _userRolesRepository.EditAsync(entity);
        }

        public async Task<UserRolesDTO> GetByUserIdAndRoleId(int userId, int roleId)
        {
            var res = await _userRolesRepository.GenericGetFirstOrDefaultAsync(x => x.UserId == userId && x.RoleId == roleId);
            return _mapper.Map<UserRolesDTO>(res);
                }
        public async Task<List<UserRolesDTO>> GetByUserId(int userId)
        {
            var res = await _userRolesRepository.GenericGet(x => x.UserId == userId );
            return _mapper.Map<List<UserRolesDTO>>(res);
                }

        public async Task<UserRolesDTO> Update(UserRolesDTO role)
        {
            var entity = _mapper.Map<UserRoles>(role);
            var update = await _userRolesRepository.Edit(entity);
            var userRolesDto = _mapper.Map<UserRolesDTO>(update);
            return userRolesDto;
        }
    }
}
