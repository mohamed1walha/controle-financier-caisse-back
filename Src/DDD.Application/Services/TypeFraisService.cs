﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DDD.Application.BaseServices;
using DDD.Application.Interfaces;
using DTO;
using DDD.Domain.Interfaces;
using DDD.Infra.Data.Repository;
using Entity;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;


namespace DDD.Application.Services
{
    public class TypeFraisService<TTypeFraisDTO, TTypeFrais, DataBaseContext> :
        AuthBaseService<TTypeFraisDTO, Typefrais, DataBaseContext>, ITypeFraisService<TTypeFraisDTO>
       where TTypeFraisDTO : TypeFraisDTO, new()
       where TTypeFrais : Typefrais, new()
       where DataBaseContext : DDDContext, new()
    {
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly ITypeFraisRepository<Typefrais> _TypeFraisRepository;

        public TypeFraisService(ICurrentContextProvider contextProvider,
            DataBaseContext dataBaseContext, IMapper mapper,
                                  ITypeFraisRepository<Typefrais> TypeFraisRepository,
                                  IConfiguration configuration
                                 ) : base(
           contextProvider, dataBaseContext, mapper)

        {
            _mapper = mapper;
            _TypeFraisRepository = TypeFraisRepository;
            _configuration = configuration;


        }

        public async Task<TypeFraisDTO> Update(TypeFraisDTO TypeFrais)
        {
            var entity = _mapper.Map<Typefrais>(TypeFrais);
            var update = await _TypeFraisRepository.Edit(entity);
            var TypeFraisDto = _mapper.Map<TypeFraisDTO>(update);
            return TypeFraisDto;
        }
    }
}
