﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DDD.Application.BaseServices;
using DDD.Application.Interfaces;
using DTO;
using DDD.Domain.Interfaces;
using DDD.Infra.Data.Repository;
using Entity;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;


namespace DDD.Application.Services
{
    public class ParamMyCashService<TParamMyCashDTO, TParamMyCash, DataBaseContext> :
        AuthBaseService<TParamMyCashDTO, ParamMyCash, DataBaseContext>, IParamMyCashService<TParamMyCashDTO>
       where TParamMyCashDTO : ParamMyCashDTO, new()
       where TParamMyCash : ParamMyCash, new()
       where DataBaseContext : DDDContext, new()
    {
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly IParamMyCashRepository<ParamMyCash> _ParamMyCashRepository;

        public ParamMyCashService(ICurrentContextProvider contextProvider,
            DataBaseContext dataBaseContext, IMapper mapper,
                                  IParamMyCashRepository<ParamMyCash> ParamMyCashRepository,
                                  IConfiguration configuration
                                 ) : base(
           contextProvider, dataBaseContext, mapper)

        {
            _mapper = mapper;
            _ParamMyCashRepository = ParamMyCashRepository;
            _configuration = configuration;


        }

        public async Task<ParamMyCashDTO> Update(ParamMyCashDTO ParamMyCash)
        {
            var entity = _mapper.Map<ParamMyCash>(ParamMyCash);
            var update = await _ParamMyCashRepository.Edit(entity);
            var ParamMyCashDto = _mapper.Map<ParamMyCashDTO>(update);
            return ParamMyCashDto;
        }
    }
}
