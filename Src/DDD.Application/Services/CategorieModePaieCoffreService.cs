﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DDD.Application.BaseServices;
using DDD.Application.Interfaces;
using DTO;
using DDD.Domain.Interfaces;
using DDD.Infra.Data.Repository;
using Entity;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;


namespace DDD.Application.Services
{
    public class CategorieModePaieCoffreService<TCategorieModePaieCoffreDTO, TCategorieModePaieCoffre, DataBaseContext> :
        AuthBaseService<TCategorieModePaieCoffreDTO, CategorieModepaieCoffre, DataBaseContext>, ICategorieModePaieCoffreService<TCategorieModePaieCoffreDTO>
       where TCategorieModePaieCoffreDTO : CategorieModePaieCoffreDTO, new()
       where TCategorieModePaieCoffre : CategorieModepaieCoffre, new()
       where DataBaseContext : DDDContext, new()
    {
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly ICategorieModePaieCoffreRepository<CategorieModepaieCoffre> _categorieModePaieCoffreRepository;

        public CategorieModePaieCoffreService(ICurrentContextProvider contextProvider,
            DataBaseContext dataBaseContext, IMapper mapper,
                                  ICategorieModePaieCoffreRepository<CategorieModepaieCoffre> CategorieModePaieCoffreRepository,
                                  IConfiguration configuration
                                 ) : base(
           contextProvider, dataBaseContext, mapper)

        {
            _mapper = mapper;
            _categorieModePaieCoffreRepository = CategorieModePaieCoffreRepository;
            _configuration = configuration;


        }

        public async Task<List<CategorieModePaieCoffreDTO>> GetAllByModePaie(List<int> modePaies)
        {
            modePaies.Select(x => x).Distinct();
            string SourceConnection = _configuration.GetConnectionString("DefaultConnection");

            var query = "select  COFFRE_ID CoffreId , COUNT(1) from CATEGORIE_MODEPAIE_COFFRE where MODEPAIE_ID in (" + String.Join(',', modePaies.Select(x => x).Distinct()) +") group by COFFRE_ID having COUNT(1) = "+ modePaies.Select(x => x).Distinct().Count();
            var coffres = await this.genericBaseRepository.ReadDataFromQueryByConnectionStringAsync<CategorieModePaieCoffreDTO>(query, SourceConnection);

            //  var res= await _categorieModePaieCoffreRepository.GenericGet(x =>
            //  modePaies.ForEach(y => { if (y == x.ModepaieId) { y=x.ModepaieId } } ) );
            return coffres;
        }

        public async Task<List<CategorieModePaieCoffreDTO>> GetByIdCoffre(int elt)
        {
            var req = new List<int>();
            var res = await _categorieModePaieCoffreRepository.GenericGet(x => x.CoffreId == elt);
            res.ForEach(x => { req.Add((int)x.ModepaieId); });
            return _mapper.Map<List<CategorieModePaieCoffreDTO>>(res);
        }

        public async Task<CategorieModePaieCoffreDTO> Update(CategorieModePaieCoffreDTO categorieModePaieCoffre)
        {
            var entity = _mapper.Map<CategorieModepaieCoffre>(categorieModePaieCoffre);
            var update = await _categorieModePaieCoffreRepository.Edit(entity);
            var categorieModePaieCoffreDto = _mapper.Map<CategorieModePaieCoffreDTO>(update);
            return categorieModePaieCoffreDto;
        }
    }
}
