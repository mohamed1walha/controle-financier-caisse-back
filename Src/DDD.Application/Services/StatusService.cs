﻿using AutoMapper;
using DDD.Application.BaseServices;
using DDD.Application.Interfaces;
using DDD.Application.utils;
using DTO;
using DDD.Domain.Interfaces;
using Entity;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;

namespace DDD.Application.Services
{
    public class StatusService<TStatusDTO, TStatus, DataBaseContext> :
        AuthBaseService<TStatusDTO, Status, DataBaseContext>, IStatusService<TStatusDTO>
       where TStatusDTO : StatusDTO, new()
       where TStatus : Status, new()
       where DataBaseContext : DDDContext, new()
    {
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly IStatusRepository<Status> _statusRepository;
        private readonly IJoursCaisseRepository<Jourscaisse> _jourCaisseRepository;
        private readonly IPiedecheService<PiedecheDTO> _piedecheService;

        public StatusService(ICurrentContextProvider contextProvider,
            DataBaseContext dataBaseContext, IMapper mapper,
                                  IStatusRepository<Status> statusRepository,
                                  IConfiguration configuration,
                                  IJoursCaisseRepository<Jourscaisse> jourCaisseRepository,
            IPiedecheService<PiedecheDTO> piedecheService
                                 ) : base(
           contextProvider, dataBaseContext, mapper)

        {
            _mapper = mapper;
            _statusRepository = statusRepository;
            _configuration = configuration;
            _jourCaisseRepository = jourCaisseRepository;
            _piedecheService=piedecheService;
        }

        public async Task<StatusDTO> Update(StatusDTO status)
        {
            var entity = _mapper.Map<Status>(status);
            var update = await _statusRepository.Edit(entity);
            var statusDto = _mapper.Map<StatusDTO>(update);
            return statusDto;
        }
        public async Task<StatusDTO> GetByCode(string code)
        {
            Status statusByCode = await _statusRepository.GetByCode(code);
            var statusDto = _mapper.Map<StatusDTO>(statusByCode);
            return statusDto;
        }

        public async Task<StatusDTO> checkStatus(int id,int userId)
        {
            bool isCash = false;
            var res = await _statusRepository.checkStatusJoursCaisse(id);
            var jourcaisse = await _jourCaisseRepository.GetById(id);
            var pideches = await _piedecheService.GetAllByJoursCaisse(jourcaisse.Id,userId);
            var statusECR = await _statusRepository.GetByCode(STATUS.EN_COURS);
            var statusVLD = await _statusRepository.GetByCode(STATUS.VALIDEE);

            foreach (var elt in pideches)
            {
                if (elt?.Modepaie?.IsCash == true && elt.StatusComptage==statusVLD.Id) { isCash = true; }

                if(elt.Status.Code== STATUS.RECEPTIONNE)
                {
                    elt.StatusComptage = statusECR.Id;
                        elt.Status = null;
                    await _piedecheService.EditPiedeche(elt, statusECR.Id);

                }
            }
            if(jourcaisse==null){return null;}
            Status status;
            var x = 0;

            foreach (var item in res)
            {
                if (item.Code == STATUS.VALIDEE)
                {
                    x++;
                }
            }
            if (res.Count == 0)
            {
                status = await _statusRepository.GetByCode(STATUS.EN_COURS);

            }else if (res.Count > 0 && x == res.Count)
            {
                status = statusVLD;

            }
            else if (res.Count > 1 && x != res.Count&& isCash==true)
            {
                status = await _statusRepository.GetByCode(STATUS.ESPECECOMPTE);

            } 
            else
            {
                status = await _statusRepository.GetByCode(STATUS.EN_COURS);
            }
            if (jourcaisse?.StatusComtage != status.Id)
            {
                jourcaisse.StatusComtage = status.Id;
                if (jourcaisse.ValidationDateDebut == null)
                {
                    jourcaisse.ValidationDateDebut = DateTime.Now;
                }
                if (status.Code == STATUS.VALIDEE)
                {
                    jourcaisse.ValidationDateFin = DateTime.Now;

                }  
                if (status.Code == STATUS.EN_COURS)
                {
                    jourcaisse.ValidationDateFin = null;

                }

                var resd = await _jourCaisseRepository.Edit(jourcaisse);
            }
            return _mapper.Map<StatusDTO>(status);
        }
    }
}
