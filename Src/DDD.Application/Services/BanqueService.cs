﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DDD.Application.BaseServices;
using DDD.Application.Interfaces;
using DTO;
using DDD.Domain.Interfaces;
using DDD.Infra.Data.Repository;
using Entity;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;


namespace DDD.Application.Services
{
    public class BanqueService<TBanqueDTO, TBanque, DataBaseContext> :
        AuthBaseService<TBanqueDTO, Banque, DataBaseContext>, IBanqueService<TBanqueDTO>
       where TBanqueDTO : BanqueDTO, new()
       where TBanque : Banque, new()
       where DataBaseContext : DDDContext, new()
    {
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly IBanqueRepository<Banque> _banqueRepository;

        public BanqueService(ICurrentContextProvider contextProvider,
            DataBaseContext dataBaseContext, IMapper mapper,
                                  IBanqueRepository<Banque> BanqueRepository,
                                  IConfiguration configuration
                                 ) : base(
           contextProvider, dataBaseContext, mapper)

        {
            _mapper = mapper;
            _banqueRepository = BanqueRepository;
            _configuration = configuration;


        }

        public async Task<BanqueDTO> Update(BanqueDTO banque)
        {
            var entity = _mapper.Map<Banque>(banque);
            var update = await _banqueRepository.Edit(entity);
            var banqueDto = _mapper.Map<BanqueDTO>(update);
            return banqueDto;
        }
    }
}
