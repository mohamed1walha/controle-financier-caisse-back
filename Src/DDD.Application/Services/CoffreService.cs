﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DDD.Application.BaseServices;
using DDD.Application.Interfaces;
using DTO;
using DDD.Domain.Interfaces;
using DDD.Infra.Data.Repository;
using Entity;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;


namespace DDD.Application.Services
{
    public class CoffreService<TCoffreDTO, TCoffre, DataBaseContext> :
        AuthBaseService<TCoffreDTO, Coffre, DataBaseContext>, ICoffreService<TCoffreDTO>
       where TCoffreDTO : CoffreDTO, new()
       where TCoffre : Coffre, new()
       where DataBaseContext : DDDContext, new()
    {
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly ICoffreRepository<Coffre> _coffreRepository;
        private readonly ICategorieModePaieCoffreService<CategorieModePaieCoffreDTO> _categorieModePaieCoffreService;
        private readonly ICoffreAlimentationService<CoffrealimentationDTO> _coffreAlimentationService;
        private readonly ICoffreSortieService<CoffresortieDTO> _coffrSortieService;
        private readonly IModePaiementService<ModepaieDTO> _modePaiementService;

        public CoffreService(ICurrentContextProvider contextProvider,
            DataBaseContext dataBaseContext, IMapper mapper,
                                  ICoffreRepository<Coffre> CoffreRepository,
                                  ICategorieModePaieCoffreService<CategorieModePaieCoffreDTO> categorieModePaieCoffreService,
                                  ICoffreAlimentationService<CoffrealimentationDTO> coffreAlimentationService,
                                  IModePaiementService<ModepaieDTO> modePaiementService,
                                  ICoffreSortieService<CoffresortieDTO> coffrSortieService,
                                  IConfiguration configuration
                                 ) : base(
           contextProvider, dataBaseContext, mapper)

        {
            _mapper = mapper;
            _coffreRepository = CoffreRepository;
            _configuration = configuration;
            _categorieModePaieCoffreService = categorieModePaieCoffreService;
            _coffreAlimentationService = coffreAlimentationService;
            _modePaiementService = modePaiementService;
            _coffrSortieService = coffrSortieService;

        }

        public async Task<OperationResult> CreateCoffre(TCoffreDTO coffreDTO)
        {
            var resultat = await _coffreRepository.GenericGet(x => x.Code == coffreDTO.Code);
            if (resultat.Count == 0)
            {
                var res = await base.Create(coffreDTO);
                foreach (int item in coffreDTO.ModePaie)
                {
                    var category = new CategorieModePaieCoffreDTO { CoffreId = res.Id, ModePaieId = item };
                    var result = await _categorieModePaieCoffreService.Create(category);
                }
                return new OperationResult(false, String.Empty, res);

            }
            else
            {
                return new OperationResult(true, "le code est exist déja ", null);
            }
        }

        public async Task<List<CoffreDTO>> GetAllByModePaie(List<int> modePaies)
        {
            List<CoffreDTO> coffreDTOs = new List<CoffreDTO>();
            var res = await _categorieModePaieCoffreService.GetAllByModePaie(modePaies);
            if (res.Count != 0)
            {
                foreach (var elt in res)
                {
                    var coffre = await _coffreRepository.GetById(elt.CoffreId);
                    coffreDTOs.Add(_mapper.Map<TCoffreDTO>(coffre));

                }
            }
            return coffreDTOs;
        }

        public async Task<List<CoffreDTO>> GetAllCoffre()
        {
            var res= await _coffreRepository.GetAllCoffre();
            return res;
            //var res = await base.GetAll();
            //foreach (var item in res)
            //{
            //    decimal soldeAlimentation = 0;
            //    decimal soldeSortie = 0;
            //    var listAlimentation = await _coffreAlimentationService.GetByCoffreId(item.Id);
            //    var listSortie = await _coffrSortieService.GetByCoffreId(item.Id);
            //    foreach (var alimentation in listAlimentation)
            //    {
            //        soldeAlimentation = (decimal)(soldeAlimentation + alimentation.Montantalimenter);
            //    }
            //    foreach (var sortie in listSortie)
            //    {
            //        soldeSortie = (decimal)(soldeSortie + sortie.Montant);
            //    }

            //    var result = await _categorieModePaieCoffreService.GetByIdCoffre(item.Id);
            //    foreach (var modePaie in result)
            //    {
            //        var modepaie = await _modePaiementService.GetById(modePaie);
            //        item.ModesPaieLibelle = item.ModesPaieLibelle + modepaie.LibelleAppFinance + ", ";
            //    }
            //    item.ModePaie = result;
            //    item.Montant = soldeAlimentation - soldeSortie;


            //}
            //return res;
        }

        public async Task<CoffreDTO> Update(CoffreDTO coffre)
        {
            var entity = _mapper.Map<Coffre>(coffre);
            var update = await _coffreRepository.Edit(entity);
            var coffreCatergory = await _categorieModePaieCoffreService.GetByIdCoffre(coffre.Id);

            foreach (int item in coffre.ModePaie)
            {
                var c = coffreCatergory.Find((x) => x.ModePaieId == item);
                if (c==null)
                {
                    var category = new CategorieModePaieCoffreDTO { CoffreId = update.Id, ModePaieId = item };
                    var result = await _categorieModePaieCoffreService.Create(category);
                }
               
            }
            foreach (CategorieModePaieCoffreDTO cc in coffreCatergory)
            {
                var c = coffre.ModePaie.Find((x) => x == cc.ModePaieId);
                if (c == 0)
                {
                    var result = await _categorieModePaieCoffreService.Delete(cc.Id);
                }
               
            }
            var coffreDto = _mapper.Map<CoffreDTO>(update);
            return coffreDto;
        }
    }
}
