﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DDD.Application.BaseServices;
using DDD.Application.Interfaces;
using DTO;
using DDD.Domain.Interfaces;
using DDD.Infra.Data.Repository;
using Entity;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;


namespace DDD.Application.Services
{
    public class ArticleService<TArticleDTO, TArticle, DataBaseContext> :
        AuthBaseService<TArticleDTO, Article, DataBaseContext>, IArticleService<TArticleDTO>
       where TArticleDTO : ArticleDTO, new()
       where TArticle : Article, new()
       where DataBaseContext : DDDContext, new()
    {
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly IArticleRepository<Article> _ArticleRepository;

        public ArticleService(ICurrentContextProvider contextProvider,
            DataBaseContext dataBaseContext, IMapper mapper,
                                  IArticleRepository<Article> ArticleRepository,
                                  IConfiguration configuration
                                 ) : base(
           contextProvider, dataBaseContext, mapper)

        {
            _mapper = mapper;
            _ArticleRepository = ArticleRepository;
            _configuration = configuration;


        }

        public async Task<List<ArticleDTO>> GetAllWithControlSolde()
        {
            return await _ArticleRepository.GetAllWithControlSolde();
        }

        public async Task<ArticleDTO> Update(ArticleDTO Article)
        {
            var entity = _mapper.Map<Article>(Article);
            var update = await _ArticleRepository.Edit(entity);
            var ArticleDto = _mapper.Map<ArticleDTO>(update);
            return ArticleDto;
        }
    }
}
