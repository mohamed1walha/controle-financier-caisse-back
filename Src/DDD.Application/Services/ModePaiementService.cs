using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using DDD.Application.BaseServices;
using DDD.Application.Interfaces;
using DTO;
using DDD.Domain.Interfaces;
using Entity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;

namespace DDD.Application.Services
{
    public class ModePaiementService<TModePaieDTO, TModePaie, DataBaseContext>
        : AuthBaseService<TModePaieDTO, TModePaie, DataBaseContext>, IModePaiementService<TModePaieDTO>
       where TModePaieDTO : ModepaieDTO, new()
       where TModePaie : Modepaie, new()
       where DataBaseContext : DDDContext, new()
    {
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly IModePaiementRepository<Modepaie> _modePaiementRepository;
        private readonly ILastsyncappRepository<Lastsyncapp> _modePaiementSyncRepository;
        private readonly ICategorieModePaieCoffreService<CategorieModePaieCoffreDTO> _categorieModePaieCoffreService;


        public ModePaiementService(ICurrentContextProvider contextProvider,
            DataBaseContext dataBaseContext, IMapper mapper,
                                 ILastsyncappRepository<Lastsyncapp> modePaiementSyncRepository,
                                  IModePaiementRepository<Modepaie> modePaiementRepository,
                                  ICategorieModePaieCoffreService<CategorieModePaieCoffreDTO> categorieModePaieCoffreService,
                                  IConfiguration configuration
                                 ) : base(
           contextProvider, dataBaseContext, mapper)
        {
            _mapper = mapper;
            _modePaiementRepository = modePaiementRepository;
            _modePaiementSyncRepository = modePaiementSyncRepository;
            _categorieModePaieCoffreService = categorieModePaieCoffreService;
            _configuration = configuration;
        }

        public async Task<bool> EditAsync(Modepaie res)
        {
           return await _modePaiementRepository.EditAsync(res);
        }

        public async Task<List<ModepaieDTO>> GetALLModePaie()
        {
            var res= await _modePaiementRepository.GetAllModePaie();
            return res;
        }

        public async Task<List<TModePaieDTO>> GetByCoffreId(int coffreId)
        {

            var mdps = await base.GetAll();
            var coffMdp = await this._categorieModePaieCoffreService.GetAll();
            var filteredList = coffMdp.Where(x => x.CoffreId == coffreId).ToList();
            var result = new List<TModePaieDTO>();
            foreach (var catMpCof in filteredList)
            {
                var mdp = mdps.Where(x => x.Id == catMpCof.ModePaieId).FirstOrDefault();
                if (mdp != null)
                {
                    result.Add(mdp);
                }
            }

            return result;

        }

        public async Task<ModepaieDTO> GetByModePaie(string gpeModepaie)
        {
            var res = _modePaiementRepository.GenericGetFirstOrDefault(x => x.MpModepaie == gpeModepaie);
            return _mapper.Map<ModepaieDTO>(res);
        }

        public async Task Update(ModepaieDTO modepaie)
        {
            var res = _mapper.Map<Modepaie>(modepaie);
            res.Devise = null;
            await _modePaiementRepository.Edit(res);
        }
       
    }



}
