﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DDD.Application.BaseServices;
using DDD.Application.Interfaces;
using DTO;
using DDD.Domain.Interfaces;
using DDD.Infra.Data.Repository;
using Entity;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;


namespace DDD.Application.Services
{
    public class LigneDepenseService<TLigneDepenseDTO, TLigneDepense, DataBaseContext> :
        AuthBaseService<TLigneDepenseDTO, LigneDepense, DataBaseContext>, ILigneDepenseService<TLigneDepenseDTO>
       where TLigneDepenseDTO : LigneDepenseDTO, new()
       where TLigneDepense : LigneDepense, new()
       where DataBaseContext : DDDContext, new()
    {
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly ILigneDepenseRepository<LigneDepense> _LigneDepenseRepository;

        public LigneDepenseService(ICurrentContextProvider contextProvider,
            DataBaseContext dataBaseContext, IMapper mapper,
                                  ILigneDepenseRepository<LigneDepense> LigneDepenseRepository,
                                  IConfiguration configuration
                                 ) : base(
           contextProvider, dataBaseContext, mapper)

        {
            _mapper = mapper;
            _LigneDepenseRepository = LigneDepenseRepository;
            _configuration = configuration;


        }

        public async Task<List<LigneDepenseDTO>> GetAllByEnteteId(int id)
        {
            var res = await _LigneDepenseRepository.GetAllByEnteteId(id);
            return res;

        }

        public List<LigneDepenseDTO>GetAllByEnteteIdSync(int id)
        {
            var res = _LigneDepenseRepository.GetAllByEnteteIdSync(id);
            return res;

        }

        public async Task<LigneDepenseDTO> Update(LigneDepenseDTO LigneDepense)
        {
            var entity = _mapper.Map<LigneDepense>(LigneDepense);
            entity.ArticleId = entity.Article.Id;
            var update = await _LigneDepenseRepository.EditLigne(entity);
            var LigneDepenseDto = _mapper.Map<LigneDepenseDTO>(update);
            return LigneDepenseDto;
        }
    }
}
