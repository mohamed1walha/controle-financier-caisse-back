﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DDD.Application.BaseServices;
using DDD.Application.Interfaces;
using DTO;
using DDD.Domain.Interfaces;
using DDD.Infra.Data.Repository;
using Entity;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;


namespace DDD.Application.Services
{
    public class BilletDetailSortieService<TBilletDetailSortieDTO, TBilletDetailSortie, DataBaseContext> :
        AuthBaseService<TBilletDetailSortieDTO, BilletDetailSortie, DataBaseContext>, IBilletDetailSortieService<TBilletDetailSortieDTO>
       where TBilletDetailSortieDTO : BilletDetailSortieDTO, new()
       where TBilletDetailSortie : BilletDetailSortie, new()
       where DataBaseContext : DDDContext, new()
    {
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly IBilletDetailSortieRepository<BilletDetailSortie> _BilletDetailSortieRepository;

        public BilletDetailSortieService(ICurrentContextProvider contextProvider,
            DataBaseContext dataBaseContext, IMapper mapper,
                                  IBilletDetailSortieRepository<BilletDetailSortie> BilletDetailSortieRepository,
                                  IConfiguration configuration
                                 ) : base(
           contextProvider, dataBaseContext, mapper)

        {
            _mapper = mapper;
            _BilletDetailSortieRepository = BilletDetailSortieRepository;
            _configuration = configuration;


        }

        public async Task<List<BilletDetailSortieDTO>> GetAllBySortieId(int SortieId)
        {
            return await _BilletDetailSortieRepository.GetAllBySortieId(SortieId);
        }

        public async Task<BilletDetailSortieDTO> Update(BilletDetailSortieDTO BilletDetailSortie)
        {
            var entity = _mapper.Map<BilletDetailSortie>(BilletDetailSortie);
            var update = await _BilletDetailSortieRepository.Edit(entity);
            var BilletDetailSortieDto = _mapper.Map<BilletDetailSortieDTO>(update);
            return BilletDetailSortieDto;
        }
    }
}
