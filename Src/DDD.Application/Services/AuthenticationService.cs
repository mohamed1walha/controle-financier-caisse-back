﻿
//using System;
//using System.Threading.Tasks;
//using Entity;
//using DTO;
//using Microsoft.AspNetCore.Identity;
//using DDD.Domain.Interfaces;
//using AU.API;
//using AU.DTO;
//using AU.DTO.AuthDTO;

//namespace DDD.Application.Services
//{
//    public class AuthenticationService<TUser> : IAuthenticationService
//        where TUser : User, new()
//    {
//        protected readonly UserManager<TUser> userManager;
//        protected readonly JwtManager jwtManager;

//        protected readonly Microsoft.Extensions.Configuration.IConfiguration _config;

//        public AuthenticationService(JwtManager jwtManager, UserManager<TUser> userManager, 
//          Microsoft.Extensions.Configuration.IConfiguration _config)
//        {
//            this.userManager = userManager;
//            this.jwtManager = jwtManager;
//            this._config = _config;
//        }

//        public async Task<AuthResult<Token>> Login(LoginDTO loginDto)
//        {
//            if (loginDto == null || string.IsNullOrEmpty(loginDto.UserName) || string.IsNullOrEmpty(loginDto.Password))
//                return AuthResult<Token>.UnvalidatedResult;

//            var user = await userManager.FindByNameAsync(loginDto.UserName);

//            if (user != null && user.Id > 0)
//            {
//                if (await userManager.CheckPasswordAsync(user, loginDto.Password))
//                {
//                    //if(user.ResetPasswordIsNeeded)
//                    //{
//                    //    var res =  await RequestResetPasswordToken(new RequestPasswordDTO { UserName = loginDto.UserName });
//                    //    if (res.Succeeded)
//                    //        throw new APICustomException("Password reset is required.", APICustomReturnType.PasswordResetIsNeeded, res.Data);
//                    //}
//                    var token = jwtManager.GenerateToken(user);
//                    return AuthResult<Token>.TokenResult(token);
//                }
//            }

//            return AuthResult<Token>.UnauthorizedResult;
//        }

//        public async Task<AuthResult<Token>> ChangePassword(ChangePasswordDTO changePasswordDto, int currentUserId)
//        {
//            if (changePasswordDto == null ||
//                string.IsNullOrEmpty(changePasswordDto.ConfirmPassword) ||
//                string.IsNullOrEmpty(changePasswordDto.Password) ||
//                changePasswordDto.Password != changePasswordDto.ConfirmPassword
//            )
//                return AuthResult<Token>.UnvalidatedResult;

//            if (currentUserId > 0)
//            {
//                var user = await userManager.FindByIdAsync(currentUserId.ToString());
//                if (await userManager.CheckPasswordAsync(user, changePasswordDto.OldPassword))
//                {
//                    var result = await userManager.ChangePasswordAsync(user, null, changePasswordDto.Password);
//                    if (result.Succeeded)
//                        return AuthResult<Token>.SucceededResult;
//                }
//                else return AuthResult<Token>.UnvalidatedResult;
               
//            }

//            return AuthResult<Token>.UnauthorizedResult;
//        }

//        public async Task<AuthResult<Token>> SignUp(SignUpDTO signUpDto)
//        {
//            if (signUpDto == null ||
//                string.IsNullOrEmpty(signUpDto.Email) ||
//                string.IsNullOrEmpty(signUpDto.Password) ||
//                string.IsNullOrEmpty(signUpDto.FirstName) ||
//                string.IsNullOrEmpty(signUpDto.LastName) ||
//                string.IsNullOrEmpty(signUpDto.ConfirmPassword) ||
//                string.IsNullOrEmpty(signUpDto.UserName) ||
//                signUpDto.Password != signUpDto.ConfirmPassword
//            )
//                return AuthResult<Token>.UnvalidatedResult;

//            var newUser = new TUser
//            {
//                UserName = signUpDto.UserName,
//                Email = signUpDto.Email,
//                FirstName = signUpDto.FirstName,
//                LastName = signUpDto.LastName
//            };

//            var result = await userManager.CreateAsync(newUser, signUpDto.Password);

//            if (result.Succeeded)
//            {
//                if (newUser.Id > 0)
//                {
//                    //await userManager.AddToRoleAsync(newUser, "User");
//                    var token = jwtManager.GenerateToken(newUser);
//                    return AuthResult<Token>.TokenResult(token);
//                }
//            }

//            return AuthResult<Token>.UnauthorizedResult;
//        }

//        private async Task<AuthResult<string>> RequestResetPasswordToken(RequestPasswordDTO requestPasswordDto)
//        {
//            if (requestPasswordDto == null ||
//           string.IsNullOrEmpty(requestPasswordDto.UserName))
//                return AuthResult<string>.UnvalidatedResult;

//            var user = await userManager.FindByNameAsync(requestPasswordDto.UserName);

//            if (user != null && user.Id > 0)
//            {
//                var passwordResetToken = await userManager.GeneratePasswordResetTokenAsync(user) ;
//                return AuthResult<string>.TokenResult(passwordResetToken, user.Email);
//            }
//            return AuthResult<string>.UnvalidatedResult;
//        }
//        //public async Task<AuthResult<string>> RequestPassword(RequestPasswordDTO requestPasswordDto, string host)
//        //{
//        //    var user = await userManager.FindByNameAsync(requestPasswordDto.UserName);
            
//        //    if (user == null )
//        //        throw new APICustomException(APICustomReturnType.UserNameNotFound, "Invalid Data");

//        //    var res =  await  RequestResetPasswordToken(requestPasswordDto);
//        //    if(res.Succeeded)
//        //    {
//        //        var encodedToken = Uri.EscapeDataString(res.Data);
//        //        var mailTemplate = (await mailRepo.GenericGet(p => p.Label == PV.passwordreset)).FirstOrDefault();

//        //        if (mailTemplate != null)
//        //        {
//        //            string mailbody = mailTemplate.Body;
//        //            mailbody = mailbody.Replace("#lnk#", encodedToken);
//        //            mailbody = mailbody.Replace("#username#", user.UserName);
//        //            mailbody = mailbody.Replace("#firstname#", user.FirstName);
//        //            mailbody = mailbody.Replace("#BaseURI#", host);// httprequest.Headers.Values);
//        //            var smtpConfig = await erpGlobalConfig.GetGlobalSmtpConfig(hostingEnvironment.EnvironmentName);
//        //            MailOptions options = new MailOptions(
//        //                mailTemplate.Subject, mailbody,
//        //               smtpConfig.SmtpAccount,
//        //                res.UserMail, String.Empty,
//        //                 "signature/fisu-signature.txt");

//        //            MailSender mail = new MailSender();
//        //            mail.SendMail(smtpConfig, options);
//        //        }

//        //       /* string mailbody = "http://sds-erp.sds.com/auth/reset-password?token=" + encodedToken;
//        //        var smtpConfig = await erpGlobalConfig.GetGlobalSmtpConfig(hostingEnvironment.EnvironmentName);
//        //        MailOptions options = new MailOptions(
//        //            "password reset", mailbody,
//        //            "phoenix.its.tunisia@gmail.com",
//        //            res.UserMail);

//        //        MailSender mail = new MailSender();
//        //        mail.SendMail(smtpConfig, options);*/
//        //    }
//        //    return res;
//        //}

//        //public async Task<AuthResult<Token>> RestorePassword(RestorePasswordDTO restorePasswordDto)
//        //{
//        //    if (restorePasswordDto == null ||
//        //        string.IsNullOrEmpty(restorePasswordDto.UserName) ||
//        //        string.IsNullOrEmpty(restorePasswordDto.Token) ||
//        //        string.IsNullOrEmpty(restorePasswordDto.NewPassword) ||
//        //        string.IsNullOrEmpty(restorePasswordDto.ConfirmPassword) ||
//        //        restorePasswordDto.ConfirmPassword != restorePasswordDto.NewPassword
//        //    )
//        //        return AuthResult<Token>.UnvalidatedResult;

//        //    var user = await userManager.FindByNameAsync(restorePasswordDto.UserName);

//        //    if (user != null && user.Id > 0)
//        //    {

//        //        var result = await userManager.ResetPasswordAsync(user, Uri.UnescapeDataString(restorePasswordDto.Token), restorePasswordDto.NewPassword);

//        //        if (result.Succeeded)
//        //        {
//        //            user.ResetPasswordIsNeeded = false;
//        //            await userManager.UpdateAsync(user);
//        //            var token = jwtManager.GenerateToken(user);
//        //            return AuthResult<Token>.TokenResult(token);
//        //        }
//        //    }
//        //    return AuthResult<Token>.UnvalidatedResult;
//        //}

//        public async Task<AuthResult<Token>> RefreshToken(RefreshTokenDTO refreshTokenDto)
//        {
//            var refreshToken = refreshTokenDto?.Token?.Refresh_token;
//            if (string.IsNullOrEmpty(refreshToken))
//                return AuthResult<Token>.UnvalidatedResult;

//            try
//            {
//                var principal = jwtManager.GetPrincipal(refreshToken, isAccessToken: false);
//                var userId = principal.GetUserId();
//                //var user = await userManager.FindByIdAsync(userId.ToString());
//                var user = await userManager.FindByNameAsync(principal.Identity.Name);

//                if (user != null && user.Id > 0)
//                {
//                    var token = jwtManager.GenerateToken(user);
//                    return AuthResult<Token>.TokenResult(token);
//                }
//            }
//            catch (Exception)
//            {
//                return AuthResult<Token>.UnauthorizedResult;
//            }

//            return AuthResult<Token>.UnauthorizedResult;
//        }

        

//        public Task<AuthResult<Token>> SignOut()
//        {
//            throw new NotImplementedException();
//        }
//    }
//}
