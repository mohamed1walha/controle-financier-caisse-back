﻿using AutoMapper;
using DDD.Application.BaseServices;
using DDD.Application.Interfaces;
using DTO;
using DDD.Domain.Interfaces;
using Entity;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace DDD.Application.Services
{
    public class DepenseService<TDepenseDTO, TDepense, DataBaseContext> :
        AuthBaseService<TDepenseDTO, Depense, DataBaseContext>, IDepenseService<TDepenseDTO>
       where TDepenseDTO : DepenseDTO, new()
       where TDepense : Depense, new()
       where DataBaseContext : DDDContext, new()
    {
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly IDepenseRepository<Depense> _DepenseRepository;

        public DepenseService(ICurrentContextProvider contextProvider,
            DataBaseContext dataBaseContext, IMapper mapper,
                                  IDepenseRepository<Depense> DepenseRepository,
                                  IConfiguration configuration
                                 ) : base(
           contextProvider, dataBaseContext, mapper)

        {
            _mapper = mapper;
            _DepenseRepository = DepenseRepository;
            _configuration = configuration;


        }

        public async Task<DepenseDTO> Update(DepenseDTO depense)
        {
            var entity = _mapper.Map<Depense>(depense);
            var update = await _DepenseRepository.Edit(entity);
            var depenseDto = _mapper.Map<DepenseDTO>(update);
            return depenseDto;
        }
    }
}
