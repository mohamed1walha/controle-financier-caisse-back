﻿using DTO;
using Entity;
using System.Collections.Generic;

namespace DDD.Application.Services
{
    public class JourscaisseEqualityComparer : IEqualityComparer<JoursCaiseDTO>
    {
        public bool Equals(JoursCaiseDTO x, JoursCaiseDTO y)
        {
            return x.Id == y.Id;
        }

        public int GetHashCode(JoursCaiseDTO obj)
        {
            return obj.Id.GetHashCode();
        }
        }
    
}