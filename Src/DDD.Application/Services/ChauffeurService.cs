﻿using AutoMapper;
using DDD.Application.BaseServices;
using DDD.Application.Interfaces;
using DTO;
using DDD.Domain.Interfaces;
using Entity;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace DDD.Application.Services
{
    public class ChauffeurService<TChauffeurDTO, TChauffeur, DataBaseContext> :
        AuthBaseService<TChauffeurDTO, Chauffeur, DataBaseContext>, IChauffeurService<TChauffeurDTO>
       where TChauffeurDTO : ChauffeurDTO, new()
       where TChauffeur : Chauffeur, new()
       where DataBaseContext : DDDContext, new()
    {
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly IChauffeurRepository<Chauffeur> _ChauffeurRepository;

        public ChauffeurService(ICurrentContextProvider contextProvider,
            DataBaseContext dataBaseContext, IMapper mapper,
                                  IChauffeurRepository<Chauffeur> ChauffeurRepository,
                                  IConfiguration configuration
                                 ) : base(
           contextProvider, dataBaseContext, mapper)

        {
            _mapper = mapper;
            _ChauffeurRepository = ChauffeurRepository;
            _configuration = configuration;


        }

        public async Task<ChauffeurDTO> Update(ChauffeurDTO chauffeur)
        {
            var entity = _mapper.Map<Chauffeur>(chauffeur);
            var update = await _ChauffeurRepository.Edit(entity);
            var chauffeurDto = _mapper.Map<ChauffeurDTO>(update);
            return chauffeurDto;
        }
    }
}
