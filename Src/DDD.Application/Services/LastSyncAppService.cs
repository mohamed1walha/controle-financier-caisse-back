﻿using AutoMapper;
using DDD.Application.BaseServices;
using DDD.Application.Interfaces;
using DTO;
using DDD.Domain.Interfaces;
using Entity;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace DDD.Application.Services
{
    public class LastSyncAppService<TLastSyncAppDTO, TLastSyncApp, DataBaseContext> :
        AuthBaseService<TLastSyncAppDTO, Lastsyncapp, DataBaseContext>, ILastSyncAppService<TLastSyncAppDTO>
       where TLastSyncAppDTO : LastSyncAppDTO, new()
       where TLastSyncApp : Lastsyncapp, new()
       where DataBaseContext : DDDContext, new()
    {
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly ILastsyncappRepository<Lastsyncapp> _LastSyncAppRepository;

        public LastSyncAppService(ICurrentContextProvider contextProvider,
            DataBaseContext dataBaseContext, IMapper mapper,
                                  ILastsyncappRepository<Lastsyncapp> LastSyncAppRepository,
                                  IConfiguration configuration
                                 ) : base(
           contextProvider, dataBaseContext, mapper)

        {
            _mapper = mapper;
            _LastSyncAppRepository = LastSyncAppRepository;
            _configuration = configuration;
          

        }

        public async Task<LastSyncAppDTO> CreateLast(LastSyncAppDTO lastSync)
        {
            var rr = _mapper.Map<Lastsyncapp>(lastSync);
            var res= await _LastSyncAppRepository.Create(rr);
           return _mapper.Map<LastSyncAppDTO>(res);
        }
        public async Task<DateTime> LastSyncModePaie()
        {
            var result = await _LastSyncAppRepository.GetAll();
            if (result.Count!=0)
            {
                return (DateTime)result.Last().Lastsync;
            }
            return DateTime.Now;
        }
    }
}
