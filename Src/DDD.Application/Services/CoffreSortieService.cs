﻿using AutoMapper;
using DDD.Application.BaseServices;
using DDD.Application.Interfaces;
using DTO;
using DDD.Domain.Interfaces;
using Entity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace DDD.Application.Services
{
    public class CoffreSortieService<TCoffresortieDTO, TCoffresortie, DataBaseContext> :
        AuthBaseService<TCoffresortieDTO, Coffresortie, DataBaseContext>, ICoffreSortieService<TCoffresortieDTO>
       where TCoffresortieDTO : CoffresortieDTO, new()
       where TCoffresortie : Coffresortie, new()
       where DataBaseContext : DDDContext, new()
    {
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly ICoffreSortieRepository<Coffresortie> _CoffresortieRepository;
        ICoffreRepository<Coffre> _coffreRep;
        IBanqueService<BanqueDTO> _banqueService;
        ITypeFraisService<TypeFraisDTO> _typeFraisService;

        public CoffreSortieService(ICurrentContextProvider contextProvider,
            DataBaseContext dataBaseContext, IMapper mapper,
                                  ICoffreSortieRepository<Coffresortie> CoffresortieRepository,
                                  IConfiguration configuration,
                                  ICoffreRepository<Coffre> coffreRep,
                                  ITypeFraisService<TypeFraisDTO> typeFraisService,
        IBanqueService<BanqueDTO> banqueService
                                 ) : base(
           contextProvider, dataBaseContext, mapper)

        {
            _mapper = mapper;
            _CoffresortieRepository = CoffresortieRepository;
            _configuration = configuration;
            _coffreRep = coffreRep;
            _banqueService = banqueService;
            _typeFraisService = typeFraisService;
        }
        public async Task<int?> DeleteSortie(int id)
        {
            var result = await base.GetById(id);
            var res = await base.Delete(id);

            return result.CoffreId;
        }
        public async Task<CoffresortieDTO> Update(CoffresortieDTO Coffresortie)
        {
            var entity = _mapper.Map<Coffresortie>(Coffresortie);
            var update = await _CoffresortieRepository.Edit(entity);
            var CoffresortieDto = _mapper.Map<CoffresortieDTO>(update);
            return CoffresortieDto;
        }

        public async Task<List<CoffresortieDTO>> GetByCoffreId(int id)
        {
            var res = await _CoffresortieRepository.GetByCoffreId(id);

            //var res = await base.GetAll();
            //List<TCoffresortieDTO> filteredList = res.Where(x => x.CoffreId == id).ToList();
            //foreach (var sortie in filteredList)
            //{
            //    var banque = await this._banqueService.GetById(sortie.BanqueId);
            //    var coffre = await this._coffreRep.GetById(sortie.CoffreId);
            //    var typeFrais = await this._typeFraisService.GetById(sortie.TypeFraisId);
            //    var cof = new CoffreDTO
            //    {
            //        Code = coffre.Code,
            //        Id = coffre.Id,
            //        Libelle = coffre.Libelle,
            //    };
            //    sortie.BanqueLibelle = banque?.Libelle;
            //    sortie.CoffreLibelle = coffre?.Libelle;
            //    sortie.TypeFraisLibelle = typeFrais?.TypeFrais1;
            //    sortie.Coffre = cof;


            //    if (sortie.TypeSortie == 1)
            //    {
            //        sortie.TypeSortieLibelle = "Remise en banque";
            //    }
            //    if (sortie.TypeSortie == 2)
            //    {
            //        sortie.TypeSortieLibelle = "Frais divers";
            //    }
            //}
            //return filteredList;
            return res;
        }

        public async Task<OperationResult> CreateSortie(TCoffresortieDTO coffreSortieDTO)
        {
            try
            {
                


                if (coffreSortieDTO.Montant > 0)
                {
                    var result = _mapper.Map<Entity.Coffresortie>(coffreSortieDTO);
                        if (coffreSortieDTO.Id == 0) {
                        coffreSortieDTO.SortieDate = DateTime.Parse(coffreSortieDTO.SortieDate.ToString()).AddHours(1);

                        var res = await base.Create(coffreSortieDTO);
                            return new OperationResult(false, String.Empty, res);

                        }
                        else {
                        result.Coffre = null;
                        result.Banque = null;
                        result.SortieDate = DateTime.Parse(coffreSortieDTO.SortieDate.ToString()).AddHours(1);
                        var res =await _CoffresortieRepository.Edit(result);
                            return new OperationResult(false, String.Empty, res);
                        }

                    }
                else
                {
                    return new OperationResult(true, "Le montant de sortie est égal à zéro");

                }

            }
            catch (Exception ex)
            {
                return new OperationResult(true, ex.Message);

            }
        }

        public async Task<List<CoffresortieDTO>> GetAllSorties()
        {
            var res = await _CoffresortieRepository.getAllSorties();
            return res;
            //var resultat = await base.GetAll();
            //foreach (var sortie in resultat)
            //{
            //    var banque = await this._banqueService.GetById(sortie.BanqueId);
            //    var coffre = await this._coffreRep.GetById(sortie.CoffreId);
            //    var typeFrais = await this._typeFraisService.GetById(sortie.TypeFraisId);
            //    var cof = new CoffreDTO
            //    {
            //        Code = coffre.Code,
            //        Id = coffre.Id,
            //        Libelle = coffre.Libelle,
            //    };
            //    sortie.BanqueLibelle = banque?.Libelle;
            //    sortie.CoffreLibelle = coffre?.Libelle;
            //    sortie.TypeFraisLibelle = typeFrais?.TypeFrais1;
            //    sortie.Coffre = cof;

            //    if (sortie.TypeSortie == 1)
            //    {
            //        sortie.TypeSortieLibelle = "Remise en banque";
            //    }
            //    if (sortie.TypeSortie == 2)
            //    {
            //        sortie.TypeSortieLibelle = "Frais divers";
            //    }
            //}


        }

        public async Task<FileStreamResult> getDocument(int id)
        {

            var sortie = await base.GetById(id);
            Stream stream = new MemoryStream(sortie.PieceJustificatif);
            string ret = "";
            if (sortie.Extention.ToUpper() == ".PDF")
            {
                ret = "application/pdf";
            }
            else if (sortie.Extention.ToUpper() == ".JPG" || sortie.Extention.ToUpper() == ".JPEG")
            {
                ret = "image/jpeg";
            }
            else if (sortie.Extention.ToUpper() == ".PNG")
            {
                ret = "image/png";
            }
            else if (sortie.Extention.ToUpper() == ".GIF")
            {
                ret = "image/gif";
            }
            else
            {
                ret = "image/" + sortie.Extention.Replace(".", "");
            }
            

            var fileExist = new FileStreamResult(stream, ret)
            {
                FileDownloadName = sortie.JustificatifName+sortie.Extention,

            };
            return fileExist;
        }
    }
}
