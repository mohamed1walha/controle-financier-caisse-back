using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection.Metadata;
using System.Threading.Tasks;
using AutoMapper;
using DDD.Application.BaseServices;
using DDD.Application.Interfaces;
using DDD.Application.utils;
using DTO;
using DDD.Domain.Interfaces;
using DDD.Infra.Data.Repository;
using DTO;
using Entity;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System.Globalization;

namespace DDD.Application.Services
{
    public class JoursCaisseService<JoursCaisseDTO, JoursCaisse, DataBaseContext> :
        AuthBaseService<JoursCaisseDTO, JoursCaisse, DataBaseContext>, IJoursCaisseService<JoursCaisseDTO>
       where JoursCaisseDTO : JoursCaiseDTO, new()
       where JoursCaisse : Jourscaisse, new()
       where DataBaseContext : DDDContext, new()
    {
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly IJoursCaisseRepository<Jourscaisse> _joursCaisseRepository;
        private readonly IPiedecheService<PiedecheDTO> _piedecheService;
        private readonly IEtabService<EtablissementDTO> _etabService;
        private readonly IParcaisseService<ParcaisseDTO> _parcaisseService;
        private readonly IModePaiementService<ModepaieDTO> _modePaiementService;
        private readonly ILastSyncAppService<LastSyncAppDTO> _lastsyncappService;
        private readonly IStatusService<StatusDTO> _statusService;
        private readonly IEmetteurService<EmetteurDTO> _emetteurService;
        private readonly IChauffeurService<ChauffeurDTO> _chauffeurService;
        private readonly IDeviseService<DeviseDTO> _deviseService;
        private readonly IUserService<UserDTO> _userService;

        public JoursCaisseService(ICurrentContextProvider contextProvider,
            DataBaseContext dataBaseContext, IMapper mapper,
                                  IJoursCaisseRepository<Jourscaisse> joursCaisseRepository,
                                  IConfiguration configuration,
                                  IPiedecheService<PiedecheDTO> piedecheService,
                                  IEtabService<EtablissementDTO> etabService,
                                 ILastSyncAppService<LastSyncAppDTO> LastSyncAppService,
                                  IModePaiementService<ModepaieDTO> modePaiementService,
                                  IStatusService<StatusDTO> statusService,
                                  IEmetteurService<EmetteurDTO> emetteurService,
                                  IChauffeurService<ChauffeurDTO> chauffeurService,
                                  IDeviseService<DeviseDTO> deviseService,
        IParcaisseService<ParcaisseDTO> parcaisseService, IUserService<UserDTO> userService

                                  ) : base(
           contextProvider, dataBaseContext, mapper)

        {
            _mapper = mapper;
            _joursCaisseRepository = joursCaisseRepository;
            _configuration = configuration;
            _piedecheService = piedecheService;
            _etabService = etabService;
            _modePaiementService = modePaiementService;
            _lastsyncappService = LastSyncAppService;
            _statusService = statusService;
            _chauffeurService = chauffeurService;
            _emetteurService = emetteurService;
            _deviseService= deviseService;
            _parcaisseService=parcaisseService;
            _userService = userService;
        }
        public async Task<List<JoursCaiseDTO>> GetAllByDate(DateTime dateferme)
        {

            var res = await _joursCaisseRepository.GenericGet(x => x.GjcDateferme >= dateferme);
            return _mapper.Map<List<JoursCaiseDTO>>(res);
        }


        public async Task<OperationResult> Charger(string[] etablissmentForm,string dateOuvFrom, string dateOuvToo, string user)
        {
            try
            {
                var devise = await _deviseService.GetByCode("Aucun");
                List<EtablissementDTO> etablissementDDD = new List<EtablissementDTO>();
                List<ParcaisseDTO> parcaisseDDD = new List<ParcaisseDTO>();
                etablissementDDD = await _etabService.GetAll();
                parcaisseDDD = await _parcaisseService.GetAll();
                string SourceConnection = _configuration.GetConnectionString("DefaultConnectionExist");
                string DestinationConnection = _configuration.GetConnectionString("DefaultConnection");

                List<Jourscaisse> jourCaisseResultFinal = new List<Jourscaisse>();
                List<EtablissementDTO> etablissements = new List<EtablissementDTO>();
                List<ParcaisseDTO> parcaisses = new List<ParcaisseDTO>();
             
                List<PiedecheDTO> piedeches = new List<PiedecheDTO>();
                List<ModepaieDTO> modePaies = new List<ModepaieDTO>();
                List<Modepaie> modePaieResult = new List<Modepaie>();
                List<string> columnsJoursCaisse = new List<string>() {
                    "GJC_NUMZCAISSE GjcNumzcaisse",
                    "GJC_CAISSE GjcCaisse",
                    "GJC_HEUREOUV GjcHeureouv",
                    "GJC_DATEFERME GjcDateferme",
                    "GJC_HEUREFERME GjcHeureferme",
                    "GJC_USERFERME GjcUserferme",
                    "GJC_ETABLISSEMENT GjcEtablissementcode",
                    "ET_LIBELLE GjcEtablissementlabel",
                    "GPK_LIBELLE GjcCaisselabel",
                    "GJC_ETAT GjcEtat",
                    "GJC_DATEOUV GjcDateouv",
                    "c.GPK_LIBELLE GjcCaisseLabel"
                };
                List<string> columnsEtablissement = new List<string>() {
                "ET_ETABLISSEMENT EtEtablissement ","ET_DEVISE EtDevise","ET_LIBELLE EtLibelle","ET_ADRESSE1 EtAdresse1",
                "ET_ADRESSE2 EtAdresse2","ET_ADRESSE3 EtAdresse3","ET_CODEPOSTAL EtCodepostal","ET_VILLE EtVille",
                "ET_CHARLIBRE2 EtPays" ,"ET_LIBREET5 EtLangue","ET_TELEPHONE EtTelephone"};
                var transformDate1 = "";
                var transformDate2 = "";
                var tab1 = dateOuvFrom.Split("-");
                var tab2 = dateOuvToo.Split("-");
                transformDate1= tab1[2] + tab1[1]+tab1[0];
                transformDate2= tab2[2] + tab2[1]+tab2[0];
                string queryJourCaisse = "SELECT CONCAT(GJC_NUMZCAISSE, GJC_CAISSE, GJC_ETABLISSEMENT) AS Code," + String.Join(",", columnsJoursCaisse) + " FROM dbo.[JOURSCAISSE] j" +
                       " left join ETABLISS e on j.GJC_ETABLISSEMENT = e.ET_ETABLISSEMENT" +
                       " left join PARCAISSE c on j.GJC_CAISSE = c.GPK_CAISSE" +
                   " where GJC_ETAT = 'CDE' and GJC_DATEOUV >=   '" + transformDate1 + "' and GJC_DATEOUV <=  '" + transformDate2 + "'";
                string queryEtab = "SELECT " + String.Join(",", columnsEtablissement) + " FROM dbo.[ETABLISS] ";
                string queryParcaisse = "SELECT [GPK_CAISSE] GpkCaisse,[GPK_LIBELLE] GpkLibelle,[GPK_ETABLISSEMENT] GpkEtablissement FROM [Parcaisse] ";
                string queryModePaie = "SELECT MP_MODEPAIE MpModepaie, MP_LIBELLE MpLibelle FROM dbo.[MODEPAIE] ";
                SqlConnection sourceConn = new SqlConnection(SourceConnection);
                string withEtab = " and [GPK_ETABLISSEMENT] in ('" + String.Join("','", etablissmentForm) + "')";
                    if (etablissmentForm.Length > 0)
                {
                    queryJourCaisse = queryJourCaisse + withEtab;
                }
                var joursCaises = await this.genericBaseRepository.ReadDataFromQueryByConnectionStringAsync<JoursCaiseDTO>(queryJourCaisse, SourceConnection);
                etablissements = await this.genericBaseRepository.ReadDataFromQueryByConnectionStringAsync<EtablissementDTO>(queryEtab, SourceConnection);
                parcaisses = await this.genericBaseRepository.ReadDataFromQueryByConnectionStringAsync<ParcaisseDTO>(queryParcaisse, SourceConnection);
                modePaies = await this.genericBaseRepository.ReadDataFromQueryByConnectionStringAsync<ModepaieDTO>(queryModePaie, SourceConnection);
                List<Jourscaisse> jourCaisseDDD = await _joursCaisseRepository.GenericGet();
                List<ModepaieDTO> modePaieDDD = await _modePaiementService.GetAll();
              
                    var status = await _statusService.GetAll();
                var statusComptage = await _statusService.GetByCode(STATUS.NONE);
                var statusReception = await _statusService.GetByCode(STATUS.EN_ATTENTE);
                foreach (ModepaieDTO element in modePaies)
                {
                    var mp = modePaieDDD.Find(x => x.MpModepaie == element.MpModepaie);
                    if (!modePaieDDD.Exists(x => x.MpModepaie == element.MpModepaie))
                    {
                        element.LibelleAppFinance = element.MpLibelle;
                        element.DateCreation = DateTime.Now.Date;
                        element.DeviseId = devise.Id;
                        element.Actif = true;
                        await _modePaiementService.Create(element);
                       }
                    else if (mp?.DeviseId==null)
                    {
                        mp.DeviseId= devise.Id;
                        var res = _mapper.Map<Modepaie>(mp);
                        res.Devise = _mapper.Map<Devise>(devise);
                        var edit =await _modePaiementService.EditAsync(res);

                    }
                }

                List<Etablissement> ets = new List<Etablissement>();
                
                foreach (EtablissementDTO etab in etablissements)
                {
                    var condition = etablissementDDD.Exists(x => x.EtEtablissement == etab.EtEtablissement);
                    if (!condition)
                    {
                        await _etabService.Create(etab);
                    }
                    else
                    {
                        string queryEtabliss = "update Etablissement " +
                            "set ET_PAYS = '" + etab.EtPays + "',ET_LANGUE='" + etab.EtLangue + "' " +
                            " where ET_ETABLISSEMENT = '" + etab.EtEtablissement + "';";
                      var x= await this.genericBaseRepository.ExecuteQueryByConnectionString(queryEtabliss, DestinationConnection);




                    }
                }
                
                foreach (ParcaisseDTO caisse in parcaisses)
                {
                    if (!parcaisseDDD.Exists(x => x.GpkCaisse != caisse.GpkCaisse))
                    {
                        await _parcaisseService.Create(caisse);
                    }
                }
              

                foreach (JoursCaiseDTO element in joursCaises)
                {
                    var etab = await _etabService.GetByEtab(element.GjcEtablissementcode);

                    if(etab!= null)
                    {
                        var exist = jourCaisseDDD.Where(x => x.GjcNumzcaisse+x.GjcCaisse+x.GjcEtablissement.EtEtablissement == element.Code).FirstOrDefault();
                        if (exist == null)
                        {
                            if (etab != null)
                            {
                                element.GjcEtablissementId = etab.Id;

                            }
                            element.DateCreate = DateTime.Now.Date;
                            element.StatusComtage = statusComptage.Id;
                            element.StatusReception = statusReception.Id;
                            element.AgentComptage = user;
                            element.MontantCaisse = 0;
                            element.IsManuel = false;
                            var resultatJourDTO = _mapper.Map<JoursCaisse>(element);
                            var jour = await _joursCaisseRepository.Create(resultatJourDTO);
                            List<PiedecheDTO> p = await this.genericBaseRepository.ReadDataFromQueryByConnectionStringAsync<PiedecheDTO>(query(element.GjcEtablissementcode, element.GjcCaisse, element.GjcNumzcaisse, element.GjcDateouv), SourceConnection);
                            if (p.Count > 0)
                            {
                                var pp = p.GroupBy(x => x.GpeModepaie).ToList();
                                decimal? tot = 0;
                                foreach (var item in pp)
                                {
                                    var modeP = await _modePaiementService.GetByModePaie(item.Key);

                                    tot += item.Sum(x => decimal.Round((decimal)x.Montant, 3));
                                    Piedeche pi = new Piedeche
                                    {
                                        ModepaieId = modeP.Id,
                                        Montant = item.Sum(x => decimal.Round((decimal)x.Montant, 3)),
                                        JourcaisseId = jour.Id,
                                        DateCreation = DateTime.Now.Date,
                                        Source = SOURCE.CEGID,
                                        StatusComptage = statusComptage.Id,
                                        EspeceCash = 0,
                                        EspeceVerse = 0,
                                        MontantCompte = 0,
                                        Depence = 0,
                                        Commentaire = "",


                                    };
                                    var pii = _mapper.Map<PiedecheDTO>(pi);
                                    if (pii.Montant != 0)
                                    {
                                        await _piedecheService.Create(pii);
                                    }
                                }
                                jour.MontantCaisse = tot;
                                if (jour.MontantCaisse == 0)
                                {
                                    var rr = await _joursCaisseRepository.Delete(jour.Id);
                                }
                                else
                                {
                                    var journew = await _joursCaisseRepository.Edit(jour);

                                    jourCaisseResultFinal.Add(journew);
                                }
                            }
                            else
                            {
                                if (jour.MontantCaisse == 0)
                                {
                                    var rr = await _joursCaisseRepository.Delete(jour.Id);
                                }
                                else
                                {
                                    jourCaisseResultFinal.Add(jour);
                                }
                            }
                        }


                    }

                }

                LastSyncAppDTO lastSync = new LastSyncAppDTO();
                DateTime lastSync1;

                if (DateTime.TryParseExact(transformDate1, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out lastSync1))
                {
                    lastSync.Lastsync = lastSync1;

                    // The string was successfully parsed as a DateTime object
                    // You can now use the 'lastSync' variable in your code
                }
                else
                {
                    lastSync.Lastsync = DateTime.Parse(dateOuvToo);

                    // The string could not be parsed as a valid DateTime object
                    // Handle the error as necessary
                }
                await _lastsyncappService.Create(lastSync);
                var resultData = new List<JoursCaiseDTO>();
                if (jourCaisseResultFinal.Count != 0)
                {
                    resultData = _mapper.Map<List<JoursCaiseDTO>>(jourCaisseResultFinal);
                }
                else
                {
                    resultData = new List<JoursCaiseDTO>();
                }
                
             
                return new OperationResult(false, string.Empty, resultData);
            }

            catch (Exception e)
            {
                return new OperationResult(true, "erreur", e.Message);

            }
        }




        private string query(string gjcEtablissement, string gjcCaisse, int gjcNumzcaisse, DateTime dateOuvFrom)
        {
            List<string> columnsPiedeche = new List<string>()
            { "GPE_MODEPAIE GpeModepaie",
                "GPE_MONTANTECHE Montant"
                };
            var res = "SELECT " + String.Join(",", columnsPiedeche) + " FROM dbo.[PIEDECHE] p" +

                                                  " where GPE_ETABLISSEMENT = '" + gjcEtablissement + "' and GPE_CAISSE = '" + gjcCaisse + "' and GPE_NUMZCAISSE = " + gjcNumzcaisse +
                                                  " and GPE_MONTANTECHE <> 0";
            return res;
        }

        public async Task<JoursCaiseDTO> SearchByCode(int numZ, int codeEtab, string codeCaisse)
        {
            JoursCaisse jourcaisse = (JoursCaisse)await _joursCaisseRepository.SearchByCode(numZ, codeEtab, codeCaisse);
            var jourcaisseDto = _mapper.Map<JoursCaisseDTO>(jourcaisse);


            return jourcaisseDto;
        }

        public async Task<List<int>> GetAllZByEtab(int etabId)
        {
            return await _joursCaisseRepository.GetAllZByEtab(etabId);
        }

        public async Task<List<string>> GetAllCaisseByEtab(int etabId)
        {
            return await _joursCaisseRepository.GetAllCaisseByEtab(etabId);
        }

        public async Task<List<JoursCaiseDTO>> GetAllByStatusCmptNone(int userId)
        {
            var statusComptage = await _statusService.GetByCode(STATUS.NONE);
            var listStatusAccept = new List<string>();
            var listStatusReject = new List<string>();
            var res = new List<JoursCaiseDTO>();
            listStatusAccept.Add(STATUS.NONE);
            var user = await _userService.GetByUserId(userId);
            if(user?.ModePaies != null&& user?.ModePaies.Count!=0)
            {
                res = await _joursCaisseRepository.GetWithModePaie(user.ModePaies.Select(x=>(int?)x.Id).ToList(),listStatusAccept,listStatusReject);
                res =res.Distinct(new JourscaisseEqualityComparer()).ToList();
            }
            else
            {
                 var c = await _joursCaisseRepository.GenericGet(x => x.StatusComtage == statusComptage.Id);
                res = _mapper.Map<List<JoursCaiseDTO>>(c);

            }
            List<JoursCaiseDTO> result = _mapper.Map<List<JoursCaiseDTO>>(res);
            foreach (var j in result)
            {
                j.GjcEtablissement = await _etabService.GetById(j.GjcEtablissementId);
                j.statusReceptionObj = statusComptage;
            }
            return result.OrderByDescending(x => x.Id).Distinct().ToList();
        }

        public async Task<List<JoursCaiseDTO>> FiltreGetAllByStatusCmptNone(DateTime? dateTime1, DateTime? dateTime2, int userId)
        {
            var statusComptage = await _statusService.GetByCode(STATUS.NONE);
            var listStatusAccept = new List<string>();
            var listStatusReject = new List<string>();
            var res = new List<JoursCaiseDTO>();
            listStatusAccept.Add(STATUS.NONE);
            var user = await _userService.GetByUserId(userId);
            if (user?.ModePaies != null && user?.ModePaies.Count != 0)
            {
                res = await _joursCaisseRepository.GetWithModePaieFiltreGetAllByStatusCmptNone(user.ModePaies.Select(x => (int?)x.Id).ToList(), listStatusAccept, listStatusReject, dateTime1, dateTime2);
                res = res.Distinct(new JourscaisseEqualityComparer()).ToList();
            }
            else
            {

                var c = await _joursCaisseRepository.GenericGet(x => x.StatusComtage == statusComptage.Id && x.GjcDateouv >= DateTime.Parse(dateTime1.ToString()).Date && x.GjcDateouv <= DateTime.Parse(dateTime2.ToString()).Date);
                res = _mapper.Map<List<JoursCaiseDTO>>(c);

            }
            List<JoursCaiseDTO> result = _mapper.Map<List<JoursCaiseDTO>>(res);
            foreach (var j in result)
            {
                j.GjcEtablissement = await _etabService.GetById(j.GjcEtablissementId);
                j.statusReceptionObj = statusComptage;
            }
            return result.OrderByDescending(x => x.Id).Distinct().ToList();
        }

        public async Task<List<JoursCaiseDTO>> GetAllByStatusEnAttente(int userId)
        {
            var statusComptage = await _statusService.GetByCode(STATUS.EN_ATTENTE);
            var statusEn_att_Imp = await _statusService.GetByCode(STATUS.EN_ATTENTE_IMPRIME);

            var listStatusAccept = new List<int?>();
            var listStatusReject = new List<int?>();
            listStatusAccept.Add(statusComptage.Id);
            listStatusAccept.Add(statusEn_att_Imp.Id);
            var res = new List<JoursCaiseDTO>();
            var user = await _userService.GetByUserId(userId);
            if (user?.ModePaies != null && user?.ModePaies.Count != 0)
            {
                res = await _joursCaisseRepository.GetWithModePaieRecep(user.ModePaies.Select(x => (int?)x.Id).ToList(), listStatusAccept, listStatusReject);
                res = res.Distinct(new JourscaisseEqualityComparer()).ToList();
            }
            else
            {
                 var c = await _joursCaisseRepository.GenericGet(x => x.StatusReception == statusComptage.Id || x.StatusReception == statusEn_att_Imp.Id);
                res= _mapper.Map<List<JoursCaiseDTO>>(c);
            }
            List<JoursCaiseDTO> result = _mapper.Map<List<JoursCaiseDTO>>(res);

            foreach (var j in result)
            {
                j.GjcEtablissement = await _etabService.GetById(j.GjcEtablissementId);
                j.statusReceptionObj = j.StatusReception == statusComptage.Id ? statusComptage : statusEn_att_Imp;

            }
            return result.OrderByDescending(x=>x.Id).ToList();
        }
        public async Task<List<JoursCaiseDTO>> GetAllByStatusReceptionne(int userId)
        {
            var statusComptage = await _statusService.GetByCode(STATUS.RECEPTIONNE);
            var listStatusAccept = new List<int?>();
            var listStatusReject = new List<int?>();
            listStatusAccept.Add(statusComptage.Id);
            var res = new List<JoursCaiseDTO>();
            var user = await _userService.GetByUserId(userId);
            if (user?.ModePaies != null && user?.ModePaies.Count != 0)
            {
                res = await _joursCaisseRepository.GetWithModePaieRecep(user.ModePaies.Select(x => (int?)x.Id).ToList(), listStatusAccept, listStatusReject);
                res = res.Distinct(new JourscaisseEqualityComparer()).ToList();
            }
            else
            {
                 var c= await _joursCaisseRepository.GenericGet(x => x.StatusReception == statusComptage.Id);
                res = _mapper.Map<List<JoursCaiseDTO>>(c);
            }

            var result = _mapper.Map<List<JoursCaiseDTO>>(res);
            foreach (var j in result)
            {
                j.GjcEtablissement = await _etabService.GetById(j.GjcEtablissementId);
                j.Chauffeur = await _chauffeurService.GetById(j.CahuffeurId);
                j.Emetteur = await _emetteurService.GetById(j.EmetteurId);
                j.statusReceptionObj = statusComptage;
                j.statusComptageObj = await _statusService.GetById(j.StatusComtage);

            }
            return result;
        }
        public async Task<List<JoursCaiseDTO>> GetAllByStatusReceptionneNonValide(int userId)
        {
            var statusRecep = await _statusService.GetByCode(STATUS.RECEPTIONNE);
            var statusComptage = await _statusService.GetByCode(STATUS.VALIDEE);

            var listStatusRecep = new List<int?>();
            var listStatusCompt = new List<int?>();
            listStatusRecep.Add(statusRecep.Id);
            listStatusCompt.Add(statusComptage.Id);

            var res = new List<JoursCaiseDTO>();
            var user = await _userService.GetByUserId(userId);
            if (user?.ModePaies != null && user?.ModePaies.Count != 0)
            {
                res = await _joursCaisseRepository.GetWithModePaieRecep(user.ModePaies.Select(x => (int?)x.Id).ToList(), listStatusRecep, listStatusCompt);
                res = res.Distinct(new JourscaisseEqualityComparer()).ToList();
            }
            else
            {
                 var c = await _joursCaisseRepository.GenericGet(x => x.StatusReception == statusRecep.Id && x.StatusComtage != statusComptage.Id);
                 res = _mapper.Map<List<JoursCaiseDTO>>(c);

            }


            var result = _mapper.Map<List<JoursCaiseDTO>>(res);
            foreach (var j in result)
            {
                j.GjcEtablissement = await _etabService.GetById(j.GjcEtablissementId);
                j.Chauffeur = await _chauffeurService.GetById(j.CahuffeurId);
                j.Emetteur = await _emetteurService.GetById(j.EmetteurId);
                j.statusReceptionObj = statusRecep;
                j.statusComptageObj = await _statusService.GetById(j.StatusComtage);

            }
            return result;
        }
        public async Task<List<JoursCaiseDTO>> FiltreGetAllByStatusReceptionneNonValide(DateTime? dateTime1, DateTime? dateTime2,int userId)
        {
            var statusRecep = await _statusService.GetByCode(STATUS.RECEPTIONNE);
            var statusComptage = await _statusService.GetByCode(STATUS.VALIDEE);

            var listStatusRecep = new List<int?>();
            var listStatusCompt = new List<int?>();
            listStatusRecep.Add(statusRecep.Id);
            listStatusCompt.Add(statusComptage.Id);

            var res = new List<JoursCaiseDTO>();
            var user = await _userService.GetByUserId(userId);
            if (user?.ModePaies != null && user?.ModePaies.Count != 0)
            {
                res = await _joursCaisseRepository.GetWithModePaieRecepFiltreEnveloppeEnAttente(user.ModePaies.Select(x => (int?)x.Id).ToList(), listStatusRecep, listStatusCompt, dateTime1, dateTime2);
                res = res.Distinct(new JourscaisseEqualityComparer()).ToList();
            }
            else
            {
                var c = await _joursCaisseRepository.GenericGet(x => x.StatusReception == statusRecep.Id && x.StatusComtage != statusComptage.Id && x.GjcDateouv >= DateTime.Parse(dateTime1.ToString()).Date && x.GjcDateouv <= DateTime.Parse(dateTime2.ToString()).Date);
                res = _mapper.Map<List<JoursCaiseDTO>>(c);

            }


            var result = _mapper.Map<List<JoursCaiseDTO>>(res);
            foreach (var j in result)
            {
                j.GjcEtablissement = await _etabService.GetById(j.GjcEtablissementId);
                j.Chauffeur = await _chauffeurService.GetById(j.CahuffeurId);
                j.Emetteur = await _emetteurService.GetById(j.EmetteurId);
                j.statusReceptionObj = statusRecep;
                j.statusComptageObj = await _statusService.GetById(j.StatusComtage);

            }
            return result;
        }

        public async Task<List<JoursCaiseDTO>> GetAllByStatusCompter(int userId)
        {
            var statusComptage = await _statusService.GetByCode(STATUS.VALIDEE);
            var statusComptageEsp = await _statusService.GetByCode(STATUS.ESPECECOMPTE);
            var listStatusAccept = new List<string>();
            var listStatusReject = new List<string>();
            var res = new List<JoursCaiseDTO>();
            listStatusAccept.Add(STATUS.VALIDEE);
            listStatusAccept.Add(STATUS.ESPECECOMPTE);
            var user = await _userService.GetByUserId(userId);
            if (user?.ModePaies != null && user?.ModePaies.Count != 0)
            {
                 res = await _joursCaisseRepository.GetWithModePaie(user.ModePaies.Select(x => (int?)x.Id).ToList(), listStatusAccept, listStatusReject);
                res = res.Distinct(new JourscaisseEqualityComparer()).ToList();
            }
            else
            {
                //var cd = await _joursCaisseRepository.GenericGet(x => x.StatusComtage == statusComptage.Id || x.StatusComtage == statusComptageEsp.Id);

                var c = await _joursCaisseRepository.GetWithModePaie(new List<int?>(), listStatusAccept, listStatusReject);
                res =  _mapper.Map<List<JoursCaiseDTO>>(c);
            }
         
            return res.OrderByDescending(x => x.Id).Distinct().ToList();
          
        }

        public async Task<List<JoursCaiseDTO>> FiltreGetAllByStatusCompter(DateTime? dateTime1, DateTime? dateTime2, int userId)
        {
            var statusComptage = await _statusService.GetByCode(STATUS.VALIDEE);
            var statusComptageEsp = await _statusService.GetByCode(STATUS.ESPECECOMPTE);
            var listStatusAccept = new List<string>();
            var listStatusReject = new List<string>();
            var res = new List<JoursCaiseDTO>();
            listStatusAccept.Add(STATUS.VALIDEE);
            listStatusAccept.Add(STATUS.ESPECECOMPTE);
            var user = await _userService.GetByUserId(userId);
            if (user?.ModePaies != null && user?.ModePaies.Count != 0)
            {
                res = await _joursCaisseRepository.GetWithModePaieFiltreGetAllByStatusCmptNone(user.ModePaies.Select(x => (int?)x.Id).ToList(), listStatusAccept, listStatusReject, dateTime1, dateTime2);
                res = res.Distinct(new JourscaisseEqualityComparer()).ToList();
            }
            else
            {
                //var cd = await _joursCaisseRepository.GenericGet(x => x.StatusComtage == statusComptage.Id || x.StatusComtage == statusComptageEsp.Id);

                var c = await _joursCaisseRepository.GetWithModePaieFiltreGetAllByStatusCmptNone(new List<int?>(), listStatusAccept, listStatusReject, dateTime1, dateTime2);
                res = _mapper.Map<List<JoursCaiseDTO>>(c);
            }

            return res.OrderByDescending(x => x.Id).Distinct().ToList();
        }

        public async Task<List<JoursCaiseDTO>> ReceptionEnveloppes(List<JoursCaiseDTO> enveloppes, int chauffeurId, int emetteurId,int currentUserId)
        {
            var enveloppesR = _mapper.Map<List<Jourscaisse>>(enveloppes);
            var statusReception = await _statusService.GetByCode(STATUS.RECEPTIONNE);
            var statusEnAttente = await _statusService.GetByCode(STATUS.EN_ATTENTE);

            var resUpdate = await _joursCaisseRepository.EditAll(enveloppesR, statusReception.Id, statusReception.Id, chauffeurId, emetteurId);
            if (resUpdate)
            {
                var status = await _statusService.GetByCode(STATUS.RECEPTIONNE);
                foreach (var item in enveloppes)
                {
                    var res = await _piedecheService.GetAllByJoursCaisse(item.Id,currentUserId);
                    foreach (var c in res)
                    {
                        c.StatusComptage = status.Id;
                        var v = _mapper.Map<PiedecheDTO>(c);
                        var res1 = await _piedecheService.EditPiedeche(v, status.Id);
                    }
                }
            }
            return await this.GetAllByStatusEnAttente(currentUserId);
        }

        public async Task ChargerResync(List<JoursCaiseDTO> enveloppes,int motifId, string comments)
        {
           
                string SourceConnection = _configuration.GetConnectionString("DefaultConnectionExist");
                List<Jourscaisse> jourCaisseResultFinal = new List<Jourscaisse>();

                List<string> columnsJoursCaisse = new List<string>() {"GJC_NUMZCAISSE GjcNumzcaisse", "GJC_CAISSE GjcCaisse",
                "GJC_HEUREOUV GjcHeureouv",
                "GJC_DATEFERME GjcDateferme", "GJC_HEUREFERME GjcHeureferme", "GJC_USERFERME GjcUserferme",
                "GJC_ETABLISSEMENT GjcEtablissementcode","ET_LIBELLE GjcEtablissementlabel", "MCJ_LIBELLE GjcCaisselabel","GJC_ETAT GjcEtat", "GJC_DATEOUV GjcDateouv",
                "c.MCJ_LIBELLE GjcCaisseLabel" };

                foreach(var enveloppe in enveloppes)
                {
                    string queryJourCaisse = "SELECT " + String.Join(",", columnsJoursCaisse) + " FROM dbo.[JOURSCAISSE] j" +
       " left join ETABLISS e on j.GJC_ETABLISSEMENT = e.ET_ETABLISSEMENT " +
       "left join PARCAISSE c on j.GPK_CAISSE = c.MCJ_CAISSON " +
       "where GJC_ETAT = 'CDE' and GJC_NUMZCAISSE = '" + enveloppe.GjcNumzcaisse + "' and GJC_CAISSE= '"+ enveloppe.GjcCaisse + "'";



                    SqlConnection sourceConn = new SqlConnection(SourceConnection);

                    var joursCaises = await this.genericBaseRepository.ReadDataFromQueryByConnectionStringAsync<JoursCaiseDTO>(queryJourCaisse, SourceConnection);
                    var statusComptage = await _statusService.GetByCode(STATUS.RECEPTIONNE);

                foreach (JoursCaiseDTO element in joursCaises)
                    {
                      
                            var etab = await _etabService.GetByEtab(element.GjcEtablissementcode);
                            if (etab != null)
                            {
                                element.GjcEtablissementId = etab.Id;

                            }
                            element.DateCreate = DateTime.Now.Date;
                            element.StatusComtage = statusComptage.Id;
                            element.MontantCaisse = 0;
                            element.MotifId = motifId;
                            element.Comments = comments;
                            element.StatusReception = enveloppe.StatusReception;
                            element.AgentComptage = enveloppe.AgentComptage;
                            element.CahuffeurId = enveloppe.CahuffeurId;
                            element.EmetteurId = enveloppe.EmetteurId;
                            element.ReceptionDate = DateTime.Parse(enveloppe.ReceptionDate.ToString()).AddHours(1);
                            element.IsManuel = false;

                    var resultatJourDTO = _mapper.Map<JoursCaisse>(element);
                            var jour = await _joursCaisseRepository.Create(resultatJourDTO);
                            List<PiedecheDTO> p = await this.genericBaseRepository.ReadDataFromQueryByConnectionStringAsync<PiedecheDTO>(query(element.GjcEtablissementcode, element.GjcCaisse, element.GjcNumzcaisse, element.GjcDateouv), SourceConnection);
                            if (p.Count > 0)
                            {
                                var pp = p.GroupBy(x => x.GpeModepaie).ToList();
                                decimal? tot = 0;
                                foreach (var item in pp)
                                {
                                    var modeP = await _modePaiementService.GetByModePaie(item.Key);

                                    tot += item.Sum(x => decimal.Round((decimal)x.Montant, 3));
                                    Piedeche pi = new Piedeche
                                    {
                                        ModepaieId = modeP.Id,
                                        Montant = item.Sum(x => decimal.Round((decimal)x.Montant, 3)),
                                        JourcaisseId = jour.Id,
                                        DateCreation = DateTime.Now.Date,
                                        Source = SOURCE.CEGID,
                                        StatusComptage = statusComptage.Id,
                                        EspeceCash = 0,
                                        EspeceVerse = 0,
                                        MontantCompte = 0,
                                        Depence = 0,
                                        Commentaire = "",


                                    };
                                    var pii = _mapper.Map<PiedecheDTO>(pi);
                                    if (pii.Montant != 0)
                                    {
                                        await _piedecheService.Create(pii);
                                    }
                                }
                                jour.MontantCaisse = tot;
                                if (jour.MontantCaisse == 0)
                                {
                                    var rr = await _joursCaisseRepository.Delete(jour.Id);
                                }
                                else
                                {
                                    var journew = await _joursCaisseRepository.Edit(jour);

                                    jourCaisseResultFinal.Add(journew);
                                }
                            }
                            else
                            {
                                if (jour.MontantCaisse == 0)
                                {
                                    var rr = await _joursCaisseRepository.Delete(jour.Id);
                                }
                                else
                                {
                                    jourCaisseResultFinal.Add(jour);
                                }
                            }
                        

                    }


                }
        }



        public async Task<List<JoursCaiseDTO>> ReceptionReSyncEnveloppes(List<JoursCaiseDTO> enveloppes, int motifId, string comments,int userId)
        {
            var enveloppesR = _mapper.Map<List<Jourscaisse>>(enveloppes);
            var statusReception = await _statusService.GetByCode(STATUS.RECEPTIONNE);
            var statusEnAttente = await _statusService.GetByCode(STATUS.EN_ATTENTE);

            List<object> ints = new List<object>();
            foreach (var env in enveloppes)
            {
                ints.Add(env.Id);
            }

            await this.DeleteMany(ints);


            await this.ChargerResync(enveloppes, motifId, comments);
            return await this.GetAllByStatusReceptionne(userId);
        }

        public async Task<StatsDTO> GetStatDashboard(int userId)
        {
            StatsDTO stats = new StatsDTO();
            var allJC = await this.GetAll();
            stats.TotJoursCaisses = allJC.Count;

            var allEnAttente = await this.GetAllByStatusEnAttente(userId);
            stats.TotEnvEnAttente = allEnAttente.Count;

            var allReceptionne = await this.GetAllByStatusReceptionne(userId);
            stats.TotEnvReceptionne = allReceptionne.Count;


            var statusComptage = await _statusService.GetByCode(STATUS.VALIDEE);
            var allCompte = await _joursCaisseRepository.GenericGet(x => x.StatusComtage == statusComptage.Id);
            stats.TotEnvCompte = allCompte.Count;

            return stats;
        }

        public async Task<List<JoursCaiseDTO>> FiltreEnveloppeEnAttente(DateTime? dateTimeF, DateTime? dateTimeT, int userId)
        {
            var statusComptageAttente = await _statusService.GetByCode(STATUS.EN_ATTENTE);
            var statusComptageImprimee = await _statusService.GetByCode(STATUS.EN_ATTENTE_IMPRIME);


            var listStatusAccept = new List<int?>();
            var listStatusReject = new List<int?>();
            listStatusAccept.Add(statusComptageAttente.Id);
            listStatusAccept.Add(statusComptageImprimee.Id);
            var res = new List<JoursCaiseDTO>();
            var user = await _userService.GetByUserId(userId);
            if (user?.ModePaies != null && user?.ModePaies.Count != 0)
            {
                res = await _joursCaisseRepository.GetWithModePaieRecepFiltreEnveloppeEnAttente(user.ModePaies.Select(x => (int?)x.Id).ToList(), listStatusAccept, listStatusReject, dateTimeF, dateTimeT);
                res = res.Distinct(new JourscaisseEqualityComparer()).ToList();

            }
            else
            {
                 var c = await _joursCaisseRepository.GenericGet(x => (x.StatusReception == statusComptageAttente.Id || x.StatusReception == statusComptageImprimee.Id) && (x.GjcDateouv >= DateTime.Parse(dateTimeF.ToString()).Date && x.GjcDateouv <= DateTime.Parse(dateTimeT.ToString()).Date));
                res = _mapper.Map<List<JoursCaiseDTO>>(c);

                foreach (var j in res)
                {
                    j.GjcEtablissement = await _etabService.GetById(j.GjcEtablissementId);
                    if (j.StatusReception == 6)
                    {
                        j.statusReceptionObj = statusComptageImprimee;
                    }
                    else
                    {
                        j.statusReceptionObj = statusComptageAttente;

                    }

                }
            }





            return res;



          
        }

        public async Task<List<JoursCaiseDTO>> FiltreEnveloppeReceptionne(DateTime? dateTimeF, DateTime? dateTimeT,int userId)
        {
            var dateTime1 = DateTime.Parse(dateTimeF.ToString()).Date;
            var dateTime2 = DateTime.Parse(dateTimeT.ToString()).Date.AddHours(23).AddMinutes(59).AddSeconds(59);

            
            var statusComptage = await _statusService.GetByCode(STATUS.RECEPTIONNE);


            var listStatusAccept = new List<int?>();
            var listStatusReject = new List<int?>();
            listStatusAccept.Add(statusComptage.Id);
            var res = new List<JoursCaiseDTO>();
            var user = await _userService.GetByUserId(userId);
            if (user?.ModePaies != null && user?.ModePaies.Count != 0)
            {
                res = await _joursCaisseRepository.GetWithModePaieRecepFilter(user.ModePaies.Select(x => (int?)x.Id).ToList(), listStatusAccept, listStatusReject,dateTime1,dateTime2);
                res = res.Distinct(new JourscaisseEqualityComparer()).ToList();

            }
            else
            {
                var c = await _joursCaisseRepository.GenericGet(x => x.StatusReception == statusComptage.Id && x.ReceptionDate >= dateTime1 && x.ReceptionDate <= dateTime2);
                res = _mapper.Map<List<JoursCaiseDTO>>(c);
                foreach (var j in res)
                {
                    j.GjcEtablissement = await _etabService.GetById(j.GjcEtablissementId);
                    j.Chauffeur = await _chauffeurService.GetById(j.CahuffeurId);
                    j.Emetteur = await _emetteurService.GetById(j.EmetteurId);
                    j.statusReceptionObj = statusComptage;
                }
            }




            
            return res;
        }

        public async Task<JoursCaiseDTO> Update(JoursCaiseDTO joursCaisse)
        {
            joursCaisse.ValidationDateDebut = DateTime.Now;
            joursCaisse.ValidationDateFin = null;
            var jc = _mapper.Map<Jourscaisse>(joursCaisse);
            var res = await _joursCaisseRepository.Edit(jc);
            return _mapper.Map<JoursCaiseDTO>(res);
        }

        public async Task<List<JoursCaiseDTO>> ChangeStatusImprime(List<JoursCaiseDTO> joursCaisse, int userId)
        {
            var statusImprime = await _statusService.GetByCode(STATUS.EN_ATTENTE_IMPRIME);

            foreach (var jourcaise in joursCaisse)
            {
                try
                {
                    var jour = await _joursCaisseRepository.GetById(jourcaise.Id);
                    if(jour != null)
                    {
                        jour.StatusReception = statusImprime.Id;
                        jour.GjcEtablissement = null;
                        jour.GjcDateouv = jour.GjcDateouv;
                        jour.GjcDateferme = jour.GjcDateferme;
                        jour.DateComptage = jour.DateComptage;
                        jour.ReceptionDate = jour.ReceptionDate;
                        jour.ValidationDateDebut = jour.ValidationDateDebut;
                        jour.ValidationDateFin = jour.ValidationDateFin;
                        var res = await _joursCaisseRepository.Edit(jour);

                    }
                }
                catch (Exception ex)
                {
                }
            }

            return await GetAllByStatusEnAttente(userId);

        }
        public async Task<List<ReportDTO>> GenerateReport(DateTime? dateTime1, DateTime? dateTimeTo1, int etablissementId, int userId)
        {
            List<ReportDTO> reportDTOs = new List<ReportDTO>();

            var allJoursCaisses = await _joursCaisseRepository
                .GetAllWithDateOuvEtab(dateTime1, dateTimeTo1, etablissementId);
  

            foreach (var jourCaisse in allJoursCaisses)
            {
                var allPiedEches = await this._piedecheService.GetAllByJoursCaisse(jourCaisse.Id, userId);
                foreach (var c in allPiedEches)
                {
                    if (c.Status.Code != "NONE")
                    {
                        reportDTOs.Add(
                new ReportDTO
                {
                    Etablissement = jourCaisse.GjcEtablissementlabel,
                    Caisse = jourCaisse.GjcCaisse,
                    ReferenceZ = jourCaisse.GjcNumzcaisse.ToString(),
                    DateOuverture = jourCaisse.GjcDateouv.Date.ToString("dd/MM/yyyy"),
                    HeureOuverture = jourCaisse.GjcHeureouv,
                    ModePaiement = c.Modepaie.MpLibelle,
                    MontantTotal = new decimal((double)(c.Montant != null ? c.Montant : 0)),
                    MontantCompte = new decimal((double)(c.MontantCompte != null ? c.MontantCompte : 0)),
                    MontantVerse = new decimal((double)(c.EspeceVerse != null ? c.EspeceVerse : 0)),
                    Depense = new decimal((double)(c.Depence != null ? c.Depence : 0)),
                    Designation = c.Source,
                    Ecart = new decimal((double)(c.Ecart != null ? c.Ecart : 0)),
                    Statut = c.Status.Statuslibelle,
                    StatutEN = c.Status.Statuslibelleen,
                    StatutCode = c.Status.Code
                }
                );
                    }


                }

            }
            return reportDTOs;
        }



       
        public  async Task<JoursCaiseDTO> GetFullById(int id)
        {
            return await _joursCaisseRepository.GetFullById(id);  }

        public async Task<List<ReportBilletDTO>> GenerateReportDetailsBillet(DateTime? startDate, DateTime? endDate, ReportBilletRequestDTO reportBilletsDTO)
        {
            string etabString = reportBilletsDTO?.EtablissmentIds != null ? string.Join(",", reportBilletsDTO.EtablissmentIds) : "NULL";
            string request = "EXEC [dbo].[sp_GetNoteTotals]  @StartDate = '*StartDate*',@EndDate = '*EndDate*', @Etablissements = *Etablissements*, @Caisses = *Caisses*  ";
            request = request.Replace("*StartDate*", startDate.ToString());
            request = request.Replace("*EndDate*", endDate.ToString());
            request = request.Replace("*Etablissements*", reportBilletsDTO?.EtablissmentIds!=null?"'"+ etabString+"'":"NULL" );
            request = request.Replace("*Caisses*", reportBilletsDTO?.CaisseIds!= null ? "'" + string.Join(",", reportBilletsDTO?.CaisseIds) + "'" : "NULL");

            List<ReportBilletDTO> p = await this.genericBaseRepository.ReadDataFromQueryAsync<ReportBilletDTO>(request);
            return p;
        }
    }

}
