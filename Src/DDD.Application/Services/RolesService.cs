﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DDD.Application.BaseServices;
using DDD.Application.Interfaces;
using DTO;
using DDD.Domain.Interfaces;
using DDD.Infra.Data.Repository;
using Entity;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;


namespace DDD.Application.Services
{
    public class RolesService<TRolesDTO, TRoles, DataBaseContext> :
        AuthBaseService<TRolesDTO, Roles, DataBaseContext>, IRolesService<TRolesDTO>
       where TRolesDTO : RolesDTO, new()
       where TRoles : Roles, new()
       where DataBaseContext : DDDContext, new()
    {
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly IRolesRepository<Roles> _rolesRepository;

        public RolesService(ICurrentContextProvider contextProvider,
            DataBaseContext dataBaseContext, IMapper mapper,
                                  IRolesRepository<Roles> RolesRepository,
                                  IConfiguration configuration
                                 ) : base(
           contextProvider, dataBaseContext, mapper)

        {
            _mapper = mapper;
            _rolesRepository = RolesRepository;
            _configuration = configuration;


        }

        public async Task<RolesDTO> Update(RolesDTO role)
        {
            var entity = _mapper.Map<Roles>(role);
            var update = await _rolesRepository.Edit(entity);
            var rolesDto = _mapper.Map<RolesDTO>(update);
            return rolesDto;
        }
    }
}
