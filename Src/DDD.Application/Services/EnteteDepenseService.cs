﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DDD.Application.BaseServices;
using DDD.Application.Interfaces;
using DTO;
using DDD.Domain.Interfaces;
using DDD.Infra.Data.Repository;
using Entity;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using DDD.Application.utils;
using System.Net.Mail;
using System.Text;
using System.Xml.Linq;

namespace DDD.Application.Services
{
    public class EnteteDepenseService<TEnteteDepenseDTO, TEnteteDepense, DataBaseContext> :
        AuthBaseService<TEnteteDepenseDTO, EnteteDepense, DataBaseContext>, IEnteteDepenseService<TEnteteDepenseDTO>
       where TEnteteDepenseDTO : EnteteDepenseDTO, new()
       where TEnteteDepense : EnteteDepense, new()
       where DataBaseContext : DDDContext, new()
    {
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly IEnteteDepenseRepository<EnteteDepense> _EnteteDepenseRepository;
        private readonly ILigneDepenseService<LigneDepenseDTO> _ligneDepenseService;
        private readonly IStatusService<StatusDTO> _statusService;
        private readonly IUserService<UserDTO> _userService;
        private readonly ICompteDepenseService<CompteDepenseDTO> _CompteDepenseService;

        private readonly ICompteDepenseRepository<CompteDepense> _compteDepenseRepository;

        public EnteteDepenseService(ICurrentContextProvider contextProvider,
            DataBaseContext dataBaseContext, IMapper mapper,
                                  IEnteteDepenseRepository<EnteteDepense> EnteteDepenseRepository,
                                  IConfiguration configuration, IStatusService<StatusDTO> statusService,
                                  ICompteDepenseService<CompteDepenseDTO> CompteDepenseService,
                                  ICompteDepenseRepository<CompteDepense> compteDepenseRepository, IUserService<UserDTO> userService,
        ILigneDepenseService<LigneDepenseDTO> ligneDepenseService
                                 ) : base(
           contextProvider, dataBaseContext, mapper)

        {
            _mapper = mapper;
            _EnteteDepenseRepository = EnteteDepenseRepository;
            _configuration = configuration;
            _ligneDepenseService = ligneDepenseService;
            _statusService = statusService;
            _compteDepenseRepository = compteDepenseRepository;
            _userService = userService;
            _CompteDepenseService = CompteDepenseService;
        }

        public async Task<EnteteDepenseDTO> CreateEnteteDepense(TEnteteDepenseDTO enteteDepenseDTO)
        {
            var entity1 = new CompteDepense();
            var status = await _statusService.GetByCode(STATUS.DEMANDE);
            var statusAcp = await _statusService.GetByCode(STATUS.ACCEPTE);
            if (enteteDepenseDTO.TypeDepense == true)
            {
                var compteDep = await this._compteDepenseRepository.GetById(enteteDepenseDTO.CompteDepenseId);
                var newSolde = compteDep.Solde- enteteDepenseDTO.TotalTtc ;
                if (compteDep != null && compteDep.Solde >= enteteDepenseDTO.TotalTtc)
                {
                    compteDep.Solde = newSolde;
                     entity1 = _mapper.Map<CompteDepense>(compteDep);
                    enteteDepenseDTO.StatutId = statusAcp.Id;
                    enteteDepenseDTO.DateReponse = DateTime.Now;

                }
                else
                {
                    enteteDepenseDTO.StatutId = status.Id;

                }
            }
            else
            {
                enteteDepenseDTO.StatutId = status.Id;

            }

            string path = _configuration.GetSection("AppSettings")["PathDirectory"];
            if (!File.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            if (enteteDepenseDTO.FileBase64 != null)
            {
                enteteDepenseDTO.PieceJointe = Guid.NewGuid().ToString().Substring(0, 5) + enteteDepenseDTO.PieceJointe;
                string file = Path.Combine(path, enteteDepenseDTO.PieceJointe);
                byte[] fileBytes = Convert.FromBase64String(enteteDepenseDTO.FileBase64);
                File.WriteAllBytes(file, fileBytes);
            }
            enteteDepenseDTO.CreateDate = DateTime.Now;
            var res = await Create(enteteDepenseDTO);
            if(res != null&& entity1?.Id!=0)
            {
                var update1 = await _compteDepenseRepository.Edit(entity1);

            }
            var entities = _mapper.Map<List<LigneDepenseDTO>>(enteteDepenseDTO.LigneDepenses);
            entities.ForEach(e => e.DepenseId = res.Id);
            var lignesDTO = await _ligneDepenseService.CreateList(entities);
            var entity = _mapper.Map<List<LigneDepenseDTO>>(lignesDTO);

            res.LigneDepenses = entity;
            if(enteteDepenseDTO.MailTo!=null && enteteDepenseDTO.TypeDepense==false)
            try
            {
                    var userValidation = await _userService.GetByEmail(enteteDepenseDTO.MailTo);
                    string validateur = userValidation?.FirstName != null ? userValidation?.FirstName + " " + userValidation?.LastName : enteteDepenseDTO.MailTo;
                    var user = await _userService.GetByUserId((int)enteteDepenseDTO.CreateByUserId);
                    MailMessage mail = new MailMessage();

                mail.To.Add(enteteDepenseDTO.MailTo);
                mail.Subject = "Nouvelle demande de dépense";
                    mail.Body = user.FirstName + " " + user.LastName + " demande une dépense de " + enteteDepenseDTO.TotalTtc + " Dt. Dans l’attente de validation " + validateur;

                    if (!String.IsNullOrEmpty(enteteDepenseDTO.MailCC)) { 
                foreach (var elt in enteteDepenseDTO.MailCC.Split(";"))
                {
                    mail.CC.Add(elt);
                }
                }


                Mail.SendMail(mail);

            }
            catch (Exception ex)
            {
            }
            return res;

        }

        public async Task<FileStreamResult> downloadFile(int id)
        {
            var entete = await GetById(id);
            string path = _configuration.GetSection("AppSettings")["PathDirectory"];
            string file = Path.Combine(path, entete.PieceJointe);
            if (!File.Exists(file))
            {
                return null;
            }
            // byte[] filebyte=System.IO.File.ReadAllBytes(file);
            FileStream fileStream = System.IO.File.OpenRead(file);
            var res = new FileStreamResult(fileStream, "image/jpg")
            {
                FileDownloadName = entete.PieceJointe,
            };
            return res;

        }
        public async Task<EnteteDepenseDTO> deleteFile(int id)
        {
            var entete = await GetById(id);
            string path = _configuration.GetSection("AppSettings")["PathDirectory"];
            string file = Path.Combine(path, entete.PieceJointe);
            File.Delete(file);
            entete.PieceJointe = null;
            var entity = _mapper.Map<EnteteDepense>(entete);
            entity.PieceJointe = null;
            var res = await _EnteteDepenseRepository.EditEntete(entity);
            if (res != 0)
            {
                var resultat = await GetFullEnteteDepenseById(id);
                return resultat;
            }
            return null;

        }

        public async Task<EnteteDepenseDTO> GetFullEnteteDepenseById(int id)
        {
            var entete = await _EnteteDepenseRepository.GetByEnteteId(id);

            if (entete != null)
            {

                var lignes = await _ligneDepenseService.GetAllByEnteteId(id);
                entete.LigneDepenses = lignes;
            }
            return entete;

        }

        public async Task<EnteteDepenseDTO> Update(EnteteDepenseDTO enteteDepense)
        {
            var entete = await GetById(enteteDepense.Id);
            if (entete.PieceJointe != enteteDepense.PieceJointe)
            {
                string path = _configuration.GetSection("AppSettings")["PathDirectory"];
                string fileNew = Path.Combine(path, enteteDepense.PieceJointe);
                if (enteteDepense.FileBase64 != null)
                {
                    byte[] fileBytes = Convert.FromBase64String(enteteDepense.FileBase64);
                    File.WriteAllBytes(fileNew, fileBytes);
                }
                if (entete.PieceJointe != null)
                {
                    string fileOld = Path.Combine(path, entete.PieceJointe);
                    File.Delete(fileOld);
                }
            }
            var ligneList = new List<LigneDepenseDTO>();
            var entity = _mapper.Map<EnteteDepense>(enteteDepense);


            var listLigneExit = await _ligneDepenseService.GetAllByEnteteId(enteteDepense.Id);
            var ligneCreate = new List<LigneDepenseDTO>();
            var ligneDelete = new List<LigneDepenseDTO>();
            var ligneUpdate = new List<LigneDepenseDTO>();
            
            if (enteteDepense.LigneDepenses != null && enteteDepense.LigneDepenses.Count > 0)
            {
                LigneDepenseDTO res = null;
                foreach (var elt in enteteDepense.LigneDepenses)
                {
                    res = null;
                    var enteteFind = listLigneExit.Find(x => x.Id == elt.Id);
                    elt.DepenseId = enteteDepense.Id;
                    if (elt.Id != 0 && enteteFind != null)
                    {
                        ligneUpdate.Add(elt);
                        res = await _ligneDepenseService.Update(elt);
                    }
                    else if (elt.Id != 0 && enteteFind == null)
                    {
                        ligneDelete.Add(elt);
                    }
                    else
                    {
                        res = await _ligneDepenseService.Create(elt);

                    }
                    if (res != null) { ligneList.Add(res); }
                }
            }
            foreach(LigneDepenseDTO elt in listLigneExit)
            {
                var exist = ligneList.Find(x => x.Id == elt.Id);
                if (exist==null)
                {
                    var del = await _ligneDepenseService.Delete(elt.Id);

                }
            }
            var update = await _EnteteDepenseRepository.EditEntete(entity);
            var enteteDepenseDto = await GetFullEnteteDepenseById(update);
            // enteteDepenseDto.LigneDepenses = ligneList;


            //if (enteteDepense.StatutId == 9)
            //{
            //    var compteDep = await this._compteDepenseRepository.GetById(enteteDepense.de);
            //    var newSolde =  compteDep.Solde - enteteDepense.TotalTtc;
            //    if (compteDep != null || newSolde > compteDep.MontantLimite)
            //    {
            //        compteDep.Solde = newSolde;
            //        var entity1 = _mapper.Map<CompteDepense>(compteDep);
            //        var update1 = await _compteDepenseRepository.Edit(entity1);
            //        return enteteDepenseDto;
            //    }
            //}

            return enteteDepenseDto;
        }

        public async Task<List<EnteteDepenseDTO>> GetAllByEtablissementIdAndUserId(int etablissementId, int userId)
        {
            return await _EnteteDepenseRepository.GetAllByEtablissementIdAndUserId(etablissementId, userId);
        }

        public async Task<List<EnteteDepenseDTO>> GetAllByValidatorId(int userId)
        {
            return await _EnteteDepenseRepository.GetAllByValidatorId( userId);
        }
        public async Task<List<EnteteDepenseDTO>> GetAllByEtabIdCaisseIdNumZRef(int etabId, string caisseId, int? numZRef)
        {
            return await _EnteteDepenseRepository.GetAllByEtabIdCaisseIdNumZRef(etabId, caisseId, numZRef);
        }  
        
        public async Task<PagedResults> GetAllEnteteLazy(GridStateDTO grid, FilterDepancesDTO filters)
        {
            return await _EnteteDepenseRepository.GetAllEnteteLazy(grid, filters);
        }

        public async Task<List<EnteteDepenseDTO>> GetAllEntete()
        {
            return await _EnteteDepenseRepository.GetAllEntete();
        }

        public async Task<List<string>> GenerateComptable(List<EnteteDepenseDTO> depensetList)
        {


            List<EnteteDepenseDTO> depensetListFondDeCaisse = new List<EnteteDepenseDTO>();
            foreach (var item in depensetList)
            {
                if(item.TypeDepense == false)
                {
                    depensetListFondDeCaisse.Add(item);
                }
            }


            List<string> annexes = new List<string>();
            List<string> annexesTitles = new List<string>();

            if (depensetListFondDeCaisse.Count > 0)
            {
                //annexe 1
                List<GenerationComptableDTO> Annexe1 = new List<GenerationComptableDTO>();
                Annexe1 = this.GenerateAnnexe1(depensetListFondDeCaisse, Annexe1);
                List<string> Lignes1 = new List<string>();
                Lignes1 = this.GenerateLignes(Annexe1, Lignes1, 1);
                string AllAnnexe1 = this.GenerateAnnexe(Lignes1);
                annexes.Add(AllAnnexe1);
                annexesTitles.Add("Annexe 1");

                //annexe 2
                List<GenerationComptableDTO> Annexe2 = new List<GenerationComptableDTO>();
                Annexe2 = this.GenerateAnnexe2(depensetListFondDeCaisse, Annexe2);
                List<string> Lignes2 = new List<string>();
                Lignes2 = this.GenerateLignes(Annexe2, Lignes2, 2);
                string AllAnnexe2 = this.GenerateAnnexe(Lignes2);
                annexes.Add(AllAnnexe2);
                annexesTitles.Add("Annexe 2");
            }

            //annexe 3
            List<GenerationComptableDTO> Annexe3 = new List<GenerationComptableDTO>();
            Annexe3 = this.GenerateAnnexe3(depensetList, Annexe3);
            List<string> Lignes3 = new List<string>();
            Lignes3 = this.GenerateLignes(Annexe3, Lignes3,3);
            string AllAnnexe3 = this.GenerateAnnexe(Lignes3);
            annexes.Add(AllAnnexe3);
            annexesTitles.Add("Annexe 3");

            ////annexe 2
            //List<GenerationComptableDTO> Annexe4 = new List<GenerationComptableDTO>();
            //Annexe4 = this.GenerateAnnexe4(depensetList, Annexe4);
            //List<string> Lignes4 = new List<string>();
            //Lignes4 = this.GenerateLignes(Annexe4, Lignes4,4);
            //string AllAnnexe4 = this.GenerateAnnexe(Lignes4);
            //annexes.Add(AllAnnexe4);
            //annexesTitles.Add("Annexe 4");

            //update isCompta
            foreach (var dep in depensetList)
            {
                dep.IsCompta = true;
                var entity = _mapper.Map<EnteteDepense>(dep);
                await this._EnteteDepenseRepository.EditEntete(entity);
            }

            return this.SaveFile(annexesTitles, annexes);
        }

        private List<string> SaveFile(List<string> annexesTitles, List<string> annexes)
        {
            List<string> files = new List<string>();
            string exportDate = DateTime.Now.ToString("dd-MM-yyyy HH-MM-ss");
            string folderPath = @$"C:\Export TRA\Export Annexe {exportDate}";

            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
                Console.WriteLine("Folder created successfully.");
            }


            for (int i = 0; i <= annexes.Count - 1; i++)
            {
                string fileName = $"{annexesTitles[i]}.tra";

                string filePath = Path.Combine(folderPath, fileName);
                if (!File.Exists(filePath))
                {
                    using (StreamWriter writer = File.CreateText(filePath))
                    {
                        writer.WriteLine(annexes[i]);
                    }

                }

            }

            if (Directory.Exists(folderPath))
            {
                string[] fileNames = Directory.GetFiles(folderPath, "*.tra");
                foreach (string fileName in fileNames)
                {
                    try
                    {
                        string text = File.ReadAllText(fileName);

                        text = RemoveDoubleEmptyLinesAtEnd(text);

                        string base64String = ConvertToBase64String(text);
                        files.Add(base64String);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"Erreur lors de la lecture du fichier {fileName}: {ex.Message}");
                    }
                }
                return files;
            }
            else
            {
                Console.WriteLine("Le dossier spécifié n'existe pas.");
            }
            return null;
        }

        private string RemoveDoubleEmptyLinesAtEnd(string text)
        {
            string[] lines = text.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            int endIndex = lines.Length - 1;
            while (endIndex >= 0 && string.IsNullOrWhiteSpace(lines[endIndex]))
            {
                endIndex--;
            }
            return string.Join(Environment.NewLine, lines.Take(endIndex + 1));
        }

        static string ConvertToBase64String(string text)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(text);
            return Convert.ToBase64String(bytes);
        }

        private List<string> GenerateLignes(List<GenerationComptableDTO> annexe, List<string> lignes, int numberOfAnnexe)
        {
            if (numberOfAnnexe != 3)
            {
                lignes.Add("!");
            }
            else
            {
                GenerationComptableEnteteDTO generationComptableLigneEntete = new GenerationComptableEnteteDTO();
                generationComptableLigneEntete.ENTETE1.VALUE = this.GenerateValue("***S5CLIJRLETE001010119000101190001000000", generationComptableLigneEntete.ENTETE1.VALUE);
                generationComptableLigneEntete.ENTETE2.VALUE = this.GenerateValue(DateTime.Now.Date.ToString("ddMMyyyy"), generationComptableLigneEntete.ENTETE2.VALUE);
                generationComptableLigneEntete.ENTETE3.VALUE = this.GenerateValue("0800", generationComptableLigneEntete.ENTETE3.VALUE);
                generationComptableLigneEntete.ENTETE4.VALUE = this.GenerateValue("-   000000   01011900001X", generationComptableLigneEntete.ENTETE4.VALUE);
                lignes.Add(this.BuildStringEntete(generationComptableLigneEntete));
            }
            annexe.ForEach(element =>
            {
                lignes.Add(this.BuildString(element));
            });
            return lignes;
        }

        public string BuildString(GenerationComptableDTO obj)
        {
            string result = "";

            var properties = obj.GetType().GetProperties();

            foreach (var prop in properties)
            {
                if (prop.PropertyType == typeof(ParamsComptableDTO))
                {
                    ParamsComptableDTO param = (ParamsComptableDTO)prop.GetValue(obj);
                    result = result + param.VALUE;
                }
            }
            return result;
        }

        public string BuildStringEntete(GenerationComptableEnteteDTO obj)
        {
            string result = "";

            var properties = obj.GetType().GetProperties();

            foreach (var prop in properties)
            {
                if (prop.PropertyType == typeof(ParamsComptableDTO))
                {
                    ParamsComptableDTO param = (ParamsComptableDTO)prop.GetValue(obj);
                    result = result + param.VALUE;
                }
            }
            return result;
        }


        private List<GenerationComptableDTO> GenerateAnnexe1(List<EnteteDepenseDTO> resultatGrouppingPvAndEt, List<GenerationComptableDTO> annexe1)
        {
            resultatGrouppingPvAndEt.ForEach(element =>
            {
                GenerationComptableDTO generationComptableLigne1 = new GenerationComptableDTO();
                GenerationComptableDTO generationComptableLigne2 = new GenerationComptableDTO();

                generationComptableLigne1.JOURNAL.VALUE = this.GenerateValue("CAI", generationComptableLigne1.JOURNAL.VALUE);
                generationComptableLigne2.JOURNAL.VALUE = this.GenerateValue("CAI", generationComptableLigne2.JOURNAL.VALUE);

                generationComptableLigne1.DATECOMPTABLE.VALUE = this.GenerateValue(element.DateDepense?.ToString("ddMMyyyy"), generationComptableLigne1.DATECOMPTABLE.VALUE);
                generationComptableLigne2.DATECOMPTABLE.VALUE = this.GenerateValue(element.DateDepense?.ToString("ddMMyyyy"), generationComptableLigne2.DATECOMPTABLE.VALUE);


                generationComptableLigne1.TYPE_PIECE.VALUE = this.GenerateValue("OD", generationComptableLigne1.TYPE_PIECE.VALUE);
                generationComptableLigne2.TYPE_PIECE.VALUE = this.GenerateValue("OD", generationComptableLigne2.TYPE_PIECE.VALUE);


                generationComptableLigne1.GENERAL.VALUE = this.GenerateValue("5800000000", generationComptableLigne1.GENERAL.VALUE);
                generationComptableLigne2.GENERAL.VALUE = this.GenerateValue("5400000000", generationComptableLigne2.GENERAL.VALUE);


                generationComptableLigne1.TYPE_CPTE.VALUE = this.GenerateValue("", generationComptableLigne1.TYPE_CPTE.VALUE);
                generationComptableLigne2.TYPE_CPTE.VALUE = this.GenerateValue("", generationComptableLigne2.TYPE_CPTE.VALUE);


                generationComptableLigne1.REFINTERNE.VALUE = this.GenerateValue(string.Concat(element.Etablissement.EtPays, element.DateDepense?.ToString("ddMMyyyy")), generationComptableLigne1.REFINTERNE.VALUE);
                generationComptableLigne2.REFINTERNE.VALUE = this.GenerateValue(string.Concat(element.Etablissement.EtPays, element.DateDepense?.ToString("ddMMyyyy")), generationComptableLigne2.REFINTERNE.VALUE);

                generationComptableLigne1.LIBELLE.VALUE = this.GenerateValue("RETRAIT ESPECE", generationComptableLigne1.LIBELLE.VALUE);
                generationComptableLigne2.LIBELLE.VALUE = this.GenerateValue("RETRAIT ESPECE", generationComptableLigne2.LIBELLE.VALUE);

                generationComptableLigne1.ECHEANCE.VALUE = this.GenerateValue(element.DateDepense?.ToString("ddMMyyyy"), generationComptableLigne1.ECHEANCE.VALUE);
                generationComptableLigne2.ECHEANCE.VALUE = this.GenerateValue(element.DateDepense?.ToString("ddMMyyyy"), generationComptableLigne2.ECHEANCE.VALUE);

                generationComptableLigne1.SENS.VALUE = this.GenerateValue("D", generationComptableLigne1.SENS.VALUE);
                generationComptableLigne2.SENS.VALUE = this.GenerateValue("C", generationComptableLigne2.SENS.VALUE);

                string MontantTotal = FormatNumber(element.TotalTtc.ToString());

                generationComptableLigne1.MONTANT1.VALUE = this.GenerateValueInverse(MontantTotal, generationComptableLigne1.MONTANT1.VALUE);
                generationComptableLigne2.MONTANT1.VALUE = this.GenerateValueInverse(MontantTotal, generationComptableLigne2.MONTANT1.VALUE);

                generationComptableLigne1.TYPE_ECRITURE.VALUE = this.GenerateValue("S", generationComptableLigne1.TYPE_ECRITURE.VALUE);
                generationComptableLigne2.TYPE_ECRITURE.VALUE = this.GenerateValue("S", generationComptableLigne2.TYPE_ECRITURE.VALUE);

                generationComptableLigne1.NUMERO.VALUE = this.GenerateValueInverse("1", generationComptableLigne1.NUMERO.VALUE);
                generationComptableLigne2.NUMERO.VALUE = this.GenerateValueInverse("1", generationComptableLigne2.NUMERO.VALUE);

                generationComptableLigne1.DEVISE.VALUE = this.GenerateValue("TND", generationComptableLigne1.DEVISE.VALUE);
                generationComptableLigne2.DEVISE.VALUE = this.GenerateValue("TND", generationComptableLigne2.DEVISE.VALUE);



                generationComptableLigne1.ETABLISSEMENT.VALUE = this.GenerateValue(element.Etablissement.EtPays, generationComptableLigne1.ETABLISSEMENT.VALUE);
                generationComptableLigne2.ETABLISSEMENT.VALUE = this.GenerateValue(element.Etablissement.EtPays, generationComptableLigne2.ETABLISSEMENT.VALUE);


                annexe1.Add(generationComptableLigne1);
                annexe1.Add(generationComptableLigne2);
            });

            return annexe1;
        }

        private List<GenerationComptableDTO> GenerateAnnexe2(List<EnteteDepenseDTO> resultatGrouppingPvAndEt, List<GenerationComptableDTO> annexe2)
        {

            resultatGrouppingPvAndEt.ForEach(async element =>
            {
                var comptedepanse = _CompteDepenseService.GetByEtablissementIdSync(element.EtablissementId);
                GenerationComptableDTO generationComptableLigne1 = new GenerationComptableDTO();
                GenerationComptableDTO generationComptableLigne2 = new GenerationComptableDTO();

                generationComptableLigne1.JOURNAL.VALUE = this.GenerateValue(comptedepanse.JOURNAL, generationComptableLigne1.JOURNAL.VALUE);
                generationComptableLigne2.JOURNAL.VALUE = this.GenerateValue(comptedepanse.JOURNAL, generationComptableLigne2.JOURNAL.VALUE);

                generationComptableLigne1.DATECOMPTABLE.VALUE = this.GenerateValue(element.DateDepense?.ToString("ddMMyyyy"), generationComptableLigne1.DATECOMPTABLE.VALUE);
                generationComptableLigne2.DATECOMPTABLE.VALUE = this.GenerateValue(element.DateDepense?.ToString("ddMMyyyy"), generationComptableLigne2.DATECOMPTABLE.VALUE);


                generationComptableLigne1.TYPE_PIECE.VALUE = this.GenerateValue("OD", generationComptableLigne1.TYPE_PIECE.VALUE);
                generationComptableLigne2.TYPE_PIECE.VALUE = this.GenerateValue("OD", generationComptableLigne2.TYPE_PIECE.VALUE);

                //CompteCaisse dep dynamique
                generationComptableLigne1.GENERAL.VALUE = this.GenerateValue(comptedepanse.CompteCaisseG, generationComptableLigne1.GENERAL.VALUE);
                generationComptableLigne2.GENERAL.VALUE = this.GenerateValue("5800000000", generationComptableLigne2.GENERAL.VALUE);


                generationComptableLigne1.TYPE_CPTE.VALUE = this.GenerateValue("", generationComptableLigne1.TYPE_CPTE.VALUE);
                generationComptableLigne2.TYPE_CPTE.VALUE = this.GenerateValue("", generationComptableLigne2.TYPE_CPTE.VALUE);


                generationComptableLigne1.REFINTERNE.VALUE = this.GenerateValue(string.Concat(element.Etablissement.EtPays, element.DateDepense?.ToString("ddMMyyyy")), generationComptableLigne1.REFINTERNE.VALUE);
                generationComptableLigne2.REFINTERNE.VALUE = this.GenerateValue(string.Concat(element.Etablissement.EtPays, element.DateDepense?.ToString("ddMMyyyy")), generationComptableLigne2.REFINTERNE.VALUE);

                generationComptableLigne1.LIBELLE.VALUE = this.GenerateValue("ALIMENTATION CAISSE", generationComptableLigne1.LIBELLE.VALUE);
                generationComptableLigne2.LIBELLE.VALUE = this.GenerateValue("ALIMENTATION CAISSE", generationComptableLigne2.LIBELLE.VALUE);

                generationComptableLigne1.ECHEANCE.VALUE = this.GenerateValue(element.DateDepense?.ToString("ddMMyyyy"), generationComptableLigne1.ECHEANCE.VALUE);
                generationComptableLigne2.ECHEANCE.VALUE = this.GenerateValue(element.DateDepense?.ToString("ddMMyyyy"), generationComptableLigne2.ECHEANCE.VALUE);

                generationComptableLigne1.SENS.VALUE = this.GenerateValue("D", generationComptableLigne1.SENS.VALUE);
                generationComptableLigne2.SENS.VALUE = this.GenerateValue("C", generationComptableLigne2.SENS.VALUE);

                string MontantTotal = FormatNumber(element.TotalTtc.ToString());

                generationComptableLigne1.MONTANT1.VALUE = this.GenerateValueInverse(MontantTotal, generationComptableLigne1.MONTANT1.VALUE);
                generationComptableLigne2.MONTANT1.VALUE = this.GenerateValueInverse(MontantTotal, generationComptableLigne2.MONTANT1.VALUE);

                generationComptableLigne1.TYPE_ECRITURE.VALUE = this.GenerateValue("S", generationComptableLigne1.TYPE_ECRITURE.VALUE);
                generationComptableLigne2.TYPE_ECRITURE.VALUE = this.GenerateValue("S", generationComptableLigne2.TYPE_ECRITURE.VALUE);

                generationComptableLigne1.NUMERO.VALUE = this.GenerateValueInverse("1", generationComptableLigne1.NUMERO.VALUE);
                generationComptableLigne2.NUMERO.VALUE = this.GenerateValueInverse("1", generationComptableLigne2.NUMERO.VALUE);

                generationComptableLigne1.DEVISE.VALUE = this.GenerateValue("TND", generationComptableLigne1.DEVISE.VALUE);
                generationComptableLigne2.DEVISE.VALUE = this.GenerateValue("TND", generationComptableLigne2.DEVISE.VALUE);


                generationComptableLigne1.ETABLISSEMENT.VALUE = this.GenerateValue(element.Etablissement.EtPays, generationComptableLigne1.ETABLISSEMENT.VALUE);
                generationComptableLigne2.ETABLISSEMENT.VALUE = this.GenerateValue(element.Etablissement.EtPays, generationComptableLigne2.ETABLISSEMENT.VALUE);

                annexe2.Add(generationComptableLigne1);
                annexe2.Add(generationComptableLigne2);
            });

            return annexe2;
        }


        private List<GenerationComptableDTO> GenerateAnnexe3(List<EnteteDepenseDTO> resultatGrouppingPvAndEt, List<GenerationComptableDTO> annexe3)
        {
            resultatGrouppingPvAndEt.ForEach(async element =>
            {
                var lignedep = _ligneDepenseService.GetAllByEnteteIdSync(element.Id);
                GenerationComptableDTO generationComptableLigne0 = new GenerationComptableDTO();
                GenerationComptableDTO generationComptableLigne1 = new GenerationComptableDTO();
                GenerationComptableDTO generationComptableLigne2 = new GenerationComptableDTO();

                generationComptableLigne0.JOURNAL.VALUE = this.GenerateValue("AC9", generationComptableLigne0.JOURNAL.VALUE);
                generationComptableLigne1.JOURNAL.VALUE = this.GenerateValue("AC9", generationComptableLigne1.JOURNAL.VALUE);
                generationComptableLigne2.JOURNAL.VALUE = this.GenerateValue("AC9", generationComptableLigne2.JOURNAL.VALUE);

                generationComptableLigne0.DATECOMPTABLE.VALUE = this.GenerateValue(element.DateDepense?.ToString("ddMMyyyy"), generationComptableLigne0.DATECOMPTABLE.VALUE);
                generationComptableLigne1.DATECOMPTABLE.VALUE = this.GenerateValue(element.DateDepense?.ToString("ddMMyyyy"), generationComptableLigne1.DATECOMPTABLE.VALUE);
                generationComptableLigne2.DATECOMPTABLE.VALUE = this.GenerateValue(element.DateDepense?.ToString("ddMMyyyy"), generationComptableLigne2.DATECOMPTABLE.VALUE);

                generationComptableLigne0.TYPE_PIECE.VALUE = this.GenerateValue("OD", generationComptableLigne0.TYPE_PIECE.VALUE);
                generationComptableLigne1.TYPE_PIECE.VALUE = this.GenerateValue("OD", generationComptableLigne1.TYPE_PIECE.VALUE);
                generationComptableLigne2.TYPE_PIECE.VALUE = this.GenerateValue("OD", generationComptableLigne2.TYPE_PIECE.VALUE);

                generationComptableLigne0.GENERAL.VALUE = this.GenerateValue(lignedep.FirstOrDefault().Article.CompteC, generationComptableLigne0.GENERAL.VALUE);
                generationComptableLigne1.GENERAL.VALUE = this.GenerateValue(lignedep.FirstOrDefault().Article.CompteC, generationComptableLigne1.GENERAL.VALUE);
                generationComptableLigne2.GENERAL.VALUE = this.GenerateValue("4011000000", generationComptableLigne2.GENERAL.VALUE);


                generationComptableLigne1.TYPE_CPTE.VALUE = this.GenerateValue("A", generationComptableLigne1.TYPE_CPTE.VALUE);
                generationComptableLigne2.TYPE_CPTE.VALUE = this.GenerateValue("X", generationComptableLigne2.TYPE_CPTE.VALUE);

                generationComptableLigne1.UXILIAIRE_OU_SECTION.VALUE = this.GenerateValue(element.Etablissement.EtLangue, generationComptableLigne1.UXILIAIRE_OU_SECTION.VALUE);
                generationComptableLigne2.UXILIAIRE_OU_SECTION.VALUE = this.GenerateValue(element.Fournisseur.CompteC, generationComptableLigne2.UXILIAIRE_OU_SECTION.VALUE);


                generationComptableLigne0.REFINTERNE.VALUE = this.GenerateValue(string.Concat(element.Etablissement.EtPays, element.DateDepense?.ToString("ddMMyyyy")), generationComptableLigne0.REFINTERNE.VALUE);
                generationComptableLigne1.REFINTERNE.VALUE = this.GenerateValue(string.Concat(element.Etablissement.EtPays, element.DateDepense?.ToString("ddMMyyyy")), generationComptableLigne1.REFINTERNE.VALUE);
                generationComptableLigne2.REFINTERNE.VALUE = this.GenerateValue(string.Concat(element.Etablissement.EtPays, element.DateDepense?.ToString("ddMMyyyy")), generationComptableLigne2.REFINTERNE.VALUE);

                generationComptableLigne0.LIBELLE.VALUE = this.GenerateValue(lignedep.FirstOrDefault().Article.Designation, generationComptableLigne0.LIBELLE.VALUE);
                generationComptableLigne1.LIBELLE.VALUE = this.GenerateValue(lignedep.FirstOrDefault().Article.Designation, generationComptableLigne1.LIBELLE.VALUE);
                generationComptableLigne2.LIBELLE.VALUE = this.GenerateValue(lignedep.FirstOrDefault().Article.Designation, generationComptableLigne2.LIBELLE.VALUE);

                generationComptableLigne0.ECHEANCE.VALUE = this.GenerateValue(element.DateDepense?.ToString("ddMMyyyy"), generationComptableLigne0.ECHEANCE.VALUE);
                generationComptableLigne1.ECHEANCE.VALUE = this.GenerateValue(element.DateDepense?.ToString("ddMMyyyy"), generationComptableLigne1.ECHEANCE.VALUE);
                generationComptableLigne2.ECHEANCE.VALUE = this.GenerateValue(element.DateDepense?.ToString("ddMMyyyy"), generationComptableLigne2.ECHEANCE.VALUE);

                generationComptableLigne0.SENS.VALUE = this.GenerateValue("D", generationComptableLigne0.SENS.VALUE);
                generationComptableLigne1.SENS.VALUE = this.GenerateValue("D", generationComptableLigne1.SENS.VALUE);
                generationComptableLigne2.SENS.VALUE = this.GenerateValue("C", generationComptableLigne2.SENS.VALUE);

                string MontantTotal = FormatNumber(element.TotalTtc.ToString());

                generationComptableLigne0.MONTANT1.VALUE = this.GenerateValueInverse(MontantTotal, generationComptableLigne0.MONTANT1.VALUE);
                generationComptableLigne1.MONTANT1.VALUE = this.GenerateValueInverse(MontantTotal, generationComptableLigne1.MONTANT1.VALUE);
                generationComptableLigne2.MONTANT1.VALUE = this.GenerateValueInverse(MontantTotal, generationComptableLigne2.MONTANT1.VALUE);

                generationComptableLigne0.TYPE_ECRITURE.VALUE = this.GenerateValue("S", generationComptableLigne0.TYPE_ECRITURE.VALUE);
                generationComptableLigne1.TYPE_ECRITURE.VALUE = this.GenerateValue("S", generationComptableLigne1.TYPE_ECRITURE.VALUE);
                generationComptableLigne2.TYPE_ECRITURE.VALUE = this.GenerateValue("S", generationComptableLigne2.TYPE_ECRITURE.VALUE);

                generationComptableLigne0.NUMERO.VALUE = this.GenerateValueInverse("1", generationComptableLigne0.NUMERO.VALUE);
                generationComptableLigne1.NUMERO.VALUE = this.GenerateValueInverse("1", generationComptableLigne1.NUMERO.VALUE);
                generationComptableLigne2.NUMERO.VALUE = this.GenerateValueInverse("1", generationComptableLigne2.NUMERO.VALUE);

                generationComptableLigne0.DEVISE.VALUE = this.GenerateValue("TND", generationComptableLigne0.DEVISE.VALUE);
                generationComptableLigne1.DEVISE.VALUE = this.GenerateValue("TND", generationComptableLigne1.DEVISE.VALUE);
                generationComptableLigne2.DEVISE.VALUE = this.GenerateValue("TND", generationComptableLigne2.DEVISE.VALUE);

                generationComptableLigne0.ETABLISSEMENT.VALUE = this.GenerateValue(element.Etablissement.EtPays, generationComptableLigne0.ETABLISSEMENT.VALUE);
                generationComptableLigne1.ETABLISSEMENT.VALUE = this.GenerateValue(element.Etablissement.EtPays, generationComptableLigne1.ETABLISSEMENT.VALUE);
                generationComptableLigne2.ETABLISSEMENT.VALUE = this.GenerateValue(element.Etablissement.EtPays, generationComptableLigne2.ETABLISSEMENT.VALUE);


                generationComptableLigne1.AXE.VALUE = this.GenerateValue("A1", generationComptableLigne1.AXE.VALUE);

                generationComptableLigne1.SOUSPLAN1.VALUE = this.GenerateValue(element.Etablissement.EtLangue, generationComptableLigne1.SOUSPLAN1.VALUE);
                generationComptableLigne2.SOUSPLAN1.VALUE = this.GenerateValue("", generationComptableLigne2.SOUSPLAN1.VALUE);


                annexe3.Add(generationComptableLigne0);
                annexe3.Add(generationComptableLigne1);
                annexe3.Add(generationComptableLigne2);
            });

            return annexe3;
        }

        private List<GenerationComptableDTO> GenerateAnnexe4(List<EnteteDepenseDTO> resultatGrouppingPvAndEt, List<GenerationComptableDTO> annexe4)
        {
            resultatGrouppingPvAndEt.ForEach(async element =>
            {
                var comptedepanse = _CompteDepenseService.GetByEtablissementIdSync(element.EtablissementId);
                var lignedep = _ligneDepenseService.GetAllByEnteteIdSync(element.Id);

                GenerationComptableDTO generationComptableLigne1 = new GenerationComptableDTO();
                GenerationComptableDTO generationComptableLigne2 = new GenerationComptableDTO();

                generationComptableLigne1.JOURNAL.VALUE = this.GenerateValue(comptedepanse.JOURNAL, generationComptableLigne1.JOURNAL.VALUE);
                generationComptableLigne2.JOURNAL.VALUE = this.GenerateValue(comptedepanse.JOURNAL, generationComptableLigne2.JOURNAL.VALUE);

                generationComptableLigne1.DATECOMPTABLE.VALUE = this.GenerateValue(element.DateDepense?.ToString("ddMMyyyy"), generationComptableLigne1.DATECOMPTABLE.VALUE);
                generationComptableLigne2.DATECOMPTABLE.VALUE = this.GenerateValue(element.DateDepense?.ToString("ddMMyyyy"), generationComptableLigne2.DATECOMPTABLE.VALUE);


                generationComptableLigne1.TYPE_PIECE.VALUE = this.GenerateValue("OD", generationComptableLigne1.TYPE_PIECE.VALUE);
                generationComptableLigne2.TYPE_PIECE.VALUE = this.GenerateValue("OD", generationComptableLigne2.TYPE_PIECE.VALUE);

                //CompteCaisse dep dynamique
                generationComptableLigne1.GENERAL.VALUE = this.GenerateValue("4011000000", generationComptableLigne1.GENERAL.VALUE);
                generationComptableLigne2.GENERAL.VALUE = this.GenerateValue(comptedepanse.CompteCaisseG, generationComptableLigne2.GENERAL.VALUE);


                generationComptableLigne1.TYPE_CPTE.VALUE = this.GenerateValue("X", generationComptableLigne1.TYPE_CPTE.VALUE);
                generationComptableLigne2.TYPE_CPTE.VALUE = this.GenerateValue("", generationComptableLigne2.TYPE_CPTE.VALUE);

                generationComptableLigne1.UXILIAIRE_OU_SECTION.VALUE = this.GenerateValue(element.Fournisseur.CompteC, generationComptableLigne1.UXILIAIRE_OU_SECTION.VALUE);
                generationComptableLigne2.UXILIAIRE_OU_SECTION.VALUE = this.GenerateValue("", generationComptableLigne2.UXILIAIRE_OU_SECTION.VALUE);



                generationComptableLigne1.REFINTERNE.VALUE = this.GenerateValue(string.Concat(element.Etablissement.EtPays, element.DateDepense?.ToString("ddMMyyyy")), generationComptableLigne1.REFINTERNE.VALUE);
                generationComptableLigne2.REFINTERNE.VALUE = this.GenerateValue(string.Concat(element.Etablissement.EtPays, element.DateDepense?.ToString("ddMMyyyy")), generationComptableLigne2.REFINTERNE.VALUE);

                generationComptableLigne1.LIBELLE.VALUE = this.GenerateValue(lignedep.FirstOrDefault().Article.Designation, generationComptableLigne1.LIBELLE.VALUE);
                generationComptableLigne2.LIBELLE.VALUE = this.GenerateValue(lignedep.FirstOrDefault().Article.Designation, generationComptableLigne2.LIBELLE.VALUE);

                generationComptableLigne1.ECHEANCE.VALUE = this.GenerateValue(element.DateDepense?.ToString("ddMMyyyy"), generationComptableLigne1.ECHEANCE.VALUE);
                generationComptableLigne2.ECHEANCE.VALUE = this.GenerateValue(element.DateDepense?.ToString("ddMMyyyy"), generationComptableLigne2.ECHEANCE.VALUE);

                generationComptableLigne1.SENS.VALUE = this.GenerateValue("D", generationComptableLigne1.SENS.VALUE);
                generationComptableLigne2.SENS.VALUE = this.GenerateValue("C", generationComptableLigne2.SENS.VALUE);

                string MontantTotal = FormatNumber(element.TotalTtc.ToString());

                generationComptableLigne1.MONTANT1.VALUE = this.GenerateValueInverse(MontantTotal, generationComptableLigne1.MONTANT1.VALUE);
                generationComptableLigne2.MONTANT1.VALUE = this.GenerateValueInverse(MontantTotal, generationComptableLigne2.MONTANT1.VALUE);

                generationComptableLigne1.TYPE_ECRITURE.VALUE = this.GenerateValue("S", generationComptableLigne1.TYPE_ECRITURE.VALUE);
                generationComptableLigne2.TYPE_ECRITURE.VALUE = this.GenerateValue("S", generationComptableLigne2.TYPE_ECRITURE.VALUE);

                generationComptableLigne1.NUMERO.VALUE = this.GenerateValueInverse("1", generationComptableLigne1.NUMERO.VALUE);
                generationComptableLigne2.NUMERO.VALUE = this.GenerateValueInverse("1", generationComptableLigne2.NUMERO.VALUE);

                generationComptableLigne1.DEVISE.VALUE = this.GenerateValue("TND", generationComptableLigne1.DEVISE.VALUE);
                generationComptableLigne2.DEVISE.VALUE = this.GenerateValue("TND", generationComptableLigne2.DEVISE.VALUE);


                generationComptableLigne1.ETABLISSEMENT.VALUE = this.GenerateValue(element.Etablissement.EtPays, generationComptableLigne1.ETABLISSEMENT.VALUE);
                generationComptableLigne2.ETABLISSEMENT.VALUE = this.GenerateValue(element.Etablissement.EtPays, generationComptableLigne2.ETABLISSEMENT.VALUE);

                annexe4.Add(generationComptableLigne1);
                annexe4.Add(generationComptableLigne2);
            });

            return annexe4;
        }

        private string GenerateValue(string value, string result)
        {
            if (value == null)
            {
                return result;
            }
            else
            {
                return value + result.Substring(value.Length);
            }
        }


        private string GenerateValueInverse(string value, string result)
        {
            if (value == null)
            {
                return result;
            }
            else
            {
                return result.Substring(value.Length) + value;
            }
        }

        private string GenerateAnnexe(List<string> lignes)
        {
            StringBuilder stringBuilder = new StringBuilder();

            foreach (string objet in lignes)
            {
                stringBuilder.Append(objet);
                stringBuilder.Append("\n");
            }

            return stringBuilder.ToString();
        }

        static string FormatNumber(string input)
        {
            // Vérifier si la chaîne contient une virgule
            if (input.Contains(','))
            {
                // Extraire la partie avant et après la virgule
                string[] parts = input.Split(',');

                // Ajouter des zéros à la partie après la virgule si nécessaire
                if (parts.Length > 1 && parts[1].Length < 3)
                {
                    parts[1] = parts[1].PadRight(3, '0');
                }

                // Rejoindre les parties avec un point
                return string.Join(".", parts);
            }
            else
            {
                // Si la chaîne ne contient pas de virgule, ajouter une virgule suivie de trois zéros
                return input + ".000";
            }
        }

    }
}
