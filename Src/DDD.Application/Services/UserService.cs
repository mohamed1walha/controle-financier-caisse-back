﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DDD.Application.BaseServices;
using DDD.Application.Interfaces;
using DTO;
using DDD.Domain.Interfaces;
using DDD.Infra.Data.Repository;
using Entity;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Identity;
using AU.DTO.AuthDTO;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Text;

namespace DDD.Application.Services
{
    public class UserService<TUserDTO, TUser, DataBaseContext> :
        AuthBaseService<TUserDTO, User, DataBaseContext>, IUserService<TUserDTO>
       where TUserDTO : UserDTO, new()
       where TUser : User, new()
       where DataBaseContext : DDDContext, new()
    {
        //private readonly UserManager<TUser> _userManager;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly IUserRepository<User> _UserRepository;

        public UserService(ICurrentContextProvider contextProvider,
            DataBaseContext dataBaseContext, IMapper mapper,
                                  IUserRepository<User> UserRepository,
                                  IConfiguration configuration
                                 ) : base(
           contextProvider, dataBaseContext, mapper)

        {
               _mapper = mapper;
            _UserRepository = UserRepository;
            _configuration = configuration;


        }

        public async Task<List<UserDTO>> GetAllUsers()
        {
            List<UserDTO> userDTOs = await _UserRepository.GetAllUsers();
            return userDTOs;
        }

        public async Task<List<UserDTO>> GetAllUsersSimpleData()
        {
            List<UserDTO> userDTOs = await _UserRepository.GetAllUsersSimpleData();
            return userDTOs;
        }
        public async Task<List<UserDTO>> GetAllByEtabId(int etabId)
        {
            List<UserDTO> userDTOs = await _UserRepository.GetAllByEtabId(etabId);
            return userDTOs;
        } 
        public async Task<List<UserDTO>> GetAllValidateur()
        {
            List<UserDTO> userDTOs = await _UserRepository.GetAllValidateur();
            return userDTOs;
        }

        public async Task<UserDTO> GetByUserName(string username)
        {
            UserDTO user = await _UserRepository.GetByUserName(username);
            return user;

        } 
      

        public async Task<UserDTO> Update(UserDTO userDTO)
        {
            var entity = _mapper.Map<User>(userDTO);
            var update = await _UserRepository.Edit(entity);
            var newuserDto = _mapper.Map<UserDTO>(update);
            return newuserDto;
        }
        public async Task<AuthResult<Token>> ChangePassword(ChangePasswordDTO changePasswordDto, int currentUserId,string password,string Newpassword)
        {
            if (changePasswordDto == null ||
                string.IsNullOrEmpty(changePasswordDto.ConfirmPassword) ||
                string.IsNullOrEmpty(changePasswordDto.Password) ||
                changePasswordDto.Password != changePasswordDto.ConfirmPassword
            )
                return AuthResult<Token>.UnvalidatedResult;

            if (currentUserId > 0)
            {           
                var userDTO = await GetByUserId(currentUserId);
                var user = _mapper.Map<User>(userDTO);
               if (user.Password== password)
                {
                    userDTO.Password = Newpassword;
                    var res = await Update(userDTO);

                    if (res != null) {
                        return AuthResult<Token>.SucceededResult;
                    }

                   
                }
                else return AuthResult<Token>.UnvalidatedResult;

            }

            return AuthResult<Token>.UnauthorizedResult;
        }

        public async Task<UserDTO> GetByIdWithRole(int id)
        {
            UserDTO user = await _UserRepository.GetByIdWithRole(id);
            return user;
        }

        public async Task<UserDTO> GetByUserId(int userId)
        {
            var user = await _UserRepository.GetByUserId(userId);
            return user;
        }

        public UserDTO GetByUserIdSync(int userId)
        {
            var user = _UserRepository.GetByUserIdSync(userId);
            return user;
        }
        public async Task<bool> RestorePassword(RestorePasswordDTO restorePasswordDto, string password)
        {
            if (restorePasswordDto == null ||
                string.IsNullOrEmpty(restorePasswordDto.UserName) ||
                string.IsNullOrEmpty(restorePasswordDto.Token) ||
                string.IsNullOrEmpty(restorePasswordDto.NewPassword) ||
                string.IsNullOrEmpty(restorePasswordDto.ConfirmPassword) ||
                restorePasswordDto.ConfirmPassword != restorePasswordDto.NewPassword
            )
                return  false;

            var user = await GetByUserName(restorePasswordDto.UserName);

            if (user != null && user.Id > 0)
            {

                user.Password = password;
                var result = await Update(user);
                if (result!=null)
                {
                                      
                    return true;
                }
            }
            return false;
        }

        public async Task<UserDTO> GetByEmail(string mailTo)
        {
            var user = await _UserRepository.GenericGetFirstOrDefaultAsync(x=>x.Email==mailTo);
            var res =_mapper.Map<UserDTO>(user);
            return res;
        }
    }
}
