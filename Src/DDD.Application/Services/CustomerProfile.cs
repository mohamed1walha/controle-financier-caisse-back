﻿using AutoMapper;
using DTO;
using DDD.Domain.Models;


namespace DDD.Application.Services
{
    public class CustomerProfile :Profile
    {
        public CustomerProfile()
        {
            CreateMap<Customer, CustomerViewModel>();
        }

    }
}
