﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DDD.Application.BaseServices;
using DDD.Application.Interfaces;
using DTO;
using DDD.Domain.Interfaces;
using DDD.Infra.Data.Repository;
using Entity;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;


namespace DDD.Application.Services
{
    public class CompteDepenseService<TCompteDepenseDTO, TCompteDepense, DataBaseContext> :
        AuthBaseService<TCompteDepenseDTO, CompteDepense, DataBaseContext>, ICompteDepenseService<TCompteDepenseDTO>
       where TCompteDepenseDTO : CompteDepenseDTO, new()
       where TCompteDepense : CompteDepense, new()
       where DataBaseContext : DDDContext, new()
    {
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly ICompteDepenseRepository<CompteDepense> _CompteDepenseRepository;

        public CompteDepenseService(ICurrentContextProvider contextProvider,
            DataBaseContext dataBaseContext, IMapper mapper,
                                  ICompteDepenseRepository<CompteDepense> CompteDepenseRepository,
                                  IConfiguration configuration
                                 ) : base(
           contextProvider, dataBaseContext, mapper)

        {
            _mapper = mapper;
            _CompteDepenseRepository = CompteDepenseRepository;
            _configuration = configuration;


        }

        public async Task<List<CompteDepenseDTO>> GetAllCompteDepense()
        {
            return await _CompteDepenseRepository.GetAllCompteDepense();
        }

        public async Task<CompteDepenseDTO> GetByEtablissementId(int? id)
        {
            try
            {
                var res = await _CompteDepenseRepository.GenericGetFirstOrDefaultAsync(x => x.EtablissementId == id);
                return _mapper.Map<CompteDepenseDTO>(res);

            }
            catch (Exception e)
            {
                return null;
            }
        }

        public CompteDepenseDTO GetByEtablissementIdSync(int? id)
        {
            var res =  _CompteDepenseRepository.GenericGetFirstOrDefault(x => x.EtablissementId == id);
            return _mapper.Map<CompteDepenseDTO>(res);
        }
        public async Task<CompteDepenseDTO> GetByEtabId(int? id)
        {
            var res= await _CompteDepenseRepository.GenericGetFirstOrDefaultAsync(x=>x.EtablissementId==id);
            return _mapper.Map<CompteDepenseDTO>(res);
        }

        public async Task<CompteDepenseDTO> Update(CompteDepenseDTO CompteDepense)
        {
            var entity = _mapper.Map<CompteDepense>(CompteDepense);
            var update = await _CompteDepenseRepository.Edit(entity);
            var CompteDepenseDto = _mapper.Map<CompteDepenseDTO>(update);
            return CompteDepenseDto;
        }
    }
}
