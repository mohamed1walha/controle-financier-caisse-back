﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DDD.Application.BaseServices;
using DDD.Application.Interfaces;
using DTO;
using DDD.Domain.Interfaces;
using DDD.Infra.Data.Repository;
using Entity;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;


namespace DDD.Application.Services
{
    public class ParcaisseService<TParcaisseDTO, TParcaisse, DataBaseContext> :
        AuthBaseService<TParcaisseDTO, Parcaisse, DataBaseContext>, IParcaisseService<TParcaisseDTO>
       where TParcaisseDTO : ParcaisseDTO, new()
       where TParcaisse : Parcaisse, new()
       where DataBaseContext : DDDContext, new()
    {
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly IParcaisseRepository<Parcaisse> _caissonRepository;

        public ParcaisseService(ICurrentContextProvider contextProvider,
            DataBaseContext dataBaseContext, IMapper mapper,
                                  IParcaisseRepository<Parcaisse> ParcaisseRepository,
                                  IConfiguration configuration
                                 ) : base(
           contextProvider, dataBaseContext, mapper)

        {
            _mapper = mapper;
            _caissonRepository = ParcaisseRepository;
            _configuration = configuration;


        }

        public async Task<List<ParcaisseDTO>> GetAllByEtabId(string etabId)
        {
            var res =await _caissonRepository.GenericGet(x=>x.GpkEtablissement==etabId.ToString());
            return _mapper.Map<List<ParcaisseDTO>>(res);
        }    
        public async Task<List<ParcaisseDTO>> GetAllByEtabIds(List<string> etabIds)
        {
            var res =await _caissonRepository.GenericGet(x=>etabIds.Contains(x.GpkEtablissement));
            return _mapper.Map<List<ParcaisseDTO>>(res);
        }

        public async Task<ParcaisseDTO> Update(ParcaisseDTO caisson)
        {
            var entity = _mapper.Map<Parcaisse>(caisson);
            var update = await _caissonRepository.Edit(entity);
            var caissonDto = _mapper.Map<ParcaisseDTO>(update);
            return caissonDto;
        }
    }
}
