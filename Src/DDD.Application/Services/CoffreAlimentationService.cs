﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DDD.Application.BaseServices;
using DDD.Application.Interfaces;
using DTO;
using DDD.Domain.Interfaces;
using DDD.Infra.Data.Repository;
using Entity;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;


namespace DDD.Application.Services
{
    public class CoffreAlimentationService<TCoffreAlimentationDTO, TCoffreAlimentation, DataBaseContext> :
        AuthBaseService<TCoffreAlimentationDTO, TCoffreAlimentation, DataBaseContext>, ICoffreAlimentationService<TCoffreAlimentationDTO>
       where TCoffreAlimentationDTO : CoffrealimentationDTO, new()
       where TCoffreAlimentation : Coffrealimentation, new()
       where DataBaseContext : DDDContext, new()
    {
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly ICoffreAlimentationRepository<Coffrealimentation> _coffreAlimentationRepository;
        private readonly IPiedecheService<PiedecheDTO> _piedecheService;
        ICoffreRepository<Coffre> _coffreRep;
        public CoffreAlimentationService(ICurrentContextProvider contextProvider,
            DataBaseContext dataBaseContext, IMapper mapper,
                                  ICoffreAlimentationRepository<Coffrealimentation> coffreAlimentationRepository,
                                  IConfiguration configuration, IPiedecheService<PiedecheDTO> piedecheService,
                                  ICoffreRepository<Coffre> coffreRep
                                 ) : base(
           contextProvider, dataBaseContext, mapper)

        {
            _mapper = mapper;
            _coffreAlimentationRepository = coffreAlimentationRepository;
            _configuration = configuration;
            _piedecheService = piedecheService;
            _coffreRep = coffreRep;

        }

        public async Task<TCoffreAlimentationDTO> CreateCoffreAlimentation(CoffrealimentationEntryDTO c)
        {
            TCoffreAlimentationDTO res = null;
            c.AlimentationDate = DateTime.Now;

            var montant = c.MontantAlimenter;
            //if (res!=null || res.Id != 0) { 
            foreach (var elt in c.PiedecheIds)
            {


                decimal? montantTot = 0;

                var alimentations = await _coffreAlimentationRepository.GenericGet(x => x.PiedecheId == elt);
                decimal? montantAlim = 0;

                //if (alimentations.Count != 0) { 
                montantTot += alimentations.Sum(x => x.Montantalimenter);
                var piedeche = await _piedecheService.GetByIdWithMontantConvertie(elt);

                if (piedeche.MontantConvertie < montant + montantTot)
                {
                    montant = Math.Abs((decimal)(montant + montantTot - piedeche.MontantConvertie));
                    montantAlim = Math.Abs((decimal)(montantTot - piedeche.MontantConvertie));
                    var coffreAl = new TCoffreAlimentationDTO
                    {
                        AlimentationDate = c.AlimentationDate,
                        Montantalimenter = montantAlim,
                        Username = c.Username,
                        CoffreId = c.CoffreId,
                        Id = c.Id,
                        PiedecheId = elt,
                        Motif = c.Motif

                    };
                    res = await base.Create(coffreAl);

                }
                else
                {
                    if (piedeche.MontantConvertie >= montant && montant != 0)
                    {
                        montantAlim = montant;
                        montant = 0;
                        var coffreAll = new TCoffreAlimentationDTO
                        {
                            AlimentationDate = c.AlimentationDate,
                            Montantalimenter = montantAlim,
                            Username = c.Username,
                            CoffreId = c.CoffreId,
                            Id = c.Id,
                            PiedecheId = elt,
                            Motif = c.Motif


                        };
                        res = await base.Create(coffreAll);
                        break;
                    }
                }
                //}


            }




            return res;
        }

        public async Task<int?> DeleteAlimentation(int id)
        {
            var result = await base.GetById(id);
            var res = await base.Delete(id);
            
            return  result.CoffreId;
        }

        public async Task<List<TCoffreAlimentationDTO>> GetAllCoffreAlimentation()
        {
            var res = await base.GetAll();

            return res;
        }

        public async Task<List<CoffrealimentationDTO>> GetByCoffreId(int id)
        {
            var res = await _coffreAlimentationRepository.GetByCoffreId(id);
            //List<TCoffreAlimentationDTO> filteredList = res.Where(x => x.CoffreId == id).ToList();
            //foreach (var alimentation in filteredList)
            //{
            //    var coffre = await this._coffreRep.GetById(alimentation.CoffreId);

            //    alimentation.CoffreLibelle = coffre?.Libelle;
            //}
            return res;
        }

        public async Task<CoffrealimentationDTO> Update(CoffrealimentationDTO coffre)
        {
            var entity = _mapper.Map<Coffrealimentation>(coffre);
            var update = await _coffreAlimentationRepository.Edit(entity);
            var coffreDto = _mapper.Map<CoffrealimentationDTO>(update);
            return coffreDto;
        }
    }
}
