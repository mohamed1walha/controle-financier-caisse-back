﻿using AutoMapper;
using DDD.Application.BaseServices;
using DDD.Application.Interfaces;
using DTO;
using DDD.Domain.Interfaces;
using Entity;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace DDD.Application.Services
{
    public class MotifService<TMotifDTO, TMotif, DataBaseContext> :
        AuthBaseService<TMotifDTO, Motif, DataBaseContext>, IMotifService<TMotifDTO>
       where TMotifDTO : MotifDTO, new()
       where TMotif : Motif, new()
       where DataBaseContext : DDDContext, new()
    {
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly IMotifRepository<Motif> _MotifRepository;

        public MotifService(ICurrentContextProvider contextProvider,
            DataBaseContext dataBaseContext, IMapper mapper,
                                  IMotifRepository<Motif> MotifRepository,
                                  IConfiguration configuration
                                 ) : base(
           contextProvider, dataBaseContext, mapper)

        {
            _mapper = mapper;
            _MotifRepository = MotifRepository;
            _configuration = configuration;


        }

        public async Task<MotifDTO> Update(MotifDTO motif)
        {
            var entity = _mapper.Map<Motif>(motif);
            var update = await _MotifRepository.Edit(entity);
            var motifDto = _mapper.Map<MotifDTO>(update);
            return motifDto;
        }
    }
}
