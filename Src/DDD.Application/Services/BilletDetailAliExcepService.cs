﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DDD.Application.BaseServices;
using DDD.Application.Interfaces;
using DTO;
using DDD.Domain.Interfaces;
using DDD.Infra.Data.Repository;
using Entity;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;


namespace DDD.Application.Services
{
    public class BilletDetailAliExcepService<TBilletDetailAliExcepDTO, TBilletDetailAliExcep, DataBaseContext> :
        AuthBaseService<TBilletDetailAliExcepDTO, BilletDetailAliExcep, DataBaseContext>, IBilletDetailAliExcepService<TBilletDetailAliExcepDTO>
       where TBilletDetailAliExcepDTO : BilletDetailAliExcepDTO, new()
       where TBilletDetailAliExcep : BilletDetailAliExcep, new()
       where DataBaseContext : DDDContext, new()
    {
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly IBilletDetailAliExcepRepository<BilletDetailAliExcep> _BilletDetailAliExcepRepository;

        public BilletDetailAliExcepService(ICurrentContextProvider contextProvider,
            DataBaseContext dataBaseContext, IMapper mapper,
                                  IBilletDetailAliExcepRepository<BilletDetailAliExcep> BilletDetailAliExcepRepository,
                                  IConfiguration configuration
                                 ) : base(
           contextProvider, dataBaseContext, mapper)

        {
            _mapper = mapper;
            _BilletDetailAliExcepRepository = BilletDetailAliExcepRepository;
            _configuration = configuration;


        }

        public async Task<BilletDetailAliExcepDTO> Update(BilletDetailAliExcepDTO BilletDetailAliExcep)
        {
            var entity = _mapper.Map<BilletDetailAliExcep>(BilletDetailAliExcep);
            var update = await _BilletDetailAliExcepRepository.Edit(entity);
            var BilletDetailAliExcepDto = _mapper.Map<BilletDetailAliExcepDTO>(update);
            return BilletDetailAliExcepDto;
        }
    }
}
