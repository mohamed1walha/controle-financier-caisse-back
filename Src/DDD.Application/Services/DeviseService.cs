﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DDD.Application.BaseServices;
using DDD.Application.Interfaces;
using DTO;
using DDD.Domain.Interfaces;
using DDD.Infra.Data.Repository;
using Entity;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;


namespace DDD.Application.Services
{
    public class DeviseService<TDeviseDTO, TDevise, DataBaseContext> :
        AuthBaseService<TDeviseDTO, Devise, DataBaseContext>, IDeviseService<TDeviseDTO>
       where TDeviseDTO : DeviseDTO, new()
       where TDevise : Devise, new()
       where DataBaseContext : DDDContext, new()
    {
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly IDeviseRepository<Devise> _deviseRepository;

        public DeviseService(ICurrentContextProvider contextProvider,
            DataBaseContext dataBaseContext, IMapper mapper,
                                  IDeviseRepository<Devise> DeviseRepository,
                                  IConfiguration configuration
                                 ) : base(
           contextProvider, dataBaseContext, mapper)

        {
            _mapper = mapper;
            _deviseRepository = DeviseRepository;
            _configuration = configuration;


        }

        public async Task<List<DeviseDTO>> GetAllByCoffreID(int id)
        {
            return await _deviseRepository.GetAllByCoffreID(id);
        }

        public async Task<DeviseDTO> GetByCode(string v)
        {
            var res= await _deviseRepository.GenericGetFirstOrDefaultAsync(x => x.Code == v);
            return _mapper.Map<DeviseDTO>(res);
        }

        public async Task<DeviseDTO> Update(DeviseDTO devise)
        {
            var entity = _mapper.Map<Devise>(devise);
            var update = await _deviseRepository.Edit(entity);
            var deviseDto = _mapper.Map<DeviseDTO>(update);
            return deviseDto;
        }
    }
}
