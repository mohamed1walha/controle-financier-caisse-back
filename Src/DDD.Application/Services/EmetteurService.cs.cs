﻿using AutoMapper;
using DDD.Application.BaseServices;
using DDD.Application.Interfaces;
using DTO;
using DDD.Domain.Interfaces;
using Entity;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace DDD.Application.Services
{
    public class EmetteurService<TEmetteurDTO, TEmetteur, DataBaseContext> :
        AuthBaseService<TEmetteurDTO, Emetteur, DataBaseContext>, IEmetteurService<TEmetteurDTO>
       where TEmetteurDTO : EmetteurDTO, new()
       where TEmetteur : Emetteur, new()
       where DataBaseContext : DDDContext, new()
    {
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly IEmetteurRepository<Emetteur> _EmetteurRepository;

        public EmetteurService(ICurrentContextProvider contextProvider,
            DataBaseContext dataBaseContext, IMapper mapper,
                                  IEmetteurRepository<Emetteur> EmetteurRepository,
                                  IConfiguration configuration
                                 ) : base(
           contextProvider, dataBaseContext, mapper)

        {
            _mapper = mapper;
            _EmetteurRepository = EmetteurRepository;
            _configuration = configuration;


        }

        public async Task<EmetteurDTO> Update(EmetteurDTO emetteur)
        {
            var entity = _mapper.Map<Emetteur>(emetteur);
            var update = await _EmetteurRepository.Edit(entity);
            var emetteurDto = _mapper.Map<EmetteurDTO>(update);
            return emetteurDto;
        }
    }
}
