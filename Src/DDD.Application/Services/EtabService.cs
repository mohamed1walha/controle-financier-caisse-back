﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using DDD.Application.BaseServices;
using DDD.Application.Interfaces;
using DTO;
using DDD.Domain.Interfaces;
using Entity;
using Microsoft.Extensions.Configuration;
namespace DDD.Application.Services
{
    public class EtabService<TEtabServiceDTO, TEtabService, DataBaseContext> 
        : AuthBaseService<TEtabServiceDTO, TEtabService, DataBaseContext>, IEtabService<TEtabServiceDTO>
       where TEtabServiceDTO : EtablissementDTO, new ()
       where TEtabService : Etablissement, new ()
       where DataBaseContext : DDDContext,       new () 
    {
        private readonly IMapper _mapper;
        private readonly IEtabRepository<Etablissement> _etabRepository;


        public EtabService(ICurrentContextProvider contextProvider,DataBaseContext dataBaseContext, IMapper mapper,
                                  IEtabRepository<Etablissement> etabRepository
                                 ) : base(contextProvider, dataBaseContext,mapper)
            
        {
            _mapper = mapper;
            _etabRepository = etabRepository;

        }

        public async Task<List<Etablissement>> CreateMany(List<Etablissement> resultatEtab)
        {
            return await _etabRepository.CreateMany(resultatEtab);
        }

        public async Task<EtablissementDTO> GetByEtab(string gjcEtablissement)
        {
            var res=  _etabRepository.GenericGetFirstOrDefault(x=>x.EtEtablissement==gjcEtablissement);
            return _mapper.Map<EtablissementDTO>(res);
        }

        //public async Task<List<EtablissementDTO>> GetAll()
        //{
        //    var res= await _etabRepository.GetAllEtab();
        //    var map = _mapper.Map< List<EtablissementDTO>>(res);
        //    return map;
        //}


    }
}
