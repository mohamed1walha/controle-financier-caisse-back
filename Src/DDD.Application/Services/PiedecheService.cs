﻿using AutoMapper;
using DDD.Application.BaseServices;
using DDD.Application.Interfaces;
using DTO;
using DDD.Domain.Interfaces;
using Entity;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DDD.Application.utils;

namespace DDD.Application.Services
{
    public class PiedecheService<TPiedecheDTO, TPiedeche, DataBaseContext>
        : AuthBaseService<TPiedecheDTO, TPiedeche, DataBaseContext>, IPiedecheService<TPiedecheDTO>
       where TPiedecheDTO : PiedecheDTO, new()
       where TPiedeche : Piedeche, new()
       where DataBaseContext : DDDContext, new()
    {
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly IPiedecheRepository<Piedeche> _piedecheRepository;
        private readonly IModePaiementService<ModepaieDTO> _modePaiementService;
        private readonly IMotifService<MotifDTO> _motifService;
        private readonly IUserService<UserDTO> _userService;


        public PiedecheService(ICurrentContextProvider contextProvider, DataBaseContext dataBaseContext, IMapper mapper,
                                  IPiedecheRepository<Piedeche> piedecheRepository,
                                   IModePaiementService<ModepaieDTO> modePaiementService,
                                   IMotifService<MotifDTO> motifService, IUserService<UserDTO> userService,



        IConfiguration configuration
                                 ) : base(contextProvider, dataBaseContext, mapper)
        {
            _mapper = mapper;
            _piedecheRepository = piedecheRepository;
            _configuration = configuration;
            _modePaiementService= modePaiementService;
            _motifService= motifService;
            _userService= userService;
        }

        public async Task<bool> EditPiedeche(TPiedecheDTO v, int id)
        {
            var c=_mapper.Map<Piedeche>(v);
            var res=  _piedecheRepository.EditPiedecheWithStatus(c,id);
            return res;
        }

        public async Task<List<PiedecheDTO>> GetAllByCoffreNull()
        {
            //var statusComptage = await _statusService.GetByCode(STATUS.VALIDEE);

            var res = await _piedecheRepository.GetAllByCoffreNull(STATUS.VALIDEE);
             res.Sort((x, y) => x.GpeModepaie.CompareTo(y.GpeModepaie));
            return res;
        }

        public async Task<List<PiedecheDTO>> GetAllByJoursCaisse(int id,int currentUserId)
        {
            
            var user = await _userService.GetByUserId(currentUserId);
            var resModePaie = user?.ModePaies.Select(x => x.Id).ToList();

            var res = await _piedecheRepository.GetAllByJoursCaisseById(id);
            res = user.ModePaies.Count != 0 ?res.Where(x=> resModePaie.Contains((int)x.ModepaieId)).ToList(): res;
            foreach(var c in res)
            {
                var motif = await _motifService.GetById(c.MotifId);
                c.Motif = motif;
            }

            return res;
        }

        public List<PiedecheDTO> GetAllByJoursCaisseSync(int id, int currentUserId)
        {
            var user =  _userService.GetByUserIdSync(currentUserId);
            var resModePaie = user?.ModePaies.Select(x => x.Id).ToList();

            var res = _piedecheRepository.GetAllByJoursCaisseByIdSync(id);
            res = user.ModePaies.Count != 0 ? res.Where(x => resModePaie.Contains((int)x.ModepaieId)).ToList() : res;
            foreach (var c in res)
            {
                var motif =  _motifService.GetByIdSync(c.MotifId);
                c.Motif = motif;
            }
            return res;
        }


        public async Task<List<PiedecheDTO>> GetAllByJoursCaisseComptage(int id)
        {
            var res = await _piedecheRepository.GetAllByJoursCaisseComptage(id);
            return _mapper.Map<List<PiedecheDTO>>(res);
        }

        public async Task<PiedecheDTO> GetByIdWithMontantConvertie(int elt)
        {
            return await _piedecheRepository.GetByIdWithMontantConvertie(elt);
        }

        //public async Task<List<DocumentComptageLigneDTO>> GetAllByJoursCaisseDocumentCpmt(int id)
        //{
        //    List<DocumentComptageLigneDTO> documentComptageLigneDTOs = new List<DocumentComptageLigneDTO>();
        //    var res = await _piedecheRepository.GenericGet(x => x.JourcaisseId == id);
        //    foreach(var p in res)
        //    {
        //        var mode = await _modePaiementService.GetById(p.ModepaieId);
        //        DocumentComptageLigneDTO documentComptageLigneDTO = new DocumentComptageLigneDTO()
        //        {
        //            PiedecheId = p.Id,
        //            ModePaieLibelle = mode.LibelleAppFinance,
        //            MonatantPiedeche = p.Montant
        //        };
        //        documentComptageLigneDTOs.Add(documentComptageLigneDTO);
        //    }
        //    return documentComptageLigneDTOs;
        //}

        public async Task<PiedecheDTO> Update(PiedecheDTO piedeche)
        {
            if (piedeche.Status != null)
            {
                piedeche.StatusComptage = piedeche.Status.Id;
                if (piedeche.Status.Code == STATUS.VALIDEE)
                {
                    piedeche.Datevalidation = DateTime.Now;
                }
            }
           
            var entity = _mapper.Map<Piedeche>(piedeche);
            entity.Modepaie.Devise = null;
            var update = entity.Id != 0 ? await _piedecheRepository.Edit(entity) : await _piedecheRepository.Create(entity);
            var piedecheDto = _mapper.Map<PiedecheDTO>(update);
            return piedecheDto;
        }

        public PiedecheDTO UpdateSync(PiedecheDTO piedeche)
        {
            if (piedeche.Status != null)
            {
                piedeche.StatusComptage = piedeche.Status.Id;
                if (piedeche.Status.Code == STATUS.VALIDEE)
                {
                    piedeche.Datevalidation = DateTime.Now;
                }
            }

            var entity = _mapper.Map<Piedeche>(piedeche);
            entity.Modepaie.Devise = null;
            var update = entity.Id != 0 ?  _piedecheRepository.EditSync(entity) : _piedecheRepository.CreateSync(entity);
            var piedecheDto = _mapper.Map<PiedecheDTO>(update);
            return piedecheDto;
        }
        //public async Task<bool> EditP(PiedecheDTO piedeche)
        //{
        //    if (piedeche.Status != null)
        //    {
        //        piedeche.StatusComptage = piedeche.Status.Id;
        //    }
        //    if (piedeche.MontantAlimenter == null)
        //    {
        //        piedeche.MontantAlimenter = 0;
        //    }
        //    var entity = _mapper.Map<Piedeche>(piedeche);
        //    var update =  _piedecheRepository.EditPiedeche(entity) ;
        //    return update;
        //}

        //public async Task<List<Piedeche>> CreateMany(List<Piedeche> resultatPiedeche)
        //{
        //    return await _piedecheRepository.CreateMany(resultatPiedeche);      
        //}

        //public async Task<List<Piedeche>> genericGet(int gjcNumzcaisse, string GjcEtablissement, string gjcCaisse)
        //{
        //    return await _piedecheRepository.GenericGet(x=>x.GpeNumzcaisse==gjcNumzcaisse && x.GpeEtablissement== GjcEtablissement &&
        //       x.GpeCaisse== gjcCaisse);
        //}

        //public async Task<List<PiedecheDTO>> GetAllByNumCaisseCaisseEtab(int numZ, string caisse, string etab)
        //{
        //    var res= await _piedecheRepository.GenericGet(x=>x.GpeNumzcaisse==numZ && x.GpeCaisse==caisse && x.GpeEtablissement==etab);
        //    if (res.Count != 0)
        //    {
        //        return _mapper.Map<List<PiedecheDTO>>(res);
        //    }
        //    else return new List<PiedecheDTO>();
        //}

        //    public async Task<List<PiedecheDTO>> getAllPiedechByEtabCaisseNumZDateP(string gjcEtablissement, string gjcCaisse,
        //        int gjcNumzcaisse, DateTime? gjcDateferme)
        //    {
        //        var res= await _piedecheRepository.GenericGet(x=>x.GpeEtablissement==gjcEtablissement &&
        //        x.GpeCaisse==gjcCaisse&&x.GpeNumzcaisse==gjcNumzcaisse&&x.GpeDateeche>= gjcDateferme);      
        //    return res.MapTo<List<PiedecheDTO>>();
        //    }

        //    public async Task<List<Piedeche>> getByDateP(DateTime dateFerme)
        //    {
        //        return await _piedecheRepository.GenericGet(x => x.GpeDateeche >= dateFerme);   }
    }
}
