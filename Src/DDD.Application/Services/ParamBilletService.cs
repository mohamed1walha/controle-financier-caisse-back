﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DDD.Application.BaseServices;
using DDD.Application.Interfaces;
using DTO;
using DDD.Domain.Interfaces;
using DDD.Infra.Data.Repository;
using Entity;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;


namespace DDD.Application.Services
{
    public class ParamBilletService<TParamBilletDTO, TParamBillet, DataBaseContext> :
        AuthBaseService<TParamBilletDTO, ParamBillet, DataBaseContext>, IParamBilletService<TParamBilletDTO>
       where TParamBilletDTO : ParamBilletDTO, new()
       where TParamBillet : ParamBillet, new()
       where DataBaseContext : DDDContext, new()
    {
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly IParamBilletRepository<ParamBillet> _ParamBilletRepository;

        public ParamBilletService(ICurrentContextProvider contextProvider,
            DataBaseContext dataBaseContext, IMapper mapper,
                                  IParamBilletRepository<ParamBillet> ParamBilletRepository,
                                  IConfiguration configuration
                                 ) : base(
           contextProvider, dataBaseContext, mapper)

        {
            _mapper = mapper;
            _ParamBilletRepository = ParamBilletRepository;
            _configuration = configuration;


        }

        public List<TParamBilletDTO> GetAllParamBillet()
        {
            var allParamBillet =  this.GetAllSync();
            allParamBillet.ForEach(param =>
            {
                if(param.Type != null)
                    param.TypeLabel = this.GetAllTypeBillet().Find(type => type.Id == param.Type).Label;
            });
            return allParamBillet;
        }

        public List<TypeBilletDTO> GetAllTypeBillet()
        {
            List<TypeBilletDTO>  allTypeBillet= new List<TypeBilletDTO>();
            allTypeBillet.Add(new TypeBilletDTO {Id = 1, Label = "Notes" });
            allTypeBillet.Add(new TypeBilletDTO {Id = 2, Label = "Coins" });
            allTypeBillet.Add(new TypeBilletDTO {Id = 3, Label = "Fc" });
            return allTypeBillet;
        }

        public async Task<ParamBilletDTO> Update(ParamBilletDTO ParamBillet)
        {
            var entity = _mapper.Map<ParamBillet>(ParamBillet);
            var update = await _ParamBilletRepository.Edit(entity);
            var ParamBilletDto = _mapper.Map<ParamBilletDTO>(update);
            return ParamBilletDto;
        }
    }
}
