﻿using AutoMapper;
using DDD.Application.BaseServices;
using DDD.Application.Interfaces;
using DTO;
using DDD.Domain.Interfaces;
using Entity;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace DDD.Application.Services
{
    public class MotifresyncService<TMotifresyncDTO, TMotifresync, DataBaseContext> :
        AuthBaseService<TMotifresyncDTO, Motifresync, DataBaseContext>, IMotifresyncService<TMotifresyncDTO>
       where TMotifresyncDTO : MotifresyncDTO, new()
       where TMotifresync : Motifresync, new()
       where DataBaseContext : DDDContext, new()
    {
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly IMotifresyncRepository<Motifresync> _MotifresyncRepository;

        public MotifresyncService(ICurrentContextProvider contextProvider,
            DataBaseContext dataBaseContext, IMapper mapper,
                                  IMotifresyncRepository<Motifresync> MotifresyncRepository,
                                  IConfiguration configuration
                                 ) : base(
           contextProvider, dataBaseContext, mapper)

        {
            _mapper = mapper;
            _MotifresyncRepository = MotifresyncRepository;
            _configuration = configuration;


        }

        public async Task<MotifresyncDTO> Update(MotifresyncDTO Motifresync)
        {
            var entity = _mapper.Map<Motifresync>(Motifresync);
            var update = await _MotifresyncRepository.Edit(entity);
            var MotifresyncDto = _mapper.Map<MotifresyncDTO>(update);
            return MotifresyncDto;
        }
    }
}
