﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DDD.Application.BaseServices;
using DDD.Application.Interfaces;
using DTO;
using DDD.Domain.Interfaces;
using DDD.Infra.Data.Repository;
using Entity;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;


namespace DDD.Application.Services
{
    public class FournisseurService<TFournisseurDTO, TFournisseur, DataBaseContext> :
        AuthBaseService<TFournisseurDTO, Fournisseur, DataBaseContext>, IFournisseurService<TFournisseurDTO>
       where TFournisseurDTO : FournisseurDTO, new()
       where TFournisseur : Fournisseur, new()
       where DataBaseContext : DDDContext, new()
    {
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly IFournisseurRepository<Fournisseur> _FournisseurRepository;

        public FournisseurService(ICurrentContextProvider contextProvider,
            DataBaseContext dataBaseContext, IMapper mapper,
                                  IFournisseurRepository<Fournisseur> FournisseurRepository,
                                  IConfiguration configuration
                                 ) : base(
           contextProvider, dataBaseContext, mapper)

        {
            _mapper = mapper;
            _FournisseurRepository = FournisseurRepository;
            _configuration = configuration;


        }

        public async Task<FournisseurDTO> Update(FournisseurDTO Fournisseur)
        {
            var entity = _mapper.Map<Fournisseur>(Fournisseur);
            var update = await _FournisseurRepository.Edit(entity);
            var FournisseurDto = _mapper.Map<FournisseurDTO>(update);
            return FournisseurDto;
        }
        public async Task<FournisseurDTO> GetIsDefault()
        {

            var fournisseur= await _FournisseurRepository.GenericGetFirstOrDefaultAsync(x => x.IsDefault == true);
            var FournisseurDto = _mapper.Map<FournisseurDTO>(fournisseur);

            return FournisseurDto;
        }
    }
}
