﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DDD.Application.BaseServices;
using DDD.Application.Interfaces;
using DTO;
using DDD.Domain.Interfaces;
using DDD.Infra.Data.Repository;
using Entity;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;


namespace DDD.Application.Services
{
    public class BilletDetailComptageService<TBilletDetailComptageDTO, TBilletDetailComptage, DataBaseContext> :
        AuthBaseService<TBilletDetailComptageDTO, BilletDetailComptage, DataBaseContext>, IBilletDetailComptageService<TBilletDetailComptageDTO>
       where TBilletDetailComptageDTO : BilletDetailComptageDTO, new()
       where TBilletDetailComptage : BilletDetailComptage, new()
       where DataBaseContext : DDDContext, new()
    {
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly IBilletDetailComptageRepository<BilletDetailComptage> _BilletDetailComptageRepository;

        public BilletDetailComptageService(ICurrentContextProvider contextProvider,
            DataBaseContext dataBaseContext, IMapper mapper,
                                  IBilletDetailComptageRepository<BilletDetailComptage> BilletDetailComptageRepository,
                                  IConfiguration configuration
                                 ) : base(
           contextProvider, dataBaseContext, mapper)

        {
            _mapper = mapper;
            _BilletDetailComptageRepository = BilletDetailComptageRepository;
            _configuration = configuration;


        }

        public async Task<BilletDetailComptageDTO> Update(BilletDetailComptageDTO BilletDetailComptage)
        {
            var entity = _mapper.Map<BilletDetailComptage>(BilletDetailComptage);
            var update = await _BilletDetailComptageRepository.Edit(entity);
            var BilletDetailComptageDto = _mapper.Map<BilletDetailComptageDTO>(update);
            return BilletDetailComptageDto;
        }

        public async Task<List<BilletDetailComptageDTO>> GetAllByPiedechId(int piedechId)
        {
            return await _BilletDetailComptageRepository.GetAllByPiedechId(piedechId);
        }

        public List<BilletDetailComptageDTO> GetAllByPiedechIdSync(int piedechId)
        {
            return _BilletDetailComptageRepository.GetAllByPiedechIdSync(piedechId);
        }
    }
}
