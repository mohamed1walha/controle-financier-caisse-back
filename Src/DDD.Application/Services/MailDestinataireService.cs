﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DDD.Application.BaseServices;
using DDD.Application.Interfaces;
using DTO;
using DDD.Domain.Interfaces;
using DDD.Infra.Data.Repository;
using Entity;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;


namespace DDD.Application.Services
{
    public class MailDestinataireService<TMailDestinataireDTO, TMailDestinataire, DataBaseContext> :
        AuthBaseService<TMailDestinataireDTO, MailDestinataire, DataBaseContext>, IMailDestinataireService<TMailDestinataireDTO>
       where TMailDestinataireDTO : MailDestinataireDTO, new()
       where TMailDestinataire : MailDestinataire, new()
       where DataBaseContext : DDDContext, new()
    {
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly IMailDestinataireRepository<MailDestinataire> _MailDestinataireRepository;

        public MailDestinataireService(ICurrentContextProvider contextProvider,
            DataBaseContext dataBaseContext, IMapper mapper,
                                  IMailDestinataireRepository<MailDestinataire> MailDestinataireRepository,
                                  IConfiguration configuration
                                 ) : base(
           contextProvider, dataBaseContext, mapper)

        {
            _mapper = mapper;
            _MailDestinataireRepository = MailDestinataireRepository;
            _configuration = configuration;


        }

        public async Task<MailDestinataireDTO> Update(MailDestinataireDTO MailDestinataire)
        {
            var entity = _mapper.Map<MailDestinataire>(MailDestinataire);
            var update = await _MailDestinataireRepository.Edit(entity);
            var MailDestinataireDto = _mapper.Map<MailDestinataireDTO>(update);
            return MailDestinataireDto;
        }
    }
}
