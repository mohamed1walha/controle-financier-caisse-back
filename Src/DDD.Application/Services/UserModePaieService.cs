﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DDD.Application.BaseServices;
using DDD.Application.Interfaces;
using DTO;
using DDD.Domain.Interfaces;
using DDD.Infra.Data.Repository;
using Entity;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;


namespace DDD.Application.Services
{
    public class UserModePaieService<TUserModePaieDTO, TUserModePaie, DataBaseContext> :
        AuthBaseService<TUserModePaieDTO, UserModePaie, DataBaseContext>, IUserModePaieService<TUserModePaieDTO>
       where TUserModePaieDTO : UserModePaieDTO, new()
       where TUserModePaie : UserModePaie, new()
       where DataBaseContext : DDDContext, new()
    {
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly IUserModePaieRepository<UserModePaie> _userModePaieRepository;

        public UserModePaieService(ICurrentContextProvider contextProvider,
            DataBaseContext dataBaseContext, IMapper mapper,
                                  IUserModePaieRepository<UserModePaie> UserModePaieRepository,
                                  IConfiguration configuration
                                 ) : base(
           contextProvider, dataBaseContext, mapper)

        {
            _mapper = mapper;
            _userModePaieRepository = UserModePaieRepository;
            _configuration = configuration;


        }

        public async Task<bool> EditAsync(UserModePaieDTO entity)
        {
            return await _userModePaieRepository.EditAsync(entity);
        }

        public async Task<UserModePaieDTO> GetByUserIdAndModePaieId(int userId, int modePaieId)
        {
            var res = await _userModePaieRepository.GenericGetFirstOrDefaultAsync(x => x.UserId == userId && x.ModePaieId == modePaieId);
            return _mapper.Map<UserModePaieDTO>(res);
                }
        public async Task<List<UserModePaieDTO>> GetByUserId(int userId)
        {
            var res = await _userModePaieRepository.GenericGet(x => x.UserId == userId );
            return _mapper.Map<List<UserModePaieDTO>>(res);
                }

        public async Task<UserModePaieDTO> Update(UserModePaieDTO modePaie)
        {
            var entity = _mapper.Map<UserModePaie>(modePaie);
            var update = await _userModePaieRepository.Edit(entity);
            var userModePaieDto = _mapper.Map<UserModePaieDTO>(update);
            return userModePaieDto;
        }
    }
}
