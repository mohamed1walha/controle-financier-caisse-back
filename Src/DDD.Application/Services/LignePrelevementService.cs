﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DDD.Application.BaseServices;
using DDD.Application.Interfaces;
using DTO;
using DDD.Domain.Interfaces;
using DDD.Infra.Data.Repository;
using Entity;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;


namespace DDD.Application.Services
{
    public class LignePrelevementService<TLignePrelevementDTO, TLignePrelevement, DataBaseContext> :
        AuthBaseService<TLignePrelevementDTO, LignePrelevement, DataBaseContext>, ILignePrelevementService<TLignePrelevementDTO>
       where TLignePrelevementDTO : LignePrelevementDTO, new()
       where TLignePrelevement : LignePrelevement, new()
       where DataBaseContext : DDDContext, new()
    {
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly ILignePrelevementRepository<LignePrelevement> _LignePrelevementRepository;

        public LignePrelevementService(ICurrentContextProvider contextProvider,
            DataBaseContext dataBaseContext, IMapper mapper,
                                  ILignePrelevementRepository<LignePrelevement> LignePrelevementRepository,
                                  IConfiguration configuration
                                 ) : base(
           contextProvider, dataBaseContext, mapper)

        {
            _mapper = mapper;
            _LignePrelevementRepository = LignePrelevementRepository;
            _configuration = configuration;


        }
        public async Task<List<LignePrelevementDTO>> GetAllByEnteteId(int id)
        {
            var res = await _LignePrelevementRepository.GetAllByEnteteId(id);
            return res;

        }
        public async Task<LignePrelevementDTO> Update(LignePrelevementDTO LignePrelevement)
        {
            var entity = _mapper.Map<LignePrelevement>(LignePrelevement);
            var update = await _LignePrelevementRepository.Edit(entity);
            var LignePrelevementDto = _mapper.Map<LignePrelevementDTO>(update);
            return LignePrelevementDto;
        }
    }
}
