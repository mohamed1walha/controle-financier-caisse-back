﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DDD.Application.BaseServices;
using DDD.Application.Interfaces;
using DTO;
using DDD.Domain.Interfaces;
using DDD.Infra.Data.Repository;
using Entity;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;


namespace DDD.Application.Services
{
    public class JustifsPrelevementService<TJustifsPrelevementDTO, TJustifsPrelevement, DataBaseContext> :
        AuthBaseService<TJustifsPrelevementDTO, JustifsPrelevement, DataBaseContext>, IJustifsPrelevementService<TJustifsPrelevementDTO>
       where TJustifsPrelevementDTO : JustifsPrelevementDTO, new()
       where TJustifsPrelevement : JustifsPrelevement, new()
       where DataBaseContext : DDDContext, new()
    {
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly IJustifsPrelevementRepository<JustifsPrelevement> _JustifsPrelevementRepository;

        public JustifsPrelevementService(ICurrentContextProvider contextProvider,
            DataBaseContext dataBaseContext, IMapper mapper,
                                  IJustifsPrelevementRepository<JustifsPrelevement> JustifsPrelevementRepository,
                                  IConfiguration configuration
                                 ) : base(
           contextProvider, dataBaseContext, mapper)

        {
            _mapper = mapper;
            _JustifsPrelevementRepository = JustifsPrelevementRepository;
            _configuration = configuration;


        }

        public async Task<JustifsPrelevementDTO> Update(JustifsPrelevementDTO JustifsPrelevement)
        {
            var entity = _mapper.Map<JustifsPrelevement>(JustifsPrelevement);
            var update = await _JustifsPrelevementRepository.Edit(entity);
            var JustifsPrelevementDto = _mapper.Map<JustifsPrelevementDTO>(update);
            return JustifsPrelevementDto;
        }
    }
}
