﻿using DDD.Application.BaseServices;
using DTO;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Domain.Interfaces
{
    public interface ICoffreAlimentationService<TEntityDTO> : IBaseService<TEntityDTO> where TEntityDTO : CoffrealimentationDTO
    {
        Task<CoffrealimentationDTO> Update(CoffrealimentationDTO coffrealimentationDTO);
        Task<TEntityDTO> CreateCoffreAlimentation(CoffrealimentationEntryDTO coffrealimentationDTO);
        Task<List<TEntityDTO>> GetAllCoffreAlimentation();
        Task<List<CoffrealimentationDTO>> GetByCoffreId(int id);
        Task<int?> DeleteAlimentation(int id);
    }
}
