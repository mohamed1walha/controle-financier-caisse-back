﻿using DDD.Application.BaseServices;
using DTO;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Domain.Interfaces
{
    public interface ICompteDepenseService<TEntityDTO> : IBaseService<TEntityDTO> where TEntityDTO : CompteDepenseDTO
    {
        Task<CompteDepenseDTO> Update(CompteDepenseDTO CompteDepenseDTO);
        Task<List<CompteDepenseDTO>> GetAllCompteDepense();
        Task<CompteDepenseDTO> GetByEtablissementId(int? id);
        Task<CompteDepenseDTO> GetByEtabId(int? id);
        CompteDepenseDTO GetByEtablissementIdSync(int? id);

    }
}
