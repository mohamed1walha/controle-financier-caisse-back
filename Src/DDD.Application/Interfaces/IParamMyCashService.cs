﻿using DDD.Application.BaseServices;
using DTO;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Domain.Interfaces
{
    public interface IParamMyCashService<TEntityDTO> : IBaseService<TEntityDTO> where TEntityDTO : ParamMyCashDTO
    {
        Task<ParamMyCashDTO> Update(ParamMyCashDTO ParamMyCashDTO);
    }
}
