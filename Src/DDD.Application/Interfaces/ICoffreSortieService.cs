﻿using DDD.Application.BaseServices;
using DTO;
using Entity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Domain.Interfaces
{
    public interface ICoffreSortieService<TEntityDTO> : IBaseService<TEntityDTO> where TEntityDTO : CoffresortieDTO
    {
        Task<CoffresortieDTO> Update(CoffresortieDTO motifDTO);
        Task<List<CoffresortieDTO>> GetByCoffreId(int id);
        Task<OperationResult> CreateSortie(TEntityDTO coffreSortieDTO);
        Task<List<CoffresortieDTO>> GetAllSorties();
        Task<FileStreamResult> getDocument(int id);
        Task<int?> DeleteSortie(int id);
    }
}
