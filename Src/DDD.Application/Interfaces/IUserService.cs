﻿using AU.DTO.AuthDTO;
using DDD.Application.BaseServices;
using DTO;
using Entity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Domain.Interfaces
{
    public interface IUserService<TEntityDTO> : IBaseService<TEntityDTO> where TEntityDTO : UserDTO
    {
        Task<UserDTO> Update(UserDTO userDTO);
        Task<UserDTO> GetByUserName(string username);
        Task<List<UserDTO>> GetAllUsers();
        Task<List<UserDTO>> GetAllUsersSimpleData();
        Task<AuthResult<Token>> ChangePassword(ChangePasswordDTO changePasswordDto, int currentUserId, string password, string Newpassword);
        Task<UserDTO> GetByIdWithRole(int id);
        Task<List<UserDTO>> GetAllValidateur();
        Task<List<UserDTO>> GetAllByEtabId(int etabId);
        Task<UserDTO> GetByUserId(int userId);
        UserDTO GetByUserIdSync(int userId);

        Task<bool> RestorePassword(RestorePasswordDTO restorePasswordDto, string password);
        Task<UserDTO> GetByEmail(string mailTo);
    }
}
