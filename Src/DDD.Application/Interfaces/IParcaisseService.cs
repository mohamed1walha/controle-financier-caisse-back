﻿using DDD.Application.BaseServices;
using DTO;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Domain.Interfaces
{
    public interface IParcaisseService<TEntityDTO> : IBaseService<TEntityDTO> where TEntityDTO : ParcaisseDTO
    {
        Task<ParcaisseDTO> Update(ParcaisseDTO parcaisse);
        Task<List<ParcaisseDTO>> GetAllByEtabId(string etabId);
        Task<List<ParcaisseDTO>> GetAllByEtabIds(List<string> etabIds);
    }
}
