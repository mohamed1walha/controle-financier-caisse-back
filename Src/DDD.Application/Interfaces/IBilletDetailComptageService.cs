﻿using DDD.Application.BaseServices;
using DTO;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Domain.Interfaces
{
    public interface IBilletDetailComptageService<TEntityDTO> : IBaseService<TEntityDTO> where TEntityDTO : BilletDetailComptageDTO
    {
        Task<List<BilletDetailComptageDTO>> GetAllByPiedechId(int piedechId);
        List<BilletDetailComptageDTO> GetAllByPiedechIdSync(int piedechId);

        Task<BilletDetailComptageDTO> Update(BilletDetailComptageDTO BilletDetailComptageDTO);
    }
}
