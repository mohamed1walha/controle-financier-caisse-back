﻿using DDD.Application.BaseServices;
using DTO;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Domain.Interfaces
{
    public interface ILignePrelevementService<TEntityDTO> : IBaseService<TEntityDTO> where TEntityDTO : LignePrelevementDTO
    {
        Task<LignePrelevementDTO> Update(LignePrelevementDTO LignePrelevementDTO);
        Task<List<LignePrelevementDTO>> GetAllByEnteteId(int id);
    }
}
