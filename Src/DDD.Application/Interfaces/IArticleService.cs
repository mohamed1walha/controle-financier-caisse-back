﻿using DDD.Application.BaseServices;
using DTO;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Domain.Interfaces
{
    public interface IArticleService<TEntityDTO> : IBaseService<TEntityDTO> where TEntityDTO : ArticleDTO
    {
        Task<ArticleDTO> Update(ArticleDTO ArticleDTO);
        Task<List<ArticleDTO>> GetAllWithControlSolde();
    }
}
