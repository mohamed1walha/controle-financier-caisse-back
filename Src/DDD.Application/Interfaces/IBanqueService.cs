﻿using DDD.Application.BaseServices;
using DTO;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Domain.Interfaces
{
    public interface IBanqueService<TEntityDTO> : IBaseService<TEntityDTO> where TEntityDTO : BanqueDTO
    {
        Task<BanqueDTO> Update(BanqueDTO banqueDTO);
    }
}
