using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DDD.Application.BaseServices;
using DTO;
using Entity;

namespace DDD.Application.Interfaces
{
    public interface IJoursCaisseService<TEntityDTO> : IBaseService<TEntityDTO> where TEntityDTO : JoursCaiseDTO
    {
        Task<OperationResult> Charger(string[] etablissment,string dateFerme, string dateFermeTo, string user);
        Task ChargerResync(List<JoursCaiseDTO> enveloppes, int motifId, string comments);
        Task<List<JoursCaiseDTO>> GetAllByDate(DateTime dateTime);
        Task<JoursCaiseDTO> SearchByCode(int numZ, int codeEtab, string codeCaisse);
        Task<List<int>> GetAllZByEtab(int etabId);
        Task<List<string>> GetAllCaisseByEtab(int etabId);
        Task<List<JoursCaiseDTO>> GetAllByStatusCmptNone(int userId);
        Task<List<JoursCaiseDTO>> FiltreGetAllByStatusCmptNone(DateTime? dateTime1, DateTime? dateTime2, int userId);

        Task<List<JoursCaiseDTO>> GetAllByStatusEnAttente(int userId);
        Task<List<JoursCaiseDTO>> GetAllByStatusReceptionne(int userId);
        Task<List<JoursCaiseDTO>> GetAllByStatusCompter(int userId);
        Task<List<JoursCaiseDTO>> FiltreGetAllByStatusCompter(DateTime? dateTime1, DateTime? dateTime2, int userId);

        Task<List<JoursCaiseDTO>> ReceptionEnveloppes(List<JoursCaiseDTO> enveloppes, int chauffeurId, int emetteurId,int userId);
        Task<List<JoursCaiseDTO>> ReceptionReSyncEnveloppes(List<JoursCaiseDTO> enveloppes, int motifId, string comments, int userId);
        Task<StatsDTO> GetStatDashboard(int userId);
        Task<List<JoursCaiseDTO>> FiltreEnveloppeEnAttente(DateTime? dateTime1, DateTime? dateTime2, int userId);
        Task<List<JoursCaiseDTO>> FiltreEnveloppeReceptionne(DateTime? dateTime1, DateTime? dateTime2, int userId);
        Task<JoursCaiseDTO> Update(JoursCaiseDTO joursCaisse);
        Task<List<JoursCaiseDTO>> GetAllByStatusReceptionneNonValide(int userId);
        Task<List<JoursCaiseDTO>> FiltreGetAllByStatusReceptionneNonValide(DateTime? dateTime1, DateTime? dateTime2,int userId);

        Task<List<JoursCaiseDTO>> ChangeStatusImprime(List<JoursCaiseDTO> joursCaisse, int userId);
        Task<List<ReportDTO>> GenerateReport(DateTime? dateOuverture, DateTime? dateOuvertureTo, int etablissementId,int userId);
        Task<JoursCaiseDTO> GetFullById(int id);
        Task<List<ReportBilletDTO>> GenerateReportDetailsBillet(DateTime? startDate, DateTime? endDate, ReportBilletRequestDTO reportBilletsDTO);
    }
}
