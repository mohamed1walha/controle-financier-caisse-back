﻿using DDD.Application.BaseServices;
using DTO;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Domain.Interfaces
{
    public interface IEmetteurService<TEntityDTO> : IBaseService<TEntityDTO> where TEntityDTO : EmetteurDTO
    {
        Task<EmetteurDTO> Update(EmetteurDTO emetteurDTO);
    }
}
