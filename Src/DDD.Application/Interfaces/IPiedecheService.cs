﻿using DDD.Application.BaseServices;
using DTO;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Application.Interfaces
{
    public interface IPiedecheService<TEntityDTO> : IBaseService<TEntityDTO> where TEntityDTO : PiedecheDTO
    {
        //Task<List<PiedecheDTO>> getAllPiedechByEtabCaisseNumZDateP(string gjcEtablissement, string gjcCaisse,
        //    int gjcNumzcaisse, DateTime? gjcDateferme);
        //Task<List<Piedeche>> getByDateP(DateTime dateFerme);
        //Task<List<Piedeche>>  CreateMany(List<Piedeche> resultatPiedeche);
        //Task<List<PiedecheDTO>> GetAllByNumCaisseCaisseEtab(int numZ, string caisse, string etab);
        //Task<List<Piedeche>> genericGet(int gjcNumzcaisse, string gjcEtat, string gjcCaisse);
        Task<List<PiedecheDTO>> GetAllByJoursCaisse(int id,int currentUserId);

        List<PiedecheDTO> GetAllByJoursCaisseSync(int id, int currentUserId);

        Task<PiedecheDTO> Update(PiedecheDTO piedecheDTO);
        PiedecheDTO UpdateSync(PiedecheDTO piedecheDTO);

        Task<List<PiedecheDTO>> GetAllByJoursCaisseComptage(int id);
        Task <bool>EditPiedeche(TEntityDTO v, int id);
        Task<List<PiedecheDTO>> GetAllByCoffreNull();
        Task<PiedecheDTO> GetByIdWithMontantConvertie(int elt);
        //Task<bool> EditP(PiedecheDTO piedeche);
        //Task<List<DocumentComptageLigneDTO>> GetAllByJoursCaisseDocumentCpmt(int id);
    }
}
