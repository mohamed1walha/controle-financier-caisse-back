﻿using DDD.Application.BaseServices;
using DTO;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Domain.Interfaces
{
    public interface IUserRolesService<TEntityDTO> : IBaseService<TEntityDTO> where TEntityDTO : UserRolesDTO
    {
         Task<UserRolesDTO> Update(UserRolesDTO userRolesDTO);
        Task<UserRolesDTO> GetByUserIdAndRoleId(int UserId, int RoleId);
        Task<bool> EditAsync(UserRolesDTO entity);
        Task<List<UserRolesDTO>> GetByUserId(int userId);
    }
}
