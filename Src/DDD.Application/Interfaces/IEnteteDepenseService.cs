﻿using DDD.Application.BaseServices;
using DTO;
using Entity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Domain.Interfaces
{
    public interface IEnteteDepenseService<TEntityDTO> : IBaseService<TEntityDTO> where TEntityDTO : EnteteDepenseDTO
    {
        Task<EnteteDepenseDTO> Update(EnteteDepenseDTO EnteteDepenseDTO);
        Task<EnteteDepenseDTO> GetFullEnteteDepenseById(int id);
        Task<EnteteDepenseDTO> CreateEnteteDepense(TEntityDTO enteteDepenseDTO);
        Task<FileStreamResult> downloadFile(int id);
        Task<EnteteDepenseDTO> deleteFile(int id);
        Task<List<EnteteDepenseDTO>> GetAllByEtablissementIdAndUserId(int etablissementId, int userId);
        Task<List<EnteteDepenseDTO>> GetAllByValidatorId(int userId);
        Task<List<EnteteDepenseDTO>> GetAllByEtabIdCaisseIdNumZRef(int etabId, string caisseId, int? numZRef);
        Task<List<EnteteDepenseDTO>> GetAllEntete();
        Task<PagedResults> GetAllEnteteLazy(GridStateDTO grid, FilterDepancesDTO filters);
        Task<List<string>> GenerateComptable(List<EnteteDepenseDTO> depensetList);
    }
}
