﻿using DDD.Application.BaseServices;
using DTO;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Domain.Interfaces
{
    public interface IBilletDetailSortieService<TEntityDTO> : IBaseService<TEntityDTO> where TEntityDTO : BilletDetailSortieDTO
    {
        Task<BilletDetailSortieDTO> Update(BilletDetailSortieDTO BilletDetailSortieDTO);
        Task<List<BilletDetailSortieDTO>> GetAllBySortieId(int SortieId);

    }
}
