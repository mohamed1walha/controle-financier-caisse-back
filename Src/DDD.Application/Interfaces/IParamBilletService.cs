﻿using DDD.Application.BaseServices;
using DTO;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Domain.Interfaces
{
    public interface IParamBilletService<TEntityDTO> : IBaseService<TEntityDTO> where TEntityDTO : ParamBilletDTO
    {
        List<TEntityDTO> GetAllParamBillet();
        List<TypeBilletDTO> GetAllTypeBillet();
        Task<ParamBilletDTO> Update(ParamBilletDTO ParamBilletDTO);
    }
}
