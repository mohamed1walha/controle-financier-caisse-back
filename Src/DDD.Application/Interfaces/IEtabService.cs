﻿using DDD.Application.BaseServices;
using DTO;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Application.Interfaces
{
    public interface IEtabService<TEntityDTO> : IBaseService<TEntityDTO> where TEntityDTO : EtablissementDTO
    {
       // Task<List<EtablissementDTO>> GetAll();
       // Task<List<Etablissement>> CreateMany(List<Etablissement> resultatEtab);
        Task<EtablissementDTO> GetByEtab(string gjcEtablissement);
    }
}
