﻿using DDD.Application.BaseServices;
using DTO;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Domain.Interfaces
{
    public interface ICategorieModePaieCoffreService<TEntityDTO> : IBaseService<TEntityDTO> where TEntityDTO : CategorieModePaieCoffreDTO
    {
        Task<CategorieModePaieCoffreDTO> Update(CategorieModePaieCoffreDTO categorieModePaieCoffreDTO);
        Task<List<CategorieModePaieCoffreDTO>> GetByIdCoffre(int elt);
        Task<List<CategorieModePaieCoffreDTO>> GetAllByModePaie(List<int> modePaies);
    }
}
