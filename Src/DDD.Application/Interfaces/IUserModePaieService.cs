﻿using DDD.Application.BaseServices;
using DTO;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Domain.Interfaces
{
    public interface IUserModePaieService<TEntityDTO> : IBaseService<TEntityDTO> where TEntityDTO : UserModePaieDTO
    {
         Task<UserModePaieDTO> Update(UserModePaieDTO userModePaieDTO);
        Task<UserModePaieDTO> GetByUserIdAndModePaieId(int UserId, int ModePaieId);
        Task<bool> EditAsync(UserModePaieDTO entity);
        Task<List<UserModePaieDTO>> GetByUserId(int userId);
    }
}
