﻿using DDD.Application.BaseServices;
using DTO;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Domain.Interfaces
{
    public interface ICoffreService<TEntityDTO> : IBaseService<TEntityDTO> where TEntityDTO : CoffreDTO
    {
        Task<CoffreDTO> Update(CoffreDTO coffreDTO);
        Task<OperationResult> CreateCoffre(TEntityDTO coffreDTO);
        Task<List<CoffreDTO>> GetAllCoffre();
        Task<List<CoffreDTO>> GetAllByModePaie(List<int> modePaies);
    }
}
