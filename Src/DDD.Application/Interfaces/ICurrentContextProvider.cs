﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Application.Interfaces
{
    public interface ICurrentContextProvider
    {
        ContextSession GetCurrentContext();
    }
}
