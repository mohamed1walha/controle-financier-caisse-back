﻿using DDD.Application.BaseServices;
using DTO;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Domain.Interfaces
{
    public interface IStatusService<TEntityDTO> : IBaseService<TEntityDTO> where TEntityDTO : StatusDTO
    {
        Task<StatusDTO> Update(StatusDTO statusDTO);
        Task<StatusDTO> GetByCode(string code);
        Task<StatusDTO> checkStatus(int id,int userId);
    }
}
