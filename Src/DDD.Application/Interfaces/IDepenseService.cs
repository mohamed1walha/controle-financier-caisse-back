﻿using DDD.Application.BaseServices;
using DTO;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Domain.Interfaces
{
    public interface IDepenseService<TEntityDTO> : IBaseService<TEntityDTO> where TEntityDTO : DepenseDTO
    {
        Task<DepenseDTO> Update(DepenseDTO depenseDTO);
    }
}
