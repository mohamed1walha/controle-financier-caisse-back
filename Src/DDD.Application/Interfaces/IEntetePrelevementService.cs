﻿using DDD.Application.BaseServices;
using DTO;
using Entity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Domain.Interfaces
{
    public interface IEntetePrelevementService<TEntityDTO> : IBaseService<TEntityDTO> where TEntityDTO : EntetePrelevementDTO
    {
        Task<EntetePrelevementDTO> Update(EntetePrelevementDTO EntetePrelevementDTO);
        Task<EntetePrelevementDTO> GetFullEntetePrelevementById(int id);
        Task<OperationResult> CreateEntetePrelevement(TEntityDTO entetePrelevementDTO);
        Task<FileStreamResult> downloadFile(int id);
        Task<EntetePrelevementDTO> deleteFile(int id);
        Task<List<EntetePrelevementDTO>> GetAllByEtablissementIdAndUserId(int etablissementId, int userId);
        Task<List<EntetePrelevementDTO>> GetAllByValidateur( int userId);
        Task<List<EntetePrelevementDTO>> GetAllByDemandeur(int userId, bool isAccepted);
        Task<List<EntetePrelevementDTO>> GetAllEntete();
        Task<List<EntetePrelevementDTO>> GetAllByEtabIdCaisseIdNumZRef(int etabId, string caisseId, string numZRef);
        //Task<List<EntetePrelevementDTO>> GetAllWithFilter(FilterPrelevementDTO filterPrelevementDTO);
        Task<PagedResults> GetAllEnteteLazy(GridStateDTO grid, FilterPrelevementDTO filters);

        Task<List<string>> GenerateComptable(List<EntetePrelevementDTO> prelevenetList);
        Task<PagedResults> GetEntetePrelevements(GridStateDTO grid);
    }
}
