﻿using DDD.Application.BaseServices;
using DTO;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Domain.Interfaces
{
    public interface ILigneDepenseService<TEntityDTO> : IBaseService<TEntityDTO> where TEntityDTO : LigneDepenseDTO
    {
        Task<LigneDepenseDTO> Update(LigneDepenseDTO LigneDepenseDTO);
        Task<List<LigneDepenseDTO>> GetAllByEnteteId(int id);

        List<LigneDepenseDTO> GetAllByEnteteIdSync(int id);

    }
}
