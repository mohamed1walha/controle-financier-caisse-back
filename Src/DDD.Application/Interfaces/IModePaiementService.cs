using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DDD.Application.BaseServices;
using DTO;
using Entity;

namespace DDD.Application.Interfaces
{
    public interface IModePaiementService<TEntityDTO> : IBaseService<TEntityDTO> where TEntityDTO : ModepaieDTO
    {

        Task<ModepaieDTO> GetByModePaie(string gpeModepaie);
        Task<List<TEntityDTO>> GetByCoffreId(int coffreId);
        Task Update(ModepaieDTO modepaie);
        Task<bool> EditAsync(Modepaie res);
        Task<List<ModepaieDTO>> GetALLModePaie();
    }
}
