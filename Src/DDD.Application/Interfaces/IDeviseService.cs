﻿using DDD.Application.BaseServices;
using DTO;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Domain.Interfaces
{
    public interface IDeviseService<TEntityDTO> : IBaseService<TEntityDTO> where TEntityDTO : DeviseDTO
    {
        Task<DeviseDTO> Update(DeviseDTO deviseDTO);
        Task<DeviseDTO> GetByCode(string v);
        Task<List<DeviseDTO>> GetAllByCoffreID(int id);
    }
}
