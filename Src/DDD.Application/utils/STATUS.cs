﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Application.utils
{
    public class STATUS
    {
        public const string EN_ATTENTE = "EAT";
        public const string EN_ATTENTE_IMPRIME = "EATI";
        public const string RECEPTIONNE = "RCP";
        public const string EN_COURS = "ECR";
        public const string VALIDEE = "VAL";
        public const string ESPECECOMPTE = "ESPC";
        public const string NONE = "NONE";
        public const string ACCEPTE = "ACP";
        public const string REJETE = "RJT";
        public const string DEMANDE = "DMD";
    }
}
