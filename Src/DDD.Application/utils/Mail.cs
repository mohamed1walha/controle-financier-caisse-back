﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Application.utils
{
    public static class Mail
    {
        public static void SendMail(MailMessage mail)
        {
            var MyConfig = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            var server = MyConfig.GetValue<string>("AppSettings:Server");
            var email = MyConfig.GetValue<string>("AppSettings:Email");
            var password = MyConfig.GetValue<string>("AppSettings:password");
            try
            {
                SmtpClient SmtpServer = new SmtpClient(server);          
                SmtpServer.Port = 587;
                SmtpServer.Credentials = new NetworkCredential(email,password);
                SmtpServer.EnableSsl = true;
                mail.From = new MailAddress(email);
                SmtpServer.Send(mail);
            }
            catch (Exception ex)
            {
            }
        }
    }
}
