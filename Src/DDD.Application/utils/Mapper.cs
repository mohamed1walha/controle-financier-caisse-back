﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Reflection;
namespace DDD.Application.utils
{
    public static class Mapper
    {
        public static TResult MapTo<TResult>(this object source)
            where TResult : class
        {
            return Mapper.MapTo<TResult>(source);
        }
        public static DestinationType GetModel<DestinationType, SourceType>(SourceType SourceObject)
        {
            object DestinationObject = Activator.CreateInstance(typeof(DestinationType));
            if (SourceObject != null)
            {
                foreach (PropertyInfo SourcePInfo in typeof(SourceType).GetProperties())
                {
                    PropertyInfo DestinationPInfo = typeof(DestinationType).GetProperty(SourcePInfo.Name);
                    if (DestinationPInfo != null)
                    {
                        try
                        {
                            DestinationPInfo.SetValue(DestinationObject, SourcePInfo.GetValue(SourceObject));
                        }
                        catch
                        {
                            //Last try --> conversion 
                            try
                            {
                                DestinationPInfo.SetValue(DestinationObject, Convert.ChangeType(SourcePInfo.GetValue(SourceObject), DestinationPInfo.PropertyType));
                            }
                            catch
                            {
                                //don nothing when an exception is raised at this level
                                //continue with the rest of the properties  
                                continue;
                            }
                            continue;
                        }
                    }
                }
            }
            return ((DestinationType)Convert.ChangeType(DestinationObject, typeof(DestinationType)));
        }
        public static List<DestinationType> GetModelList<DestinationType, SourceType>(List<SourceType> SourceObjects)
        {
            List<DestinationType> DestinationObjects = new List<DestinationType>();
            foreach (SourceType SourceObject in SourceObjects)
            {
                DestinationType result = GetModel<DestinationType, SourceType>(SourceObject);
                DestinationObjects.Add(result);
            }

            return (DestinationObjects);
        }
        public static IMappingExpression<TSource, TDestination> IgnoreAllNonExisting<TSource, TDestination>
(this IMappingExpression<TSource, TDestination> expression)
        {
            var flags = BindingFlags.Public | BindingFlags.Instance;
            var sourceType = typeof(TSource);
            var destinationProperties = typeof(TDestination).GetProperties(flags);

            foreach (var property in destinationProperties)
            {
                if (sourceType.GetProperty(property.Name, flags) == null)
                {
                    expression.ForMember(property.Name, opt => opt.Ignore());
                }
            }
            return expression;
        }
        public static DestinationType FillModel<DestinationType, SourceType>(SourceType SourceObject, DestinationType DestinationObject)
        {

            if (SourceObject != null)
            {
                foreach (PropertyInfo SourcePInfo in typeof(SourceType).GetProperties())
                {
                    PropertyInfo DestinationPInfo = typeof(DestinationType).GetProperty(SourcePInfo.Name);
                    if (DestinationPInfo != null)
                    {
                        try
                        {
                            DestinationPInfo.SetValue(DestinationObject, SourcePInfo.GetValue(SourceObject));
                        }
                        catch
                        {
                            //Last try --> conversion 
                            try
                            {
                                DestinationPInfo.SetValue(DestinationObject, Convert.ChangeType(SourcePInfo.GetValue(SourceObject), DestinationPInfo.PropertyType));
                            }
                            catch
                            {
                                //don nothing when an exception is raised at this level
                                //continue with the rest of the properties  
                                continue;
                            }
                            continue;
                        }
                    }
                }
            }
            return (DestinationObject);
        }


    }
}
