using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DDD.Domain.Common;
using DDD.Domain.Interfaces;
using DDD.Infra.Data.Context;
using DTO;
using Entity;
using Microsoft.EntityFrameworkCore;

namespace DDD.Infra.Data.Repository
{
    public class DeviseRepository : GenericBaseRepository<Devise, DDDContext>,
          IDeviseRepository<Devise>, IDisposable
    {
        public DeviseRepository(DDDContext context)
            : base(context)
        {
            dbContext = context;
        }

        public async Task<List<DeviseDTO>> GetAllByCoffreID(int id)
        {
            var res = await(from coffre in dbContext.Coffre
                            join category in dbContext.CategorieModepaieCoffre on coffre.Id equals category.CoffreId
                            join mm in dbContext.Modepaie on category.ModepaieId equals mm.Id
                            join devise in dbContext.Devise on mm.DeviseId equals devise.Id

                            where mm.Id == category.ModepaieId && category.CoffreId == coffre.Id && id==coffre.Id

                            
                            select new DeviseDTO
                            {
                                Id = devise.Id,
                                Code=devise.Code,
                                Label=devise.Label, 
                                TauxChange=devise.TauxChange
                               

                            }).Distinct().ToListAsync();
            return res;
        }
    }
}
