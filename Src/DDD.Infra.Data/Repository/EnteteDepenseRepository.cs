using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using DDD.Domain.Common;
using DDD.Domain.Interfaces;
using DDD.Infra.Data.Context;
using DTO;
using Entity;
using Microsoft.EntityFrameworkCore;

namespace DDD.Infra.Data.Repository
{
    public class EnteteDepenseRepository : GenericBaseRepository<EnteteDepense, DDDContext>,
          IEnteteDepenseRepository<EnteteDepense>, IDisposable
    {
        public EnteteDepenseRepository(DDDContext context)
            : base(context)
        {
            dbContext = context;
        }

        public async Task<int> EditEntete(EnteteDepense entity)
        {
            var query =
                  await (from jc in dbContext.EnteteDepense
                   where jc.Id == entity.Id
                   select jc).FirstOrDefaultAsync();

            query.PieceJointe = entity.PieceJointe;
            query.StatutId =entity.StatutId;
            query.TotalHt=entity.TotalHt;
            query.CodeJournal=entity.CodeJournal;
            query.DateReponse=entity.DateReponse;
            query.EtablissementId=entity.EtablissementId;
            query.FournisseurId=entity.FournisseurId;
            query.TotalTva=entity.TotalTva;
            query.TotalTtc=entity.TotalTtc;
            query.ValidateurUserId=entity.ValidateurUserId;
            query.TypeDepense=entity.TypeDepense;
            query.DateDepense=entity.DateDepense;
            query.Commentaire=entity.Commentaire;
            query.CompteSource=entity.CompteSource;
            query.CompteDepenseId = entity.CompteDepenseId;
            query.IsCompta = entity.IsCompta;

               

            

            try
            {
                dbContext.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

        

            return entity.Id;
        }

        public async Task<List<EnteteDepenseDTO>> GetAllByEtablissementIdAndUserId(int etablissementId, int userId)
        {
            var res = await (
                 from entete in dbContext.EnteteDepense
                 join status in dbContext.Status on entete.StatutId equals status.Id
                 join etab in dbContext.Etablissement on entete.EtablissementId equals etab.Id
                 join four in dbContext.Fournisseur on entete.FournisseurId equals four.Id
                 join user in dbContext.User on entete.ValidateurUserId equals user.Id into users
                 from user in users.DefaultIfEmpty()
                 join userCreate in dbContext.User on entete.CreateByUserId equals userCreate.Id
                 where etab.Id == etablissementId && userCreate.Id == userId
                 select new EnteteDepenseDTO
                 {
                     Id = entete.Id,
                     CodeJournal = entete.CodeJournal,
                     commentaire = entete.Commentaire,
                     CompteSource = entete.CompteSource,
                     CreateByUserId = entete.CreateByUserId,
                     ValidateurUserId = entete.ValidateurUserId,
                     CreateDate = entete.CreateDate,
                     DateDepense = entete.DateDepense,
                     DateReponse = entete.DateReponse,
                     FournisseurId = entete.FournisseurId,
                     PieceJointe = entete.PieceJointe,
                     EtablissementId = entete.EtablissementId,
                     StatutId = entete.StatutId,
                     StatutLabel = status.Statuslibelle,
                     TotalTva = entete.TotalTva,
                     TotalHt = entete.TotalHt,
                     CompteDepenseId = entete.CompteDepenseId,
                     TotalTtc = entete.TotalTtc,
                     TypeDepense = entete.TypeDepense,
                     UserCreatedFullName= userCreate.FirstName+' '+ userCreate.LastName,
                     UserValidatorFullName=user.FirstName+' '+user.LastName,
                     Fournisseur = new FournisseurDTO
                     {
                         Id = four.Id,
                         Libelle = four.Libelle,
                         CompteC = four.CompteC
                     },
                     status =new StatusDTO { Id=status.Id, Code=status.Code,Statuslibelle=status.Statuslibelle,Statuslibelleen=status.Statuslibelleen },
                     Etablissement= new EtablissementDTO
                     {
                         Id = etab.Id,
                         EtLibelle = etab.EtLibelle,
                         EtDevise = etab.EtDevise,
                         EtEtablissement = etab.EtEtablissement,
                         EtLangue = etab.EtLangue,
                         EtAdresse1 = etab.EtAdresse1,
                         EtAdresse2 = etab.EtAdresse2,
                         EtAdresse3 = etab.EtAdresse3,
                         EtCodepostal = etab.EtCodepostal,
                         EtPays = etab.EtPays,
                         EtTelephone = etab.EtTelephone,
                         EtVille = etab.EtVille,
                     }


                 }
                 ).OrderByDescending(x => x.DateDepense).ToListAsync();
            return res;
        }

        public async Task<List<EnteteDepenseDTO>> GetAllByValidatorId( int userId)
        {
            var res = await (
                 from entete in dbContext.EnteteDepense
                 join status in dbContext.Status on entete.StatutId equals status.Id
                 join etab in dbContext.Etablissement on entete.EtablissementId equals etab.Id
                 join four in dbContext.Fournisseur on entete.FournisseurId equals four.Id
                 join user in dbContext.User on entete.ValidateurUserId equals user.Id into users
                 from user in users.DefaultIfEmpty()
                 join userCreate in dbContext.User on entete.CreateByUserId equals userCreate.Id
                 where user.Id == userId
                 select new EnteteDepenseDTO
                 {
                     Id = entete.Id,
                     CodeJournal = entete.CodeJournal,
                     commentaire = entete.Commentaire,
                     CompteSource = entete.CompteSource,
                     CompteDepenseId = entete.CompteDepenseId,
                     CreateByUserId = entete.CreateByUserId,
                     ValidateurUserId = entete.ValidateurUserId,
                     CreateDate = entete.CreateDate,
                     DateDepense = entete.DateDepense,
                     DateReponse = entete.DateReponse,
                     FournisseurId = entete.FournisseurId,
                     PieceJointe = entete.PieceJointe,
                     EtablissementId = entete.EtablissementId,
                     StatutId = entete.StatutId,
                     StatutLabel = status.Statuslibelle,
                     TotalTva = entete.TotalTva,
                     TotalHt = entete.TotalHt,
                     TotalTtc = entete.TotalTtc,
                     TypeDepense = entete.TypeDepense,
                     UserCreatedFullName = userCreate.FirstName + ' ' + userCreate.LastName,
                     UserValidatorFullName = user.FirstName + ' ' + user.LastName,
                     Fournisseur = new FournisseurDTO
                     {
                         Id = four.Id,
                         Libelle = four.Libelle,
                         CompteC = four.CompteC
                     },
                     status = new StatusDTO { Id = status.Id, Code = status.Code, Statuslibelle = status.Statuslibelle, Statuslibelleen = status.Statuslibelleen },
                     Etablissement = new EtablissementDTO
                     {
                         Id = etab.Id,
                         EtLibelle = etab.EtLibelle,
                         EtDevise = etab.EtDevise,
                         EtEtablissement = etab.EtEtablissement,
                         EtLangue = etab.EtLangue,
                         EtAdresse1 = etab.EtAdresse1,
                         EtAdresse2 = etab.EtAdresse2,
                         EtAdresse3 = etab.EtAdresse3,
                         EtCodepostal = etab.EtCodepostal,
                         EtPays = etab.EtPays,
                         EtTelephone = etab.EtTelephone,
                         EtVille = etab.EtVille,
                     }


                 }
                 ).OrderByDescending(x=>x.DateDepense).ToListAsync();
            return res;
        }     
        public async Task<List<EnteteDepenseDTO>> GetAllByEtabIdCaisseIdNumZRef(int etabId, string caisseId, int? numZRef)
        {
            var res = await (
                 from entete in dbContext.EnteteDepense
                 join ligne in dbContext.LigneDepense on entete.Id equals ligne.DepenseId
                 join status in dbContext.Status on entete.StatutId equals status.Id
                 join etab in dbContext.Etablissement on entete.EtablissementId equals etab.Id
                 join four in dbContext.Fournisseur on entete.FournisseurId equals four.Id
                 join user in dbContext.User on entete.ValidateurUserId equals user.Id into users
                 from user in users.DefaultIfEmpty()
                 join parcaisse in dbContext.Parcaisse on ligne.Caisse equals parcaisse.Id

                 join userCreate in dbContext.User on entete.CreateByUserId equals userCreate.Id
                 where etab.Id == etabId && ligne.NumZref == numZRef && status.Code != "RJT" && parcaisse.GpkCaisse==caisseId
                 select new EnteteDepenseDTO
                 {
                     Id = entete.Id,
                     CodeJournal = entete.CodeJournal,
                     commentaire = entete.Commentaire,
                     CompteSource = entete.CompteSource,
                     CompteDepenseId = entete.CompteDepenseId,
                     CreateByUserId = entete.CreateByUserId,
                     ValidateurUserId = entete.ValidateurUserId,
                     CreateDate = entete.CreateDate,
                     DateDepense = entete.DateDepense,
                     DateReponse = entete.DateReponse,
                     FournisseurId = entete.FournisseurId,
                     PieceJointe = entete.PieceJointe,
                     EtablissementId = entete.EtablissementId,
                     StatutId = entete.StatutId,
                     StatutLabel = status.Statuslibelle,
                     TotalTva = ligne.MontantTva,
                     TotalHt = ligne.MontantHt,
                     TotalTtc = ligne.MontantTtc,
                     TypeDepense = entete.TypeDepense,
                     UserCreatedFullName = userCreate.FirstName + ' ' + userCreate.LastName,
                     UserValidatorFullName = user.FirstName + ' ' + user.LastName,
                     Fournisseur = new FournisseurDTO
                     {
                         Id = four.Id,
                         Libelle = four.Libelle,
                         CompteC = four.CompteC
                     },
                     status = new StatusDTO { Id = status.Id, Code = status.Code, Statuslibelle = status.Statuslibelle, Statuslibelleen = status.Statuslibelleen },
                     Etablissement = new EtablissementDTO
                     {
                         Id = etab.Id,
                         EtLibelle = etab.EtLibelle,
                         EtDevise = etab.EtDevise,
                         EtEtablissement = etab.EtEtablissement,
                         EtLangue = etab.EtLangue,
                         EtAdresse1 = etab.EtAdresse1,
                         EtAdresse2 = etab.EtAdresse2,
                         EtAdresse3 = etab.EtAdresse3,
                         EtCodepostal = etab.EtCodepostal,
                         EtPays = etab.EtPays,
                         EtTelephone = etab.EtTelephone,
                         EtVille = etab.EtVille,
                     }


                 }
                 ).ToListAsync();
            return res;
        }

        public async Task<List<EnteteDepenseDTO>> GetAllEntete()
        {
            var res = await (
                 from entete in dbContext.EnteteDepense
                 join ligne in dbContext.LigneDepense on entete.Id equals ligne.DepenseId
                 join status in dbContext.Status on entete.StatutId equals status.Id
                 join etab in dbContext.Etablissement on entete.EtablissementId equals etab.Id
                 join four in dbContext.Fournisseur on entete.FournisseurId equals four.Id
                 join user in dbContext.User on entete.ValidateurUserId equals user.Id into users
                 from user in users.DefaultIfEmpty()
                 join userCreate in dbContext.User on entete.CreateByUserId equals userCreate.Id
                 select new EnteteDepenseDTO
                 {
                     Id = entete.Id,
                     CodeJournal = entete.CodeJournal,
                     commentaire = entete.Commentaire,
                     CompteSource = entete.CompteSource,
                     CompteDepenseId = entete.CompteDepenseId,
                     CreateByUserId = entete.CreateByUserId,
                     ValidateurUserId = entete.ValidateurUserId,
                     CreateDate = entete.CreateDate,
                     DateDepense = entete.DateDepense,
                     DateReponse = entete.DateReponse,
                     FournisseurId = entete.FournisseurId,
                     PieceJointe = entete.PieceJointe,
                     EtablissementId = entete.EtablissementId,
                     StatutId = entete.StatutId,
                     StatutLabel = status.Statuslibelle,
                     TotalTva = entete.TotalTva,
                     TotalHt = entete.TotalHt,
                     TotalTtc = entete.TotalTtc,
                     TypeDepense = entete.TypeDepense,
                     UserCreatedFullName = userCreate.FirstName + ' ' + userCreate.LastName,
                     UserValidatorFullName = user.FirstName + ' ' + user.LastName,
                     Fournisseur = new FournisseurDTO
                     {
                         Id = four.Id,
                         Libelle = four.Libelle,
                         CompteC = four.CompteC
                     },
                     status = new StatusDTO { Id = status.Id, Code = status.Code, Statuslibelle = status.Statuslibelle, Statuslibelleen = status.Statuslibelleen },
                     Etablissement = new EtablissementDTO
                     {
                         Id = etab.Id,
                         EtLibelle = etab.EtLibelle,
                         EtDevise = etab.EtDevise,
                         EtEtablissement = etab.EtEtablissement,
                         EtLangue = etab.EtLangue,
                         EtAdresse1 = etab.EtAdresse1,
                         EtAdresse2 = etab.EtAdresse2,
                         EtAdresse3 = etab.EtAdresse3,
                         EtCodepostal = etab.EtCodepostal,
                         EtPays = etab.EtPays,
                         EtTelephone = etab.EtTelephone,
                         EtVille = etab.EtVille,
                     }


                 }
                 )
            .OrderByDescending(x => x.CreateDate).ToListAsync();
            return res;
        }

        public async Task<PagedResults> GetAllEnteteLazy(GridStateDTO grid, FilterDepancesDTO filters)
        {
            if (grid.Page == 0)
                grid.Page = 1;

            if (grid.PageSize == 0)
                grid.PageSize = int.MaxValue;

            var resQuery = dbContext.EnteteDepense
                .Include(entete => entete.LigneDepense)
                .Include(entete => entete.Statut)
                .Include(entete => entete.Etablissement)
                .Include(entete => entete.Fournisseur)
                .Where(x => 
                                 (filters.EtablissementId!= null && filters.EtablissementId.Count>0 ? filters.EtablissementId.Contains(x.EtablissementId): x.Id > 0) &&
                                 (filters.UserCreated != null && filters.UserCreated.Count > 0 ? filters.UserCreated.Contains(x.CreateByUserId) : x.Id > 0) &&
                                 (filters.Validateur != null && filters.Validateur.Count > 0 ? filters.Validateur.Contains(x.ValidateurUserId) : x.Id > 0) &&
                                 (filters.TypeDepance != null && filters.TypeDepance.Count > 0 ? filters.TypeDepance.Contains(x.TypeDepense) : x.Id > 0) &&
                                 (filters.MontantTotalFrom != null && filters.MontantTotalFrom > 0 ? filters.MontantTotalFrom<= x.TotalTtc : x.Id > 0)&&
                                 (filters.MontantTotalTo != null && filters.MontantTotalTo > 0 ? filters.MontantTotalTo >= x.TotalTtc : x.Id > 0) &&
                                 (filters.DateDepanceFrom != null ? filters.DateDepanceFrom.Value.Date.AddDays(1) <= x.DateDepense.Value.Date : x.Id > 0) &&
                                 (filters.DateDepanceTo != null ? filters.DateDepanceTo.Value.Date.AddDays(1) >= x.DateDepense.Value.Date : x.Id > 0) &&
                                 (filters.DateCreationFrom != null ? filters.DateCreationFrom.Value.Date.AddDays(1) <= x.CreateDate.Value.Date : x.Id > 0) &&
                                 (filters.DateCreationTo != null ? filters.DateCreationTo.Value.Date.AddDays(1) >= x.CreateDate.Value.Date : x.Id > 0) &&
                                 (filters.IsCompta == true ? x.IsCompta==true : x.IsCompta == null || x.IsCompta==false) &&
                                 (filters.StatutId == x.StatutId)
                );

            
        


            var res = await resQuery
                .OrderByDescending(entete => entete.Id)
                .Skip((grid.Page - 1) * grid.PageSize)
                .Take(grid.PageSize)
                .Select(entete => new EnteteDepenseDTO
                {
                    Id = entete.Id,
                    CodeJournal = entete.CodeJournal,
                    commentaire = entete.Commentaire,
                    CompteSource = entete.CompteSource,
                    CompteDepenseId = entete.CompteDepenseId,
                    CreateByUserId = entete.CreateByUserId,
                    ValidateurUserId = entete.ValidateurUserId,
                    CreateDate = entete.CreateDate,
                    DateDepense = entete.DateDepense,
                    DateReponse = entete.DateReponse,
                    FournisseurId = entete.FournisseurId,
                    PieceJointe = entete.PieceJointe,
                    EtablissementId = entete.EtablissementId,
                    StatutId = entete.StatutId,
                    StatutLabel = entete.Statut.Statuslibelle,
                    TotalTva = entete.TotalTva,
                    TotalHt = entete.TotalHt,
                    TotalTtc = entete.TotalTtc,
                    TypeDepense = entete.TypeDepense,
                    UserCreatedFullName = (from user in dbContext.User where  user.Id== entete.CreateByUserId select user.FirstName + " " + user.LastName).FirstOrDefault(),
                    UserValidatorFullName =  (from user in dbContext.User where user.Id == entete.ValidateurUserId select user.FirstName + " " + user.LastName).FirstOrDefault(),
                    Fournisseur = new FournisseurDTO
                    {
                        Id = entete.Fournisseur.Id,
                        Libelle = entete.Fournisseur.Libelle,
                        CompteC = entete.Fournisseur.CompteC
                    },
                    status = new StatusDTO
                    {
                        Id = entete.Statut.Id,
                        Code = entete.Statut.Code,
                        Statuslibelle = entete.Statut.Statuslibelle,
                        Statuslibelleen = entete.Statut.Statuslibelleen
                    },
                    Etablissement = new EtablissementDTO
                    {
                        Id = entete.Etablissement.Id,
                        EtLibelle = entete.Etablissement.EtLibelle,
                        EtDevise = entete.Etablissement.EtDevise,
                        EtEtablissement = entete.Etablissement.EtEtablissement,
                        EtLangue = entete.Etablissement.EtLangue,
                        EtAdresse1 = entete.Etablissement.EtAdresse1,
                        EtAdresse2 = entete.Etablissement.EtAdresse2,
                        EtAdresse3 = entete.Etablissement.EtAdresse3,
                        EtCodepostal = entete.Etablissement.EtCodepostal,
                        EtPays = entete.Etablissement.EtPays,
                        EtTelephone = entete.Etablissement.EtTelephone,
                        EtVille = entete.Etablissement.EtVille,
                    }
                })
                .ToListAsync();

            var count = await resQuery.CountAsync();

            var retVal = new PagedResults(res, count, grid.Page, grid.PageSize);
            return retVal;
        }




        public async Task<EnteteDepenseDTO> GetByEnteteId(int id)
        {
            var res = await (
                 from entete in dbContext.EnteteDepense
                 join ligne in dbContext.LigneDepense on entete.Id equals ligne.DepenseId
                 join status in dbContext.Status on entete.StatutId equals status.Id
                 join etab in dbContext.Etablissement on entete.EtablissementId equals etab.Id
                 join four in dbContext.Fournisseur on entete.FournisseurId equals four.Id
                 join user in dbContext.User on entete.ValidateurUserId equals user.Id into users
                 from user in users.DefaultIfEmpty()
                 join userCreate in dbContext.User on entete.CreateByUserId equals userCreate.Id
                 where entete.Id==id
                 select new EnteteDepenseDTO
                 {
                     Id = entete.Id,
                     CodeJournal = entete.CodeJournal,
                     commentaire = entete.Commentaire,
                     CompteSource = entete.CompteSource,
                     CompteDepenseId = entete.CompteDepenseId,
                     CreateByUserId = entete.CreateByUserId,
                     ValidateurUserId = entete.ValidateurUserId,
                     CreateDate = entete.CreateDate,
                     DateDepense = entete.DateDepense,
                     DateReponse = entete.DateReponse,
                     FournisseurId = entete.FournisseurId,
                     PieceJointe = entete.PieceJointe,
                     EtablissementId = entete.EtablissementId,
                     StatutId = entete.StatutId,
                     StatutLabel = status.Statuslibelle,
                     TotalTva = entete.TotalTva,
                     TotalHt = entete.TotalHt,
                     TotalTtc = entete.TotalTtc,
                     TypeDepense = entete.TypeDepense,
                     UserCreatedFullName = userCreate.FirstName + ' ' + userCreate.LastName,
                     UserValidatorFullName = user.FirstName + ' ' + user.LastName,
                     Fournisseur= new FournisseurDTO
                     {
                         Id=four.Id,
                         Libelle=four.Libelle,
                         CompteC=four.CompteC
                     },
                     status = new StatusDTO { Id = status.Id, Code = status.Code, Statuslibelle = status.Statuslibelle, Statuslibelleen = status.Statuslibelleen },
                     Etablissement = new EtablissementDTO
                     {
                         Id = etab.Id,
                         EtLibelle = etab.EtLibelle,
                         EtDevise = etab.EtDevise,
                         EtEtablissement = etab.EtEtablissement,
                         EtLangue = etab.EtLangue,
                         EtAdresse1 = etab.EtAdresse1,
                         EtAdresse2 = etab.EtAdresse2,
                         EtAdresse3 = etab.EtAdresse3,
                         EtCodepostal = etab.EtCodepostal,
                         EtPays = etab.EtPays,
                         EtTelephone = etab.EtTelephone,
                         EtVille = etab.EtVille,
                     }


                 }
                 ).FirstOrDefaultAsync();
            return res;
        }
    }
}
