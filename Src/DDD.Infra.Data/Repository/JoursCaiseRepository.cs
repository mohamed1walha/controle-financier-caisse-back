using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DDD.Domain.Common;
using DDD.Domain.Interfaces;
using DDD.Infra.Data.Context;
using DTO;
using Entity;
using Microsoft.EntityFrameworkCore;

namespace DDD.Infra.Data.Repository
{
    public class JoursCaiseRepository : GenericBaseRepository<Jourscaisse, DDDContext>,
        IJoursCaisseRepository<Jourscaisse>, IDisposable
    {
        public JoursCaiseRepository(DDDContext context)
            : base(context)
        {
            dbContext = context;
        }

        public async Task<IEnumerable<Jourscaisse>> GetAllAll()
        {
            DateTime dateTime = new DateTime(2021, 01, 01);
            ;
            return await (from j in dbContext.Jourscaisse
                          where j.GjcEtat == "CDE" && j.GjcDateferme >= dateTime
                          select new Jourscaisse
                          {
                              GjcCaisse = j.GjcCaisse,
                              GjcDateferme = j.GjcDateferme,
                              GjcEtat = j.GjcEtat,
                              GjcDateouv = j.GjcDateouv,
                              GjcEtablissement = j.GjcEtablissement,
                              GjcHeureferme = j.GjcHeureferme,
                              GjcHeureouv = j.GjcHeureouv,
                              GjcNumzcaisse = j.GjcNumzcaisse,
                              GjcUserferme = j.GjcUserferme,



                          }).ToListAsync();
        }



        public async Task<Jourscaisse> SearchByCode(int numZ, int codeEtab, string codeCaisse)
        {
            return await (from j in dbContext.Jourscaisse
                          where j.GjcNumzcaisse == numZ && j.GjcEtablissementId == codeEtab && j.GjcCaisse == codeCaisse
                          select j
                          ).FirstOrDefaultAsync();
        }

        public async Task<List<string>> GetAllCaisseByEtab(int etabId)
        {
            var listCaisse = await (from j in dbContext.Jourscaisse
                                    where j.GjcEtablissement.Id == etabId
                                    select j.GjcCaisse
                                  ).ToListAsync();
            listCaisse = listCaisse.Distinct().ToList();
            return listCaisse;
        }

        public async Task<List<int>> GetAllZByEtab(int etabId)
        {
            var listZ = await (from j in dbContext.Jourscaisse
                               where j.GjcEtablissement.Id == etabId
                               select j.GjcNumzcaisse
                                  ).ToListAsync();
            listZ = listZ.Distinct().ToList();
            return listZ;
        }

        public async Task<bool> EditAll(List<Jourscaisse> enveloppesR, int statusReception, int statusEnAttente, int chauffeurId, int emetteurId)
        {
            foreach (var enveloppe in enveloppesR)
            {
                var query =
                    from jc in dbContext.Jourscaisse
                    where jc.Id == enveloppe.Id
                    select jc;

                foreach (Jourscaisse jc in query)
                {
                    jc.CahuffeurId = chauffeurId;
                    jc.EmetteurId = emetteurId;
                    jc.StatusReception = statusReception;
                    jc.StatusComtage = statusEnAttente;
                    jc.ReceptionDate = DateTime.Now;

                }

                try
                {
                    dbContext.SaveChanges();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }

            }

            return true;
        }
        
        public async Task<bool> EditReSyncAll(List<Jourscaisse> enveloppesR, int statusReception, int statusEnAttente, /*int chauffeurId, int emetteurId,*/ int motifId, string comments)
        {
            foreach (var enveloppe in enveloppesR)
            {
                if (enveloppe.StatusComtage == 2)
                {
                    var query =
                    from jc in dbContext.Jourscaisse
                    where jc.Id == enveloppe.Id
                    select jc;

                    foreach (Jourscaisse jc in query)
                    {
                        //jc.CahuffeurId = chauffeurId;
                        //jc.EmetteurId = emetteurId;
                        jc.StatusReception = statusReception;
                        jc.StatusComtage = statusEnAttente;
                        jc.ReceptionDate = DateTime.Now;
                        jc.MotifId = motifId;
                        jc.Comments = comments;
                        jc.Commentaire = "";

                        var piedeche =
                        from pd in dbContext.Piedeche
                        where pd.JourcaisseId == jc.Id
                        select pd;

                        foreach (Piedeche pd in piedeche)
                        {
                            pd.MotifId = null;
                            pd.MontantCompte = 0;
                            pd.EspeceVerse = 0;
                            pd.EspeceCash = 0;
                            pd.MontantTotal = null;
                            pd.Ecart = null;
                            pd.Commentaire = "";
                            pd.Depence = 0;
                            pd.StatusComptage = 2;
                            pd.AgentComptage = null;
                        }

                    }

                    try
                    {
                        dbContext.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                }
            }

            return true;
        }

        public async Task<List<JoursCaiseDTO>> GetAllByStatusCompter()
        {
            var res = await (from jourscaisse in dbContext.Jourscaisse
                             join piedeche in dbContext.Piedeche on jourscaisse.Id equals piedeche.JourcaisseId
                             join status in dbContext.Status on jourscaisse.StatusComtage equals status.Id



                             where status.Code == "VAL" || status.Code == "ESPC" && jourscaisse.StatusComtage==status.Id
                             select new JoursCaiseDTO
                             {
                                 statusComptageObj = new StatusDTO { Code = status.Code, Id = status.Id, Statuslibelle = status.Statuslibelle, Statuslibelleen = status.Statuslibelleen },
                                 GjcUserferme = jourscaisse.GjcUserferme,
                                 GjcNumzcaisse = jourscaisse.GjcNumzcaisse,
                                 GjcHeureouv = jourscaisse.GjcHeureouv,
                                 MontantCaisse = jourscaisse.MontantCaisse,
                                 Id = jourscaisse.Id,
                                 GjcHeureferme = jourscaisse.GjcHeureferme,
                                 GjcEtat = jourscaisse.GjcEtat,
                                 AgentComptage = jourscaisse.AgentComptage,
                                 CahuffeurId = jourscaisse.CahuffeurId,
                                 Commentaire = jourscaisse.Commentaire,
                                 Comments = jourscaisse.Comments,
                                 ValidationDateFin = jourscaisse.ValidationDateFin,
                                 DateComptage = jourscaisse.DateComptage,
                                 DateCreate = jourscaisse.DateCreate,
                                 EmetteurId = jourscaisse.EmetteurId,
                                 GjcCaisse = jourscaisse.GjcCaisse,
                                 GjcDateferme = jourscaisse.GjcDateferme,
                                 GjcDateouv = jourscaisse.GjcDateouv,
                                 GjcCaisselabel = jourscaisse.GjcCaisselabel,
                                 GjcEtablissementlabel = jourscaisse.GjcEtablissementlabel,
                                 MotifId = jourscaisse.MotifId,
                                 ValidationDateDebut= jourscaisse.ValidationDateDebut,
                                 ReceptionDate= jourscaisse.ReceptionDate,
                                 GjcEtablissementId=jourscaisse.GjcEtablissementId,
                                 StatusComtage=jourscaisse.StatusComtage,
                                 StatusReception=jourscaisse.StatusReception,
                                 Chauffeur=(from chauffeur in dbContext.Chauffeur where jourscaisse.CahuffeurId==chauffeur.Id
                                            select new ChauffeurDTO { Id=chauffeur.Id,ChauffeurLibelle=chauffeur.ChauffeurLibelle}).First() ,
                                 GjcEtablissement=(from etab in dbContext.Etablissement where jourscaisse.GjcEtablissementId==etab.Id
                                            select new EtablissementDTO { Id= etab.Id,EtLibelle= etab.EtLibelle,EtDevise=etab.EtDevise,
                                            EtEtablissement=etab.EtEtablissement,
                                            EtLangue=etab.EtLangue,EtAdresse1=etab.EtAdresse1,EtAdresse2=etab.EtAdresse2,
                                            EtAdresse3= etab.EtAdresse3,EtCodepostal=etab.EtCodepostal,EtPays=etab.EtPays,  EtTelephone=etab.EtTelephone,
                                            EtVille=etab.EtVille,
                                            }).First() ,
                                 Emetteur=(from emetteur in dbContext.Emetteur where jourscaisse.EmetteurId == emetteur.Id
                                            select new EmetteurDTO { Id= emetteur.Id, EmetteurLibelle = emetteur.EmetteurLibelle}).First(),
                                 IsEcart = (from piedeche in dbContext.Piedeche
                                            where jourscaisse.Id == piedeche.JourcaisseId
                                            group piedeche by new { piedeche.JourcaisseId } into newPiedeche
                                            select new
                                            {
                                                Montant = newPiedeche.Sum(x => x.Ecart)

                                            }).First().Montant > 0 ? true : (from piedeche in dbContext.Piedeche
                                                                             where jourscaisse.Id == piedeche.JourcaisseId
                                                                             group piedeche by new { piedeche.JourcaisseId } into newPiedeche
                                                                             select new
                                                                             {
                                                                                 Montant = newPiedeche.Sum(x => x.Ecart)

                                                                             }).First().Montant < 0 ? false : null

                             }).Distinct().ToListAsync();
            return res;
        }

        public async Task<JoursCaiseDTO> GetFullById(int id)
        {
            var res = await(from jourscaisse in dbContext.Jourscaisse
                            join status in dbContext.Status on jourscaisse.StatusComtage equals status.Id



                            where  jourscaisse.Id==id
                            select new JoursCaiseDTO
                            {
                                statusComptageObj = new StatusDTO { Code = status.Code, Id = status.Id, Statuslibelle = status.Statuslibelle, Statuslibelleen = status.Statuslibelleen },
                                GjcUserferme = jourscaisse.GjcUserferme,
                                GjcNumzcaisse = jourscaisse.GjcNumzcaisse,
                                GjcHeureouv = jourscaisse.GjcHeureouv,
                                MontantCaisse = jourscaisse.MontantCaisse,
                                Id = jourscaisse.Id,
                                GjcHeureferme = jourscaisse.GjcHeureferme,
                                GjcEtat = jourscaisse.GjcEtat,
                                AgentComptage = jourscaisse.AgentComptage,
                                CahuffeurId = jourscaisse.CahuffeurId,
                                Commentaire = jourscaisse.Commentaire,
                                Comments = jourscaisse.Comments,
                                ValidationDateFin = jourscaisse.ValidationDateFin,
                                DateComptage = jourscaisse.DateComptage,
                                DateCreate = jourscaisse.DateCreate,
                                EmetteurId = jourscaisse.EmetteurId,
                                GjcCaisse = jourscaisse.GjcCaisse,
                                GjcDateferme = jourscaisse.GjcDateferme,
                                GjcDateouv = jourscaisse.GjcDateouv,
                                GjcCaisselabel = jourscaisse.GjcCaisselabel,
                                GjcEtablissementlabel = jourscaisse.GjcEtablissementlabel,
                                MotifId = jourscaisse.MotifId,
                                ValidationDateDebut = jourscaisse.ValidationDateDebut,
                                ReceptionDate = jourscaisse.ReceptionDate,
                                GjcEtablissementId = jourscaisse.GjcEtablissementId,
                                StatusComtage = jourscaisse.StatusComtage,
                                StatusReception = jourscaisse.StatusReception,
                                Chauffeur = (from chauffeur in dbContext.Chauffeur
                                             where jourscaisse.CahuffeurId == chauffeur.Id
                                             select new ChauffeurDTO { Id = chauffeur.Id, ChauffeurLibelle = chauffeur.ChauffeurLibelle }).First(),
                                GjcEtablissement = (from etab in dbContext.Etablissement
                                                    where jourscaisse.GjcEtablissementId == etab.Id
                                                    select new EtablissementDTO
                                                    {
                                                        Id = etab.Id,
                                                        EtLibelle = etab.EtLibelle,
                                                        EtDevise = etab.EtDevise,
                                                        EtEtablissement = etab.EtEtablissement,
                                                        EtLangue = etab.EtLangue,
                                                        EtAdresse1 = etab.EtAdresse1,
                                                        EtAdresse2 = etab.EtAdresse2,
                                                        EtAdresse3 = etab.EtAdresse3,
                                                        EtCodepostal = etab.EtCodepostal,
                                                        EtPays = etab.EtPays,
                                                        EtTelephone = etab.EtTelephone,
                                                        EtVille = etab.EtVille,
                                                    }).First(),
                                Emetteur = (from emetteur in dbContext.Emetteur
                                            where jourscaisse.EmetteurId == emetteur.Id
                                            select new EmetteurDTO { Id = emetteur.Id, EmetteurLibelle = emetteur.EmetteurLibelle }).First(),
                                IsEcart = (from piedeche in dbContext.Piedeche
                                           where jourscaisse.Id == piedeche.JourcaisseId
                                           group piedeche by new { piedeche.JourcaisseId } into newPiedeche
                                           select new
                                           {
                                               Montant = newPiedeche.Sum(x => x.Ecart)

                                           }).First().Montant > 0 ? true : (from piedeche in dbContext.Piedeche
                                                                            where jourscaisse.Id == piedeche.JourcaisseId
                                                                            group piedeche by new { piedeche.JourcaisseId } into newPiedeche
                                                                            select new
                                                                            {
                                                                                Montant = newPiedeche.Sum(x => x.Ecart)

                                                                            }).First().Montant < 0 ? false : null




                            }).FirstAsync();
            return res;
        }

        public async Task<List<JoursCaiseDTO>> GetWithModePaie(List<int?> modePaies, List<string> statusAccept, List<string> statusReject)
        {
            var res = await (from jourscaisse in dbContext.Jourscaisse
                             join piedeche in dbContext.Piedeche on jourscaisse.Id equals piedeche.JourcaisseId
                             join userModePaies in dbContext.UserModePaie on piedeche.ModepaieId equals userModePaies.ModePaieId into userModePaies1

                             from userModePaies in userModePaies1.DefaultIfEmpty()
                             join status in dbContext.Status on jourscaisse.StatusComtage equals status.Id
                             join statusR in dbContext.Status on jourscaisse.StatusReception equals statusR.Id

                             where statusAccept.Contains(status.Code) &&!statusReject.Contains(status.Code) && jourscaisse.StatusComtage == status.Id&&
                              (modePaies == null || (modePaies.Count == 0 || modePaies.Contains(userModePaies.ModePaieId)))
                             select new JoursCaiseDTO
                             {
                                 statusReceptionObj = new StatusDTO { Code = statusR.Code, Id = statusR.Id, Statuslibelle = statusR.Statuslibelle, Statuslibelleen = statusR.Statuslibelleen },

                                 statusComptageObj = new StatusDTO { Code = status.Code, Id = status.Id, Statuslibelle = status.Statuslibelle, Statuslibelleen = status.Statuslibelleen },
                                 GjcUserferme = jourscaisse.GjcUserferme,
                                 GjcNumzcaisse = jourscaisse.GjcNumzcaisse,
                                 GjcHeureouv = jourscaisse.GjcHeureouv,
                                 MontantCaisse = (from piedeche1 in dbContext.Piedeche
                                                  join jourscaisse1 in dbContext.Jourscaisse on piedeche1.JourcaisseId equals jourscaisse1.Id
                                                  join userModePaies1 in dbContext.UserModePaie on piedeche1.ModepaieId equals userModePaies1.ModePaieId into modePaieGroup
                                                  from userModePaie in modePaieGroup.DefaultIfEmpty()
                                                  where jourscaisse.Id == jourscaisse1.Id
                                                  select piedeche1.Montant).Sum(),





                                 Id = jourscaisse.Id,
                                 GjcHeureferme = jourscaisse.GjcHeureferme,
                                 GjcEtat = jourscaisse.GjcEtat,
                                 AgentComptage = jourscaisse.AgentComptage,
                                 CahuffeurId = jourscaisse.CahuffeurId,
                                 Commentaire = jourscaisse.Commentaire,
                                 Comments = jourscaisse.Comments,
                                 ValidationDateFin = jourscaisse.ValidationDateFin,
                                 DateComptage = jourscaisse.DateComptage,
                                 DateCreate = jourscaisse.DateCreate,
                                 EmetteurId = jourscaisse.EmetteurId,
                                 GjcCaisse = jourscaisse.GjcCaisse,
                                 GjcDateferme = jourscaisse.GjcDateferme,
                                 GjcDateouv = jourscaisse.GjcDateouv,
                                 GjcCaisselabel = jourscaisse.GjcCaisselabel,
                                 GjcEtablissementlabel = jourscaisse.GjcEtablissementlabel,
                                 MotifId = jourscaisse.MotifId,
                                 ValidationDateDebut = jourscaisse.ValidationDateDebut,
                                 ReceptionDate = jourscaisse.ReceptionDate,
                                 GjcEtablissementId = jourscaisse.GjcEtablissementId,
                                 StatusComtage = jourscaisse.StatusComtage,
                                 StatusReception = jourscaisse.StatusReception,
                                 RefExterne=jourscaisse.RefExterne,
                                 IsManuel=jourscaisse.IsManuel,




                                 GjcEtablissement = (from etab in dbContext.Etablissement
                                                     where jourscaisse.GjcEtablissementId == etab.Id
                                                     select new EtablissementDTO
                                                     {
                                                         Id = etab.Id,
                                                         EtLibelle = etab.EtLibelle,
                                                         EtDevise = etab.EtDevise,
                                                         EtEtablissement = etab.EtEtablissement,
                                                         EtLangue = etab.EtLangue,
                                                         EtAdresse1 = etab.EtAdresse1,
                                                         EtAdresse2 = etab.EtAdresse2,
                                                         EtAdresse3 = etab.EtAdresse3,
                                                         EtCodepostal = etab.EtCodepostal,
                                                         EtPays = etab.EtPays,
                                                         EtTelephone = etab.EtTelephone,
                                                         EtVille = etab.EtVille,
                                                     }).First(),

                             }).Distinct().ToListAsync();
            return res;
        }



        public async Task<List<JoursCaiseDTO>> GetWithModePaieFiltreGetAllByStatusCmptNone(List<int?> modePaies, List<string> statusAccept, List<string> statusReject, DateTime? dateTime1, DateTime? dateTime2)
        {
            var res = await (from jourscaisse in dbContext.Jourscaisse
                             join piedeche in dbContext.Piedeche on jourscaisse.Id equals piedeche.JourcaisseId
                             join userModePaies in dbContext.UserModePaie on piedeche.ModepaieId equals userModePaies.ModePaieId into userModePaies1

                             from userModePaies in userModePaies1.DefaultIfEmpty()
                             join status in dbContext.Status on jourscaisse.StatusComtage equals status.Id
                             join statusR in dbContext.Status on jourscaisse.StatusReception equals statusR.Id

                             where statusAccept.Contains(status.Code) && !statusReject.Contains(status.Code) && jourscaisse.StatusComtage == status.Id &&
                              (modePaies == null || (modePaies.Count == 0 || modePaies.Contains(userModePaies.ModePaieId))) && jourscaisse.GjcDateouv >= dateTime1 && jourscaisse.GjcDateouv <= dateTime2
                             select new JoursCaiseDTO
                             {
                                 statusReceptionObj = new StatusDTO { Code = statusR.Code, Id = statusR.Id, Statuslibelle = statusR.Statuslibelle, Statuslibelleen = statusR.Statuslibelleen },

                                 statusComptageObj = new StatusDTO { Code = status.Code, Id = status.Id, Statuslibelle = status.Statuslibelle, Statuslibelleen = status.Statuslibelleen },
                                 GjcUserferme = jourscaisse.GjcUserferme,
                                 GjcNumzcaisse = jourscaisse.GjcNumzcaisse,
                                 GjcHeureouv = jourscaisse.GjcHeureouv,
                                 MontantCaisse = (from piedeche1 in dbContext.Piedeche
                                                  join jourscaisse1 in dbContext.Jourscaisse on piedeche1.JourcaisseId equals jourscaisse1.Id
                                                  join userModePaies1 in dbContext.UserModePaie on piedeche1.ModepaieId equals userModePaies1.ModePaieId into modePaieGroup
                                                  from userModePaie in modePaieGroup.DefaultIfEmpty()
                                                  where jourscaisse.Id == jourscaisse1.Id
                                                  select piedeche1.Montant).Sum(),





                                 Id = jourscaisse.Id,
                                 GjcHeureferme = jourscaisse.GjcHeureferme,
                                 GjcEtat = jourscaisse.GjcEtat,
                                 AgentComptage = jourscaisse.AgentComptage,
                                 CahuffeurId = jourscaisse.CahuffeurId,
                                 Commentaire = jourscaisse.Commentaire,
                                 Comments = jourscaisse.Comments,
                                 ValidationDateFin = jourscaisse.ValidationDateFin,
                                 DateComptage = jourscaisse.DateComptage,
                                 DateCreate = jourscaisse.DateCreate,
                                 EmetteurId = jourscaisse.EmetteurId,
                                 GjcCaisse = jourscaisse.GjcCaisse,
                                 GjcDateferme = jourscaisse.GjcDateferme,
                                 GjcDateouv = jourscaisse.GjcDateouv,
                                 GjcCaisselabel = jourscaisse.GjcCaisselabel,
                                 GjcEtablissementlabel = jourscaisse.GjcEtablissementlabel,
                                 MotifId = jourscaisse.MotifId,
                                 ValidationDateDebut = jourscaisse.ValidationDateDebut,
                                 ReceptionDate = jourscaisse.ReceptionDate,
                                 GjcEtablissementId = jourscaisse.GjcEtablissementId,
                                 StatusComtage = jourscaisse.StatusComtage,
                                 StatusReception = jourscaisse.StatusReception,
                                 RefExterne = jourscaisse.RefExterne,
                                 IsManuel = jourscaisse.IsManuel,

                                 IsEcart = (from piedeche in dbContext.Piedeche
                                            where jourscaisse.Id == piedeche.JourcaisseId
                                            group piedeche by new { piedeche.JourcaisseId } into newPiedeche
                                            select new
                                            {
                                                Montant = newPiedeche.Sum(x => x.Ecart)

                                            }).First().Montant > 0 ? true : (from piedeche in dbContext.Piedeche
                                                                             where jourscaisse.Id == piedeche.JourcaisseId
                                                                             group piedeche by new { piedeche.JourcaisseId } into newPiedeche
                                                                             select new
                                                                             {
                                                                                 Montant = newPiedeche.Sum(x => x.Ecart)

                                                                             }).First().Montant < 0 ? false : null,

                                 GjcEtablissement = (from etab in dbContext.Etablissement
                                                     where jourscaisse.GjcEtablissementId == etab.Id
                                                     select new EtablissementDTO
                                                     {
                                                         Id = etab.Id,
                                                         EtLibelle = etab.EtLibelle,
                                                         EtDevise = etab.EtDevise,
                                                         EtEtablissement = etab.EtEtablissement,
                                                         EtLangue = etab.EtLangue,
                                                         EtAdresse1 = etab.EtAdresse1,
                                                         EtAdresse2 = etab.EtAdresse2,
                                                         EtAdresse3 = etab.EtAdresse3,
                                                         EtCodepostal = etab.EtCodepostal,
                                                         EtPays = etab.EtPays,
                                                         EtTelephone = etab.EtTelephone,
                                                         EtVille = etab.EtVille,
                                                     }).First(),

                             }).Distinct().ToListAsync();
            return res;
        }

        public async Task<List<JoursCaiseDTO>> GetWithModePaieRecep(List<int?> modePaies, List<int?> statusRecept, List<int?> statusCompt)
        {
            var res = await (from jourscaisse in dbContext.Jourscaisse
                             join piedeche in dbContext.Piedeche on jourscaisse.Id equals piedeche.JourcaisseId
                             join userModePaies in dbContext.UserModePaie on piedeche.ModepaieId equals userModePaies.ModePaieId
                             join status in dbContext.Status on jourscaisse.StatusReception equals status.Id
                             join statusC in dbContext.Status on jourscaisse.StatusComtage equals statusC.Id



                             where statusRecept.Contains(status.Id) && !statusCompt.Contains(jourscaisse.StatusComtage) && jourscaisse.StatusReception == status.Id && modePaies.Contains(userModePaies.ModePaieId)
                             select new JoursCaiseDTO
                             {
                                 statusComptageObj = new StatusDTO { Code = statusC.Code, Id = statusC.Id, Statuslibelle = statusC.Statuslibelle, Statuslibelleen = statusC.Statuslibelleen },

                                 statusReceptionObj = new StatusDTO { Code = status.Code, Id = status.Id, Statuslibelle = status.Statuslibelle, Statuslibelleen = status.Statuslibelleen },
                                 GjcUserferme = jourscaisse.GjcUserferme,
                                 GjcNumzcaisse = jourscaisse.GjcNumzcaisse,
                                 GjcHeureouv = jourscaisse.GjcHeureouv,
                                 MontantCaisse = (from piedeche1 in dbContext.Piedeche
                                                  join jourscaisse1 in dbContext.Jourscaisse on piedeche1.JourcaisseId equals jourscaisse1.Id
                                                  join userModePaies1 in dbContext.UserModePaie on piedeche1.ModepaieId equals userModePaies1.ModePaieId

                                                  where jourscaisse.Id == jourscaisse1.Id select piedeche1.Montant).Sum(),




                                 Id = jourscaisse.Id,
                                 GjcHeureferme = jourscaisse.GjcHeureferme,
                                 GjcEtat = jourscaisse.GjcEtat,
                                 AgentComptage = jourscaisse.AgentComptage,
                                 CahuffeurId = jourscaisse.CahuffeurId,
                                 Commentaire = jourscaisse.Commentaire,
                                 Comments = jourscaisse.Comments,
                                 ValidationDateFin = jourscaisse.ValidationDateFin,
                                 DateComptage = jourscaisse.DateComptage,
                                 DateCreate = jourscaisse.DateCreate,
                                 EmetteurId = jourscaisse.EmetteurId,
                                 GjcCaisse = jourscaisse.GjcCaisse,
                                 GjcDateferme = jourscaisse.GjcDateferme,
                                 GjcDateouv = jourscaisse.GjcDateouv,
                                 GjcCaisselabel = jourscaisse.GjcCaisselabel,
                                 GjcEtablissementlabel = jourscaisse.GjcEtablissementlabel,
                                 MotifId = jourscaisse.MotifId,
                                 ValidationDateDebut = jourscaisse.ValidationDateDebut,
                                 ReceptionDate = jourscaisse.ReceptionDate,
                                 GjcEtablissementId = jourscaisse.GjcEtablissementId,
                                 StatusComtage = jourscaisse.StatusComtage,
                                 StatusReception = jourscaisse.StatusReception,
                                 RefExterne = jourscaisse.RefExterne,
                                 IsManuel = jourscaisse.IsManuel,
                                 GjcEtablissement = (from etab in dbContext.Etablissement
                                                     where jourscaisse.GjcEtablissementId == etab.Id
                                                     select new EtablissementDTO
                                                     {
                                                         Id = etab.Id,
                                                         EtLibelle = etab.EtLibelle,
                                                         EtDevise = etab.EtDevise,
                                                         EtEtablissement = etab.EtEtablissement,
                                                         EtLangue = etab.EtLangue,
                                                         EtAdresse1 = etab.EtAdresse1,
                                                         EtAdresse2 = etab.EtAdresse2,
                                                         EtAdresse3 = etab.EtAdresse3,
                                                         EtCodepostal = etab.EtCodepostal,
                                                         EtPays = etab.EtPays,
                                                         EtTelephone = etab.EtTelephone,
                                                         EtVille = etab.EtVille,
                                                     }).First(),



                             }).Distinct().ToListAsync();
            return res;
        }
        public async Task<List<JoursCaiseDTO>> GetWithModePaieRecepFilter(List<int?> modePaies, List<int?> listStatusAccept, List<int?> listStatusReject, DateTime dateTime1, DateTime dateTime2) { 

        
            var res = await (from jourscaisse in dbContext.Jourscaisse
                             join piedeche in dbContext.Piedeche on jourscaisse.Id equals piedeche.JourcaisseId
                             join userModePaies in dbContext.UserModePaie on piedeche.ModepaieId equals userModePaies.ModePaieId
                             join status in dbContext.Status on jourscaisse.StatusReception equals status.Id
                             join statusC in dbContext.Status on jourscaisse.StatusComtage equals statusC.Id



                             where listStatusAccept.Contains(status.Id) && !listStatusReject.Contains(jourscaisse.StatusComtage) && jourscaisse.StatusReception == status.Id 
                             && modePaies.Contains(userModePaies.ModePaieId)&&jourscaisse.ReceptionDate>=dateTime1 && jourscaisse.ReceptionDate <= dateTime2
                             select new JoursCaiseDTO
                             {
                                 statusComptageObj = new StatusDTO { Code = statusC.Code, Id = statusC.Id, Statuslibelle = statusC.Statuslibelle, Statuslibelleen = statusC.Statuslibelleen },

                                 statusReceptionObj = new StatusDTO { Code = status.Code, Id = status.Id, Statuslibelle = status.Statuslibelle, Statuslibelleen = status.Statuslibelleen },
                                 GjcUserferme = jourscaisse.GjcUserferme,
                                 GjcNumzcaisse = jourscaisse.GjcNumzcaisse,
                                 GjcHeureouv = jourscaisse.GjcHeureouv,
                                 MontantCaisse = (from piedeche1 in dbContext.Piedeche
                                                  join jourscaisse1 in dbContext.Jourscaisse on piedeche1.JourcaisseId equals jourscaisse1.Id
                                                  join userModePaies1 in dbContext.UserModePaie on piedeche1.ModepaieId equals userModePaies1.ModePaieId

                                                  where jourscaisse.Id == jourscaisse1.Id select piedeche1.Montant).Sum(),




                                 Id = jourscaisse.Id,
                                 GjcHeureferme = jourscaisse.GjcHeureferme,
                                 GjcEtat = jourscaisse.GjcEtat,
                                 AgentComptage = jourscaisse.AgentComptage,
                                 CahuffeurId = jourscaisse.CahuffeurId,
                                 Commentaire = jourscaisse.Commentaire,
                                 Comments = jourscaisse.Comments,
                                 ValidationDateFin = jourscaisse.ValidationDateFin,
                                 DateComptage = jourscaisse.DateComptage,
                                 DateCreate = jourscaisse.DateCreate,
                                 EmetteurId = jourscaisse.EmetteurId,
                                 GjcCaisse = jourscaisse.GjcCaisse,
                                 GjcDateferme = jourscaisse.GjcDateferme,
                                 GjcDateouv = jourscaisse.GjcDateouv,
                                 GjcCaisselabel = jourscaisse.GjcCaisselabel,
                                 GjcEtablissementlabel = jourscaisse.GjcEtablissementlabel,
                                 MotifId = jourscaisse.MotifId,
                                 ValidationDateDebut = jourscaisse.ValidationDateDebut,
                                 ReceptionDate = jourscaisse.ReceptionDate,
                                 GjcEtablissementId = jourscaisse.GjcEtablissementId,
                                 StatusComtage = jourscaisse.StatusComtage,
                                 StatusReception = jourscaisse.StatusReception,
                                 RefExterne = jourscaisse.RefExterne,
                                 IsManuel = jourscaisse.IsManuel,
                                 GjcEtablissement = (from etab in dbContext.Etablissement
                                                     where jourscaisse.GjcEtablissementId == etab.Id
                                                     select new EtablissementDTO
                                                     {
                                                         Id = etab.Id,
                                                         EtLibelle = etab.EtLibelle,
                                                         EtDevise = etab.EtDevise,
                                                         EtEtablissement = etab.EtEtablissement,
                                                         EtLangue = etab.EtLangue,
                                                         EtAdresse1 = etab.EtAdresse1,
                                                         EtAdresse2 = etab.EtAdresse2,
                                                         EtAdresse3 = etab.EtAdresse3,
                                                         EtCodepostal = etab.EtCodepostal,
                                                         EtPays = etab.EtPays,
                                                         EtTelephone = etab.EtTelephone,
                                                         EtVille = etab.EtVille,
                                                     }).First(),



                             }).Distinct().ToListAsync();
            return res;
        }
        public async Task<List<JoursCaiseDTO>> GetWithModePaieRecepFiltreEnveloppeEnAttente(List<int?> modePaies, List<int?> listStatusAccept, List<int?> listStatusReject, DateTime? dateTime1, DateTime? dateTime2) { 

        
            var res = await (from jourscaisse in dbContext.Jourscaisse
                             join piedeche in dbContext.Piedeche on jourscaisse.Id equals piedeche.JourcaisseId
                             join userModePaies in dbContext.UserModePaie on piedeche.ModepaieId equals userModePaies.ModePaieId
                             join status in dbContext.Status on jourscaisse.StatusReception equals status.Id
                             join statusC in dbContext.Status on jourscaisse.StatusComtage equals statusC.Id



                             where listStatusAccept.Contains(status.Id) && !listStatusReject.Contains(jourscaisse.StatusComtage) && jourscaisse.StatusReception == status.Id 
                             && modePaies.Contains(userModePaies.ModePaieId)&&jourscaisse.GjcDateouv >= dateTime1 && jourscaisse.GjcDateouv <= dateTime2
                             select new JoursCaiseDTO
                             {
                                 statusComptageObj = new StatusDTO { Code = statusC.Code, Id = statusC.Id, Statuslibelle = statusC.Statuslibelle, Statuslibelleen = statusC.Statuslibelleen },

                                 statusReceptionObj = new StatusDTO { Code = status.Code, Id = status.Id, Statuslibelle = status.Statuslibelle, Statuslibelleen = status.Statuslibelleen },
                                 GjcUserferme = jourscaisse.GjcUserferme,
                                 GjcNumzcaisse = jourscaisse.GjcNumzcaisse,
                                 GjcHeureouv = jourscaisse.GjcHeureouv,
                                 MontantCaisse = (from piedeche1 in dbContext.Piedeche
                                                  join jourscaisse1 in dbContext.Jourscaisse on piedeche1.JourcaisseId equals jourscaisse1.Id
                                                  join userModePaies1 in dbContext.UserModePaie on piedeche1.ModepaieId equals userModePaies1.ModePaieId

                                                  where jourscaisse.Id == jourscaisse1.Id select piedeche1.Montant).Sum(),




                                 Id = jourscaisse.Id,
                                 GjcHeureferme = jourscaisse.GjcHeureferme,
                                 GjcEtat = jourscaisse.GjcEtat,
                                 AgentComptage = jourscaisse.AgentComptage,
                                 CahuffeurId = jourscaisse.CahuffeurId,
                                 Commentaire = jourscaisse.Commentaire,
                                 Comments = jourscaisse.Comments,
                                 ValidationDateFin = jourscaisse.ValidationDateFin,
                                 DateComptage = jourscaisse.DateComptage,
                                 DateCreate = jourscaisse.DateCreate,
                                 EmetteurId = jourscaisse.EmetteurId,
                                 GjcCaisse = jourscaisse.GjcCaisse,
                                 GjcDateferme = jourscaisse.GjcDateferme,
                                 GjcDateouv = jourscaisse.GjcDateouv,
                                 GjcCaisselabel = jourscaisse.GjcCaisselabel,
                                 GjcEtablissementlabel = jourscaisse.GjcEtablissementlabel,
                                 MotifId = jourscaisse.MotifId,
                                 ValidationDateDebut = jourscaisse.ValidationDateDebut,
                                 ReceptionDate = jourscaisse.ReceptionDate,
                                 GjcEtablissementId = jourscaisse.GjcEtablissementId,
                                 StatusComtage = jourscaisse.StatusComtage,
                                 StatusReception = jourscaisse.StatusReception,
                                 RefExterne = jourscaisse.RefExterne,
                                 IsManuel = jourscaisse.IsManuel,
                                 GjcEtablissement = (from etab in dbContext.Etablissement
                                                     where jourscaisse.GjcEtablissementId == etab.Id
                                                     select new EtablissementDTO
                                                     {
                                                         Id = etab.Id,
                                                         EtLibelle = etab.EtLibelle,
                                                         EtDevise = etab.EtDevise,
                                                         EtEtablissement = etab.EtEtablissement,
                                                         EtLangue = etab.EtLangue,
                                                         EtAdresse1 = etab.EtAdresse1,
                                                         EtAdresse2 = etab.EtAdresse2,
                                                         EtAdresse3 = etab.EtAdresse3,
                                                         EtCodepostal = etab.EtCodepostal,
                                                         EtPays = etab.EtPays,
                                                         EtTelephone = etab.EtTelephone,
                                                         EtVille = etab.EtVille,
                                                     }).First(),



                             }).Distinct().ToListAsync();
            return res;
        }

        public async Task<List<Jourscaisse>> GetAllWithDateOuvEtab(DateTime? dateTime1, DateTime? dateTimeTo1, int etablissementId)
        {
            var res = await dbContext.Jourscaisse
                .Where(x => (etablissementId == 0 || x.GjcEtablissementId == etablissementId)
                            && (dateTime1 == null || x.GjcDateouv >= dateTime1.Value)
                            && (dateTimeTo1 == null || x.GjcDateouv <= dateTimeTo1.Value))
                .ToListAsync();
            return res;
        }
    }
}
