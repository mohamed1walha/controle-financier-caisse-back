using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DDD.Domain.Common;
using DDD.Domain.Interfaces;
using DDD.Infra.Data.Context;
using DTO;
using Entity;
using Microsoft.EntityFrameworkCore;

namespace DDD.Infra.Data.Repository
{
    public class UserRolesRepository : GenericBaseRepository<UserRoles, DDDContext>,
          IUserRolesRepository<UserRoles>, IDisposable
    {
        public UserRolesRepository(DDDContext context)
            : base(context)
        {
            dbContext = context;
        }

        public async Task<bool> EditAsync(UserRolesDTO entity)
        {
            var m = new UserRolesDTO();
            var query =
                   from jc in dbContext.UserRoles
                   where jc.Id == entity.Id
                   select jc;
            foreach (var item in query)
            {
                item.RoleId = entity.RoleId;
            }




            try
            {
                dbContext.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }


            return true;
        }
    }
    
}
