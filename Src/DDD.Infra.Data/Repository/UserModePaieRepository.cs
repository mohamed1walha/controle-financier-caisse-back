using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DDD.Domain.Common;
using DDD.Domain.Interfaces;
using DDD.Infra.Data.Context;
using DTO;
using Entity;
using Microsoft.EntityFrameworkCore;

namespace DDD.Infra.Data.Repository
{
    public class UserModePaieRepository : GenericBaseRepository<UserModePaie, DDDContext>,
          IUserModePaieRepository<UserModePaie>, IDisposable
    {
        public UserModePaieRepository(DDDContext context)
            : base(context)
        {
            dbContext = context;
        }

        public async Task<bool> EditAsync(UserModePaieDTO entity)
        {
            var m = new UserModePaieDTO();
            var query =
                   from jc in dbContext.UserModePaie
                   where jc.Id == entity.Id
                   select jc;
            foreach (var item in query)
            {
                item.ModePaieId = entity.ModePaieId;
            }




            try
            {
                dbContext.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }


            return true;
        }
    }
    
}
