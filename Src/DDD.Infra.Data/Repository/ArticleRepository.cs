using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DDD.Domain.Common;
using DDD.Domain.Interfaces;
using DDD.Infra.Data.Context;
using DTO;
using Entity;
using Microsoft.EntityFrameworkCore;

namespace DDD.Infra.Data.Repository
{
    public class ArticleRepository : GenericBaseRepository<Article, DDDContext>,
          IArticleRepository<Article>, IDisposable
    {
        public ArticleRepository(DDDContext context)
            : base(context)
        {
            dbContext = context;
        }

        public async Task<List<ArticleDTO>> GetAllWithControlSolde()
        {
            var res = await(from article in dbContext.Article
                            where article.ControleSolde==true
                            select new ArticleDTO
                            {ControleSolde=article.ControleSolde,
                            Code=article.Code,
                            CompteC=article.CompteC,
                            CompteTva=article.CompteTva,
                            Designation=article.Designation,
                            Id=article.Id,
                            PrixHt=article.PrixHt,
                            TauxTva=article.TauxTva,
                            TypeTva=article.TypeTva


                            }).ToListAsync();
            return res;
        }
    }
}
