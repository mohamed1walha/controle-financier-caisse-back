using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DDD.Domain.Common;
using DDD.Domain.Interfaces;
using DDD.Infra.Data.Context;
using DTO;
using Entity;
using Microsoft.EntityFrameworkCore;

namespace DDD.Infra.Data.Repository
{
    public class CompteDepenseRepository : GenericBaseRepository<CompteDepense, DDDContext>,
          ICompteDepenseRepository<CompteDepense>, IDisposable
    {
        public CompteDepenseRepository(DDDContext context)
            : base(context)
        {
            dbContext = context;
        }

        public async Task<List<CompteDepenseDTO>> GetAllCompteDepense()
        {
            var res = await (from cp in dbContext.CompteDepense

                             select new CompteDepenseDTO
                             {
                                 Id = cp.Id,
                                 Libelle = cp.Libelle,
                                 MontantLimite = cp.MontantLimite,
                                 Solde = cp.Solde,
                                 CompteCaisseG=cp.CompteCaisseG,
                                 JOURNAL=cp.JOURNAL,
                                 EtablissementId = cp.EtablissementId,
                                 Etablissement = (from etab in dbContext.Etablissement
                                                  where cp.EtablissementId == etab.Id
                                                  select new EtablissementDTO
                                                  {
                                                      Id = etab.Id,
                                                      EtLibelle = etab.EtLibelle,
                                                      EtDevise = etab.EtDevise,
                                                      EtEtablissement = etab.EtEtablissement,
                                                      EtLangue = etab.EtLangue,
                                                      EtAdresse1 = etab.EtAdresse1,
                                                      EtAdresse2 = etab.EtAdresse2,
                                                      EtAdresse3 = etab.EtAdresse3,
                                                      EtCodepostal = etab.EtCodepostal,
                                                      EtPays = etab.EtPays,
                                                      EtTelephone = etab.EtTelephone,
                                                      EtVille = etab.EtVille,
                                                  }).First(),
                             
                             }
                     ).ToListAsync();
            return res;
        }
    }
}
