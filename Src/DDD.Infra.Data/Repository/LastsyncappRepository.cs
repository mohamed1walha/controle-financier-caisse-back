using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DDD.Domain.Common;
using DDD.Domain.Interfaces;
using DDD.Infra.Data.Context;
using Entity;
using Microsoft.EntityFrameworkCore;

namespace DDD.Infra.Data.Repository
{
    public class LastsyncappRepository : GenericBaseRepository<Lastsyncapp, DDDContext>,
        ILastsyncappRepository<Lastsyncapp>, IDisposable
    {
        public LastsyncappRepository(DDDContext context)
            : base(context)
        {
            dbContext = context;
        }
    }
}
