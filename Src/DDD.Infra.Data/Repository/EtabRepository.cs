﻿using DDD.Domain.Common;
using DDD.Domain.Interfaces;
using Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Infra.Data.Repository
{
    public class EtabRepository : GenericBaseRepository<Etablissement, DDDContext>,
        IEtabRepository<Etablissement>, IDisposable
    {
        public EtabRepository(DDDContext context)
            : base(context)
        {
            dbContext = context;
        }

       
        //public Task<List<Etablissement>> GetAllEtab()
        //{
        //    var res = (from j in dbContext.Etablissement
        //               join jourscaisse in dbContext.Jourscaisse on j.EtEtablissement equals jourscaisse.GjcEtablissement
        //               select new Etablissement
        //               {
        //                   EtEtablissement = j.EtEtablissement,
        //                   EtLibelle = j.EtLibelle,
        //               }

        //              ).Distinct().ToListAsync();
        //    return res; 

        //}
    }
}
