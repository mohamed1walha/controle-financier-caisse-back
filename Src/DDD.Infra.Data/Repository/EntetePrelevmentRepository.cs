using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DDD.Domain.Common;
using DDD.Domain.Interfaces;
using DDD.Infra.Data.Context;
using DTO;
using Entity;
using Microsoft.EntityFrameworkCore;

namespace DDD.Infra.Data.Repository
{
    public class EntetePrelevementRepository : GenericBaseRepository<EntetePrelevement, DDDContext>,
          IEntetePrelevementRepository<EntetePrelevement>, IDisposable
    {
        public EntetePrelevementRepository(DDDContext context)
            : base(context)
        {
            dbContext = context;
        }

        public async Task<int> EditEntete(EntetePrelevement entity)
        {
            var query =
                  await (from jc in dbContext.EntetePrelevement
                         where jc.Id == entity.Id
                         select jc).FirstOrDefaultAsync();

            query.PieceJointe = entity.PieceJointe;
            query.StatutId = entity.StatutId;
            query.CodeJournal = entity.CodeJournal;
            query.DateReponse = entity.DateReponse;
            query.EtablissementId = entity.EtablissementId;
            query.MontantTotal = entity.MontantTotal;
            query.ValidateurUserId = entity.ValidateurUserId;
            query.TypePrelevement = entity.TypePrelevement;
            query.DatePrelevement = entity.DatePrelevement;
            query.Commentaire = entity.Commentaire;
            query.IsCompta=entity.IsCompta;




            try
            {
                dbContext.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }



            return entity.Id;
        }

        public async Task<List<EntetePrelevementDTO>> GetAllByUserId(int userId)
        {
            var res = await (
                 from entete in dbContext.EntetePrelevement
                 join status in dbContext.Status on entete.StatutId equals status.Id
                 join etab in dbContext.Etablissement on entete.EtablissementId equals etab.Id
                 join user in dbContext.User on entete.ValidateurUserId equals user.Id into users
                 from user in users.DefaultIfEmpty()
                 join userCreate in dbContext.User on entete.CreateByUserId equals userCreate.Id
                  where userCreate.Id == userId
                 select new EntetePrelevementDTO
                 {
                     Id = entete.Id,
                     CodeJournal = entete.CodeJournal,
                     Commentaire = entete.Commentaire,
                     CreateByUserId = entete.CreateByUserId,
                     ValidateurUserId = entete.ValidateurUserId,
                     CreateDate = entete.CreateDate,
                     DatePrelevement = entete.DatePrelevement,
                     DateReponse = entete.DateReponse,
                     MontantTotal = entete.MontantTotal,
                     PieceJointe = entete.PieceJointe,
                     EtablissementId = entete.EtablissementId,
                     StatutId = entete.StatutId,
                     StatutLabel = status.Statuslibelle,
                     TypePrelevement = entete.TypePrelevement,
                     UserCreatedFullName = userCreate.FirstName + ' ' + userCreate.LastName,
                     UserValidatorFullName = user.FirstName + ' ' + user.LastName,
                     status = new StatusDTO { Id = status.Id, Code = status.Code, Statuslibelle = status.Statuslibelle, Statuslibelleen = status.Statuslibelleen },
                     Etablissement = new EtablissementDTO
                     {
                         Id = etab.Id,
                         EtLibelle = etab.EtLibelle,
                         EtDevise = etab.EtDevise,
                         EtEtablissement = etab.EtEtablissement,
                         EtLangue = etab.EtLangue,
                         EtAdresse1 = etab.EtAdresse1,
                         EtAdresse2 = etab.EtAdresse2,
                         EtAdresse3 = etab.EtAdresse3,
                         EtCodepostal = etab.EtCodepostal,
                         EtPays = etab.EtPays,
                         EtTelephone = etab.EtTelephone,
                         EtVille = etab.EtVille,
                     }


                 }
                 ).ToListAsync();
            return res;
        }
        public async Task<List<EntetePrelevementDTO>> GetAllByEtablissementIdAndUserId(int etablissementId, int userId)
        {
            var res = await (
                 from entete in dbContext.EntetePrelevement
                 join status in dbContext.Status on entete.StatutId equals status.Id
                 join etab in dbContext.Etablissement on entete.EtablissementId equals etab.Id
                 join user in dbContext.User on entete.ValidateurUserId equals user.Id into users
                 from user in users.DefaultIfEmpty()
                 join userCreate in dbContext.User on entete.CreateByUserId equals userCreate.Id
                 where etab.Id == etablissementId && userCreate.Id == userId
                 select new EntetePrelevementDTO
                 {
                     Id = entete.Id,
                     CodeJournal = entete.CodeJournal,
                     Commentaire = entete.Commentaire,
                     CreateByUserId = entete.CreateByUserId,
                     ValidateurUserId = entete.ValidateurUserId,
                     CreateDate = entete.CreateDate,
                     DatePrelevement = entete.DatePrelevement,
                     DateReponse = entete.DateReponse,
                     MontantTotal = entete.MontantTotal,
                     PieceJointe = entete.PieceJointe,
                     EtablissementId = entete.EtablissementId,
                     StatutId = entete.StatutId,
                     StatutLabel = status.Statuslibelle,
                     TypePrelevement = entete.TypePrelevement,
                     UserCreatedFullName = userCreate.FirstName + ' ' + userCreate.LastName,
                     UserValidatorFullName = user.FirstName + ' ' + user.LastName,
                     status = new StatusDTO { Id = status.Id, Code = status.Code, Statuslibelle = status.Statuslibelle, Statuslibelleen = status.Statuslibelleen },
                     Etablissement = new EtablissementDTO
                     {
                         Id = etab.Id,
                         EtLibelle = etab.EtLibelle,
                         EtDevise = etab.EtDevise,
                         EtEtablissement = etab.EtEtablissement,
                         EtLangue = etab.EtLangue,
                         EtAdresse1 = etab.EtAdresse1,
                         EtAdresse2 = etab.EtAdresse2,
                         EtAdresse3 = etab.EtAdresse3,
                         EtCodepostal = etab.EtCodepostal,
                         EtPays = etab.EtPays,
                         EtTelephone = etab.EtTelephone,
                         EtVille = etab.EtVille,
                     }


                 }
                 ).OrderByDescending(x => x.DatePrelevement).ToListAsync();
            return res;
        }

        public async Task<List<EntetePrelevementDTO>> GetAllByValidateur(int userId)
        {
            var res = await (
                 from entete in dbContext.EntetePrelevement
                 join status in dbContext.Status on entete.StatutId equals status.Id
                 join etab in dbContext.Etablissement on entete.EtablissementId equals etab.Id
                 join user in dbContext.User on entete.ValidateurUserId equals user.Id into users
                 from user in users.DefaultIfEmpty()
                 join userCreate in dbContext.User on entete.CreateByUserId equals userCreate.Id
                 where user.Id == userId
                 select new EntetePrelevementDTO
                 {
                     Id = entete.Id,
                     CodeJournal = entete.CodeJournal,
                     Commentaire = entete.Commentaire,
                     CreateByUserId = entete.CreateByUserId,
                     ValidateurUserId = entete.ValidateurUserId,
                     CreateDate = entete.CreateDate,
                     DatePrelevement = entete.DatePrelevement,
                     DateReponse = entete.DateReponse,
                     MontantTotal = entete.MontantTotal,
                     PieceJointe = entete.PieceJointe,
                     EtablissementId = entete.EtablissementId,
                     StatutId = entete.StatutId,
                     StatutLabel = status.Statuslibelle,
                     TypePrelevement = entete.TypePrelevement,
                     UserCreatedFullName = userCreate.FirstName + ' ' + userCreate.LastName,
                     UserValidatorFullName = user.FirstName + ' ' + user.LastName,
                     status = new StatusDTO { Id = status.Id, Code = status.Code, Statuslibelle = status.Statuslibelle, Statuslibelleen = status.Statuslibelleen },
                     Etablissement = new EtablissementDTO
                     {
                         Id = etab.Id,
                         EtLibelle = etab.EtLibelle,
                         EtDevise = etab.EtDevise,
                         EtEtablissement = etab.EtEtablissement,
                         EtLangue = etab.EtLangue,
                         EtAdresse1 = etab.EtAdresse1,
                         EtAdresse2 = etab.EtAdresse2,
                         EtAdresse3 = etab.EtAdresse3,
                         EtCodepostal = etab.EtCodepostal,
                         EtPays = etab.EtPays,
                         EtTelephone = etab.EtTelephone,
                         EtVille = etab.EtVille,
                     }


                 }
                 ).OrderByDescending(x => x.DatePrelevement).ToListAsync();
            return res;
        }
        public async Task<List<EntetePrelevementDTO>> GetAllByDemandeur(int userId, bool isAccepted)
        {
            var res = await (
                 from entete in dbContext.EntetePrelevement
                 join status in dbContext.Status on entete.StatutId equals status.Id
                 join etab in dbContext.Etablissement on entete.EtablissementId equals etab.Id
                 join userscreate in dbContext.User on entete.CreateByUserId equals userscreate.Id
                 join usersValidate in dbContext.User on entete.ValidateurUserId equals usersValidate.Id into usersValidates
                 from usersValidate in usersValidates.DefaultIfEmpty()
                 where entete.CreateByUserId== userId && (isAccepted == true? status.Id >0 : status.Statuslibelle != "Accept�")
                 select new EntetePrelevementDTO
                 {
                     Id = entete.Id,
                     CodeJournal = entete.CodeJournal,
                     Commentaire = entete.Commentaire,
                     CreateByUserId = entete.CreateByUserId,
                     ValidateurUserId = entete.ValidateurUserId,
                     CreateDate = entete.CreateDate,
                     DatePrelevement = entete.DatePrelevement,
                     DateReponse = entete.DateReponse,
                     MontantTotal = entete.MontantTotal,
                     PieceJointe = entete.PieceJointe,
                     EtablissementId = entete.EtablissementId,
                     StatutId = entete.StatutId,
                     StatutLabel = status.Statuslibelle,
                     TypePrelevement = entete.TypePrelevement,
                     UserCreatedFullName = userscreate.FirstName + ' ' + userscreate.LastName,
                     UserValidatorFullName = usersValidate.FirstName + ' ' + usersValidate.LastName,
                     status = new StatusDTO { Id = status.Id, Code = status.Code, Statuslibelle = status.Statuslibelle, Statuslibelleen = status.Statuslibelleen },
                     Etablissement = new EtablissementDTO
                     {
                         Id = etab.Id,
                         EtLibelle = etab.EtLibelle,
                         EtDevise = etab.EtDevise,
                         EtEtablissement = etab.EtEtablissement,
                         EtLangue = etab.EtLangue,
                         EtAdresse1 = etab.EtAdresse1,
                         EtAdresse2 = etab.EtAdresse2,
                         EtAdresse3 = etab.EtAdresse3,
                         EtCodepostal = etab.EtCodepostal,
                         EtPays = etab.EtPays,
                         EtTelephone = etab.EtTelephone,
                         EtVille = etab.EtVille,
                     }


                 }
                 ).OrderByDescending(x => x.DatePrelevement).ToListAsync();
            return res;
        }
        public async Task<List<EntetePrelevementDTO>> GetAllEntete()
        {
            var res = await (
                 from entete in dbContext.EntetePrelevement
                 join status in dbContext.Status on entete.StatutId equals status.Id
                 join etab in dbContext.Etablissement on entete.EtablissementId equals etab.Id
                 join user in dbContext.User on entete.ValidateurUserId equals user.Id into users
                 from user in users.DefaultIfEmpty()
                 join userCreate in dbContext.User on entete.CreateByUserId equals userCreate.Id
                 select new EntetePrelevementDTO
                 {
                     Id = entete.Id,
                     CodeJournal = entete.CodeJournal,
                     Commentaire = entete.Commentaire,
                     CreateByUserId = entete.CreateByUserId,
                     ValidateurUserId = entete.ValidateurUserId,
                     CreateDate = entete.CreateDate,
                     DatePrelevement = entete.DatePrelevement,
                     DateReponse = entete.DateReponse,
                     MontantTotal = entete.MontantTotal,
                     PieceJointe = entete.PieceJointe,
                     EtablissementId = entete.EtablissementId,
                     StatutId = entete.StatutId,
                     StatutLabel = status.Statuslibelle,
                     TypePrelevement = entete.TypePrelevement,
                     UserCreatedFullName = userCreate.FirstName + ' ' + userCreate.LastName,
                     UserValidatorFullName = user.FirstName + ' ' + user.LastName,
                     status = new StatusDTO { Id = status.Id, Code = status.Code, Statuslibelle = status.Statuslibelle, Statuslibelleen = status.Statuslibelleen },
                     Etablissement = new EtablissementDTO
                     {
                         Id = etab.Id,
                         EtLibelle = etab.EtLibelle,
                         EtDevise = etab.EtDevise,
                         EtEtablissement = etab.EtEtablissement,
                         EtLangue = etab.EtLangue,
                         EtAdresse1 = etab.EtAdresse1,
                         EtAdresse2 = etab.EtAdresse2,
                         EtAdresse3 = etab.EtAdresse3,
                         EtCodepostal = etab.EtCodepostal,
                         EtPays = etab.EtPays,
                         EtTelephone = etab.EtTelephone,
                         EtVille = etab.EtVille,
                     }


                 }
                 ).OrderByDescending(x => x.CreateDate).ToListAsync();
            return res;
        }

        public async Task<PagedResults> GetAllEnteteLazy(GridStateDTO grid, FilterPrelevementDTO filters)
        {
            if (grid.Page == 0)
                grid.Page = 1;

            if (grid.PageSize == 0)
                grid.PageSize = int.MaxValue;

            var resQuery = dbContext.EntetePrelevement
                .Include(entete => entete.LignePrelevement)
                .Include(entete => entete.Statut)
                .Include(entete => entete.Etablissement)
                .Where(x =>
                                 (filters.EtablissementId != null && filters.EtablissementId.Count > 0 ? filters.EtablissementId.Contains(x.EtablissementId) : x.Id > 0) &&
                                 (filters.UserCreated != null && filters.UserCreated.Count > 0 ? filters.UserCreated.Contains(x.CreateByUserId) : x.Id > 0) &&
                                 (filters.Validateur != null && filters.Validateur.Count > 0 ? filters.Validateur.Contains(x.ValidateurUserId) : x.Id > 0) &&
                                 (filters.TypePrelevement != null && filters.TypePrelevement.Count > 0 ? filters.TypePrelevement.Contains(x.TypePrelevement) : x.Id > 0) &&
                                 (filters.MontantTotalFrom != null && filters.MontantTotalFrom > 0 ? filters.MontantTotalFrom <= x.MontantTotal : x.Id > 0) &&
                                 (filters.MontantTotalTo != null && filters.MontantTotalTo > 0 ? filters.MontantTotalTo >= x.MontantTotal : x.Id > 0) &&
                                 (filters.DatePrelevementFrom != null ? filters.DatePrelevementFrom.Value.Date.AddDays(1) <= x.DatePrelevement.Value.Date : x.Id > 0) &&
                                 (filters.DatePrelevementTo != null ? filters.DatePrelevementTo.Value.Date.AddDays(1) >= x.DatePrelevement.Value.Date : x.Id > 0) &&
                                 (filters.DateCreationFrom != null ? filters.DateCreationFrom.Value.Date.AddDays(1) <= x.CreateDate.Value.Date : x.Id > 0) &&
                                 (filters.DateCreationTo != null ? filters.DateCreationTo.Value.Date.AddDays(1) >= x.CreateDate.Value.Date : x.Id > 0) &&
                                 (filters.IsCompta == true ? x.IsCompta == true : x.IsCompta == null || x.IsCompta == false) &&
                                 (filters.StatutId == x.StatutId)
                );





            var res = await resQuery
                .OrderByDescending(entete => entete.Id)
                .Skip((grid.Page - 1) * grid.PageSize)
                .Take(grid.PageSize)
                .Select(entete => new EntetePrelevementDTO
                {
                    Id = entete.Id,
                    CodeJournal = entete.CodeJournal,
                    Commentaire = entete.Commentaire,
                    CreateByUserId = entete.CreateByUserId,
                    ValidateurUserId = entete.ValidateurUserId,
                    CreateDate = entete.CreateDate,
                    DatePrelevement = entete.DatePrelevement,
                    DateReponse = entete.DateReponse,
                    MontantTotal = entete.MontantTotal,
                    PieceJointe = entete.PieceJointe,
                    EtablissementId = entete.EtablissementId,
                    StatutId = entete.StatutId,
                    StatutLabel = entete.Statut.Statuslibelle,
                    TypePrelevement = entete.TypePrelevement,
                    UserCreatedFullName = (from user in dbContext.User where user.Id == entete.CreateByUserId select user.FirstName + " " + user.LastName).FirstOrDefault(),
                    UserValidatorFullName = (from user in dbContext.User where user.Id == entete.ValidateurUserId select user.FirstName + " " + user.LastName).FirstOrDefault(),
                    status = new StatusDTO
                    {
                        Id = entete.Statut.Id,
                        Code = entete.Statut.Code,
                        Statuslibelle = entete.Statut.Statuslibelle,
                        Statuslibelleen = entete.Statut.Statuslibelleen
                    },
                    Etablissement = new EtablissementDTO
                    {
                        Id = entete.Etablissement.Id,
                        EtLibelle = entete.Etablissement.EtLibelle,
                        EtDevise = entete.Etablissement.EtDevise,
                        EtEtablissement = entete.Etablissement.EtEtablissement,
                        EtLangue = entete.Etablissement.EtLangue,
                        EtAdresse1 = entete.Etablissement.EtAdresse1,
                        EtAdresse2 = entete.Etablissement.EtAdresse2,
                        EtAdresse3 = entete.Etablissement.EtAdresse3,
                        EtCodepostal = entete.Etablissement.EtCodepostal,
                        EtPays = entete.Etablissement.EtPays,
                        EtTelephone = entete.Etablissement.EtTelephone,
                        EtVille = entete.Etablissement.EtVille,
                    }
                })
                .ToListAsync();

            var count = await resQuery.CountAsync();

            var retVal = new PagedResults(res, count, grid.Page, grid.PageSize);
            return retVal;
        }


        //public async Task<List<EntetePrelevementDTO>> GetAllWithFilter(FilterPrelevementDTO filters)
        //{
        //    var res = await (
        //         from entete in dbContext.EntetePrelevement
        //         join status in dbContext.Status on entete.StatutId equals status.Id
        //         join etab in dbContext.Etablissement on entete.EtablissementId equals etab.Id
        //         join user in dbContext.User on entete.ValidateurUserId equals user.Id into users
        //         from user in users.DefaultIfEmpty()
        //         join userCreate in dbContext.User on entete.CreateByUserId equals userCreate.Id
        //         where (
        //           (filters.EtablissementId == null || (filters.EtablissementId.Count == 0 || filters.EtablissementId.Contains(etab.Id))) &&
        //           (filters.StatutId == null || (filters.StatutId.Count == 0 || filters.StatutId.Contains(status.Id))) &&
        //           (filters.TypePrelevement == null || (filters.TypePrelevement.Count == 0 || filters.TypePrelevement.Contains(entete.TypePrelevement))) &&
        //           (filters.UserCreated == null || (filters.UserCreated.Count == 0 || filters.UserCreated.Contains(userCreate.Id))) &&
        //           (filters.UserValidator == null || (filters.UserValidator.Count == 0 || filters.UserValidator.Contains(user.Id))) &&
        //            (filters.CompteDepenseId == null || (filters.CompteDepenseId.Count == 0 || filters.CompteDepenseId.Contains(entete.CompteDepenseId))) &&
        //            (filters.DatePrelevementFrom == null || (filters.DatePrelevementFrom<=entete.DatePrelevement))&&
        //            (filters.DatePrelevementTo == null || (filters.DatePrelevementTo >= entete.DatePrelevement))&&
        //            (filters.DateReponseFrom == null || (filters.DateReponseFrom >= entete.DateReponse))&&
        //            (filters.DateReponseTo == null || (filters.DateReponseTo >= entete.DatePrelevement))&&
        //            (filters.CreateDateFrom == null || (filters.CreateDateFrom >= entete.CreateDate))&&
        //            (filters.CreateDateTo == null || (filters.CreateDateTo >= entete.CreateDate))&&
        //            (filters.MontantTotal == null || (filters.MontantTotal == entete.MontantTotal)) &&
        //            (filters.IsCompta == true ? entete.IsCompta == true : entete.IsCompta == null || entete.IsCompta == false)


        //         )
        //         select new EntetePrelevementDTO
        //         {
        //             Id = entete.Id,
        //             CodeJournal = entete.CodeJournal,
        //             Commentaire = entete.Commentaire,
        //             CreateByUserId = entete.CreateByUserId,
        //             ValidateurUserId = entete.ValidateurUserId,
        //             CreateDate = entete.CreateDate,
        //             DatePrelevement = entete.DatePrelevement,
        //             DateReponse = entete.DateReponse,
        //             MontantTotal = entete.MontantTotal,
        //             PieceJointe = entete.PieceJointe,
        //             EtablissementId = entete.EtablissementId,
        //             StatutId = entete.StatutId,
        //             StatutLabel = status.Statuslibelle,
        //             TypePrelevement = entete.TypePrelevement,
        //             UserCreatedFullName = userCreate.FirstName + ' ' + userCreate.LastName,
        //             UserValidatorFullName = user.FirstName + ' ' + user.LastName,
        //             status = new StatusDTO { Id = status.Id, Code = status.Code, Statuslibelle = status.Statuslibelle, Statuslibelleen = status.Statuslibelleen },
        //             Etablissement = new EtablissementDTO
        //             {
        //                 Id = etab.Id,
        //                 EtLibelle = etab.EtLibelle,
        //                 EtDevise = etab.EtDevise,
        //                 EtEtablissement = etab.EtEtablissement,
        //                 EtLangue = etab.EtLangue,
        //                 EtAdresse1 = etab.EtAdresse1,
        //                 EtAdresse2 = etab.EtAdresse2,
        //                 EtAdresse3 = etab.EtAdresse3,
        //                 EtCodepostal = etab.EtCodepostal,
        //                 EtPays = etab.EtPays,
        //                 EtTelephone = etab.EtTelephone,
        //                 EtVille = etab.EtVille,
        //             }


        //         }
        //         ).OrderByDescending(x => x.CreateDate).ToListAsync();
        //    return res;
        //} 



        public async Task<EntetePrelevementDTO> GetByEnteteId(int id)
        {
            var res = await (
                 from entete in dbContext.EntetePrelevement
                 join status in dbContext.Status on entete.StatutId equals status.Id
                 join etab in dbContext.Etablissement on entete.EtablissementId equals etab.Id
                 join user in dbContext.User on entete.ValidateurUserId equals user.Id into users
                 from user in users.DefaultIfEmpty()
                 join userCreate in dbContext.User on entete.CreateByUserId equals userCreate.Id
                 where entete.Id == id

                 select new EntetePrelevementDTO
                 {
                     Id = entete.Id,
                     CodeJournal = entete.CodeJournal,
                     Commentaire = entete.Commentaire,
                     CreateByUserId = entete.CreateByUserId,
                     ValidateurUserId = entete.ValidateurUserId,
                     CreateDate = entete.CreateDate,
                     DatePrelevement = entete.DatePrelevement,
                     DateReponse = entete.DateReponse,
                     MontantTotal = entete.MontantTotal,
                     PieceJointe = entete.PieceJointe,
                     EtablissementId = entete.EtablissementId,
                     StatutId = entete.StatutId,
                     StatutLabel = status.Statuslibelle,
                     TypePrelevement = entete.TypePrelevement,
                     UserCreatedFullName = userCreate.FirstName + ' ' + userCreate.LastName,
                     UserValidatorFullName = user.FirstName + ' ' + user.LastName,
                     status = new StatusDTO { Id = status.Id, Code = status.Code, Statuslibelle = status.Statuslibelle, Statuslibelleen = status.Statuslibelleen },
                     Etablissement = new EtablissementDTO
                     {
                         Id = etab.Id,
                         EtLibelle = etab.EtLibelle,
                         EtDevise = etab.EtDevise,
                         EtEtablissement = etab.EtEtablissement,
                         EtLangue = etab.EtLangue,
                         EtAdresse1 = etab.EtAdresse1,
                         EtAdresse2 = etab.EtAdresse2,
                         EtAdresse3 = etab.EtAdresse3,
                         EtCodepostal = etab.EtCodepostal,
                         EtPays = etab.EtPays,
                         EtTelephone = etab.EtTelephone,
                         EtVille = etab.EtVille,
                     }


                 }
                 ).FirstOrDefaultAsync();
            return res;
        }
        public async Task<List<EntetePrelevementDTO>> GetAllByEtabIdCaisseIdNumZRef(int etabId,string caisseId,string numZRef)
        {
            var res = await (
                 from entete in dbContext.EntetePrelevement
                 join ligne in dbContext.LignePrelevement on entete.Id equals ligne.PrelevementId
                 join parcaisse in dbContext.Parcaisse on ligne.ParcaisseId equals parcaisse.Id

                 join status in dbContext.Status on entete.StatutId equals status.Id
                 join etab in dbContext.Etablissement on entete.EtablissementId equals etab.Id
                 join user in dbContext.User on entete.ValidateurUserId equals user.Id into users
                 from user in users.DefaultIfEmpty()
                 join userCreate in dbContext.User on entete.CreateByUserId equals userCreate.Id
                 where etab.Id == etabId && ligne.NumZref == numZRef && parcaisse.GpkCaisse == caisseId && status.Code != "RJT"
                 select new EntetePrelevementDTO
                 {
                     Id = entete.Id,
                     CodeJournal = entete.CodeJournal,
                     Commentaire = entete.Commentaire,
                     CreateByUserId = entete.CreateByUserId,
                     ValidateurUserId = entete.ValidateurUserId,
                     CreateDate = entete.CreateDate,
                     DatePrelevement = entete.DatePrelevement,
                     DateReponse = entete.DateReponse,
                     MontantTotal = ligne.Montant,
                     PieceJointe = entete.PieceJointe,
                     EtablissementId = entete.EtablissementId,
                     StatutId = entete.StatutId,
                     StatutLabel = status.Statuslibelle,
                     TypePrelevement = entete.TypePrelevement,
                     UserCreatedFullName = userCreate.FirstName + ' ' + userCreate.LastName,
                     UserValidatorFullName = user.FirstName + ' ' + user.LastName,
                     status = new StatusDTO { Id = status.Id, Code = status.Code, Statuslibelle = status.Statuslibelle, Statuslibelleen = status.Statuslibelleen },
                     Etablissement = new EtablissementDTO
                     {
                         Id = etab.Id,
                         EtLibelle = etab.EtLibelle,
                         EtDevise = etab.EtDevise,
                         EtEtablissement = etab.EtEtablissement,
                         EtLangue = etab.EtLangue,
                         EtAdresse1 = etab.EtAdresse1,
                         EtAdresse2 = etab.EtAdresse2,
                         EtAdresse3 = etab.EtAdresse3,
                         EtCodepostal = etab.EtCodepostal,
                         EtPays = etab.EtPays,
                         EtTelephone = etab.EtTelephone,
                         EtVille = etab.EtVille,
                     }


                 }
                 ).ToListAsync();
            return res;
        }
    }
}
