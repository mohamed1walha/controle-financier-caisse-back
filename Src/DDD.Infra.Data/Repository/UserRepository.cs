using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DDD.Domain.Common;
using DDD.Domain.Interfaces;
using DDD.Infra.Data.Context;
using DTO;
using Entity;
using Microsoft.EntityFrameworkCore;

namespace DDD.Infra.Data.Repository
{
    public class UserRepository : GenericBaseRepository<User, DDDContext>,
          IUserRepository<User>, IDisposable
    {
        public UserRepository(DDDContext context)
            : base(context)
        {
            dbContext = context;
        }

        public async Task<List<UserDTO>> GetAllUsers()
        {
            var res = await (from user in dbContext.User
                            
                             select new UserDTO
                             {
                                 Email = user.Email,
                                 Id = user.Id,
                                 FirstName = user.FirstName,
                                 LastName = user.LastName,
                                 Password = user.Password,
                                 IsActive = user.IsActive,
                                 IsAdmin = user.IsAdmin,
                                 Username = user.UserName,
                                 EtablissementId=user.EtablissementId,
                                 Etablissement = (from etab in dbContext.Etablissement
                                                     where user.EtablissementId == etab.Id
                                                     select new EtablissementDTO
                                                     {
                                                         Id = etab.Id,
                                                         EtLibelle = etab.EtLibelle,
                                                         EtDevise = etab.EtDevise,
                                                         EtEtablissement = etab.EtEtablissement,
                                                         EtLangue = etab.EtLangue,
                                                         EtAdresse1 = etab.EtAdresse1,
                                                         EtAdresse2 = etab.EtAdresse2,
                                                         EtAdresse3 = etab.EtAdresse3,
                                                         EtCodepostal = etab.EtCodepostal,
                                                         EtPays = etab.EtPays,
                                                         EtTelephone = etab.EtTelephone,
                                                         EtVille = etab.EtVille,
                                                     }).First(),
                                 Roles = (from role in dbContext.Roles
                                          join userRole in dbContext.UserRoles on role.Id equals userRole.RoleId
                                          where user.Id == userRole.UserId
                                          select new RolesDTO
                                          {
                                              Id = role.Id,
                                              Description = role.Description,
                                              Label = role.Label
                                          }).ToList(),
                                 ModePaies = (from modePaie in dbContext.Modepaie
                                              join devise in dbContext.Devise on modePaie.DeviseId equals devise.Id
                                              join userModePaies in dbContext.UserModePaie on modePaie.Id equals userModePaies.ModePaieId 
                                              where user.Id == userModePaies.UserId
                                              select new ModepaieDTO
                                              {
                                                  Id = modePaie.Id,
                                                  MpLibelle = modePaie.MpLibelle,
                                                  LibelleAppFinance = modePaie.LibelleAppFinance,
                                                  MpModepaie = modePaie.MpModepaie,
                                                  IsCash = modePaie.IsCash,
                                                  DateCreation = modePaie.DateCreation,
                                                  DateModification = modePaie.DateModification,
                                                  Actif = modePaie.Actif,
                                                  ShowCalculette = modePaie.ShowCalculette,
                                                  DeviseId = modePaie.DeviseId,
                                                  DeviseLabel=devise.Label
                                              }).ToList(),
                             }
                        ).ToListAsync();
            return res;
        }


        public async Task<List<UserDTO>> GetAllUsersSimpleData()
        {
            var res = await (from user in dbContext.User

                             select new UserDTO
                             {
                                 Email = user.Email,
                                 Id = user.Id,
                                 FirstName = user.FirstName,
                                 LastName = user.LastName,
                                 IsActive = user.IsActive,
                                 IsAdmin = user.IsAdmin,
                                 Username = user.UserName,
                                 EtablissementId = user.EtablissementId
                             }
                        ).ToListAsync();
            return res;
        }



        public async Task<List<UserDTO>> GetAllValidateur()
        {
            var res = await (from user in dbContext.User
                             join userRole in dbContext.UserRoles on user.Id equals userRole.UserId
                             join role in dbContext.Roles on userRole.RoleId equals role.Id
                             where role.Label==PV.Validators
                             select new UserDTO
                             {
                                 Email = user.Email,
                                 Id = user.Id,
                                 FirstName = user.FirstName+' '+ user.LastName,
                                 LastName = user.LastName,
                                 Password = user.Password,
                                 IsActive = user.IsActive,
                                 IsAdmin = user.IsAdmin,
                                 Username = user.UserName,
                                 EtablissementId=user.EtablissementId,
                                 Etablissement = (from etab in dbContext.Etablissement
                                                     where user.EtablissementId == etab.Id
                                                     select new EtablissementDTO
                                                     {
                                                         Id = etab.Id,
                                                         EtLibelle = etab.EtLibelle,
                                                         EtDevise = etab.EtDevise,
                                                         EtEtablissement = etab.EtEtablissement,
                                                         EtLangue = etab.EtLangue,
                                                         EtAdresse1 = etab.EtAdresse1,
                                                         EtAdresse2 = etab.EtAdresse2,
                                                         EtAdresse3 = etab.EtAdresse3,
                                                         EtCodepostal = etab.EtCodepostal,
                                                         EtPays = etab.EtPays,
                                                         EtTelephone = etab.EtTelephone,
                                                         EtVille = etab.EtVille,
                                                     }).First(),
                                 Roles = (from role in dbContext.Roles
                                          join userRole in dbContext.UserRoles on role.Id equals userRole.RoleId
                                          where user.Id == userRole.UserId
                                          select new RolesDTO
                                          {
                                              Id = role.Id,
                                              Description = role.Description,
                                              Label = role.Label
                                          }).ToList(),
                             }
                        ).ToListAsync();
            return res;
        } 
        public async Task<List<UserDTO>> GetAllByEtabId(int etabId)
        {
            var res = await (from user in dbContext.User
                             join userRole in dbContext.UserRoles on user.Id equals userRole.UserId
                             join role in dbContext.Roles on userRole.RoleId equals role.Id
                             where role.Label==PV.Demandeur && user.EtablissementId==etabId
                             select new UserDTO
                             {
                                 Email = user.Email,
                                 Id = user.Id,
                                 FirstName = user.FirstName+' '+ user.LastName,
                                 LastName = user.LastName,
                                 Password = user.Password,
                                 IsActive = user.IsActive,
                                 IsAdmin = user.IsAdmin,
                                 Username = user.UserName,
                                 EtablissementId=user.EtablissementId,
                                 Etablissement = (from etab in dbContext.Etablissement
                                                     where user.EtablissementId == etab.Id
                                                     select new EtablissementDTO
                                                     {
                                                         Id = etab.Id,
                                                         EtLibelle = etab.EtLibelle,
                                                         EtDevise = etab.EtDevise,
                                                         EtEtablissement = etab.EtEtablissement,
                                                         EtLangue = etab.EtLangue,
                                                         EtAdresse1 = etab.EtAdresse1,
                                                         EtAdresse2 = etab.EtAdresse2,
                                                         EtAdresse3 = etab.EtAdresse3,
                                                         EtCodepostal = etab.EtCodepostal,
                                                         EtPays = etab.EtPays,
                                                         EtTelephone = etab.EtTelephone,
                                                         EtVille = etab.EtVille,
                                                     }).First(),
                                 Roles = (from role in dbContext.Roles
                                          join userRole in dbContext.UserRoles on role.Id equals userRole.RoleId
                                          where user.Id == userRole.UserId
                                          select new RolesDTO
                                          {
                                              Id = role.Id,
                                              Description = role.Description,
                                              Label = role.Label
                                          }).ToList(),
                             }
                        ).ToListAsync();
            return res;
        }

        public async Task<UserDTO> GetByIdWithRole(int id)
        {
            var res = await(from user in dbContext.User
                            join userRoles in dbContext.UserRoles on user.Id equals userRoles.UserId into userRole
                            from userRoles in userRole.DefaultIfEmpty()
                            where
                         user.Id == id 
                            select new UserDTO
                            {
                                Email = user.Email,
                                Id = user.Id,
                                FirstName = user.FirstName,
                                LastName = user.LastName,
                                Password = user.Password,
                                IsActive = user.IsActive,
                                IsAdmin = user.IsAdmin,
                                Username = user.UserName,
                                EtablissementId = user.EtablissementId,
                                Roles = (from role in dbContext.Roles
                                         where user.Id == userRoles.UserId
                                         select new RolesDTO
                                         {
                                             Id = role.Id,
                                             Description = role.Description,
                                             Label = role.Label
                                         }).ToList(),
                            }
                                   ).FirstOrDefaultAsync();
            return res;
        }

        public async Task<UserDTO> GetByUserName(string username)
        {
            var res = await (from user in dbContext.User
                          join userRoles in dbContext.UserRoles on user.Id equals userRoles.UserId into userRole
                             from userRoles in userRole.DefaultIfEmpty()
                             join userModePaies in dbContext.UserModePaie on user.Id equals userModePaies.UserId into userModePaie
                             from userModePaies in userModePaie.DefaultIfEmpty()
                             where
                          user.UserName == username 
                             select new UserDTO
                          {
                              Email = user.Email, Id = user.Id,
                              FirstName = user.FirstName, LastName = user.LastName,
                              Password = user.Password,
                              IsActive = user.IsActive,
                                 IsAdmin = user.IsAdmin,
                                 EtablissementId = user.EtablissementId,
                                 Username = username,
                                 Roles = (from role in dbContext.Roles
                                          where user.Id == userRoles.UserId
                                          select new RolesDTO
                                          {
                                              Id = role.Id,
                                              Description = role.Description,
                                              Label = role.Label
                                          }).ToList(),
                                 ModePaies = (from modePaie in dbContext.Modepaie
                                              where user.Id == userModePaies.UserId
                                          select new ModepaieDTO
                                          {
                                              Id = modePaie.Id,
                                              MpLibelle= modePaie.MpLibelle,
                                              LibelleAppFinance= modePaie.LibelleAppFinance,
                                              MpModepaie= modePaie.MpModepaie,
                                              IsCash= modePaie.IsCash,
                                              DateCreation= modePaie.DateCreation,
                                              DateModification= modePaie.DateModification,
                                              Actif= modePaie.Actif,
                                              ShowCalculette= modePaie.ShowCalculette,  
                                              DeviseId= modePaie.DeviseId
                                          }).ToList(),
                             }
                        ).FirstOrDefaultAsync();
            return res;
        }
        
        public async Task<UserDTO> GetByUserId(int userId)
        {
            var res = await (from user in dbContext.User
                          
                             where
                          user.Id == userId
                             select new UserDTO
                          {
                              Email = user.Email, Id = user.Id,
                              FirstName = user.FirstName, LastName = user.LastName,
                              Password = user.Password,
                              IsActive = user.IsActive,
                                 IsAdmin = user.IsAdmin,
                                 EtablissementId = user.EtablissementId,
                                 Username = user.UserName,
                                 Roles = (from role in dbContext.Roles
                                          join userRoles in dbContext.UserRoles on role.Id equals userRoles.RoleId

                                          where user.Id == userRoles.UserId
                                          select new RolesDTO
                                          {
                                              Id = role.Id,
                                              Description = role.Description,
                                              Label = role.Label
                                          }).ToList(),
                                 ModePaies = (from modePaie in dbContext.Modepaie
                                              join userModePaies in dbContext.UserModePaie on modePaie.Id equals userModePaies.ModePaieId
                                              where user.Id == userModePaies.UserId
                                          select new ModepaieDTO
                                          {
                                              Id = modePaie.Id,
                                              MpLibelle= modePaie.MpLibelle,
                                              LibelleAppFinance= modePaie.LibelleAppFinance,
                                              MpModepaie= modePaie.MpModepaie,
                                              IsCash= modePaie.IsCash,
                                              DateCreation= modePaie.DateCreation,
                                              DateModification= modePaie.DateModification,
                                              Actif= modePaie.Actif,
                                              ShowCalculette= modePaie.ShowCalculette,  
                                              DeviseId= modePaie.DeviseId
                                          }).ToList(),
                             }
                        ).FirstOrDefaultAsync();
            return res;
        }

        public UserDTO GetByUserIdSync(int userId)
        {
            var res =  (from user in dbContext.User

                             where
                          user.Id == userId
                             select new UserDTO
                             {
                                 Email = user.Email,
                                 Id = user.Id,
                                 FirstName = user.FirstName,
                                 LastName = user.LastName,
                                 Password = user.Password,
                                 IsActive = user.IsActive,
                                 IsAdmin = user.IsAdmin,
                                 EtablissementId = user.EtablissementId,
                                 Username = user.UserName,
                                 Roles = (from role in dbContext.Roles
                                          join userRoles in dbContext.UserRoles on role.Id equals userRoles.RoleId

                                          where user.Id == userRoles.UserId
                                          select new RolesDTO
                                          {
                                              Id = role.Id,
                                              Description = role.Description,
                                              Label = role.Label
                                          }).ToList(),
                                 ModePaies = (from modePaie in dbContext.Modepaie
                                              join userModePaies in dbContext.UserModePaie on modePaie.Id equals userModePaies.ModePaieId
                                              where user.Id == userModePaies.UserId
                                              select new ModepaieDTO
                                              {
                                                  Id = modePaie.Id,
                                                  MpLibelle = modePaie.MpLibelle,
                                                  LibelleAppFinance = modePaie.LibelleAppFinance,
                                                  MpModepaie = modePaie.MpModepaie,
                                                  IsCash = modePaie.IsCash,
                                                  DateCreation = modePaie.DateCreation,
                                                  DateModification = modePaie.DateModification,
                                                  Actif = modePaie.Actif,
                                                  ShowCalculette = modePaie.ShowCalculette,
                                                  DeviseId = modePaie.DeviseId
                                              }).ToList(),
                             }
                        ).FirstOrDefault();
            return res;
        }

    }
}
