using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DDD.Domain.Common;
using DDD.Domain.Interfaces;
using DDD.Infra.Data.Context;
using DTO;
using Entity;
using Microsoft.EntityFrameworkCore;

namespace DDD.Infra.Data.Repository
{
    public class ModePaiementRepository : GenericBaseRepository<Modepaie, DDDContext>,
        IModePaiementRepository<Modepaie>, IDisposable
    {
        public ModePaiementRepository(DDDContext context)
            : base(context)
        {
            dbContext = context;
        }

        public async Task<bool> EditAsync(Modepaie res)
        {
            
                var m = new Modepaie();
                var query =
                       from jc in dbContext.Modepaie
                       where jc.Id == res.Id
                       select jc;
                foreach (var item in query)
                {
                    item.DeviseId = res.DeviseId;
                }




                try
                {
                    dbContext.SaveChanges();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }


                return true;
            }

        public async Task<List<ModepaieDTO>> GetAllModePaie()
        {
            var res =await (from m in dbContext.Modepaie
                       join devise in dbContext.Devise on m.DeviseId equals devise.Id
                       select new ModepaieDTO
                       {
                           Id = m.Id,
                           DeviseId = m.DeviseId,
                           MpLibelle = m.MpLibelle,
                           DeviseLabel = devise.Label,
                           LibelleAppFinance = m.LibelleAppFinance,
                           DateCreation = m.DateCreation,
                           MpModepaie = m.MpModepaie,
                           Actif = m.Actif,
                           DateModification = m.DateModification,
                           IsCash = m.IsCash,
                           ShowCalculette=m.ShowCalculette


                       }).ToListAsync();
            return res;
        }
    }
}
