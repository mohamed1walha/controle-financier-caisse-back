using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DDD.Domain.Common;
using DDD.Domain.Interfaces;
using DDD.Infra.Data.Context;
using DTO;
using Entity;
using Microsoft.EntityFrameworkCore;

namespace DDD.Infra.Data.Repository
{
    public class BilletDetailSortieRepository : GenericBaseRepository<BilletDetailSortie, DDDContext>,
          IBilletDetailSortieRepository<BilletDetailSortie>, IDisposable
    {
        public BilletDetailSortieRepository(DDDContext context)
            : base(context)
        {
            dbContext = context;
        }

        public async Task<List<BilletDetailSortieDTO>> GetAllBySortieId(int sortieId)
        {
            var res = await (from billetDetailSortie in dbContext.BilletDetailSortie
                             where billetDetailSortie.SortieId == sortieId


                             select new BilletDetailSortieDTO
                             {
                                Id= billetDetailSortie.Id,
                                BilletId= billetDetailSortie.BilletId,
                                SortieId=billetDetailSortie.SortieId,
                                Montant = billetDetailSortie.Montant,
                                Quantite = billetDetailSortie.Quantite,
                                Total = billetDetailSortie.Total


                             }).ToListAsync();
            return res;
        }
    }
}
