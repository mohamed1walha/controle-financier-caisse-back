using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DDD.Domain.Common;
using DDD.Domain.Interfaces;
using DDD.Infra.Data.Context;
using Entity;
using Microsoft.EntityFrameworkCore;

namespace DDD.Infra.Data.Repository
{
    public class EmetteurRepository : GenericBaseRepository<Emetteur, DDDContext>,
        IEmetteurRepository<Emetteur>,IDisposable
    {
        public EmetteurRepository(DDDContext context)
            : base(context)
        {
            dbContext = context;
        }

   

     
        
    }
}
