using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DDD.Domain.Common;
using DDD.Domain.Interfaces;
using DDD.Infra.Data.Context;
using DTO;
using Entity;
using Microsoft.EntityFrameworkCore;

namespace DDD.Infra.Data.Repository
{
    public class CoffreAlimentationRepository : GenericBaseRepository<Coffrealimentation, DDDContext>,
          ICoffreAlimentationRepository<Coffrealimentation>, IDisposable
    {
        public CoffreAlimentationRepository(DDDContext context)
            : base(context)
        {
            dbContext = context;
        }

        public async Task<List<CoffrealimentationDTO>> GetByCoffreId(int id)
        {
            var res = await(from coffreAlim in dbContext.Coffrealimentation
                                join coffre in dbContext.Coffre on coffreAlim.CoffreId equals coffre.Id
                            
                            where coffre.Id == id
                            select new CoffrealimentationDTO
                            {
                                Id = coffreAlim.Id,
                                AlimentationDate=coffreAlim.AlimentationDate,
                                CoffreLibelle = coffre.Libelle,
                                CoffreId = coffre.Id,
                                Username=coffreAlim.Username,
                                ModePaieId=coffreAlim.ModePaieId,
                                Montantalimenter=coffreAlim.Montantalimenter,
                                MontantConvertie=  (coffreAlim.PiedecheId== null? (from devise in dbContext.Devise
                                                                               join modePaie in dbContext.Modepaie on devise.Id equals modePaie.DeviseId
                                                                               where coffreAlim.ModePaieId == modePaie.Id
                                                                               select (decimal)coffreAlim.Montantalimenter* (decimal)devise.TauxChange).First()
                                                                             : (from devise in dbContext.Devise
                                                                                 join modePaie in dbContext.Modepaie on devise.Id equals modePaie.DeviseId
                                                                                join piedeche in dbContext.Piedeche on modePaie.Id equals piedeche.ModepaieId
                                                                                where coffreAlim.PiedecheId == piedeche.Id
                                                                                 select (decimal)coffreAlim.Montantalimenter * (decimal)devise.TauxChange).First()),
                                                                                    
                                Motif =coffreAlim.Motif,
                                PiedecheId=coffreAlim.PiedecheId
                                
                            }).ToListAsync();
            return res;
        }
    }
}
