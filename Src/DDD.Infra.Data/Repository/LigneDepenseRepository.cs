using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DDD.Domain.Common;
using DDD.Domain.Interfaces;
using DDD.Infra.Data.Context;
using DTO;
using Entity;
using Microsoft.EntityFrameworkCore;

namespace DDD.Infra.Data.Repository
{
    public class LigneDepenseRepository : GenericBaseRepository<LigneDepense, DDDContext>,
          ILigneDepenseRepository<LigneDepense>, IDisposable
    {
        public LigneDepenseRepository(DDDContext context)
            : base(context)
        {
            dbContext = context;
        }

        public async Task<LigneDepense> EditLigne(LigneDepense entity)
        {
            var query =
          await(from jc in dbContext.LigneDepense
                where jc.Id == entity.Id
                select jc).FirstOrDefaultAsync();

            query.ArticleId = entity.ArticleId;
            query.MontantHt = entity.MontantHt;
            query.TauxTva = entity.TauxTva;
            query.MontantTtc = entity.MontantTtc;
            query.DepenseId = entity.DepenseId;
            query.CompteTva = entity.CompteTva;
            query.MontantTva = entity.MontantTva;
            query.NumZref = entity.NumZref;
            query.Caisse = entity.Caisse;







            try
            {
                dbContext.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }



            return query;
        
    }

        public async Task<List<LigneDepenseDTO>> GetAllByEnteteId(int id)
        {
            var res = await (from lignes in dbContext.LigneDepense
                             join article in dbContext.Article on lignes.ArticleId equals article.Id

                             where lignes.DepenseId==id
                             select new LigneDepenseDTO
                             {
                                 DepenseId = lignes.DepenseId,
                                 ArticleId = lignes.ArticleId,
                                 Article= new ArticleDTO {
                                                        CompteC= article.CompteC,
                                                        Code= article.Code,
                                                        Id= article.Id,
                                                        CompteTva= article.CompteTva,
                                                        ControleSolde = article.ControleSolde,
                                                        Designation= article.Designation,
                                                        PrixHt= article.PrixHt,
                                                        TauxTva= article.TauxTva,
                                                        TypeTva= article.TypeTva
                                 },
                                 CompteTva= lignes.CompteTva,
                                 MontantHt= lignes.MontantHt,
                                 Id= lignes.Id,
                                 MontantTva= lignes.MontantTva,
                                 MontantTtc=lignes.MontantTtc,
                                 TauxTva= lignes.TauxTva,
                                 NumZref=lignes.NumZref,
                                 Caisse=lignes.Caisse
                                 

                             }
                             ).ToListAsync();

            return res;
        }

        public List<LigneDepenseDTO> GetAllByEnteteIdSync(int id)
        {
            var res =  (from lignes in dbContext.LigneDepense
                             join article in dbContext.Article on lignes.ArticleId equals article.Id

                             where lignes.DepenseId == id
                             select new LigneDepenseDTO
                             {
                                 DepenseId = lignes.DepenseId,
                                 ArticleId = lignes.ArticleId,
                                 Article = new ArticleDTO
                                 {
                                     CompteC = article.CompteC,
                                     Code = article.Code,
                                     Id = article.Id,
                                     CompteTva = article.CompteTva,
                                     ControleSolde = article.ControleSolde,
                                     Designation = article.Designation,
                                     PrixHt = article.PrixHt,
                                     TauxTva = article.TauxTva,
                                     TypeTva = article.TypeTva
                                 },
                                 CompteTva = lignes.CompteTva,
                                 MontantHt = lignes.MontantHt,
                                 Id = lignes.Id,
                                 MontantTva = lignes.MontantTva,
                                 MontantTtc = lignes.MontantTtc,
                                 TauxTva = lignes.TauxTva,
                                 NumZref = lignes.NumZref,
                                 Caisse = lignes.Caisse


                             }
                             ).ToList();

            return res;
        }


    }
}
