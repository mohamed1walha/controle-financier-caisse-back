using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DDD.Domain.Common;
using DDD.Domain.Interfaces;
using DTO;
using Entity;
using Microsoft.EntityFrameworkCore;

namespace DDD.Infra.Data.Repository
{
    public class CoffreRepository : GenericBaseRepository<Coffre, DDDContext>,
          ICoffreRepository<Coffre>, IDisposable
    {
        public CoffreRepository(DDDContext context)
            : base(context)
        {
            dbContext = context;
        }

        public async Task<List<CoffreDTO>> GetAllCoffre()
        {
            var res = await(from coffre in dbContext.Coffre
                            //join sortie in dbContext.Coffresortie on coffre.Id equals sortie.CoffreId
                            //join alimentation in dbContext.Coffrealimentation on coffre.Id equals alimentation.CoffreId
                            //join category in dbContext.CategorieModepaieCoffre on coffre.Id equals category.CoffreId
                            //join modepaie in dbContext.Modepaie on category.ModepaieId equals modepaie.Id

                            //where (piedeche.MontantCompte != (from a in dbContext.Coffrealimentation
                            //                                  where piedeche.Id == a.PiedecheId
                            //                                  select a.Montantalimenter).Sum())
                            select new CoffreDTO
                            {
                                Id = coffre.Id,
                                Code = coffre.Code,
                                Libelle = coffre.Libelle,
                                MontantAlimenterExcep= (from a in dbContext.Coffrealimentation
                                                        join modePaie in dbContext.Modepaie on a.ModePaieId equals modePaie.Id
                                                        join devise in dbContext.Devise on modePaie.DeviseId equals devise.Id
                                                        where a.CoffreId == coffre.Id
                                                        select a.Montantalimenter*devise.TauxChange).Sum(),
                                           
                                MontantConvertie = ((from a in dbContext.Coffrealimentation
                                                    join modePaie in dbContext.Modepaie on a.ModePaieId equals modePaie.Id
                                                    join devise in dbContext.Devise on modePaie.DeviseId equals devise.Id
                                                    where a.CoffreId == coffre.Id
                                                    select a.Montantalimenter * devise.TauxChange).Sum()+(from a in dbContext.Coffrealimentation
                                                   join piedeche in dbContext.Piedeche on a.PiedecheId equals piedeche.Id
                                                    join modePaie in dbContext.Modepaie on piedeche.ModepaieId equals modePaie.Id
                                                    join devise in dbContext.Devise on modePaie.DeviseId equals devise.Id
                                                    where a.CoffreId == coffre.Id
                                                   select a.Montantalimenter * devise.TauxChange).Sum()) - (from b in dbContext.Coffresortie
                                                                                                            join devise in dbContext.Devise on b.DeviseId equals devise.Id
                                                                                                            where b.CoffreId == coffre.Id
                                                                                                            select b.Montant*devise.TauxChange).Sum(),
                                Montant = (from a in dbContext.Coffrealimentation
                                           where a.CoffreId == coffre.Id
                                           select a.Montantalimenter).Sum()-
                                           (from b in dbContext.Coffresortie
                                           where b.CoffreId == coffre.Id
                                           select b.Montant).Sum(),

                                ModePaie = (from m in dbContext.Modepaie
                                            join category in dbContext.CategorieModepaieCoffre on m.Id equals category.ModepaieId

                                            where m.Id == category.ModepaieId && category.CoffreId == coffre.Id
                                            select m.Id).ToList(),
                                ModesPaieLibelle = String.Join(",",(from mp in dbContext.Modepaie
                                                    join category in dbContext.CategorieModepaieCoffre on mp.Id equals category.ModepaieId
                                                    where mp.Id == category.ModepaieId && category.CoffreId==coffre.Id
                                                    select   mp.LibelleAppFinance))
                            }).ToListAsync();
            return res;
        }
    }
}
//*(decimal)(from category in dbContext.CategorieModepaieCoffre
//           join mm in dbContext.Modepaie on category.ModepaieId equals mm.Id
//           join devise in dbContext.Devise on mm.DeviseId equals devise.Id

//           where mm.Id == category.ModepaieId && category.CoffreId == coffre.Id

//           select new DeviseDTO { TauxChange = devise.TauxChange }).First().TauxChange,