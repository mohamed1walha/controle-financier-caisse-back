using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DDD.Domain.Common;
using DDD.Domain.Interfaces;
using DDD.Infra.Data.Context;
using DTO;
using Entity;
using Microsoft.EntityFrameworkCore;

namespace DDD.Infra.Data.Repository
{
    public class CoffreSortieRepository : GenericBaseRepository<Coffresortie, DDDContext>,
        ICoffreSortieRepository<Coffresortie>, IDisposable
    {
        public CoffreSortieRepository(DDDContext context)
            : base(context)
        {
            dbContext = context;
        }

        public async Task<List<CoffresortieDTO>> getAllSorties()
        {
            var res = await(from sortie in dbContext.Coffresortie
                         
                            join coffre in dbContext.Coffre on sortie.CoffreId equals coffre.Id
                            join devise in dbContext.Devise on sortie.DeviseId equals devise.Id
                            join banque in dbContext.Banque on sortie.BanqueId equals banque.Id into banque1
                            from banque in banque1.DefaultIfEmpty()
                            join type in dbContext.Typefrais on sortie.TypeFraisId equals type.Id into typeFrais1
                            from type in typeFrais1.DefaultIfEmpty()

                            select new CoffresortieDTO
                            {
                                Id = sortie.Id,
                                BanqueId = banque.Id,
                               SiteConcernee = sortie.SiteConcernee,
                                TypeSortie = sortie.TypeSortie,
                                Justificatif = sortie.Justificatif,
                                PieceJustificatif= sortie.PieceJustificatif,
                                ServiceConcernee= sortie.ServiceConcernee,
                               Montant = sortie.Montant,
                                MontantConvertie = sortie.Montant * devise.TauxChange,
                                JustificatifName = sortie.JustificatifName,
                               SortieDate = sortie.SortieDate,
                               TypeFraisId = sortie.TypeFraisId,
                               DeviseId = sortie.DeviseId,
                               CoffreId = sortie.CoffreId,
                               Commentaire = sortie.Commentaire,
                               Extention = sortie.Extention,
                               Username = sortie.Username,
                               TypeSortieLibelle = sortie.TypeSortie==1?"Remise en banque": "Frais divers",
                               BanqueLibelle = banque.Libelle,
                               CoffreLibelle = coffre.Libelle,
                               TypeFraisLibelle = type.TypeFrais1,
                               Coffre= new CoffreDTO
                                {
                                    Code = coffre.Code,
                                    Id = coffre.Id,
                                    Libelle = coffre.Libelle,
                                }


        }).ToListAsync();
            return res;
        }

        public async Task<List<CoffresortieDTO>> GetByCoffreId(int id)
        {
            var res = await(from sortie in dbContext.Coffresortie
                            join banque in dbContext.Banque on sortie.BanqueId equals banque.Id into banque1
                            from banque in banque1.DefaultIfEmpty()
                            join coffre in dbContext.Coffre on sortie.CoffreId equals coffre.Id
                            join devise in dbContext.Devise on sortie.DeviseId equals devise.Id
                            join type in dbContext.Typefrais on sortie.TypeFraisId equals type.Id into typeFrais1
                            from type in typeFrais1.DefaultIfEmpty()
                            where sortie.Coffre.Id == id
                            select new CoffresortieDTO
                            {
                                Id = sortie.Id,
                                BanqueId = banque.Id,
                                SiteConcernee = sortie.SiteConcernee,
                                TypeSortie = sortie.TypeSortie,
                                Justificatif = sortie.Justificatif,
                                PieceJustificatif = sortie.PieceJustificatif,
                                ServiceConcernee = sortie.ServiceConcernee,
                                Montant = sortie.Montant,
                                MontantConvertie=sortie.Montant*devise.TauxChange,
                                JustificatifName = sortie.JustificatifName,
                                SortieDate = sortie.SortieDate,
                                TypeFraisId = sortie.TypeFraisId,
                                DeviseId = sortie.DeviseId,
                                CoffreId = sortie.CoffreId,
                                Commentaire = sortie.Commentaire,
                                Extention = sortie.Extention,
                                Username = sortie.Username,
                                TypeSortieLibelle = sortie.TypeSortie == 1 ? "Remise en banque" : "Frais divers",
                                BanqueLibelle = banque.Libelle,
                                CoffreLibelle = coffre.Libelle,
                                TypeFraisLibelle = type.TypeFrais1,
                                Coffre = new CoffreDTO
                                {
                                    Code = coffre.Code,
                                    Id = coffre.Id,
                                    Libelle = coffre.Libelle,
                                }


                            }).ToListAsync();
            return res;
        }
    }
}
