using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DDD.Domain.Common;
using DDD.Domain.Interfaces;
using DDD.Infra.Data.Context;
using Entity;
using Microsoft.EntityFrameworkCore;

namespace DDD.Infra.Data.Repository
{
    public class StatusRepository : GenericBaseRepository<Status, DDDContext>,
        IStatusRepository<Status>, IDisposable
    {
        public StatusRepository(DDDContext context)
            : base(context)
        {
            dbContext = context;
        }

        public async Task<List<Status>> checkStatusJoursCaisse(int id)
        {
            return await(from piedeche in dbContext.Piedeche
                         join status in dbContext.Status on piedeche.StatusComptage equals status.Id
                         where
                         piedeche.JourcaisseId == id
                         select status
                        ).ToListAsync();
        }

        public async Task<Status> GetByCode(string code)
        {
            return await (from status in dbContext.Status
                          where
                          status.Code == code
                          select status
                        ).FirstOrDefaultAsync();
        }
    }
}
