﻿using DDD.Domain.Common;
using DDD.Domain.Interfaces;
using DTO;
using Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDD.Infra.Data.Repository
{
    public class PiedechRepository : GenericBaseRepository<Piedeche, DDDContext>,
        IPiedecheRepository<Piedeche>, IDisposable
    {
        public PiedechRepository(DDDContext context)
            : base(context)
        {
            dbContext = context;
        }

        //public bool EditPiedeche(Piedeche c)
        //{
        //    var p = new Piedeche();
        //    var query =
        //           from jc in dbContext.Piedeche
        //           where jc.Id == c.Id
        //           select jc;

        //    foreach (var item in query)
        //    {
        //        item.MontantAlimenter = c.MontantAlimenter;
        //    }



        //    try
        //    {
        //        dbContext.SaveChanges();
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine(e);
        //    }



        //    return true;
        //}

        public bool EditPiedecheWithStatus(Piedeche c,int id)
        {
            var p = new Piedeche();
            var query =
                   from jc in dbContext.Piedeche
                   where jc.Id == c.Id
                   select jc;
            foreach(var item in query)
            {
                item.StatusComptage = id;
            }
            

          

            try
            {
                dbContext.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

        

            return  true;
        }

        public async Task<List<PiedecheDTO>> GetAllByCoffreNull(string statusCode)
        {
            var res = await (from piedeche in dbContext.Piedeche
                             join jourscaisse in dbContext.Jourscaisse on piedeche.JourcaisseId equals jourscaisse.Id
                             join modPaie in dbContext.Modepaie on piedeche.ModepaieId equals modPaie.Id
                             // join alimentation in dbContext.Coffrealimentation on piedeche.Id equals alimentation.PiedecheId
                             join status in dbContext.Status on piedeche.StatusComptage equals status.Id
                             join devise in dbContext.Devise on modPaie.DeviseId equals devise.Id

                             where (status.Code== statusCode&& piedeche.MontantCompte  != (from a in dbContext.Coffrealimentation
                                                               where piedeche.Id == a.PiedecheId select a.Montantalimenter).Sum())
                             select new PiedecheDTO
                             {
                                 Id = piedeche.Id,
                                 Montant = piedeche.Montant,
                                 MontantTotal = piedeche.MontantTotal,
                                 MontantCompte = piedeche.MontantCompte,
                                 MontantAlimenter = (from a in dbContext.Coffrealimentation
                                                     where piedeche.Id == a.PiedecheId
                                                     select a.Montantalimenter).Sum() ,
                                 Source = piedeche.Source,
                                 EspeceVerse = piedeche.EspeceVerse,
                                 EspeceCash = piedeche.EspeceCash,
                                 Ecart = piedeche.Ecart,
                                 Depence = piedeche.Depence,
                                 Commentaire = piedeche.Commentaire,
                                 StatusComptage = piedeche.StatusComptage,
                                 Jourcaisse =  (from j in dbContext.Jourscaisse where jourscaisse.Id == j.Id select new JoursCaiseDTO {
                                     Id = j.Id,
                                     AgentComptage = j.AgentComptage,
                                     CahuffeurId = j.CahuffeurId,
                                     Commentaire = j.Commentaire,
                                     Comments = j.Comments,
                                     DateComptage = j.DateComptage,
                                     DateCreate = j.DateCreate,
                                     EmetteurId = j.EmetteurId,
                                     GjcCaisse = j.GjcCaisse,
                                     GjcCaisselabel = j.GjcCaisselabel,
                                     GjcDateferme = j.GjcDateferme,
                                     GjcDateouv = j.GjcDateouv,
                                     GjcEtablissementId=j.GjcEtablissementId,
                                     GjcEtat=j.GjcEtat,
                                     GjcHeureferme= j.GjcHeureferme,
                                     GjcHeureouv=j.GjcHeureouv,
                                     GjcNumzcaisse=j.GjcNumzcaisse,
                                     MontantCaisse = j.MontantCaisse,
                                     MotifId = j.MotifId,
                                     GjcUserferme=j.GjcUserferme,   
                                     ReceptionDate = j.ReceptionDate,
                                     StatusReception = j.StatusReception,
                                     StatusComtage = j.StatusComtage,
                                     ValidationDateDebut = j.ValidationDateDebut,   
                                     ValidationDateFin=j.ValidationDateFin
                                           }).First(),
                                 GpeModepaie = modPaie.LibelleAppFinance,
                                 ModepaieId = piedeche.ModepaieId,
                                 AgentComptage = piedeche.AgentComptage,
                                 Modepaie = (from mm in dbContext.Modepaie
                                             join devise in dbContext.Devise on mm.DeviseId equals devise.Id
                                             where modPaie.Id == mm.Id
                                             select new ModepaieDTO
                                             {MpModepaie=mm.MpModepaie,
                                             Actif= mm.Actif,
                                             DateCreation= mm.DateCreation,
                                             DateModification= mm.DateModification, 
                                             DeviseId= mm.DeviseId,
                                             DeviseLabel=devise.Label,
                                             Id= mm.Id,
                                             LibelleAppFinance=mm.LibelleAppFinance,
                                             MpLibelle=mm.MpLibelle,
                                             IsCash= mm.IsCash,ShowCalculette=mm.ShowCalculette,
                                                 
                                             }).First(),
                                 Etablissement = (from e in dbContext.Etablissement
                                                  where jourscaisse.GjcEtablissementId == e.Id
                                                  select new EtablissementDTO
                                                  {
                                                      
                                                      Id = e.Id,
                                                      EtAdresse1 = e.EtAdresse1,
                                                      EtAdresse2 = e.EtAdresse2,
                                                      EtAdresse3= e.EtAdresse3,
                                                      EtCodepostal=e.EtCodepostal,
                                                      EtDevise=e.EtDevise,
                                                      EtLangue=e.EtLangue,
                                                      EtLibelle=e.EtLibelle,
                                                      EtPays=e.EtPays,
                                                      EtTelephone=e.EtTelephone,
                                                      EtVille=e.EtVille,
                                                      EtEtablissement=e.EtEtablissement,

                                                  }).First(),
                                 Datevalidation=piedeche.Datevalidation,
                                 MontantConvertie=piedeche.MontantCompte*(decimal)(from devise in dbContext.Devise
                                                   join modPaie1 in dbContext.Modepaie on piedeche.ModepaieId equals modPaie1.Id
                                                   where modPaie1.DeviseId == devise.Id

                                                  select new DeviseDTO { TauxChange=devise.TauxChange }).First().TauxChange,


                             }).ToListAsync();
            return res;
        }
        public async Task<PiedecheDTO> GetByIdWithMontantConvertie(int id)
        {
            var res = await (from piedeche in dbContext.Piedeche
                             join jourscaisse in dbContext.Jourscaisse on piedeche.JourcaisseId equals jourscaisse.Id
                             join modPaie in dbContext.Modepaie on piedeche.ModepaieId equals modPaie.Id

                             where (id==piedeche.Id)
                             select new PiedecheDTO
                             {
                                 Id = piedeche.Id,
                                 Montant = piedeche.Montant,
                                 MontantTotal = piedeche.MontantTotal,
                                 MontantCompte = piedeche.MontantCompte,
                                 MontantAlimenter = (from a in dbContext.Coffrealimentation
                                                     where piedeche.Id == a.PiedecheId
                                                     select a.Montantalimenter).Sum() * (decimal)(from devise in dbContext.Devise
                                                                                                  join modPaie1 in dbContext.Modepaie on piedeche.ModepaieId equals modPaie1.Id
                                                                                                  where modPaie1.DeviseId == devise.Id

                                                                                                  select new DeviseDTO { TauxChange = devise.TauxChange }).First().TauxChange,
                                 Source = piedeche.Source,
                                 EspeceVerse = piedeche.EspeceVerse,
                                 EspeceCash = piedeche.EspeceCash,
                                 Ecart = piedeche.Ecart,
                                 Depence = piedeche.Depence,
                                 Commentaire = piedeche.Commentaire,
                                 StatusComptage = piedeche.StatusComptage,
                                 Jourcaisse =  (from j in dbContext.Jourscaisse where jourscaisse.Id == j.Id select new JoursCaiseDTO {
                                     Id = j.Id,
                                     AgentComptage = j.AgentComptage,
                                     CahuffeurId = j.CahuffeurId,
                                     Commentaire = j.Commentaire,
                                     Comments = j.Comments,
                                     DateComptage = j.DateComptage,
                                     DateCreate = j.DateCreate,
                                     EmetteurId = j.EmetteurId,
                                     GjcCaisse = j.GjcCaisse,
                                     GjcCaisselabel = j.GjcCaisselabel,
                                     GjcDateferme = j.GjcDateferme,
                                     GjcDateouv = j.GjcDateouv,
                                     GjcEtablissementId=j.GjcEtablissementId,
                                     GjcEtat=j.GjcEtat,
                                     GjcHeureferme= j.GjcHeureferme,
                                     GjcHeureouv=j.GjcHeureouv,
                                     GjcNumzcaisse=j.GjcNumzcaisse,
                                     MontantCaisse = j.MontantCaisse,
                                     MotifId = j.MotifId,
                                     GjcUserferme=j.GjcUserferme,   
                                     ReceptionDate = j.ReceptionDate,
                                     StatusReception = j.StatusReception,
                                     StatusComtage = j.StatusComtage,
                                     ValidationDateDebut = j.ValidationDateDebut,   
                                     ValidationDateFin=j.ValidationDateFin
                                           }).First(),
                                 GpeModepaie = modPaie.LibelleAppFinance,
                                 ModepaieId = piedeche.ModepaieId,
                                 AgentComptage = piedeche.AgentComptage,
                                 Modepaie = (from mm in dbContext.Modepaie
                                             join devise in dbContext.Devise on mm.DeviseId equals devise.Id
                                             where modPaie.Id == mm.Id && devise.Id==mm.DeviseId 
                                             select new ModepaieDTO
                                             {MpModepaie=mm.MpModepaie,
                                             Actif= mm.Actif,
                                             DateCreation= mm.DateCreation,
                                             DateModification= mm.DateModification, 
                                             DeviseId= mm.DeviseId,
                                             DeviseLabel=devise.Label,
                                             Id= mm.Id,
                                             LibelleAppFinance=mm.LibelleAppFinance,
                                             MpLibelle=mm.MpLibelle,
                                                 IsCash = mm.IsCash,
                                                 ShowCalculette = mm.ShowCalculette

                                             }).First(),
                                 Etablissement = (from e in dbContext.Etablissement
                                                  where jourscaisse.GjcEtablissementId == e.Id
                                                  select new EtablissementDTO
                                                  {
                                                      
                                                      Id = e.Id,
                                                      EtAdresse1 = e.EtAdresse1,
                                                      EtAdresse2 = e.EtAdresse2,
                                                      EtAdresse3= e.EtAdresse3,
                                                      EtCodepostal=e.EtCodepostal,
                                                      EtDevise=e.EtDevise,
                                                      EtLangue=e.EtLangue,
                                                      EtLibelle=e.EtLibelle,
                                                      EtPays=e.EtPays,
                                                      EtTelephone=e.EtTelephone,
                                                      EtVille=e.EtVille,
                                                      EtEtablissement=e.EtEtablissement,

                                                  }).First(),
                                 Datevalidation=piedeche.Datevalidation,
                                 MontantConvertie=piedeche.MontantCompte*(decimal)(from devise in dbContext.Devise
                                                   join modPaie1 in dbContext.Modepaie on piedeche.ModepaieId equals modPaie1.Id
                                                                                   where modPaie1.DeviseId == devise.Id

                                                                                   select new DeviseDTO { TauxChange=devise.TauxChange }).First().TauxChange,


                             }).FirstAsync();
            return res;
        }

        private decimal? Calcule(decimal? montantCompte, decimal? v)
        {
            return montantCompte * v;
        }

        public async Task<List<PiedecheDTO>> GetAllByJoursCaisse(int id)
        {
            var res= await (from piedeche in dbContext.Piedeche
                          join jourscaisse in dbContext.Jourscaisse on piedeche.JourcaisseId equals jourscaisse.Id
                          join modPaie in dbContext.Modepaie on piedeche.ModepaieId equals modPaie.Id
                            where jourscaisse.Id == id
                            group piedeche by new { piedeche.ModepaieId, modPaie.LibelleAppFinance } into newPiedeche
                          select new PiedecheDTO
                          {
                              Montant = newPiedeche.Sum(x => x.Montant),
                              GpeModepaie = newPiedeche.Key.LibelleAppFinance,

                          }).ToListAsync();
            return res;
                         
        }

        public async Task<List<PiedecheDTO>> GetAllByJoursCaisseComptage(int id)
        {
            var res = await(from piedeche in dbContext.Piedeche
                            join jourscaisse in dbContext.Jourscaisse on piedeche.JourcaisseId equals jourscaisse.Id
                            join modPaie in dbContext.Modepaie on piedeche.ModepaieId equals modPaie.Id
                            where jourscaisse.Id == id
                            group piedeche by new { piedeche.ModepaieId, modPaie.LibelleAppFinance } into newPiedeche
                            select new PiedecheDTO
                            {
                                Montant = newPiedeche.Sum(x => x.Montant),
                                GpeModepaie = newPiedeche.Key.LibelleAppFinance,
                                MontantCompte= newPiedeche.First().Montant

                            }).ToListAsync();
            return res;
        }

        public async Task<List<PiedecheDTO>> GetAllByJoursCaisseById(int id)
        {
            var res = await (from piedeche in dbContext.Piedeche
                             join jourscaisse in dbContext.Jourscaisse on piedeche.JourcaisseId equals jourscaisse.Id
                             join modPaie in dbContext.Modepaie on piedeche.ModepaieId equals modPaie.Id



                             where jourscaisse.Id == id
                             select new PiedecheDTO
                             {
                                 Status = (from status in dbContext.Status
                                           where status.Id == piedeche.StatusComptage 
                                           select new StatusDTO { Code = status.Code, Id = status.Id, Statuslibelle = status.Statuslibelle, Statuslibelleen = status.Statuslibelleen }).First(),
                                 Modepaie = (from modPaie1 in dbContext.Modepaie
                                             join devise in dbContext.Devise on modPaie1.DeviseId equals devise.Id
                                             where modPaie1.Id == piedeche.ModepaieId
                                             select new ModepaieDTO { Id = modPaie.Id,DeviseLabel=devise.Label,IsCash=modPaie.IsCash, Actif = modPaie.Actif, DateCreation = modPaie.DateCreation,
                                     DateModification = modPaie.DateModification, DeviseId = modPaie.DeviseId, LibelleAppFinance = modPaie.LibelleAppFinance,
                                     MpLibelle = modPaie.MpLibelle, MpModepaie = modPaie.MpModepaie,ShowCalculette=modPaie.ShowCalculette
                                 }).First(),
                                 Montant = piedeche.Montant,
                                 GpeModepaie = modPaie.LibelleAppFinance,
                                 MontantCompte = piedeche.MontantCompte,
                                 Depence = piedeche.Depence,
                                 Ecart = piedeche.Ecart,
                                 AgentComptage = piedeche.AgentComptage,
                                 Coffre = piedeche.Coffre,
                                 CoffreId = piedeche.CoffreId,
                                 Commentaire = piedeche.Commentaire,
                                 DateCreation = piedeche.DateCreation,
                                  Datevalidation = piedeche.Datevalidation,
                                  EspeceCash = piedeche.EspeceCash, 
                                  EspeceVerse= piedeche.EspeceVerse,
                                  Id = piedeche.Id,
                                  JourcaisseId=piedeche.JourcaisseId,
                                  ModepaieId=piedeche.ModepaieId,
                                  MontantAlimenter= piedeche.MontantAlimenter,
                                  MontantTotal= piedeche.MontantTotal,  
                                    StatusComptage= piedeche.StatusComptage,
                                    MotifId= piedeche.MotifId,
                                    Source= piedeche.Source,    
                             }).Distinct().ToListAsync();
            return res;
        }

        public List<PiedecheDTO> GetAllByJoursCaisseByIdSync(int id)
        {
            var res =  (from piedeche in dbContext.Piedeche
                             join jourscaisse in dbContext.Jourscaisse on piedeche.JourcaisseId equals jourscaisse.Id
                             join modPaie in dbContext.Modepaie on piedeche.ModepaieId equals modPaie.Id



                             where jourscaisse.Id == id
                             select new PiedecheDTO
                             {
                                 Status = (from status in dbContext.Status
                                           where status.Id == piedeche.StatusComptage
                                           select new StatusDTO { Code = status.Code, Id = status.Id, Statuslibelle = status.Statuslibelle, Statuslibelleen = status.Statuslibelleen }).First(),
                                 Modepaie = (from modPaie1 in dbContext.Modepaie
                                             join devise in dbContext.Devise on modPaie1.DeviseId equals devise.Id
                                             where modPaie1.Id == piedeche.ModepaieId
                                             select new ModepaieDTO
                                             {
                                                 Id = modPaie.Id,
                                                 DeviseLabel = devise.Label,
                                                 IsCash = modPaie.IsCash,
                                                 Actif = modPaie.Actif,
                                                 DateCreation = modPaie.DateCreation,
                                                 DateModification = modPaie.DateModification,
                                                 DeviseId = modPaie.DeviseId,
                                                 LibelleAppFinance = modPaie.LibelleAppFinance,
                                                 MpLibelle = modPaie.MpLibelle,
                                                 MpModepaie = modPaie.MpModepaie,
                                                 ShowCalculette = modPaie.ShowCalculette
                                             }).First(),
                                 Montant = piedeche.Montant,
                                 GpeModepaie = modPaie.LibelleAppFinance,
                                 MontantCompte = piedeche.MontantCompte,
                                 Depence = piedeche.Depence,
                                 Ecart = piedeche.Ecart,
                                 AgentComptage = piedeche.AgentComptage,
                                 Coffre = piedeche.Coffre,
                                 CoffreId = piedeche.CoffreId,
                                 Commentaire = piedeche.Commentaire,
                                 DateCreation = piedeche.DateCreation,
                                 Datevalidation = piedeche.Datevalidation,
                                 EspeceCash = piedeche.EspeceCash,
                                 EspeceVerse = piedeche.EspeceVerse,
                                 Id = piedeche.Id,
                                 JourcaisseId = piedeche.JourcaisseId,
                                 ModepaieId = piedeche.ModepaieId,
                                 MontantAlimenter = piedeche.MontantAlimenter,
                                 MontantTotal = piedeche.MontantTotal,
                                 StatusComptage = piedeche.StatusComptage,
                                 MotifId = piedeche.MotifId,
                                 Source = piedeche.Source,
                             }).Distinct().ToList();
            return res;
        }

    }


}
