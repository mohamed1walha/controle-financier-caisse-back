using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DDD.Domain.Common;
using DDD.Domain.Interfaces;
using DDD.Infra.Data.Context;
using DTO;
using Entity;
using Microsoft.EntityFrameworkCore;

namespace DDD.Infra.Data.Repository
{
    public class LignePrelevementRepository : GenericBaseRepository<LignePrelevement, DDDContext>,
          ILignePrelevementRepository<LignePrelevement>, IDisposable
    {
        public LignePrelevementRepository(DDDContext context)
            : base(context)
        {
            dbContext = context;
        }

        public async Task<LignePrelevement> EditLigne(LignePrelevement entity)
        {
            var query =
          await (from jc in dbContext.LignePrelevement
                 where jc.Id == entity.Id
                 select jc).FirstOrDefaultAsync();

            query.ParcaisseId = entity.ParcaisseId;
            query.Montant = entity.Montant;
            query.NumZref = entity.NumZref;
            
            query.PrelevementId = entity.PrelevementId;



            try
            {
                dbContext.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }



            return query;

        }

        public async Task<List<LignePrelevementDTO>> GetAllByEnteteId(int id)
        {
            var res = await (from lignes in dbContext.LignePrelevement
                             join parcaisse in dbContext.Parcaisse on lignes.ParcaisseId equals parcaisse.Id

                             where lignes.PrelevementId == id
                             select new LignePrelevementDTO
                             {
                                 PrelevementId = lignes.PrelevementId,
                                 ParcaisseId = lignes.ParcaisseId,
                                 Parcaisse = new ParcaisseDTO
                                 {
                                    
                                     Id = parcaisse.Id,
                                     GpkCaisse = parcaisse.GpkCaisse,
                                     GpkEtablissement = parcaisse.GpkEtablissement,
                                     GpkLibelle = parcaisse.GpkLibelle,
                                   
                                     
                                 },
                                
                                 Id = lignes.Id,
                                 Montant = lignes.Montant,
                                NumZref=lignes.NumZref,


                             }
                             ).ToListAsync();

            return res;
        }

    }
}
