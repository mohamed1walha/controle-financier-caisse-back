using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DDD.Domain.Common;
using DDD.Domain.Interfaces;
using DDD.Infra.Data.Context;
using DTO;
using Entity;
using Microsoft.EntityFrameworkCore;

namespace DDD.Infra.Data.Repository
{
    public class BilletDetailComptageRepository : GenericBaseRepository<BilletDetailComptage, DDDContext>,
          IBilletDetailComptageRepository<BilletDetailComptage>, IDisposable
    {
        public BilletDetailComptageRepository(DDDContext context)
            : base(context)
        {
            dbContext = context;
        }

        public async Task<List<BilletDetailComptageDTO>> GetAllByPiedechId(int piedechId)
        {
            var res = await (from billetDetailComptage in dbContext.BilletDetailComptage
                             where billetDetailComptage.PiedecheId == piedechId


                             select new BilletDetailComptageDTO
                             {
                                 Id = billetDetailComptage.Id,
                                 BilletId = billetDetailComptage.BilletId,
                                 PiedecheId = billetDetailComptage.PiedecheId,
                                 Montant = billetDetailComptage.Montant,
                                 Quantite = billetDetailComptage.Quantite,
                                 Total = billetDetailComptage.Total


                             }).ToListAsync();
            return res;
        }


        public List<BilletDetailComptageDTO> GetAllByPiedechIdSync(int piedechId)
        {
            var res =  (from billetDetailComptage in dbContext.BilletDetailComptage
                             where billetDetailComptage.PiedecheId == piedechId


                             select new BilletDetailComptageDTO
                             {
                                 Id = billetDetailComptage.Id,
                                 BilletId = billetDetailComptage.BilletId,
                                 PiedecheId = billetDetailComptage.PiedecheId,
                                 Montant = billetDetailComptage.Montant,
                                 Quantite = billetDetailComptage.Quantite,
                                 Total = billetDetailComptage.Total


                             }).ToList();
            return res;
        }





    }
}
