using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DDD.Domain.Core.Models;
using DDD.Domain.Models;
using Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace DDD.Infra.Data.Context
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Jourscaisse> JOURSCAISSE { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Jourscaisse>(entity =>
            {
                entity.ToTable("JOURSCAISSE");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.GjcCaisse)
                    .IsRequired()
                    .HasColumnName("GJC_CAISSE")
                    .HasMaxLength(8);

                entity.Property(e => e.GjcDateferme)
                    .HasColumnName("GJC_DATEFERME")
                    .HasColumnType("datetime");

                entity.Property(e => e.GjcDateouv)
                    .HasColumnName("GJC_DATEOUV")
                    .HasColumnType("datetime");

                entity.Property(e => e.GjcEtablissement)
                    .HasColumnName("GJC_ETABLISSEMENT")
                    .HasMaxLength(6);

                entity.Property(e => e.GjcEtat)
                    .HasColumnName("GJC_ETAT")
                    .HasMaxLength(3)
                    .IsFixedLength();

                entity.Property(e => e.GjcHeureferme)
                    .HasColumnName("GJC_HEUREFERME")
                    .HasMaxLength(8);

                entity.Property(e => e.GjcHeureouv)
                    .HasColumnName("GJC_HEUREOUV")
                    .HasMaxLength(8);

                entity.Property(e => e.GjcNumzcaisse).HasColumnName("GJC_NUMZCAISSE");

                entity.Property(e => e.GjcUserferme)
                    .HasColumnName("GJC_USERFERME")
                    .HasMaxLength(3)
                    .IsFixedLength();
            });

            base.OnModelCreating(modelBuilder);
        }

        // public override int SaveChanges()
        // {
        //     OnBeforeSaving();
        //     return base.SaveChanges();
        // }

        // public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        // {
        //     OnBeforeSaving();
        //     return await base.SaveChangesAsync(cancellationToken);
        // }

        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            OnBeforeSaving();
            return base.SaveChanges(acceptAllChangesOnSuccess);
        }

        public override async Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default)
        {
            OnBeforeSaving();
            return await base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        private void OnBeforeSaving()
        {
            var entities = ChangeTracker.Entries()
                .Where(x => x.Entity is EntityAudit)
                .ToList();
            UpdateSoftDelete(entities);
            UpdateTimestamps(entities);
        }

        private void UpdateSoftDelete(List<EntityEntry> entries)
        {
            var filtered = entries
                .Where(x => x.State == EntityState.Added
                    || x.State == EntityState.Deleted);

            foreach (var entry in filtered)
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        //entry.CurrentValues["IsDeleted"] = false;
                        ((EntityAudit)entry.Entity).IsDeleted = false;
                        break;
                    case EntityState.Deleted:
                        entry.State = EntityState.Modified;
                        //entry.CurrentValues["IsDeleted"] = true;
                        ((EntityAudit)entry.Entity).IsDeleted = true;
                        break;
                }
            }
        }

        private void UpdateTimestamps(List<EntityEntry> entries)
        {
            var filtered = entries
                .Where(x => x.State == EntityState.Added
                    || x.State == EntityState.Modified);

            // TODO: Get real current user id
            var currentUserId = 1;

            foreach (var entry in filtered)
            {
                if (entry.State == EntityState.Added)
                {
                    ((EntityAudit)entry.Entity).CreatedAt = DateTime.UtcNow;
                    ((EntityAudit)entry.Entity).CreatedBy = currentUserId;
                }

                ((EntityAudit)entry.Entity).UpdatedAt = DateTime.UtcNow;
                ((EntityAudit)entry.Entity).UpdatedBy = currentUserId;
            }
        }
    }
}
