using DDD.Application.BaseServices;
using DDD.Application.Interfaces;
using DDD.Application.Services;
using DTO;
using DDD.Domain.Interfaces;
using DDD.Domain.Services;
using DDD.Infra.Data.Repository;
using DDD.Infra.Data.UoW;
using Entity;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace DDD.Infra.CrossCutting.IoC
{
    public class NativeInjectorBootStrapper
    {
        public static void RegisterServices(IServiceCollection services)
        {
            services.AddTransient<IHttpContextAccessor, HttpContextAccessor>();

            services.AddTransient<HttpContext>(p => p.GetService<IHttpContextAccessor>()?.HttpContext);

            services.AddDbContext<DDDContext>();

            // ASP.NET HttpContext dependency
            services.AddHttpContextAccessor();


            // Application
            services.AddScoped<IJoursCaisseService<JoursCaiseDTO>, JoursCaisseService<JoursCaiseDTO, Jourscaisse, DDDContext>>();
            services.AddScoped<IModePaiementService<ModepaieDTO>, ModePaiementService<ModepaieDTO, Modepaie, DDDContext>>();
            services.AddScoped<IPiedecheService<PiedecheDTO>, PiedecheService<PiedecheDTO, Piedeche, DDDContext>>();
            services.AddScoped<IEtabService<EtablissementDTO>, EtabService<EtablissementDTO, Etablissement, DDDContext>>();
            services.AddScoped<IBanqueService<BanqueDTO>, BanqueService<BanqueDTO, Banque, DDDContext>>();
            services.AddScoped<ITypeFraisService<TypeFraisDTO>, TypeFraisService<TypeFraisDTO, Typefrais, DDDContext>>();
            services.AddScoped<IStatusService<StatusDTO>, StatusService<StatusDTO, Status, DDDContext>>();
            services.AddScoped<IEmetteurService<EmetteurDTO>, EmetteurService<EmetteurDTO, Emetteur, DDDContext>>();
            services.AddScoped<IChauffeurService<ChauffeurDTO>, ChauffeurService<ChauffeurDTO, Chauffeur, DDDContext>>();
            services.AddScoped<IMotifresyncService<MotifresyncDTO>, MotifresyncService<MotifresyncDTO, Motifresync, DDDContext>>();
            services.AddScoped<IDepenseService<DepenseDTO>, DepenseService<DepenseDTO, Depense, DDDContext>>();
            services.AddScoped<IMotifService<MotifDTO>, MotifService<MotifDTO, Motif, DDDContext>>();
            services.AddScoped<ILastSyncAppService<LastSyncAppDTO>, LastSyncAppService<LastSyncAppDTO, Lastsyncapp, DDDContext>>();
            services.AddScoped<IUserService<UserDTO>, UserService<UserDTO, User, DDDContext>>();
            services.AddScoped<IUserRolesService<UserRolesDTO>, UserRolesService<UserRolesDTO, UserRoles, DDDContext>>();
            services.AddScoped<IUserModePaieService<UserModePaieDTO>, UserModePaieService<UserModePaieDTO, UserModePaie, DDDContext>>();
            services.AddScoped<IRolesService<RolesDTO>, RolesService<RolesDTO, Roles, DDDContext>>();
            services.AddScoped<ICoffreService<CoffreDTO>, CoffreService<CoffreDTO, Coffre, DDDContext>>();
            services.AddScoped<ICoffreAlimentationService<CoffrealimentationDTO>, CoffreAlimentationService<CoffrealimentationDTO, Coffrealimentation, DDDContext>>();
            services.AddScoped<ICategorieModePaieCoffreService<CategorieModePaieCoffreDTO>, CategorieModePaieCoffreService<CategorieModePaieCoffreDTO, CategorieModepaieCoffre, DDDContext>>();
            services.AddScoped<ICoffreSortieService<CoffresortieDTO>, CoffreSortieService<CoffresortieDTO, Coffresortie, DDDContext>>();
            services.AddScoped<IDeviseService<DeviseDTO>, DeviseService<DeviseDTO, Devise, DDDContext>>();
            services.AddScoped<IBilletDetailSortieService<BilletDetailSortieDTO>, BilletDetailSortieService<BilletDetailSortieDTO, BilletDetailSortie, DDDContext>>();
            services.AddScoped<IBilletDetailComptageService<BilletDetailComptageDTO>, BilletDetailComptageService<BilletDetailComptageDTO, BilletDetailComptage, DDDContext>>();
            services.AddScoped<IBilletDetailAliExcepService<BilletDetailAliExcepDTO>, BilletDetailAliExcepService<BilletDetailAliExcepDTO, BilletDetailAliExcep, DDDContext>>();
            services.AddScoped<IParamBilletService<ParamBilletDTO>, ParamBilletService<ParamBilletDTO, ParamBillet, DDDContext>>();

            services.AddScoped<IArticleService<ArticleDTO>, ArticleService<ArticleDTO, Article, DDDContext>>();
            services.AddScoped<ICompteDepenseService<CompteDepenseDTO>, CompteDepenseService<CompteDepenseDTO, CompteDepense, DDDContext>>();
            services.AddScoped<IEnteteDepenseService<EnteteDepenseDTO>, EnteteDepenseService<EnteteDepenseDTO, EnteteDepense, DDDContext>>();
            services.AddScoped<IEntetePrelevementService<EntetePrelevementDTO>, EntetePrelevementService<EntetePrelevementDTO, EntetePrelevement, DDDContext>>();
            services.AddScoped<IFournisseurService<FournisseurDTO>, FournisseurService<FournisseurDTO, Fournisseur, DDDContext>>();
            services.AddScoped<IJustifsPrelevementService<JustifsPrelevementDTO>, JustifsPrelevementService<JustifsPrelevementDTO, JustifsPrelevement, DDDContext>>();
            services.AddScoped<ILigneDepenseService<LigneDepenseDTO>, LigneDepenseService<LigneDepenseDTO, LigneDepense, DDDContext>>();
            services.AddScoped<ILignePrelevementService<LignePrelevementDTO>, LignePrelevementService<LignePrelevementDTO, LignePrelevement, DDDContext>>();
            services.AddScoped<IMailDestinataireService<MailDestinataireDTO>, MailDestinataireService<MailDestinataireDTO, MailDestinataire, DDDContext>>();
            services.AddScoped<IParamMyCashService<ParamMyCashDTO>, ParamMyCashService<ParamMyCashDTO, ParamMyCash, DDDContext>>();
            services.AddScoped<IParcaisseService<ParcaisseDTO>, ParcaisseService<ParcaisseDTO, Parcaisse, DDDContext>>();


            services.AddScoped<ICurrentContextProvider, CurrentContextProvider>();



            // Infra - Data
            services.AddScoped<IJoursCaisseRepository<Jourscaisse>, JoursCaiseRepository>();
            services.AddScoped<IEtabRepository<Etablissement>, EtabRepository>();
            services.AddScoped<IPiedecheRepository<Piedeche>, PiedechRepository>();
            services.AddScoped<IModePaiementRepository<Modepaie>, ModePaiementRepository>();
            services.AddScoped<ILastsyncappRepository<Lastsyncapp>, LastsyncappRepository>();
            services.AddScoped<IDepenseRepository<Depense>, DepenseRepository>();
            services.AddScoped<IChauffeurRepository<Chauffeur>, ChauffeurRepository>();
            services.AddScoped<IMotifresyncRepository<Motifresync>, MotifresyncRepository>();
            services.AddScoped<IMotifRepository<Motif>, MotifRepository>();
            services.AddScoped<IEmetteurRepository<Emetteur>, EmetteurRepository>();
            services.AddScoped<IStatusRepository<Status>, StatusRepository>();
            services.AddScoped<IBanqueRepository<Banque>, BanqueRepository>();
            services.AddScoped<ITypeFraisRepository<Typefrais>, TypeFraisRepository>();
            services.AddScoped<IUserRepository<User>, UserRepository>();
            services.AddScoped<IParcaisseRepository<Parcaisse>, ParcaisseRepository>();
            services.AddScoped<IUserRolesRepository<UserRoles>, UserRolesRepository>();
            services.AddScoped<IUserModePaieRepository<UserModePaie>, UserModePaieRepository>();
            services.AddScoped<IRolesRepository<Roles>, RolesRepository>();
            services.AddScoped<ICoffreRepository<Coffre>, CoffreRepository>();
            services.AddScoped<ICoffreAlimentationRepository<Coffrealimentation>, CoffreAlimentationRepository>();
            services.AddScoped<ICategorieModePaieCoffreRepository<CategorieModepaieCoffre>, CategorieModePaieCoffreRepository>();
            services.AddScoped<ICoffreSortieRepository<Coffresortie>, CoffreSortieRepository>();
            services.AddScoped<IDeviseRepository<Devise>, DeviseRepository>();
            services.AddScoped<IBilletDetailSortieRepository<BilletDetailSortie>, BilletDetailSortieRepository>();
            services.AddScoped<IBilletDetailComptageRepository<BilletDetailComptage>, BilletDetailComptageRepository>();
            services.AddScoped<IBilletDetailAliExcepRepository<BilletDetailAliExcep>, BilletDetailAliExcepRepository>();
            services.AddScoped<IParamBilletRepository<ParamBillet>, ParamBilletRepository>();

            services.AddScoped<IArticleRepository<Article>, ArticleRepository>();
            services.AddScoped<ICompteDepenseRepository<CompteDepense>, CompteDepenseRepository>();
            services.AddScoped<IEnteteDepenseRepository<EnteteDepense>, EnteteDepenseRepository>();
            services.AddScoped<IEntetePrelevementRepository<EntetePrelevement>, EntetePrelevementRepository>();
            services.AddScoped<IFournisseurRepository<Fournisseur>, FournisseurRepository>();
            services.AddScoped<IJustifsPrelevementRepository<JustifsPrelevement>, JustifsPrelevementRepository>();
            services.AddScoped<ILigneDepenseRepository<LigneDepense>, LigneDepenseRepository>();
            services.AddScoped<ILignePrelevementRepository<LignePrelevement>, LignePrelevementRepository>();
            services.AddScoped<IMailDestinataireRepository<MailDestinataire>, MailDestinataireRepository>();
            services.AddScoped<IParamMyCashRepository<ParamMyCash>, ParamMyCashRepository>();



            services.AddScoped<IUnitOfWork, UnitOfWork>();






        }
    }
}
