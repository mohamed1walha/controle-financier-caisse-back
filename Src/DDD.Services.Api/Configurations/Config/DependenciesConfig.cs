﻿
using Microsoft.Extensions.DependencyInjection;

using AU;
using AU.API;
using AU.API.Config;
using AU.API.Identity;

using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;

using DDD.Application.Interfaces;
using DDD.Infra.CrossCutting.IoC;
using Entity;
using DDD.Application.BaseServices;

namespace AU.API.Config
{
    public class DependenciesConfig
    {
        public static void ConfigureDependencies(IServiceCollection services)
        {
            services.AddMemoryCache();
         
            services.AddScoped<ICurrentContextProvider, CurrentContextProvider>();
            
            services.AddSingleton<JwtManager>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddHttpContextAccessor();
            NativeInjectorBootStrapper.RegisterServices(services);
            services.AddDbContext<DDDContext>();
            services.AddIdentity<IdentityUser, IdentityRole>()
                            .AddEntityFrameworkStores<DDDContext>()
                            .AddDefaultTokenProviders();
        }
    }
}
