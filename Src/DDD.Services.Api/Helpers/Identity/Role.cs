﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace AU.API.Identity
{
    public static class Role
    {
        public static List<string> UserRoles(AuthorizationFilterContext context)
        {
            List<string> res = new List<string>();
            if (context.HttpContext.User == null)
                return res;

            var userIdentity = (ClaimsIdentity)context.HttpContext.User.Identity;
            if (userIdentity == null)
                return res;

            return(userIdentity.Claims
                .Where(c => c.Type == userIdentity.RoleClaimType)
                .Select(p=>p.Value)
                .ToList());
        }
    }
}
