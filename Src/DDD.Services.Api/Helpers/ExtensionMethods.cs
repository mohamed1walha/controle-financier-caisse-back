using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DTO;
using Entity;
using Microsoft.AspNetCore.Http;

namespace Helpers
{
    public static class ExtensionMethods
    {
        public static List<UserDTO> WithoutPasswords(this List<UserDTO> users)
        {
            List<UserDTO> userDTOs = new List<UserDTO>();
            foreach (UserDTO userDTO in users)
            {
                userDTO.Password = null;
                userDTOs.Add(userDTO);
            }
            return userDTOs;
        }

        public static UserDTO WithoutPassword(this UserDTO user)
        {
            user.Password = null;
            return user;
        }



        public static string HashSHA256(this string value)
        {
            var HashSHA256 = System.Security.Cryptography.SHA256.Create();
            var inputBytes = Encoding.ASCII.GetBytes(value + "MySecretKeyForJWTTokenEncryption");
            var hash = HashSHA256.ComputeHash(inputBytes);
            var sb = new StringBuilder();
            for (var i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }
    }
}