﻿using DTO;
using DDD.Domain.Interfaces;
using Entity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net.Mail;
using DDD.Application.utils;
using System;
using DDD.Application.Services;
using Newtonsoft.Json;

namespace DDD.Services.Api.Controllers
{
    [Authorize]
    [ApiController]
    public class EnteteDepenseController : GenericController
    {
        private readonly IEnteteDepenseService<EnteteDepenseDTO> _EnteteDepenseService;

        public EnteteDepenseController(
             IEnteteDepenseService<EnteteDepenseDTO> EnteteDepenseService)
        {
            _EnteteDepenseService = EnteteDepenseService;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/EnteteDepense/GetAll")]
        public async Task<List<EnteteDepenseDTO>> GetAll()
        {
            return await _EnteteDepenseService.GetAllEntete();
        }
        [HttpGet]
        [AllowAnonymous]
        [Route("/EnteteDepense/GetAllByEtablissementIdAndUserId/{etablissementId}/{userId}")]
        public async Task<List<EnteteDepenseDTO>> GetAllByEtablissementIdAndUserId(int etablissementId ,int userId)
        {
            return await _EnteteDepenseService.GetAllByEtablissementIdAndUserId(etablissementId,userId);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/EnteteDepense/GetAllByValidatorId/{userId}")]
        public async Task<List<EnteteDepenseDTO>> GetAllByValidatorId( int userId)
        {
            return await _EnteteDepenseService.GetAllByValidatorId(userId);
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("/EnteteDepense/Create")]
        public async Task<EnteteDepenseDTO> Create(EnteteDepenseDTO EnteteDepenseDTO)
        {
            EnteteDepenseDTO.CreateDate=DateTime.Now;   
            return await _EnteteDepenseService.CreateEnteteDepense(EnteteDepenseDTO);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/EnteteDepense/GetById")]
        public async Task<EnteteDepenseDTO> GetById(int id)
        {
            //try
            //{
            //    MailMessage mail = new MailMessage();

            //    mail.From = new MailAddress("amirzouari25@gmail.com");
            //    mail.To.Add("amirzouari22@gmail.com");
            //    mail.Subject = "Test Mail";
            //    mail.Body = "This is a test email";
            //    mail.CC.Add("amir.zouari@live.fr");


            //    Mail.SendMail(mail);

            //}
            //catch (Exception ex)
            //{
            //}
            return await _EnteteDepenseService.GetFullEnteteDepenseById(id);
        }
        [HttpGet]
        [AllowAnonymous]
        [Route("/EnteteDepense/GetAllByEtabIdCaisseIdNumZRef")]
        public async Task<List<EnteteDepenseDTO>> GetAllByEtabIdCaisseIdNumZRef(int etabId, string caisseId, int? numZRef)
        {
            return await _EnteteDepenseService.GetAllByEtabIdCaisseIdNumZRef(etabId, caisseId, numZRef);
        }
        [HttpGet]
        [AllowAnonymous]
        [Route("/EnteteDepense/downloadFile/{id}")]
        public async Task<FileStreamResult> downloadFile(int id)
        {
            var res = await _EnteteDepenseService.downloadFile(id);
            return res;
        } 
        [HttpGet]
        [AllowAnonymous]
        [Route("/EnteteDepense/deleteFile/{id}")]
        public async Task<EnteteDepenseDTO> deleteFile(int id)
        {
            var res = await _EnteteDepenseService.deleteFile(id);
            return res;
        }

        [HttpPut]
        [AllowAnonymous]
        [Route("/EnteteDepense/Update")]
        public async Task<EnteteDepenseDTO> Update(EnteteDepenseDTO EnteteDepenseDTO)
        {
            return await _EnteteDepenseService.Update(EnteteDepenseDTO);
        }

        [HttpDelete]
        [AllowAnonymous]
        [Route("/EnteteDepense/Delete")]
        public async Task<int> Delete(int id)
        {
            return await _EnteteDepenseService.Delete(id);
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("/EnteteDepense/GenerateComptable")]
        public async Task<OperationResult> GenerateComptable(List<EnteteDepenseDTO> depensetList)
        {
            List<string> data = await _EnteteDepenseService.GenerateComptable(depensetList);
            OperationResult operationResult = new OperationResult();
            operationResult.Data = data;
            return operationResult;
        }
        [HttpPost]
        [AllowAnonymous]
        [Route("/EnteteDepense/GetAllLazy")]
        public async Task<PagedResults> GetAllLazy(GridStateDTO grid)
        {
            FilterDepancesDTO filterObject = JsonConvert.DeserializeObject<FilterDepancesDTO>(grid.Filters.ToString());
            return await _EnteteDepenseService.GetAllEnteteLazy(grid, filterObject);
        }
        [HttpGet]
        [AllowAnonymous]
        [Route("/EnteteDepense/FiltersModel")]
        public async Task<FilterDepancesDTO> GetAllFilters()
        {
            throw new NotImplementedException();
        }
    }
}
