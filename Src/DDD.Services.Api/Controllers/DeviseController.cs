﻿using DTO;
using DDD.Domain.Interfaces;
using Entity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DDD.Services.Api.Controllers
{
    [Authorize]
    [ApiController]
    public class DeviseController : GenericController
    {
        private readonly IDeviseService<DeviseDTO> _deviseService;

        public DeviseController(
             IDeviseService<DeviseDTO> DeviseService)
        {
            _deviseService = DeviseService;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/Devise/GetAll")]
        public async Task<List<DeviseDTO>> GetAll()
        {
            return await _deviseService.GetAll();
        }   
        [HttpGet]
        [AllowAnonymous]
        [Route("/Devise/GetAllByCoffre")]
        public async Task<List<DeviseDTO>> GetAllByCoffreID(int value)
        {
            return await _deviseService.GetAllByCoffreID(value);
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("/Devise/Create")]
        public async Task<DeviseDTO> Create(DeviseDTO DeviseDTO)
        {
            return await _deviseService.Create(DeviseDTO);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/Devise/GetById")]
        public async Task<DeviseDTO> GetById(int id)
        {
            return await _deviseService.GetById(id);
        }

        [HttpPut]
        [AllowAnonymous]
        [Route("/Devise/Update")]
        public async Task<DeviseDTO> Update(DeviseDTO DeviseDTO)
        {
            return await _deviseService.Update(DeviseDTO);
        }

        [HttpDelete]
        [AllowAnonymous]
        [Route("/Devise/Delete")]
        public async Task<int> Delete(int id)
        {
            return await _deviseService.Delete(id);
        }

    }
}
