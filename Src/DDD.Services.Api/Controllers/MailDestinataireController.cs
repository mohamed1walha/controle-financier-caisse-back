﻿using DTO;
using DDD.Domain.Interfaces;
using Entity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DDD.Services.Api.Controllers
{
    [Authorize]
    [ApiController]
    public class MailDestinataireController : GenericController
    {
        private readonly IMailDestinataireService<MailDestinataireDTO> _MailDestinataireService;

        public MailDestinataireController(
             IMailDestinataireService<MailDestinataireDTO> MailDestinataireService)
        {
            _MailDestinataireService = MailDestinataireService;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/MailDestinataire/GetAll")]
        public async Task<List<MailDestinataireDTO>> GetAll()
        {
            return await _MailDestinataireService.GetAll();
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("/MailDestinataire/Create")]
        public async Task<MailDestinataireDTO> Create(MailDestinataireDTO MailDestinataireDTO)
        {
            return await _MailDestinataireService.Create(MailDestinataireDTO);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/MailDestinataire/GetById")]
        public async Task<MailDestinataireDTO> GetById(int id)
        {
            return await _MailDestinataireService.GetById(id);
        }

        [HttpPut]
        [AllowAnonymous]
        [Route("/MailDestinataire/Update")]
        public async Task<MailDestinataireDTO> Update(MailDestinataireDTO MailDestinataireDTO)
        {
            return await _MailDestinataireService.Update(MailDestinataireDTO);
        }

        [HttpDelete]
        [AllowAnonymous]
        [Route("/MailDestinataire/Delete")]
        public async Task<int> Delete(int id)
        {
            return await _MailDestinataireService.Delete(id);
        }

    }
}
