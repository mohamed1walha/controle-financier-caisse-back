﻿using DTO;
using DDD.Domain.Interfaces;
using Entity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DDD.Services.Api.Controllers
{
    [Authorize]
    [ApiController]
    public class ParamBilletController : GenericController
    {
        private readonly IParamBilletService<ParamBilletDTO> _ParamBilletService;

        public ParamBilletController(
             IParamBilletService<ParamBilletDTO> ParamBilletService)
        {
            _ParamBilletService = ParamBilletService;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/ParamBillet/GetAll")]
        public List<ParamBilletDTO> GetAll()
        {
            return _ParamBilletService.GetAllParamBillet();
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("/ParamBillet/Create")]
        public async Task<ParamBilletDTO> Create(ParamBilletDTO ParamBilletDTO)
        {
            return await _ParamBilletService.Create(ParamBilletDTO);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/ParamBillet/GetById")]
        public async Task<ParamBilletDTO> GetById(int id)
        {
            return await _ParamBilletService.GetById(id);
        }

        [HttpPut]
        [AllowAnonymous]
        [Route("/ParamBillet/Update")]
        public async Task<ParamBilletDTO> Update(ParamBilletDTO ParamBilletDTO)
        {
            return await _ParamBilletService.Update(ParamBilletDTO);
        }

        [HttpDelete]
        [AllowAnonymous]
        [Route("/ParamBillet/Delete")]
        public async Task<int> Delete(int id)
        {
            return await _ParamBilletService.Delete(id);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/ParamBillet/GetAllTypeBillet")]
        public List<TypeBilletDTO> GetAllTypeBillet()
        {
            return _ParamBilletService.GetAllTypeBillet();
        }

    }
}
