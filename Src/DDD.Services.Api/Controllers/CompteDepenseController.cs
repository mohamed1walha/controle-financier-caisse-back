﻿using DTO;
using DDD.Domain.Interfaces;
using Entity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DDD.Services.Api.Controllers
{
    [Authorize]
    [ApiController]
    public class CompteDepenseController : GenericController
    {
        private readonly ICompteDepenseService<CompteDepenseDTO> _CompteDepenseService;

        public CompteDepenseController(
             ICompteDepenseService<CompteDepenseDTO> CompteDepenseService)
        {
            _CompteDepenseService = CompteDepenseService;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/CompteDepense/GetAll")]
        public async Task<List<CompteDepenseDTO>> GetAll()
        {
            return await _CompteDepenseService.GetAllCompteDepense();
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("/CompteDepense/Create")]
        public async Task<CompteDepenseDTO> Create(CompteDepenseDTO CompteDepenseDTO)
        {
            return await _CompteDepenseService.Create(CompteDepenseDTO);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/CompteDepense/GetById")]
        public async Task<CompteDepenseDTO> GetById(int id)
        {
            return await _CompteDepenseService.GetById(id);
        }
             [HttpGet]
        [AllowAnonymous]
        [Route("/CompteDepense/GetByEtabId/{etabId}")]
        public async Task<CompteDepenseDTO> GetByEtabId(int etabId)
        {
            return await _CompteDepenseService.GetByEtabId(etabId);
        }

        [HttpPut]
        [AllowAnonymous]
        [Route("/CompteDepense/Update")]
        public async Task<CompteDepenseDTO> Update(CompteDepenseDTO CompteDepenseDTO)
        {
            return await _CompteDepenseService.Update(CompteDepenseDTO);
        }

        [HttpDelete]
        [AllowAnonymous]
        [Route("/CompteDepense/Delete")]
        public async Task<int> Delete(int id)
        {
            return await _CompteDepenseService.Delete(id);
        }

    }
}
