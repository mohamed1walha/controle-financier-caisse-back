﻿using DTO;
using DDD.Domain.Interfaces;
using Entity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DDD.Services.Api.Controllers
{
    [Authorize]
    [ApiController]
    public class TypeFraisController : GenericController
    {
        private readonly ITypeFraisService<TypeFraisDTO> _TypeFraisService;

        public TypeFraisController(
             ITypeFraisService<TypeFraisDTO> TypeFraisService)
        {
            _TypeFraisService = TypeFraisService;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/TypeFrais/GetAll")]
        public async Task<List<TypeFraisDTO>> GetAll()
        {
            return await _TypeFraisService.GetAll();
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("/TypeFrais/Create")]
        public async Task<TypeFraisDTO> Create(TypeFraisDTO TypeFraisDTO)
        {
            return await _TypeFraisService.Create(TypeFraisDTO);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/TypeFrais/GetById")]
        public async Task<TypeFraisDTO> GetById(int id)
        {
            return await _TypeFraisService.GetById(id);
        }

        [HttpPut]
        [AllowAnonymous]
        [Route("/TypeFrais/Update")]
        public async Task<TypeFraisDTO> Update(TypeFraisDTO TypeFraisDTO)
        {
            return await _TypeFraisService.Update(TypeFraisDTO);
        }

        [HttpDelete]
        [AllowAnonymous]
        [Route("/TypeFrais/Delete")]
        public async Task<int> Delete(int id)
        {
            return await _TypeFraisService.Delete(id);
        }

    }
}
