﻿using DTO;
using DDD.Domain.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DDD.Services.Api.Controllers
{
    [Authorize]
    [ApiController]
    public class LastSyncAppController : GenericController
    {
        private readonly ILastSyncAppService<LastSyncAppDTO> _LastSyncAppService;

        public LastSyncAppController(
             ILastSyncAppService<LastSyncAppDTO> LastSyncAppService)
        {
            _LastSyncAppService = LastSyncAppService;
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("/LastSyncApp/GetAll")]
        public async Task<List<LastSyncAppDTO>> Get()
        {
            return await _LastSyncAppService.GetAll();
        }
        [HttpGet]
        [AllowAnonymous]
        [Route("LastSyncApp/LastSyncModePaie")]
        public async Task<DateTime> LastSyncModePaie()
        {
            return await _LastSyncAppService.LastSyncModePaie();
        }


    }
}
