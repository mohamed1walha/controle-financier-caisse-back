﻿using DTO;
using DDD.Domain.Interfaces;
using Entity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DDD.Services.Api.Controllers
{
    [Authorize]
    [ApiController]
    public class LigneDepenseController : GenericController
    {
        private readonly ILigneDepenseService<LigneDepenseDTO> _LigneDepenseService;

        public LigneDepenseController(
             ILigneDepenseService<LigneDepenseDTO> LigneDepenseService)
        {
            _LigneDepenseService = LigneDepenseService;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/LigneDepense/GetAll")]
        public async Task<List<LigneDepenseDTO>> GetAll()
        {
            return await _LigneDepenseService.GetAll();
        }
        [HttpGet]
        [AllowAnonymous]
        [Route("/LigneDepense/GetByEnteteId")]
        public async Task<List<LigneDepenseDTO>> GetByEnteteId(int id)
        {
            return await _LigneDepenseService.GetAllByEnteteId(id);
        }
        [HttpPost]
        [AllowAnonymous]
        [Route("/LigneDepense/Create")]
        public async Task<LigneDepenseDTO> Create(LigneDepenseDTO LigneDepenseDTO)
        {
            return await _LigneDepenseService.Create(LigneDepenseDTO);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/LigneDepense/GetById")]
        public async Task<LigneDepenseDTO> GetById(int id)
        {
            return await _LigneDepenseService.GetById(id);
        }

        [HttpPut]
        [AllowAnonymous]
        [Route("/LigneDepense/Update")]
        public async Task<LigneDepenseDTO> Update(LigneDepenseDTO LigneDepenseDTO)
        {
            return await _LigneDepenseService.Update(LigneDepenseDTO);
        }

        [HttpDelete]
        [AllowAnonymous]
        [Route("/LigneDepense/Delete")]
        public async Task<int> Delete(int id)
        {
            return await _LigneDepenseService.Delete(id);
        }

    }
}
