﻿using DTO;
using DDD.Domain.Interfaces;
using Entity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DDD.Services.Api.Controllers
{
    [Authorize]
    [ApiController]
    public class BilletDetailAliExcepController : GenericController
    {
        private readonly IBilletDetailAliExcepService<BilletDetailAliExcepDTO> _BilletDetailAliExcepService;

        public BilletDetailAliExcepController(
             IBilletDetailAliExcepService<BilletDetailAliExcepDTO> BilletDetailAliExcepService)
        {
            _BilletDetailAliExcepService = BilletDetailAliExcepService;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/BilletDetailAliExcep/GetAll")]
        public async Task<List<BilletDetailAliExcepDTO>> GetAll()
        {
            return await _BilletDetailAliExcepService.GetAll();
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("/BilletDetailAliExcep/Create")]
        public async Task<BilletDetailAliExcepDTO> CreateList(BilletDetailAliExcepDTO BilletDetailAliExcepDTO)
        {
            return await _BilletDetailAliExcepService.Create(BilletDetailAliExcepDTO);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/BilletDetailAliExcep/GetById")]
        public async Task<BilletDetailAliExcepDTO> GetById(int id)
        {
            return await _BilletDetailAliExcepService.GetById(id);
        }

        [HttpPut]
        [AllowAnonymous]
        [Route("/BilletDetailAliExcep/Update")]
        public async Task<BilletDetailAliExcepDTO> Update(BilletDetailAliExcepDTO BilletDetailAliExcepDTO)
        {
            return await _BilletDetailAliExcepService.Update(BilletDetailAliExcepDTO);
        }

        [HttpDelete]
        [AllowAnonymous]
        [Route("/BilletDetailAliExcep/Delete")]
        public async Task<int> Delete(int id)
        {
            return await _BilletDetailAliExcepService.Delete(id);
        }

    }
}
