﻿using DTO;
using DDD.Domain.Interfaces;
using Entity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DDD.Services.Api.Controllers
{
    [Authorize]
    [ApiController]
    public class CoffreAlimentationController : GenericController
    {
        private readonly ICoffreAlimentationService<CoffrealimentationDTO> _coffreAlimentationService;
        private readonly IBilletDetailAliExcepService<BilletDetailAliExcepDTO> _BilletDetailAliExcepService;

        public CoffreAlimentationController(
             ICoffreAlimentationService<CoffrealimentationDTO> coffreAlimentationService, IBilletDetailAliExcepService<BilletDetailAliExcepDTO> billetDetailAliExcepService)
        {
            _coffreAlimentationService = coffreAlimentationService;
            _BilletDetailAliExcepService = billetDetailAliExcepService;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/CoffreAlimentation/GetAll")]
        public async Task<List<CoffrealimentationDTO>> GetAll()
        {
            return await _coffreAlimentationService.GetAllCoffreAlimentation();
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("/CoffreAlimentation/Create")]
        public async Task<CoffrealimentationDTO> Create(CoffrealimentationEntryDTO coffrealimentationDTOs)
        {
            return await _coffreAlimentationService.CreateCoffreAlimentation(coffrealimentationDTOs);
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("/CoffreAlimentation/CreateExp")]
        public async Task<CoffrealimentationDTO> CreateExp(CoffrealimentationDTO coffrealimentationDTO)
        {
            coffrealimentationDTO.AlimentationDate = System.DateTime.Parse(coffrealimentationDTO.AlimentationDate.ToString()).AddHours(1);
            var res= await _coffreAlimentationService.Create(coffrealimentationDTO);
            if (res != null && coffrealimentationDTO.billetDetailAliExcepDTOs != null)
            {
                coffrealimentationDTO.billetDetailAliExcepDTOs.ForEach(async ligne =>
                {
                    ligne.AlimentationId = res.Id;
                });
                await _BilletDetailAliExcepService.CreateList(coffrealimentationDTO.billetDetailAliExcepDTOs);
            }
            return res;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/CoffreAlimentation/GetById")]
        public async Task<CoffrealimentationDTO> GetById(int id)
        {
            return await _coffreAlimentationService.GetById(id);
        }

        [HttpPut]
        [AllowAnonymous]
        [Route("/CoffreAlimentation/Update")]
        public async Task<CoffrealimentationDTO> Update(CoffrealimentationDTO coffrealimentationDTO)
        {
            return await _coffreAlimentationService.Update(coffrealimentationDTO);
        }

        [HttpDelete]
        [AllowAnonymous]
        [Route("/CoffreAlimentation/Delete")]
        public async Task<int?> Delete(int id)
        {

            return await _coffreAlimentationService.DeleteAlimentation(id);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/CoffreAlimentation/GetByCoffreID/{coffreId}")]
        public async Task<List<CoffrealimentationDTO>> GetByCoffreId(int coffreId)
        {
            var res = await _coffreAlimentationService.GetByCoffreId(coffreId);
            return res;
        }

    }
}
