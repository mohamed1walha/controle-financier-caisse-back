﻿using DTO;
using DDD.Domain.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DDD.Services.Api.Controllers
{
    [Authorize]
    [ApiController]
    public class DepenseController : GenericController
    {
        private readonly IDepenseService<DepenseDTO> _DepenseService;

        public DepenseController(
             IDepenseService<DepenseDTO> DepenseService)
        {
            _DepenseService = DepenseService;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/Depense/GetAll")]
        public async Task<List<DepenseDTO>> GetAll()
        {
            return await _DepenseService.GetAll();
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("/Depense/Create")]
        public async Task<DepenseDTO> Create(DepenseDTO depenseDTO)
        {
            return await _DepenseService.Create(depenseDTO);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/Depense/GetById")]
        public async Task<DepenseDTO> GetById(int id)
        {
            return await _DepenseService.GetById(id);
        }

        [HttpPut]
        [AllowAnonymous]
        [Route("/Depense/Update")]
        public async Task<DepenseDTO> Update(DepenseDTO depenseDTO)
        {
            return await _DepenseService.Update(depenseDTO);
        }

        [HttpDelete]
        [AllowAnonymous]
        [Route("/Depense/Delete")]
        public async Task<int> Delete(int id)
        {
            return await _DepenseService.Delete(id);
        }

    }
}
