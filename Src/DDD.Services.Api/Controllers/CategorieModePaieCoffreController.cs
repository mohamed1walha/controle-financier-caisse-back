﻿using DTO;
using DDD.Domain.Interfaces;
using Entity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DDD.Services.Api.Controllers
{
    [Authorize]
    [ApiController]
    public class CategorieModePaieCoffreController : GenericController
    {
        private readonly ICategorieModePaieCoffreService<CategorieModePaieCoffreDTO> _categorieModePaieCoffreService;

        public CategorieModePaieCoffreController(
             ICategorieModePaieCoffreService<CategorieModePaieCoffreDTO> categorieModePaieCoffreService)
        {
            _categorieModePaieCoffreService = categorieModePaieCoffreService;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/CategorieModePaieCoffre/GetAll")]
        public async Task<List<CategorieModePaieCoffreDTO>> GetAll()
        {
            return await _categorieModePaieCoffreService.GetAll();
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("/CategorieModePaieCoffre/Create")]
        public async Task<CategorieModePaieCoffreDTO> Create(CategorieModePaieCoffreDTO CategorieModePaieCoffreDTO)
        {
            return await _categorieModePaieCoffreService.Create(CategorieModePaieCoffreDTO);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/CategorieModePaieCoffre/GetById")]
        public async Task<CategorieModePaieCoffreDTO> GetById(int id)
        {
            return await _categorieModePaieCoffreService.GetById(id);
        }

        [HttpPut]
        [AllowAnonymous]
        [Route("/CategorieModePaieCoffre/Update")]
        public async Task<CategorieModePaieCoffreDTO> Update(CategorieModePaieCoffreDTO CategorieModePaieCoffreDTO)
        {
            return await _categorieModePaieCoffreService.Update(CategorieModePaieCoffreDTO);
        }

        [HttpDelete]
        [AllowAnonymous]
        [Route("/CategorieModePaieCoffre/Delete")]
        public async Task<int> Delete(int id)
        {
            return await _categorieModePaieCoffreService.Delete(id);
        }

    }
}
