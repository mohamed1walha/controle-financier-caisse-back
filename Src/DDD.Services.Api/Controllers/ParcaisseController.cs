﻿using DTO;
using DDD.Domain.Interfaces;
using Entity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DDD.Services.Api.Controllers
{
    [Authorize]
    [ApiController]
    public class ParcaisseController : GenericController
    {
        private readonly IParcaisseService<ParcaisseDTO> _ParcaisseService;

        public ParcaisseController(
             IParcaisseService<ParcaisseDTO> ParcaisseService)
        {
            _ParcaisseService = ParcaisseService;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/Parcaisse/GetAll")]
        public async Task<List<ParcaisseDTO>> GetAll()
        {
            return await _ParcaisseService.GetAll();
        }    
        [HttpGet]
        [AllowAnonymous]
        [Route("/Parcaisse/GetAllByEtabId/{etabId}")]
        public async Task<List<ParcaisseDTO>> GetAllByEtabId(string etabId)
        {
            return await _ParcaisseService.GetAllByEtabId(etabId);
        }
        [HttpPost]
        [AllowAnonymous]
        [Route("/Parcaisse/GetAllByEtabIds")]
        public async Task<List<ParcaisseDTO>> GetAllByEtabIds([FromBody] List<string> etabIds)
        {
            return await _ParcaisseService.GetAllByEtabIds(etabIds);
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("/Parcaisse/Create")]
        public async Task<ParcaisseDTO> Create(ParcaisseDTO ParcaisseDTO)
        {
            return await _ParcaisseService.Create(ParcaisseDTO);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/Parcaisse/GetById")]
        public async Task<ParcaisseDTO> GetById(int id)
        {
            return await _ParcaisseService.GetById(id);
        }

        [HttpPut]
        [AllowAnonymous]
        [Route("/Parcaisse/Update")]
        public async Task<ParcaisseDTO> Update(ParcaisseDTO ParcaisseDTO)
        {
            return await _ParcaisseService.Update(ParcaisseDTO);
        }

        [HttpDelete]
        [AllowAnonymous]
        [Route("/Parcaisse/Delete")]
        public async Task<int> Delete(int id)
        {
            return await _ParcaisseService.Delete(id);
        }

    }
}
