using DDD.Application.Interfaces;
using DTO;
using DDD.Domain.Core.Bus;
using DDD.Domain.Core.Notifications;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Entity;
using DDD.Domain.Interfaces;
using AutoMapper;

namespace DDD.Services.Api.Controllers
{
    [Authorize]

    public class PiedecheController : GenericController
    {

        private readonly IPiedecheService<PiedecheDTO> _PiedecheService;
        private readonly IBilletDetailComptageService<BilletDetailComptageDTO> _BilletDetailComptageService;
        private readonly IJoursCaisseService<JoursCaiseDTO> _joursCaisseService;
        private readonly IJoursCaisseRepository<Jourscaisse> _joursCaisseRepository;
        private readonly IMapper _mapper;

        public PiedecheController(
             IPiedecheService<PiedecheDTO> PiedecheService,
             IBilletDetailComptageService<BilletDetailComptageDTO> billetDetailComptageService,
             IJoursCaisseService<JoursCaiseDTO> joursCaisseService,
             IJoursCaisseRepository<Jourscaisse> joursCaisseRepository,
             IMapper mapper

            )
        {
            _PiedecheService = PiedecheService;
            _BilletDetailComptageService = billetDetailComptageService;
            _joursCaisseService = joursCaisseService;
            _joursCaisseRepository = joursCaisseRepository;
            _mapper= mapper;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/Piedeche/GetAll")]
        public async Task<List<PiedecheDTO>> GetAll()
        {
            return await _PiedecheService.GetAll();
        }  
        [HttpGet]
        [AllowAnonymous]
        [Route("/Piedeche/GetAllByCoffreNull")]
        public async Task<List<PiedecheDTO>> GetAllByCoffreNull()
        {
            return await _PiedecheService.GetAllByCoffreNull();
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("/Piedeche/Create")]
        public async Task<PiedecheDTO> Create(PiedecheDTO PiedecheDTO)
        {
            return await _PiedecheService.Create(PiedecheDTO);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/Piedeche/GetById")]
        public async Task<PiedecheDTO> GetById(int id)
        {
            return await _PiedecheService.GetById(id);
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("/Piedeche/Update")]
        public PiedecheDTO UpdatePiedeche([FromBody]PiedecheDTO piedeche)
        {
            GetCurrentUserIdFromClaim();

            if (piedeche.Motif?.Id == 0)
            {
                piedeche.Motif = null;
            }
            var res = _PiedecheService.UpdateSync(piedeche);

                var jourCaisseDto = _joursCaisseService.GetByIdSync((int)piedeche.JourcaisseId);
                var allPiedEches = _PiedecheService.GetAllByJoursCaisseSync((int)piedeche.JourcaisseId, currentUserId);
                decimal somme = 0;
                foreach (var item in allPiedEches)
                {
                    somme = somme + (decimal)item.Montant;
                }
            jourCaisseDto.MontantCaisse = somme;




            if (res != null && piedeche.billetDetailComptageDTO.Count != 0)
            {
                if (piedeche.Id == 0)
                {
                    PiedecheDTO newPiedech = res as PiedecheDTO;


                    piedeche.billetDetailComptageDTO.ForEach(ligne =>
                    {
                        ligne.PiedecheId = newPiedech.Id;
                    });
                    _BilletDetailComptageService.CreateListSync(piedeche.billetDetailComptageDTO);

                }
                else if (piedeche.Id > 0)
                {
                    PiedecheDTO newPiedech = res as PiedecheDTO;
                    var allCalculetteByPiedech =  _BilletDetailComptageService.GetAllByPiedechIdSync(newPiedech.Id);

                    List<BilletDetailComptageDTO> allUpdatedLigne = new List<BilletDetailComptageDTO>();
                    List<BilletDetailComptageDTO> allCreatedLigne = new List<BilletDetailComptageDTO>();
                    piedeche.billetDetailComptageDTO.ForEach(ligne =>
                    {
                        var lignePiedechDataBase = allCalculetteByPiedech.Find(x => x.BilletId == ligne.BilletId);
                        if (lignePiedechDataBase != null)
                        {
                            ligne.PiedecheId = newPiedech.Id;
                            ligne.Id = lignePiedechDataBase.Id;
                            allUpdatedLigne.Add(ligne);
                        }
                        else
                        {
                            ligne.PiedecheId = newPiedech.Id;
                            allCreatedLigne.Add(ligne);
                        }
                    });
                    _BilletDetailComptageService.UpdateListSync(allUpdatedLigne);
                    _BilletDetailComptageService.CreateListSync(allCreatedLigne);
                }
            }
            return res;
        }

        [HttpDelete]
        [AllowAnonymous]
        [Route("/Piedeche/Delete")]
        public async Task<int> Delete(int id)
        {
            return await _PiedecheService.Delete(id);
        }
        [HttpPost]
        [AllowAnonymous]
        [Route("/Piedeche/GetAllByJoursCaisse/{id}")]
        public async Task<List<PiedecheDTO>> GetAllByJoursCaisse(int id)
        {
            GetCurrentUserIdFromClaim();
            var res= await _PiedecheService.GetAllByJoursCaisse(id,currentUserId);
            return res;
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("/Piedeche/GetAllByJoursCaisseComptage/{id}")]
        public async Task<List<PiedecheDTO>> GetAllByJoursCaisseComptage(int id)
        {

            return await _PiedecheService.GetAllByJoursCaisseComptage(id);
        }
    }
}
