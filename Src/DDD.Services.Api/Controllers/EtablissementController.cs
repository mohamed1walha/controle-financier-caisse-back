﻿using DDD.Application.Interfaces;
using DTO;
using DDD.Domain.Core.Bus;
using DDD.Domain.Core.Notifications;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DDD.Services.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EtablissementController : GenericController
    {

        private readonly IEtabService<EtablissementDTO> _etabService;

        public EtablissementController(
             IEtabService<EtablissementDTO> etabService
            )
        {
            _etabService = etabService;
        }
        [HttpPost]
        [AllowAnonymous]
        [Route("/Etablissement/GetAll")]
        public async Task<List<EtablissementDTO>> GetAll()
        {
            List<EtablissementDTO> etablissementDDD = new List<EtablissementDTO>();

            try
            {
                etablissementDDD = await _etabService.GetAll();
            }
            catch (Exception e)
            {

            }
            return etablissementDDD;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/Etablissement/GetById")]
        public async Task<EtablissementDTO> GetById(int id)
        {
            return await _etabService.GetById(id);
        }
    }
}

