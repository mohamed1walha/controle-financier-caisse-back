﻿using DTO;
using DDD.Domain.Interfaces;
using Entity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DDD.Services.Api.Controllers
{
    [Authorize]
    [ApiController]
    public class LignePrelevementController : GenericController
    {
        private readonly ILignePrelevementService<LignePrelevementDTO> _LignePrelevementService;

        public LignePrelevementController(
             ILignePrelevementService<LignePrelevementDTO> LignePrelevementService)
        {
            _LignePrelevementService = LignePrelevementService;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/LignePrelevement/GetAll")]
        public async Task<List<LignePrelevementDTO>> GetAll()
        {
            return await _LignePrelevementService.GetAll();
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("/LignePrelevement/Create")]
        public async Task<LignePrelevementDTO> Create(LignePrelevementDTO LignePrelevementDTO)
        {
            return await _LignePrelevementService.Create(LignePrelevementDTO);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/LignePrelevement/GetById")]
        public async Task<LignePrelevementDTO> GetById(int id)
        {
            return await _LignePrelevementService.GetById(id);
        }
           [HttpGet]
        [AllowAnonymous]
        [Route("/LignePrelevement/GetByEnteteId")]
        public async Task<List<LignePrelevementDTO>> GetByEnteteId(int id)
        {
            return await _LignePrelevementService.GetAllByEnteteId(id);
        }

        [HttpPut]
        [AllowAnonymous]
        [Route("/LignePrelevement/Update")]
        public async Task<LignePrelevementDTO> Update(LignePrelevementDTO LignePrelevementDTO)
        {
            return await _LignePrelevementService.Update(LignePrelevementDTO);
        }

        [HttpDelete]
        [AllowAnonymous]
        [Route("/LignePrelevement/Delete")]
        public async Task<int> Delete(int id)
        {
            return await _LignePrelevementService.Delete(id);
        }

    }
}
