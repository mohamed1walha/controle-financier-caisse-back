﻿using DTO;
using DDD.Domain.Interfaces;
using Entity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DDD.Services.Api.Controllers
{
    [Authorize]
    [ApiController]
    public class BilletDetailSortieController : GenericController
    {
        private readonly IBilletDetailSortieService<BilletDetailSortieDTO> _BilletDetailSortieService;

        public BilletDetailSortieController(
             IBilletDetailSortieService<BilletDetailSortieDTO> BilletDetailSortieService)
        {
            _BilletDetailSortieService = BilletDetailSortieService;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/BilletDetailSortie/GetAll")]
        public async Task<List<BilletDetailSortieDTO>> GetAll()
        {
            return await _BilletDetailSortieService.GetAll();
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("/BilletDetailSortie/Create")]
        public async Task<BilletDetailSortieDTO> Create(BilletDetailSortieDTO BilletDetailSortieDTO)
        {
            return await _BilletDetailSortieService.Create(BilletDetailSortieDTO);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/BilletDetailSortie/GetById")]
        public async Task<BilletDetailSortieDTO> GetById(int id)
        {
            return await _BilletDetailSortieService.GetById(id);
        }

        [HttpPut]
        [AllowAnonymous]
        [Route("/BilletDetailSortie/Update")]
        public async Task<BilletDetailSortieDTO> Update(BilletDetailSortieDTO BilletDetailSortieDTO)
        {
            return await _BilletDetailSortieService.Update(BilletDetailSortieDTO);
        }

        [HttpDelete]
        [AllowAnonymous]
        [Route("/BilletDetailSortie/Delete")]
        public async Task<int> Delete(int id)
        {
            return await _BilletDetailSortieService.Delete(id);
        }


        [HttpGet]
        [AllowAnonymous]
        [Route("/BilletDetailSortie/GetAllBySortieId")]
        public async Task<List<BilletDetailSortieDTO>> GetAllBySortieId(int sortieId)
        {
            return await _BilletDetailSortieService.GetAllBySortieId(sortieId);
        }

    }
}
