﻿//using AU.DTO;
//using Microsoft.AspNetCore.Authorization;
//using Microsoft.AspNetCore.Mvc;
//using System.Threading.Tasks;

//using AU.DTO.AuthDTO;

//using Microsoft.Extensions.Hosting;
//using Microsoft.Extensions.Primitives;
//using Microsoft.Extensions.Localization;
//using System;
//using Org.BouncyCastle.Bcpg.OpenPgp;
//using DDD.Services.Api.Controllers.config_controller;
//using AU.API.Identity;
//using DTO;
//using DDD.Domain.Interfaces;

//namespace DDD.Services.Api.Controllers
//{
//    [Route("")]
//    public class AuthController : ControllerBase
//    {
//        protected readonly IAuthenticationService authService;
//        protected readonly IHostEnvironment hostingEnvironment;
//        public AuthController(IAuthenticationService authService,
//            IHostEnvironment hostingEnvironment)        {
//            this.authService = authService;
//            this.hostingEnvironment = hostingEnvironment;
//        }

//        /// <summary>
//        /// authentication by login and password
//        /// </summary>

//        /// <returns></returns>
//        [HttpPost("/login")]
//        [AllowAnonymous]
//        public async Task<IActionResult> Login([FromBody] LoginDTO loginDto)
//        {
//            var result = await authService.Login(loginDto);
 
//            if (result.Succeeded)
//            {
//                return Ok(new { token = result.Data});
//            }
//            if (result.IsModelValid)
//            {
//                return Ok( new {message = "loginError" });
//            }

//            return BadRequest();
//        }
//        //[HttpGet("/auth/sentTestMail/{Password}/{Account}/{Mail}/{Port}")]
//        //[AllowAnonymous]
//        //public ActionResult SendTestMail(string Password, string Account, string Mail, int Port)
//        //{
//        //    try
//        //    {
//        //        //   var smtpConfig = await erpGlobalConfig.GetGlobalSmtpConfig(hostingEnvironment.EnvironmentName);
//        //        //"TKRTZDgRfPa7djbeidv/2g=="
//        //        SMTPOptions o = new SMTPOptions(Account, Port, Mail, Password, true, System.Net.Mail.SmtpDeliveryMethod.Network);
//        //        MailOptions options = new MailOptions(
//        //           "subject", "mail body",
//        //           o.SmtpAccount,
//        //            "samer.mery@phoenix-its.com", String.Empty,
//        //             String.Empty);




//        //        MailSender mail = new MailSender();
//        //        mail.SendMail(o, options);
//        //        return Ok();
//        //    }
//        //    catch (Exception ex)
//        //    {
//        //        return Ok(ex.Message + ex.InnerException == null ? String.Empty : ex.InnerException.ToString());
//        //    }

//        //}


//        [HttpGet("/auth/get-host-evironment")]
//        public  IActionResult GetHostEnvironment()
//        {
//                return  Ok(hostingEnvironment.EnvironmentName);
//        }

//        /// <summary>
//        /// change password
//        /// </summary>
//        /// <param name="changePasswordDto"></param>
//        /// <returns></returns>
//        [HttpPost("/auth/reset-pass")]
//        [Authorize]
//        public async Task<IActionResult> ChangePassword([FromBody] ChangePasswordDTO changePasswordDto)
//        {
//            var currentUserId = User.GetUserId();

//            var result = await authService.ChangePassword(changePasswordDto, currentUserId);

//            if (result.Succeeded)
//            {
//                return Ok();
//            }

//            return BadRequest();
//        }

//        //[HttpPost("/auth/admin-rest-pass")]
//        //public async Task<IActionResult> AdminChangePassword([FromBody] AdminChangePasswordDTO changePasswordDto)
//        //{
           
//        //    var result = await authService.ChangePassword(changePasswordDto, changePasswordDto.UserID);

//        //    if (result.Succeeded)
//        //    {
//        //        return Ok();
//        //    }

//        //    return BadRequest();
//        //}


//        /// <summary>
//        /// logout
//        /// </summary>
//        /// <returns></returns>
//        [HttpPost("/auth/sign-out")]
//        public IActionResult SignOut()
//        {
//            return Ok();
//        }

//        /// <summary>
//        /// refresh token if token expired
//        /// </summary>
//        /// <param name="refreshTokenDTO"></param>
//        /// <returns></returns>
//        [HttpPost("/auth/refresh-token")]
//        [AllowAnonymous]
//        public async Task<IActionResult> RefreshToken([FromBody] RefreshTokenDTO refreshTokenDTO)
//        {
//            var result = await authService.RefreshToken(refreshTokenDTO);

//            if (result.Succeeded)
//                return Ok(new { token = result.Data });

//            return BadRequest();
//        }

//        //[HttpPost("/auth/RestorePassword")]
//        //[AllowAnonymous]
//        //public async Task<IActionResult> RestorePassword([FromBody] RestorePasswordDTO restorePasswordDto)
//        //{
//        //    var result = await authService.RestorePassword(restorePasswordDto);

//        //    if (result.Succeeded)
//        //        return Ok(new { token = result.Data });
//        //    return BadRequest();
//        //}
//        /// <summary>
//        /// request password by email
//        /// </summary>
//        /// <param name="requestPasswordDto"></param>
//        /// <returns></returns>
//        //[HttpPost("/auth/request-pass")]
//        //[AllowAnonymous]
//        //public async Task<IActionResult> RequestPassword([FromBody] RequestPasswordDTO requestPasswordDto)
//        //{
//        //    this.HttpContext.Request.Headers.TryGetValue(PV.Origin, out StringValues val);
//        //    var result = await authService.RequestPassword(requestPasswordDto, val.ToString());
            
//        //    if (result.Succeeded)
//        //    {
//        //        return Ok();
//        //    }
//        //    return BadRequest();
//        //}



//    }
//}
