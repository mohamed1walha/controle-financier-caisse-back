﻿using DTO;
using DDD.Domain.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using Helpers;
using Microsoft.VisualBasic;
using DDD.Application.Services;
using Newtonsoft.Json;

namespace DDD.Services.Api.Controllers
{
    [ApiController]
    public class EntetePrelevmentController : GenericController
    {
        private readonly IEntetePrelevementService<EntetePrelevementDTO> _EntetePrelevementService;

        public EntetePrelevmentController(
             IEntetePrelevementService<EntetePrelevementDTO> EntetePrelevmentService)
        {
            _EntetePrelevementService = EntetePrelevmentService;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/EntetePrelevement/GetAll")]
        public async Task<List<EntetePrelevementDTO>> GetAll()
        {
            return await _EntetePrelevementService.GetAllEntete();
        }
        //[HttpPost]
        //[AllowAnonymous]
        //[Route("/EntetePrelevement/GetAllWithFilter")]
        //public async Task<List<EntetePrelevementDTO>> GetAllWithFilter([FromBody] FilterPrelevementDTO filterPrelevementDTO)
        //{
        //    return await _EntetePrelevementService.GetAllWithFilter(filterPrelevementDTO);
        //}

        [HttpPost]
        [AllowAnonymous]
        [Route("/EntetePrelevement/GetAllLazy")]
        public async Task<PagedResults> GetAllLazy(GridStateDTO grid)
        {
            FilterPrelevementDTO filterObject = JsonConvert.DeserializeObject<FilterPrelevementDTO>(grid.Filters.ToString());
            return await _EntetePrelevementService.GetAllEnteteLazy(grid, filterObject);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/EntetePrelevement/GetAllByEtablissementIdAndUserId/{etablissementId}/{userId}")]
        public async Task<List<EntetePrelevementDTO>> GetAllByEtablissementIdAndUserId(int etablissementId, int userId)
        {
            return await _EntetePrelevementService.GetAllByEtablissementIdAndUserId(etablissementId, userId);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/EntetePrelevement/GetAllByValidateur/{userId}")]
        public async Task<List<EntetePrelevementDTO>> GetAllByValidateur(int userId)
        {
            return await _EntetePrelevementService.GetAllByValidateur(userId);
        }
        [HttpGet]
        [AllowAnonymous]
        [Route("/EntetePrelevement/GetAllByEtabIdCaisseIdNumZRef")]
        public async Task<List<EntetePrelevementDTO>> GetAllByEtabIdCaisseIdNumZRef(int etabId, string caisseId, string numZRef)
        {
            return await _EntetePrelevementService.GetAllByEtabIdCaisseIdNumZRef(etabId, caisseId, numZRef);
        }
        [HttpGet]
        [AllowAnonymous]
        [Route("/EntetePrelevement/GetAllByDemandeur/{userId}/{isAccepted}")]
        public async Task<List<EntetePrelevementDTO>> GetAllByDemandeur(int userId, bool isAccepted)
        {
            return await _EntetePrelevementService.GetAllByDemandeur(userId, isAccepted);
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("/EntetePrelevement/Create")]
        public async Task<OperationResult> Create(EntetePrelevementDTO EntetePrelevementDTO)
        {
            EntetePrelevementDTO.CreateDate = DateTime.Now;
            return await _EntetePrelevementService.CreateEntetePrelevement(EntetePrelevementDTO);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/EntetePrelevement/GetById")]
        public async Task<EntetePrelevementDTO> GetById(int id)
        {
            //try
            //{
            //    MailMessage mail = new MailMessage();

            //    mail.From = new MailAddress("amirzouari25@gmail.com");
            //    mail.To.Add("amirzouari22@gmail.com");
            //    mail.Subject = "Test Mail";
            //    mail.Body = "This is a test email";
            //    mail.CC.Add("amir.zouari@live.fr");


            //    Mail.SendMail(mail);

            //}
            //catch (Exception ex)
            //{
            //}
            return await _EntetePrelevementService.GetFullEntetePrelevementById(id);
        }
        [HttpGet]
        [AllowAnonymous]
        [Route("/EntetePrelevement/downloadFile/{id}")]
        public async Task<FileStreamResult> downloadFile(int id)
        {
            var res = await _EntetePrelevementService.downloadFile(id);
            return res;
        }
        [HttpGet]
        [AllowAnonymous]
        [Route("/EntetePrelevement/deleteFile/{id}")]
        public async Task<EntetePrelevementDTO> deleteFile(int id)
        {
            var res = await _EntetePrelevementService.deleteFile(id);
            return res;
        }

        [HttpPut]
        [AllowAnonymous]
        [Route("/EntetePrelevement/Update")]
        public async Task<EntetePrelevementDTO> Update(EntetePrelevementDTO EntetePrelevementDTO)
        {
            return await _EntetePrelevementService.Update(EntetePrelevementDTO);
        }

        [HttpDelete]
        [AllowAnonymous]
        [Route("/EntetePrelevement/Delete")]
        public async Task<int> Delete(int id)
        {
            return await _EntetePrelevementService.Delete(id);
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("/EntetePrelevement/GenerateComptable")]
        public async Task<OperationResult> GenerateComptable(List<EntetePrelevementDTO> prelevenetList)
        {
            List<string> data= await _EntetePrelevementService.GenerateComptable(prelevenetList);
            OperationResult operationResult = new OperationResult();
            operationResult.Data = data;
            return operationResult;
        }


        [HttpPost]
        [AllowAnonymous]
        [Route("/EntetePrelevement/GetEntetePrelevementsGrid")]
        public async Task<PagedResults> GetEntetePrelevements(
          GridStateDTO grid)
        {
            return await _EntetePrelevementService.GetEntetePrelevements(grid);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/EntetePrelevement/FiltersModel")]
        public async Task<FilterPrelevementDTO> GetAllFilters()
        {
            throw new NotImplementedException();
        }
    }
}
