﻿using DTO;
using DDD.Domain.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DDD.Services.Api.Controllers
{
    [Authorize]
    [ApiController]
    public class MotifController : GenericController
    {
        private readonly IMotifService<MotifDTO> _MotifService;

        public MotifController(
             IMotifService<MotifDTO> MotifService)
        {
            _MotifService = MotifService;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/Motif/GetAll")]
        public async Task<List<MotifDTO>> GetAll()
        {
            return await _MotifService.GetAll();
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("/Motif/Create")]
        public async Task<MotifDTO> Create(MotifDTO motifDTO)
        {
            return await _MotifService.Create(motifDTO);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/Motif/GetById")]
        public async Task<MotifDTO> GetById(int id)
        {
            return await _MotifService.GetById(id);
        }

        [HttpPut]
        [AllowAnonymous]
        [Route("/Motif/Update")]
        public async Task<MotifDTO> Update(MotifDTO motifDTO)
        {
            return await _MotifService.Update(motifDTO);
        }

        [HttpDelete]
        [AllowAnonymous]
        [Route("/Motif/Delete")]
        public async Task<int> Delete(int id)
        {
            return await _MotifService.Delete(id);
        }

    }
}
