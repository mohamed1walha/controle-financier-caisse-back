﻿using DTO;
using DDD.Domain.Interfaces;
using Entity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DDD.Services.Api.Controllers
{
    [Authorize]
    [ApiController]
    public class ParamMyCashController : GenericController
    {
        private readonly IParamMyCashService<ParamMyCashDTO> _ParamMyCashService;

        public ParamMyCashController(
             IParamMyCashService<ParamMyCashDTO> ParamMyCashService)
        {
            _ParamMyCashService = ParamMyCashService;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/ParamMyCash/GetAll")]
        public async Task<List<ParamMyCashDTO>> GetAll()
        {
            return await _ParamMyCashService.GetAll();
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("/ParamMyCash/Create")]
        public async Task<ParamMyCashDTO> Create(ParamMyCashDTO ParamMyCashDTO)
        {
            return await _ParamMyCashService.Create(ParamMyCashDTO);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/ParamMyCash/GetById")]
        public async Task<ParamMyCashDTO> GetById(int id)
        {
            return await _ParamMyCashService.GetById(id);
        }

        [HttpPut]
        [AllowAnonymous]
        [Route("/ParamMyCash/Update")]
        public async Task<ParamMyCashDTO> Update(ParamMyCashDTO ParamMyCashDTO)
        {
            return await _ParamMyCashService.Update(ParamMyCashDTO);
        }

        [HttpDelete]
        [AllowAnonymous]
        [Route("/ParamMyCash/Delete")]
        public async Task<int> Delete(int id)
        {
            return await _ParamMyCashService.Delete(id);
        }

    }
}
