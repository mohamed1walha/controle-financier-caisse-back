﻿using DTO;
using DDD.Domain.Interfaces;
using Entity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;
using Helpers;
using AutoMapper;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System;
using DDD.Services.Api.Controllers.config_controller;
using AU.DTO.AuthDTO;

namespace DDD.Services.Api.Controllers
{
    [Authorize]
    [ApiController]
    public class UserController : GenericController
    {
        private readonly IUserService<UserDTO> _UserService;
        private readonly ICompteDepenseService<CompteDepenseDTO> _compteDepenseService;
        private readonly IUserRolesService<UserRolesDTO> _userRolesService;
        private readonly IUserModePaieService<UserModePaieDTO> _userModePaieService;
        private readonly IRolesService<RolesDTO> _rolesService;

        private readonly ILogger<UserController> _logger;
        private readonly IMapper _mapper;
        public IConfiguration Configuration { get; }


        public UserController(
             IConfiguration configuration,
             IMapper mapper,
             ILogger<UserController> logger, 
             IUserRolesService<UserRolesDTO> userRolesService,
             IRolesService<RolesDTO> rolesService,
             ICompteDepenseService<CompteDepenseDTO> compteDepenseService,
             IUserService<UserDTO> UserService, IUserModePaieService<UserModePaieDTO> userModePaieService)
        {
            Configuration = configuration;
            _mapper = mapper;
            _logger = logger;
            _UserService = UserService;
            _userRolesService = userRolesService;
            _rolesService = rolesService;
            _userModePaieService = userModePaieService;

            _compteDepenseService = compteDepenseService;
        }


        [HttpPost]
        [AllowAnonymous]
        [Route("/User/Authenticate")]
        public async Task<IActionResult> Authenticate([FromBody] LoginDTO loginDTO)
        {
            var configuration = new ConfigurationBuilder()
         .AddJsonFile("appsettings.json")
         .Build();
            string decimalSeparator = configuration["AppSettings:DecimalSeparator"];
            int digitsAfterDecimal = int.Parse(configuration["AppSettings:DigitsAfterDecimal"]);

            IActionResult response = BadRequest(new { message = "L'identifiant ou le mot de passe est incorrect" });
            var user = await _UserService.GetByUserName(loginDTO.Username);
            if (user != null&&user?.IsAdmin==true)
            {
                var dp = await _compteDepenseService.GetByEtablissementId(user.EtablissementId);

                response = BadRequest(new { message = "Le mot de passe est incorrect" });

                if (user?.Password == loginDTO.Password.HashSHA256())
                {

                    // authentication successful so generate jwt token
                    var tokenHandler = new JwtSecurityTokenHandler();
                    var key = Encoding.ASCII.GetBytes(Configuration["AppSettings:Secret"]);
                    var tokenDescriptor = new SecurityTokenDescriptor
                    {
                        Subject = new ClaimsIdentity(new Claim[]
                        {
                          new Claim(ClaimTypes.NameIdentifier, user.Id.ToString())
                        }),
                        Expires = DateTime.Now.AddDays(2),
                        SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                    };
                    var token = tokenHandler.CreateToken(tokenDescriptor);
                    var Token = tokenHandler.WriteToken(token);
                    List<UserRolesDTO> userRoles = await _userRolesService.GetByUserId(user.Id);

                    response = Ok(new { 
                        token = Token, 
                        expires = tokenDescriptor.Expires, 
                        currentUser = user.Username,
                        compteDepenseId=dp?.Id,
                        etablissementId=user?.EtablissementId ,
                        decimalSeparator = decimalSeparator,
                        digitsAfterDecimal = digitsAfterDecimal
                    });
                }
            }
            return response;
        }
        [HttpPost]
        [AllowAnonymous]
        [Route("/User/Authenticate/Depense")]
        public async Task<IActionResult> AuthenticateDepense([FromBody] LoginDTO loginDTO)
        {
            var configuration = new ConfigurationBuilder()
       .AddJsonFile("appsettings.json")
       .Build();
            string decimalSeparator = configuration["AppSettings:DecimalSeparator"];
            int digitsAfterDecimal = int.Parse(configuration["AppSettings:DigitsAfterDecimal"]);

            IActionResult response = BadRequest(new { message = "L'identifiant ou le mot de passe est incorrect" });
            var user = await _UserService.GetByUserName(loginDTO.Username);
            if (user != null && user?.IsAdmin==false)
            {
                var dp = await _compteDepenseService.GetByEtablissementId(user.EtablissementId);

                response = BadRequest(new { message = "Le mot de passe est incorrect" });

                if (user?.Password == loginDTO.Password.HashSHA256())
                {

                    // authentication successful so generate jwt token
                    var tokenHandler = new JwtSecurityTokenHandler();
                    var key = Encoding.ASCII.GetBytes(Configuration["AppSettings:Secret"]);
                    var tokenDescriptor = new SecurityTokenDescriptor
                    {
                        Subject = new ClaimsIdentity(new Claim[]
                        {
                          new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                          new Claim(ClaimTypes.Country, user.EtablissementId.ToString())
                        }),
                        Expires = DateTime.Now.AddDays(2),
                        SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                    };
                    var token = tokenHandler.CreateToken(tokenDescriptor);
                    var Token = tokenHandler.WriteToken(token);
                    List<UserRolesDTO> userRoles = await _userRolesService.GetByUserId(user.Id);
                    List<string> roles= new List<string>();
                    foreach (var role in userRoles)
                    {
                        var r = await _rolesService.GetById(role.RoleId);
                        roles.Add(r.Label);
                    }

                    response = Ok(new { token = Token, expires = tokenDescriptor.Expires, currentUser = user.Username,
                        compteDepenseId = dp?.Id,
                        etablissementId = user?.EtablissementId,
                        decimalSeparator = decimalSeparator,
                        digitsAfterDecimal = digitsAfterDecimal,
                        roles = roles,
                    });
                }
            }
            return response;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/User/GetAll")]
        public async Task<List<UserDTO>> GetAll()
        {
            return (await _UserService.GetAllUsers()).WithoutPasswords();
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/User/GetAllSimpleData")]
        public async Task<List<UserDTO>> GetAllSimpleData()
        {
            return await _UserService.GetAllUsersSimpleData();
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/User/GetAllByEtabId/{etabId}")]
        public async Task<List<UserDTO>> GetAllByEtabId(int etabId)
        {
            return (await _UserService.GetAllByEtabId(etabId)).WithoutPasswords();
        }
           [HttpGet]
        [AllowAnonymous]
        [Route("/User/GetAllValidateur")]
        public async Task<List<UserDTO>> GetAllValidateur()
        {
            return (await _UserService.GetAllValidateur()).WithoutPasswords();
        }
        [HttpPost("/User/RestorePassword")]
        [AllowAnonymous]
        public async Task<bool> RestorePassword([FromBody] RestorePasswordDTO restorePasswordDto)
        {
            var result = await _UserService.RestorePassword(restorePasswordDto, restorePasswordDto.NewPassword.HashSHA256());

            if (result)
                return result;
            return false;
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("/User/Create")]
        public async Task<IActionResult> Create(UserDTO newUserDTO)
        {
            var user = await _UserService.GetByUserName(newUserDTO.Username);
            if (user == null)
            {
                newUserDTO.Password = newUserDTO.Password.HashSHA256();
                newUserDTO.EtablissementId = newUserDTO?.Etablissement?.Id;
                newUserDTO.Etablissement = null;
                var res =await _UserService.Create(newUserDTO);
                if (res != null && res.IsAdmin == false) { 
                foreach (var role in newUserDTO.Roles)
                {
                    var userRoles = new UserRolesDTO { RoleId = role.Id, UserId = res.Id };
                    var res1 = await _userRolesService.Create(userRoles);
                } 
                  
                }
                foreach (var role in newUserDTO.ModePaies)
                {
                    var userModePaie = new UserModePaieDTO { ModePaieId = role.Id, UserId = res.Id };
                    var res1 = await _userModePaieService.Create(userModePaie);
                }
                return Ok(newUserDTO);
            }
            else
            {
                return BadRequest(new { message = "Username déja existe" });
            }
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/User/GetById")]
        public async Task<UserDTO> GetById(int id)
        {
            return (await _UserService.GetById(id)).WithoutPassword();
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/User/GetByUserName")]
        public async Task<UserDTO> GetByUserName(string username)
        {
            return (await _UserService.GetByUserName(username)).WithoutPassword();
        }

        [HttpPut]
        [AllowAnonymous]
        [Route("/User/Update")]
        public async Task<UserDTO> Update(UserDTO updateUser)
        {
            updateUser.EtablissementId=updateUser.Etablissement.Id;
            updateUser.Etablissement = null;
               var user = await _UserService.GetByIdWithRole(updateUser.Id);
            List<RolesDTO> idsDelete =new List<RolesDTO>();
            List<UserRolesDTO> userRoles =new List<UserRolesDTO>();
            List<object> ints = new List<object>(); 
            List<ModepaieDTO> idsMPDelete =new List<ModepaieDTO>();
            List<UserModePaieDTO> userModePaies = new List<UserModePaieDTO>();
            List<object> intsMp = new List<object>();

            if (user!=null )
            {
                userRoles=await _userRolesService.GetByUserId(user.Id);
                if (userRoles.Count != 0 && updateUser.IsAdmin == true)
                {
                    userRoles.ForEach(x => ints.Add(x.Id));
                    var res = await _userRolesService.DeleteMany(ints);
                }
                //  var coffreCatergory = await _categorieModePaieCoffreService.GetByIdCoffre(coffre.Id);
                else
                {
                    foreach (RolesDTO item in updateUser.Roles)
                    {
                        var c = userRoles.Find((x) => x.RoleId == item.Id);
                        if (c == null)
                        {
                            var userRolesEdit = new UserRolesDTO { RoleId = item.Id, UserId = user.Id };
                            var res2 = await _userRolesService.Create(userRolesEdit);
                        }

                    }
                    foreach (UserRolesDTO cc in userRoles)
                    {
                        var c = updateUser.Roles.Find((x) => x.Id == cc.RoleId);
                        if (c == null)
                        {
                            var res3 = await _userRolesService.Delete(cc.Id);
                        }

                    }
                } 
                    
                    userModePaies=await _userModePaieService.GetByUserId(user.Id);
                if(userModePaies.Count!=0&&(updateUser.ModePaies?.Count ==0 || updateUser?.ModePaies==null))
                {
                    userModePaies.ForEach(x=>intsMp.Add(x.Id));
                    var res = await _userModePaieService.DeleteMany(intsMp);
                }
                else { 
                foreach (ModepaieDTO item in updateUser.ModePaies)
                {
                    var c = userModePaies.Find((x) => x.ModePaieId == item.Id);
                    if (c == null)
                    {
                        var userModePaiesEdit = new UserModePaieDTO { ModePaieId = item.Id, UserId = user.Id };
                        var res2 = await _userModePaieService.Create(userModePaiesEdit);
                    }

                }
                foreach (UserModePaieDTO cc in userModePaies)
                {
                    var c = updateUser.ModePaies.Find((x) => x.Id == cc.ModePaieId);
                    if (c == null)
                    {
                        var res3 = await _userModePaieService.Delete(cc.Id);
                    }

                }
                //else if (userRoles.Count != 0 &&user.IsAdmin != true && userRoles.Count>updateUser.Roles.Count)
                //{
                //    foreach (var elt in userRoles)
                //    {

                //        if (updateUser.Roles.Exists(x => x.Id == elt.RoleId)==false)
                //        {
                //            var userRolesEdit = new UserRolesDTO { RoleId = elt.RoleId, UserId = user.Id };
                //            var res2 = await _userRolesService.Create(userRolesEdit);
                //        }
                //        else

                //        {
                //            var res3 = await _userRolesService.Delete(elt.Id);

                //        }
                    }

                    
                    
                  
                //}
                //else
                //{
                //    foreach (var elt in updateUser.Roles)
                //    {
                //        if (userRoles.Exists(x => x.RoleId == elt.Id) == false)
                //        {
                //            var userRolesEdit = new UserRolesDTO { RoleId = elt.Id, UserId = user.Id };
                //            var res2 = await _userRolesService.Create(userRolesEdit);
                //        }
                //        //else
                //        //{
                //        //    var res3 = await _userRolesService.Delete(elt.Id);

                //        //}
                //    }


                //}

            }
            updateUser.Password = user.Password;
            return await _UserService.Update(updateUser);
        }

        [HttpDelete]
        [AllowAnonymous]
        [Route("/User/Delete")]
        public async Task<int> Delete(int id)
        {
            return await _UserService.Delete(id);
        }
        [HttpPost("/User/request-pass")]
        [Authorize]
        public async Task<IActionResult> ChangePassword([FromBody] ChangePasswordDTO changePasswordDto)
        {
            var currentUserId = User.GetUserId();

            var result = await _UserService.ChangePassword(changePasswordDto, currentUserId, changePasswordDto.OldPassword.HashSHA256(), changePasswordDto.Password.HashSHA256());

            if (result.Succeeded)
            {
                return Ok();
            }

            return BadRequest();
        }


    }
}
