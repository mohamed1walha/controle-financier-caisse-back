﻿using DTO;
using DDD.Domain.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DDD.Services.Api.Controllers
{
    [Authorize]
    [ApiController]
    public class ChauffeurController : GenericController
    {
        private readonly IChauffeurService<ChauffeurDTO> _ChauffeurService;

        public ChauffeurController(
             IChauffeurService<ChauffeurDTO> ChauffeurService)
        {
            _ChauffeurService = ChauffeurService;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/Chauffeur/GetAll")]
        public async Task<List<ChauffeurDTO>> GetAll()
        {
            return await _ChauffeurService.GetAll();
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("/Chauffeur/Create")]
        public async Task<ChauffeurDTO> Create(ChauffeurDTO chauffeurDTO)
        {
            return await _ChauffeurService.Create(chauffeurDTO);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/Chauffeur/GetById")]
        public async Task<ChauffeurDTO> GetById(int id)
        {
            return await _ChauffeurService.GetById(id);
        }

        [HttpPut]
        [AllowAnonymous]
        [Route("/Chauffeur/Update")]
        public async Task<ChauffeurDTO> Update(ChauffeurDTO chauffeurDTO)
        {
            return await _ChauffeurService.Update(chauffeurDTO);
        }

        [HttpDelete]
        [AllowAnonymous]
        [Route("/Chauffeur/Delete")]
        public async Task<int> Delete(int id)
        {
            return await _ChauffeurService.Delete(id);
        }

    }
}
