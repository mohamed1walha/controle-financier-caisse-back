﻿using DTO;
using DDD.Domain.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DDD.Services.Api.Controllers
{
    [Authorize]
    [ApiController]
    public class MotifresyncController : GenericController
    {
        private readonly IMotifresyncService<MotifresyncDTO> _MotifresyncService;

        public MotifresyncController(
             IMotifresyncService<MotifresyncDTO> MotifresyncService)
        {
            _MotifresyncService = MotifresyncService;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/Motifresync/GetAll")]
        public async Task<List<MotifresyncDTO>> GetAll()
        {
            return await _MotifresyncService.GetAll();
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("/Motifresync/Create")]
        public async Task<MotifresyncDTO> Create(MotifresyncDTO MotifresyncDTO)
        {
            return await _MotifresyncService.Create(MotifresyncDTO);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/Motifresync/GetById")]
        public async Task<MotifresyncDTO> GetById(int id)
        {
            return await _MotifresyncService.GetById(id);
        }

        [HttpPut]
        [AllowAnonymous]
        [Route("/Motifresync/Update")]
        public async Task<MotifresyncDTO> Update(MotifresyncDTO MotifresyncDTO)
        {
            return await _MotifresyncService.Update(MotifresyncDTO);
        }

        [HttpDelete]
        [AllowAnonymous]
        [Route("/Motifresync/Delete")]
        public async Task<int> Delete(int id)
        {
            return await _MotifresyncService.Delete(id);
        }

    }
}
