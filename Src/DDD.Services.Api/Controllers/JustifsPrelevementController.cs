﻿using DTO;
using DDD.Domain.Interfaces;
using Entity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DDD.Services.Api.Controllers
{
    [Authorize]
    [ApiController]
    public class JustifsPrelevementController : GenericController
    {
        private readonly IJustifsPrelevementService<JustifsPrelevementDTO> _JustifsPrelevementService;

        public JustifsPrelevementController(
             IJustifsPrelevementService<JustifsPrelevementDTO> JustifsPrelevementService)
        {
            _JustifsPrelevementService = JustifsPrelevementService;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/JustifsPrelevement/GetAll")]
        public async Task<List<JustifsPrelevementDTO>> GetAll()
        {
            return await _JustifsPrelevementService.GetAll();
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("/JustifsPrelevement/Create")]
        public async Task<JustifsPrelevementDTO> Create(JustifsPrelevementDTO JustifsPrelevementDTO)
        {
            return await _JustifsPrelevementService.Create(JustifsPrelevementDTO);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/JustifsPrelevement/GetById")]
        public async Task<JustifsPrelevementDTO> GetById(int id)
        {
            return await _JustifsPrelevementService.GetById(id);
        }

        [HttpPut]
        [AllowAnonymous]
        [Route("/JustifsPrelevement/Update")]
        public async Task<JustifsPrelevementDTO> Update(JustifsPrelevementDTO JustifsPrelevementDTO)
        {
            return await _JustifsPrelevementService.Update(JustifsPrelevementDTO);
        }

        [HttpDelete]
        [AllowAnonymous]
        [Route("/JustifsPrelevement/Delete")]
        public async Task<int> Delete(int id)
        {
            return await _JustifsPrelevementService.Delete(id);
        }

    }
}
