﻿using DTO;
using DDD.Domain.Interfaces;
using Entity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using DDD.Application.Services;

namespace DDD.Services.Api.Controllers
{
    [Authorize]
    [ApiController]
    public class BilletDetailComptageController : GenericController
    {
        private readonly IBilletDetailComptageService<BilletDetailComptageDTO> _BilletDetailComptageService;

        public BilletDetailComptageController(
             IBilletDetailComptageService<BilletDetailComptageDTO> BilletDetailComptageService)
        {
            _BilletDetailComptageService = BilletDetailComptageService;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/BilletDetailComptage/GetAll")]
        public async Task<List<BilletDetailComptageDTO>> GetAll()
        {
            return await _BilletDetailComptageService.GetAll();
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("/BilletDetailComptage/Create")]
        public async Task<BilletDetailComptageDTO> Create(BilletDetailComptageDTO BilletDetailComptageDTO)
        {
            return await _BilletDetailComptageService.Create(BilletDetailComptageDTO);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/BilletDetailComptage/GetById")]
        public async Task<BilletDetailComptageDTO> GetById(int id)
        {
            return await _BilletDetailComptageService.GetById(id);
        }

        [HttpPut]
        [AllowAnonymous]
        [Route("/BilletDetailComptage/Update")]
        public async Task<BilletDetailComptageDTO> Update(BilletDetailComptageDTO BilletDetailComptageDTO)
        {
            return await _BilletDetailComptageService.Update(BilletDetailComptageDTO);
        }

        [HttpDelete]
        [AllowAnonymous]
        [Route("/BilletDetailComptage/Delete")]
        public async Task<int> Delete(int id)
        {
            return await _BilletDetailComptageService.Delete(id);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/BilletDetailComptage/GetAllByPiedechId")]
        public async Task<List<BilletDetailComptageDTO>> GetAllByPiedechId(int piedechId)
        {
            return await _BilletDetailComptageService.GetAllByPiedechId(piedechId);
        }


    }
}
