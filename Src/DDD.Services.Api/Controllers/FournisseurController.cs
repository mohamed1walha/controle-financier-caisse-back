﻿using DTO;
using DDD.Domain.Interfaces;
using Entity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DDD.Services.Api.Controllers
{
    [Authorize]
    [ApiController]
    public class FournisseurController : GenericController
    {
        private readonly IFournisseurService<FournisseurDTO> _FournisseurService;

        public FournisseurController(
             IFournisseurService<FournisseurDTO> FournisseurService)
        {
            _FournisseurService = FournisseurService;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/Fournisseur/GetAll")]
        public async Task<List<FournisseurDTO>> GetAll()
        {
            return await _FournisseurService.GetAll();
        }
        [HttpGet]
        [AllowAnonymous]
        [Route("/Fournisseur/GetIsDefault")]
        public async Task<FournisseurDTO> GetIsDefault()
        {
            return await _FournisseurService.GetIsDefault();
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("/Fournisseur/Create")]
        public async Task<FournisseurDTO> Create(FournisseurDTO FournisseurDTO)
        {
            return await _FournisseurService.Create(FournisseurDTO);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/Fournisseur/GetById")]
        public async Task<FournisseurDTO> GetById(int id)
        {
            return await _FournisseurService.GetById(id);
        }

        [HttpPut]
        [AllowAnonymous]
        [Route("/Fournisseur/Update")]
        public async Task<FournisseurDTO> Update(FournisseurDTO FournisseurDTO)
        {
            return await _FournisseurService.Update(FournisseurDTO);
        }

        [HttpDelete]
        [AllowAnonymous]
        [Route("/Fournisseur/Delete")]
        public async Task<int> Delete(int id)
        {
            return await _FournisseurService.Delete(id);
        }

    }
}
