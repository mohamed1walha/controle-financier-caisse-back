﻿using Microsoft.AspNetCore.Mvc;

namespace DDD.Services.Api.Controllers
{
    public class GenericController : ControllerBase
    {
        protected string CurrentUserName;
        protected int currentUserId;
        protected void GetCurrentUserNameFromClaim()
        {
            CurrentUserName = ClaimsTools.GetLoggedUserName(User);
            if (string.IsNullOrEmpty(CurrentUserName))
                throw new APICustomException(APICustomReturnType.UserNotLoggedIn);
        }
        protected void GetCurrentUserIdFromClaim()
        {
            currentUserId = ClaimsTools.GetLoggedUserId(User);
            if (currentUserId == 0)
                throw new APICustomException(APICustomReturnType.UserNotLoggedIn);
        }
    }
}

