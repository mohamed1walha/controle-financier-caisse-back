﻿using System;

namespace DDD.Services.Api.Controllers
{
    public enum APICustomReturnType
    {
        DataNotFound = 1,
        NotAuthorized = 2,
        UserNotLoggedIn = 3,
        ForeignKeyConflict = 4,
        UserClaimNotFound = 5,
        SystemException = 6,
        AlreadyAssigned = 7,
        ErrorSendMail = 8,
        PasswordResetIsNeeded = 9,
        FilterTypeNotRecognized = 10,
        ModuleIdNotFound = 11,
        AgencyCodeNotFound = 12,
        RequiredField = 13,
        CodeAlreadyExist = 14,
        VoucherCannotBeCreated = 15,
        CurrencyMatch = 16,
        UndefinedSerie = 17,
        currencyCodeNotFound = 18,
        VoucherNotFound = 19,
        VoucherTypeNotFound = 20,
        VoucherDetailsCannotBeCreated = 21,
        VoucherDetailsCannotBeUpdated = 22,
        VoucherNotTaxable = 23,
        VoucherCannotBeUpdated = 24,
        ReferenceFileNotFound = 25,
        FileNotFound = 26,
        UserNameNotFound = 27,
    }
    public class APICustomException : Exception
    {

        public APICustomReturnType ErrorType { get; set; }
        public object CData { get; set; }

        public APICustomException(string Message, APICustomReturnType ErrorType, Exception ex, object Data = null) : base(Message, ex)
        {
            this.ErrorType = ErrorType;
            this.CData = Data;
        }
        public APICustomException(string Message, APICustomReturnType ErrorType, object Data = null) : base(Message)
        {
            this.CData = Data;
            this.ErrorType = ErrorType;
        }

        public APICustomException(APICustomReturnType ErrorType, object Data = null)
        {
            this.ErrorType = ErrorType;
            this.CData = Data;
        }

        public APICustomException(string Message, Exception ex = null) : base(Message, ex)
        {

        }
    }
    public class ForeighKeyConflictCustomException : APICustomException
    {
        public ForeighKeyConflictCustomException(string Message) : base(Message, APICustomReturnType.ForeignKeyConflict)
        {
        }
    }

}
