﻿using System;
using System.Security.Claims;

namespace DDD.Services.Api.Controllers
{
    public static class ClaimsTools
    {
        /// <summary>
        /// Considers the user logged --> throws exception if not
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static string GetFirstClaimValue(ClaimsPrincipal user, string claimType)
        {
            string claimValue = user.FindFirst(claimType)?.Value;
            return claimValue;
        }

        public static string GetLoggedUserName(ClaimsPrincipal user)
        {
            string userName = user.FindFirst(ClaimTypes.Name)?.Value;
            if (String.IsNullOrEmpty(userName)) throw new APICustomException(APICustomReturnType.UserNotLoggedIn);
            return userName.ToUpper();

        }

        public static int GetLoggedUserId(ClaimsPrincipal user)
        {
            string userName = user.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            if (String.IsNullOrEmpty(userName)) throw new APICustomException(APICustomReturnType.UserNotLoggedIn);
            return Convert.ToInt32(userName);

        }

    }
}