﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DDD.Application.Interfaces;
using DTO;
using DDD.Domain.Core.Bus;
using DDD.Domain.Core.Notifications;
using DTO;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DDD.Services.Api.Controllers
{
    [Authorize]
    public class ModePaiementController : GenericController
    {
        private readonly IModePaiementService<ModepaieDTO> _modePaiementService;

        public ModePaiementController(
              IModePaiementService<ModepaieDTO> modePaiementService)
        {
            _modePaiementService = modePaiementService;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("modePaiement/getALL")]
        public async Task<IEnumerable<ModepaieDTO>> GetALL()
        {
            return await _modePaiementService.GetALLModePaie();
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("modePaiement/Edit")]
        public async Task<ModepaieDTO> Edit([FromBody] ModepaieDTO modepaie)
        {
            await _modePaiementService.Update(modepaie);
            return modepaie;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("modePaiement/GetById")]
        public async Task<ModepaieDTO> GetById(int id)
        {
            return await _modePaiementService.GetById(id);
        }

        [HttpDelete]
        [AllowAnonymous]
        [Route("modePaiement/Delete")]
        public async Task<int> Delete(int id)
        {
            return await _modePaiementService.Delete(id);
        }

        //[HttpGet]
        //[AllowAnonymous]
        //[Route("modePaiement/LastSyncModePaie")]
        //public async Task<DateTime> LastSyncModePaie()
        //{
        //    return await _modePaiementService.LastSyncModePaie();
        //}


    }
}
