﻿using DTO;
using DDD.Domain.Interfaces;
using Entity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DDD.Services.Api.Controllers
{
    [Authorize]
    [ApiController]
    public class CoffreController : GenericController
    {
        private readonly ICoffreService<CoffreDTO> _coffreService;

        public CoffreController(
             ICoffreService<CoffreDTO> coffreService)
        {
            _coffreService = coffreService;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/Coffre/GetAll")]
        public async Task<List<CoffreDTO>> GetAll()
        {
            return await _coffreService.GetAllCoffre();
        }
        [HttpPost]
        [AllowAnonymous]
        [Route("/Coffre/GetAllByModePaie")]
        public async Task<List<CoffreDTO>> GetAllByModePaie([FromBody] List<int> modePaies)
        {
            return await _coffreService.GetAllByModePaie(modePaies);
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("/Coffre/Create")]
        public async Task<OperationResult> Create(CoffreDTO coffreDTO)
        {
            return await _coffreService.CreateCoffre(coffreDTO);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/Coffre/GetById")]
        public async Task<CoffreDTO> GetById(int id)
        {
            return await _coffreService.GetById(id);
        }

        [HttpPut]
        [AllowAnonymous]
        [Route("/Coffre/Update")]
        public async Task<CoffreDTO> Update(CoffreDTO CoffreDTO)
        {
            return await _coffreService.Update(CoffreDTO);
        }

        [HttpDelete]
        [AllowAnonymous]
        [Route("/Coffre/Delete")]
        public async Task<int> Delete(int id)
        {
            return await _coffreService.Delete(id);
        }

    }
}
