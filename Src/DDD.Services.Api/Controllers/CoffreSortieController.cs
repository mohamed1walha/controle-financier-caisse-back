﻿using DTO;
using DDD.Domain.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DDD.Application.Services;
using DDD.Application.utils;
using Entity;

namespace DDD.Services.Api.Controllers
{
    [Authorize]
    [ApiController]
    public class CoffreSortieController : GenericController
    {
        private readonly ICoffreSortieService<CoffresortieDTO> _CoffreSortieService;
        private readonly IBilletDetailSortieService<BilletDetailSortieDTO> _BilletDetailSortieService;

        public CoffreSortieController(
             ICoffreSortieService<CoffresortieDTO> CoffreSortieService, IBilletDetailSortieService<BilletDetailSortieDTO> billetDetailSortieService)
        {
            _CoffreSortieService = CoffreSortieService;
            _BilletDetailSortieService = billetDetailSortieService;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/CoffreSortie/GetAll")]
        public async Task<List<CoffresortieDTO>> GetAll()
        {
            return await _CoffreSortieService.GetAllSorties();
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("/CoffreSortie/Create")]
        public async Task<OperationResult> Create(CoffresortieDTO CoffreSortieDTO)
        {
            var res= await _CoffreSortieService.CreateSortie(CoffreSortieDTO);

            if (res.Data != null && CoffreSortieDTO.billetDetailSortieDTOs.Count != 0)
            {
                if(CoffreSortieDTO.Id == 0)
                {
                    CoffresortieDTO sortie = res.Data as CoffresortieDTO;


                    CoffreSortieDTO.billetDetailSortieDTOs.ForEach(async ligne =>
                    {
                        ligne.SortieId = sortie.Id;
                    });
                    await _BilletDetailSortieService.CreateList(CoffreSortieDTO.billetDetailSortieDTOs);

                }
                else if (CoffreSortieDTO.Id > 0)
                {
                    Coffresortie sortie = res.Data as Coffresortie;
                    var allCalculetteBySortie = await _BilletDetailSortieService.GetAllBySortieId(sortie.Id);
                    
                    List<BilletDetailSortieDTO> allUpdatedLigne = new List<BilletDetailSortieDTO>();
                    List<BilletDetailSortieDTO> allCreatedLigne = new List<BilletDetailSortieDTO>();
                    CoffreSortieDTO.billetDetailSortieDTOs.ForEach(async ligne =>
                    {
                        var ligneSortieDataBase = allCalculetteBySortie.Find(x => x.BilletId == ligne.BilletId);
                        if (ligneSortieDataBase != null)
                        {
                            ligne.SortieId = sortie.Id;
                            ligne.Id = ligneSortieDataBase.Id;
                            allUpdatedLigne.Add(ligne);
                        }
                        else
                        {
                            ligne.SortieId = sortie.Id;
                            allCreatedLigne.Add(ligne);
                        }
                    });


                    await _BilletDetailSortieService.UpdateList(allUpdatedLigne);
                    await _BilletDetailSortieService.CreateList(allCreatedLigne);
                }
            }
            return res;
        }
      
        [HttpGet("/CoffreSortie/getDocument/{id}")]
        [AllowAnonymous]

        public async Task<FileStreamResult> getDocument(int id)
        {
                return await _CoffreSortieService.getDocument(id);
        }

        [HttpDelete]
        [AllowAnonymous]
        [Route("/CoffreSortie/Delete")]
        public async Task<int?> Delete(int id)
        {

            return await _CoffreSortieService.DeleteSortie(id);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/CoffreSortie/GetById")]
        public async Task<CoffresortieDTO> GetById(int id)
        {
            return await _CoffreSortieService.GetById(id);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/CoffreSortie/GetByCoffreID/{coffreId}")]
        public async Task<List<CoffresortieDTO>> GetByCoffreId(int coffreId)
        {
            var res = await _CoffreSortieService.GetByCoffreId(coffreId);
            return res;
        }
    }
}
