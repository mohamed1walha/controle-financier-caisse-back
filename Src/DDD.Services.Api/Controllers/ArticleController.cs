﻿using DTO;
using DDD.Domain.Interfaces;
using Entity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DDD.Services.Api.Controllers
{
    [Authorize]
    [ApiController]
    public class ArticleController : GenericController
    {
        private readonly IArticleService<ArticleDTO> _ArticleService;

        public ArticleController(
             IArticleService<ArticleDTO> ArticleService)
        {
            _ArticleService = ArticleService;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/Article/GetAll")]
        public async Task<List<ArticleDTO>> GetAll()
        {
            return await _ArticleService.GetAll();
        }   
        [HttpGet]
        [AllowAnonymous]
        [Route("/Article/GetAllWithControlSolde")]
        public async Task<List<ArticleDTO>> GetAllWithControlSolde()
        {
            return await _ArticleService.GetAllWithControlSolde();
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("/Article/Create")]
        public async Task<ArticleDTO> Create(ArticleDTO ArticleDTO)
        {
            return await _ArticleService.Create(ArticleDTO);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/Article/GetById")]
        public async Task<ArticleDTO> GetById(int id)
        {
            return await _ArticleService.GetById(id);
        }

        [HttpPut]
        [AllowAnonymous]
        [Route("/Article/Update")]
        public async Task<ArticleDTO> Update(ArticleDTO ArticleDTO)
        {
            return await _ArticleService.Update(ArticleDTO);
        }

        [HttpDelete]
        [AllowAnonymous]
        [Route("/Article/Delete")]
        public async Task<int> Delete(int id)
        {
            return await _ArticleService.Delete(id);
        }

    }
}
