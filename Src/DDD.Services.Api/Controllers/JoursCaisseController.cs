using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DDD.Application.Interfaces;
using DTO;
using DDD.Domain.Core.Bus;
using DDD.Domain.Core.Notifications;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Globalization;

namespace DDD.Services.Api.Controllers
{
    [Authorize]
    [ApiController]
    public class JoursCaisseController : GenericController
    {

        private readonly IJoursCaisseService<JoursCaiseDTO> _joursCaisseService;

        public JoursCaisseController(
             IJoursCaisseService<JoursCaiseDTO> joursCaisseService)
        {
            _joursCaisseService = joursCaisseService;
        }

        [HttpPost]
        [Authorize]
        [Route("/JoursCaisse/Create")]
        public async Task<JoursCaiseDTO> Create(JoursCaiseDTO joursCaiseDTO)
        {
            if (joursCaiseDTO.Id == 0)
            {
                joursCaiseDTO.IsManuel = true;
                joursCaiseDTO.StatusComtage = 5;
                joursCaiseDTO.StatusReception = 1;
                joursCaiseDTO.GjcDateouv = joursCaiseDTO.GjcDateouv.AddHours(1);
                return await _joursCaisseService.Create(joursCaiseDTO);
            }
            else
            {
                return await _joursCaisseService.Edit(joursCaiseDTO, joursCaiseDTO.Id);
            }

        }

        [HttpPost]
        [Authorize]
        [Route("/JoursCaisse/Edit")]
        public async Task<JoursCaiseDTO> Edit(JoursCaiseDTO joursCaiseDTO)
        {
            return await _joursCaisseService.Edit(joursCaiseDTO, joursCaiseDTO.Id);
        }

        [HttpPost]
        [Authorize]
        [Route("/JoursCaisse/GetAll")]
        public async Task<List<JoursCaiseDTO>> Get(string date)
        {
            var dateTime = DateTime.Parse(date);
            return await _joursCaisseService.GetAllByDate(dateTime);
        }
        [HttpPost]
        [Authorize]
        [Route("/JoursCaisse/GetAllByStatusCmptNone")]
        public async Task<List<JoursCaiseDTO>> GetAllByStatusCmptNone()
        {
            //var CurrentUserId = ClaimsTools.GetLoggedUserId(User);
            GetCurrentUserIdFromClaim();
            return await _joursCaisseService.GetAllByStatusCmptNone(currentUserId);
        }


        [HttpPost]
        [Authorize]
        [Route("/JoursCaisse/FiltreGetAllByStatusCmptNone")]
        public async Task<List<JoursCaiseDTO>> FiltreGetAllByStatusCmptNone(string dateOuverture, string dateOuvertureTo)
        {
            GetCurrentUserIdFromClaim();
            DateTime? startDate = null;
            DateTime? endDate = null;
            DateTime startDateOut;
            DateTime endDateOut;

            if (!string.IsNullOrEmpty(dateOuverture))
            {

                bool success = DateTime.TryParseExact(dateOuverture, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out startDateOut);

                if (success)
                {
                    startDate = startDateOut;

                }
                else
                {
                    startDate = DateTime.ParseExact(dateOuverture, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                }

            }

            if (!string.IsNullOrEmpty(dateOuvertureTo))
            {
                bool success = DateTime.TryParseExact(dateOuvertureTo, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out endDateOut);

                if (success)
                {
                    endDate = endDateOut;
                }
                else
                {
                    endDate = DateTime.ParseExact(dateOuvertureTo, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }

            }
            return await _joursCaisseService.FiltreGetAllByStatusCmptNone(startDate,endDate,currentUserId);
        }




        [HttpDelete]
        [Authorize]
        [Route("/JoursCaisse/Delete")]
        public async Task<int> Delete(int id)
        {
            return await _joursCaisseService.Delete(id);
        }



        [HttpPut]
        [Authorize]
        [Route("/JoursCaisse/Update")]
        public async Task<JoursCaiseDTO> Update(JoursCaiseDTO joursCaisse)
        {
            JoursCaiseDTO j = new JoursCaiseDTO
            {
                Id = joursCaisse.Id,
                AgentComptage = joursCaisse.AgentComptage,
                CahuffeurId = joursCaisse.CahuffeurId,
                Commentaire = joursCaisse.Commentaire,
                EmetteurId = joursCaisse.EmetteurId,
                GjcCaisse = joursCaisse.GjcCaisse,
                GjcCaisselabel = joursCaisse.GjcCaisselabel,
                GjcEtat = joursCaisse.GjcEtat,
                GjcEtablissement = joursCaisse.GjcEtablissement,
                GjcHeureferme = joursCaisse.GjcHeureferme,
                GjcHeureouv = joursCaisse.GjcHeureouv,
                GjcNumzcaisse = joursCaisse.GjcNumzcaisse,
                GjcUserferme = joursCaisse.GjcUserferme,
                MontantCaisse = joursCaisse.MontantCaisse,
                StatusComtage = joursCaisse.StatusComtage,
                GjcEtablissementcode = joursCaisse.GjcEtablissementcode,
                GjcEtablissementId = joursCaisse.GjcEtablissementId,
                StatusReception = joursCaisse.StatusReception,
                GjcEtablissementlabel = joursCaisse.GjcEtablissementlabel,
                DateComptage = joursCaisse.DateComptage,
                DateCreate = joursCaisse.DateCreate.Value.AddHours(1),
                GjcDateferme = DateTime.Parse(joursCaisse.GjcDateferme.ToString()).AddHours(1),
                GjcDateouv = joursCaisse.GjcDateouv.AddHours(1),
                ReceptionDate = DateTime.Parse(joursCaisse.ReceptionDate.ToString()).AddHours(1),
                ValidationDateDebut = joursCaisse.ValidationDateDebut,
                ValidationDateFin = joursCaisse.ValidationDateFin,
                MotifId = joursCaisse.MotifId,
                Comments = joursCaisse.Comments,
                IsManuel = true,
                RefExterne = joursCaisse.RefExterne
                
               
                
            };
            return await _joursCaisseService.Update(j);
        }
          [HttpPost]
        [Authorize]
        [Route("/JoursCaisse/ChangeStatusImprime")]
        public async Task<List<JoursCaiseDTO>> ChangeStatusImprime(List<JoursCaiseDTO> joursCaisse)
        {
            GetCurrentUserIdFromClaim();

            return await _joursCaisseService.ChangeStatusImprime(joursCaisse,currentUserId);
        }


        [HttpPost]
        [Authorize]
        [Route("/JoursCaisse/GetAllByStatusEnAttente")]
        public async Task<List<JoursCaiseDTO>> GetAllByStatusEnAttente()
        {
            GetCurrentUserIdFromClaim();
            return await _joursCaisseService.GetAllByStatusEnAttente(currentUserId);
        }

        [HttpPost]
        [Authorize]
        [Route("/JoursCaisse/GetAllByStatusReceptionne")]
        public async Task<List<JoursCaiseDTO>> GetAllByStatusReceptionne()
        {
            GetCurrentUserIdFromClaim();
            return await _joursCaisseService.GetAllByStatusReceptionne(currentUserId);
        }  
        [HttpPost]
        [Authorize]
        [Route("/JoursCaisse/GetAllByStatusReceptionneNonValide")]
        public async Task<List<JoursCaiseDTO>> GetAllByStatusReceptionneNonValide()
        {
            GetCurrentUserIdFromClaim();
            return await _joursCaisseService.GetAllByStatusReceptionneNonValide(currentUserId);
        }
        [HttpPost]
        [Authorize]
        [Route("/JoursCaisse/FiltreGetAllByStatusReceptionneNonValide")]
        public async Task<List<JoursCaiseDTO>> FiltreGetAllByStatusReceptionneNonValide(string dateOuverture, string dateOuvertureTo)
        {
            GetCurrentUserIdFromClaim();
            DateTime? startDate = null;
            DateTime? endDate = null;
            DateTime startDateOut;
            DateTime endDateOut;

            if (!string.IsNullOrEmpty(dateOuverture))
            {

                bool success = DateTime.TryParseExact(dateOuverture, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out startDateOut);

                if (success)
                {
                    startDate = startDateOut;

                }
                else
                {
                    startDate = DateTime.ParseExact(dateOuverture, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                }

            }

            if (!string.IsNullOrEmpty(dateOuvertureTo))
            {
                bool success = DateTime.TryParseExact(dateOuvertureTo, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out endDateOut);

                if (success)
                {
                    endDate = endDateOut;
                }
                else
                {
                    endDate = DateTime.ParseExact(dateOuvertureTo, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }

            }
            //return await _joursCaisseService.FiltreGetAllByStatusCmptNone(startDate, endDate, currentUserId);
            return await _joursCaisseService.FiltreGetAllByStatusReceptionneNonValide(startDate, endDate,currentUserId);
        }


        [HttpPost]
        [Authorize]
        [Route("/JoursCaisse/GetAllByStatusCompter")]
        public async Task<List<JoursCaiseDTO>> GetAllByStatusCompter()
        {
            GetCurrentUserIdFromClaim();
            return await _joursCaisseService.GetAllByStatusCompter(currentUserId);
        }

        [HttpPost]
        [Authorize]
        [Route("/JoursCaisse/FiltreGetAllByStatusCompter")]
        public async Task<List<JoursCaiseDTO>> FiltreGetAllByStatusCompter(string dateOuverture, string dateOuvertureTo)
        {
            GetCurrentUserIdFromClaim();
            DateTime? startDate = null;
            DateTime? endDate = null;
            DateTime startDateOut;
            DateTime endDateOut;

            if (!string.IsNullOrEmpty(dateOuverture))
            {

                bool success = DateTime.TryParseExact(dateOuverture, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out startDateOut);

                if (success)
                {
                    startDate = startDateOut;

                }
                else
                {
                    startDate = DateTime.ParseExact(dateOuverture, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                }

            }

            if (!string.IsNullOrEmpty(dateOuvertureTo))
            {
                bool success = DateTime.TryParseExact(dateOuvertureTo, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out endDateOut);

                if (success)
                {
                    endDate = endDateOut;
                }
                else
                {
                    endDate = DateTime.ParseExact(dateOuvertureTo, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }

            }
            //return await _joursCaisseService.FiltreGetAllByStatusCmptNone(startDate, endDate, currentUserId);
            return await _joursCaisseService.FiltreGetAllByStatusCompter(startDate,endDate, currentUserId);
        }


        [HttpPost]
        [Authorize]
        [Route("/JoursCaisse/GetAllByFilters")]
        public async Task<List<JoursCaiseDTO>> GetAllByFilters()
        {GetCurrentUserIdFromClaim();
            return await _joursCaisseService.GetAllByStatusCmptNone(currentUserId);
        }



        [HttpPost]
        [Authorize]
        [Route("/chargerJoursCaisse/CopieDatabase")]
        public async Task<OperationResult> CopieDatabase([FromBody] string[] etablissment ,string dateFermeF, string dateFermeT,string user)
        {
            return await _joursCaisseService.Charger(etablissment,dateFermeF, dateFermeT, user) ;
        } 
     
        [HttpPost]
        [Authorize]
        [Route("/chargerJoursCaisse/FiltreEnveloppeEnAttente")]
        public async Task<List<JoursCaiseDTO>> FiltreEnveloppeEnAttente(string dateOuverture, string dateOuvertureTo)
        {

            GetCurrentUserIdFromClaim();
              DateTime? startDate = null;
            DateTime? endDate = null;
            DateTime startDateOut ;
            DateTime endDateOut ;

            if (!string.IsNullOrEmpty(dateOuverture))
            {
              
                bool success = DateTime.TryParseExact(dateOuverture, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out startDateOut);

                if (success)
                {
                    startDate = startDateOut;

                }
                else {
                    startDate = DateTime.ParseExact(dateOuverture, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                }

            }

            if (!string.IsNullOrEmpty(dateOuvertureTo))
            {
                bool success = DateTime.TryParseExact(dateOuvertureTo, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out endDateOut);

                if (success)
                {
                    endDate = endDateOut;
                }
                else
                {
                    endDate = DateTime.ParseExact(dateOuvertureTo, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }

            }
            return await _joursCaisseService.FiltreEnveloppeEnAttente(startDate, endDate, currentUserId);
        }

        [HttpPost]
        [Authorize]
        [Route("/chargerJoursCaisse/FiltreEnveloppeReceptionne")]
        public async Task<List<JoursCaiseDTO>> FiltreEnveloppeReceptionne(string dateFermeFrom, string dateFermeTo)
        {

            GetCurrentUserIdFromClaim();
            DateTime? startDate = null;
            DateTime? endDate = null;
            DateTime startDateOut;
            DateTime endDateOut;

            if (!string.IsNullOrEmpty(dateFermeFrom))
            {

                bool success = DateTime.TryParseExact(dateFermeFrom, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out startDateOut);

                if (success)
                {
                    startDate = startDateOut;

                }
                else
                {
                    startDate = DateTime.ParseExact(dateFermeFrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                }

            }

            if (!string.IsNullOrEmpty(dateFermeTo))
            {
                bool success = DateTime.TryParseExact(dateFermeTo, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out endDateOut);

                if (success)
                {
                    endDate = endDateOut;
                }
                else
                {
                    endDate = DateTime.ParseExact(dateFermeTo, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }

            }
            return await _joursCaisseService.FiltreEnveloppeReceptionne(startDate, endDate, currentUserId);
        }

        [HttpGet]
        [Authorize]
        [Route("/chargerJoursCaisse/SearchByCode")]
        public async Task<JoursCaiseDTO> SearchByCode(int numZ, int codeEtab, string codeCaisse)
        {
            return await _joursCaisseService.SearchByCode(numZ, codeEtab, codeCaisse);
        }

        [HttpGet]
        [Authorize]
        [Route("/chargerJoursCaisse/GetAllZByEtablissement")]
        public async Task<List<int>> GetAllZByEtab(int etabId)
        {
            return await _joursCaisseService.GetAllZByEtab(etabId);
        }

        [HttpGet]
        [Authorize]
        [Route("/chargerJoursCaisse/GetAllCaisseByEtablissement")]
        public async Task<List<string>> GetAllCaisseByEtab(int etabId)
        {
            return await _joursCaisseService.GetAllCaisseByEtab(etabId);
        }

        [HttpGet]
        [Authorize]
        [Route("/chargerJoursCaisse/GetById")]
        public async Task<JoursCaiseDTO> GetById(int id)
        {
            var res= await _joursCaisseService.GetFullById(id);
            return res;
        }

        [HttpPost]
        [Authorize]
        [Route("/chargerJoursCaisse/ReceptionEnveloppes")]
        public async Task<List<JoursCaiseDTO>> ReceptionEnveloppes(List<JoursCaiseDTO> enveloppes, int chauffeurId, int emetteurId)
        {
            GetCurrentUserIdFromClaim();

            return await _joursCaisseService.ReceptionEnveloppes(enveloppes, chauffeurId, emetteurId,currentUserId);
        }

        [HttpPost]
        [Authorize]
        [Route("/chargerJoursCaisse/ReceptionReSyncEnveloppes")]
        public async Task<List<JoursCaiseDTO>> ReceptionReSyncEnveloppes(List<JoursCaiseDTO> enveloppes, int motifId, string comments)
        {
            GetCurrentUserIdFromClaim();
            return await _joursCaisseService.ReceptionReSyncEnveloppes(enveloppes, motifId, comments,currentUserId);
        }

        [HttpGet]
        [Authorize]
        [Route("/chargerJoursCaisse/StatDashboard")]
        public async Task<StatsDTO> GetStatDashboard()
        {
            GetCurrentUserIdFromClaim();

            return await _joursCaisseService.GetStatDashboard(currentUserId);
        }


        [HttpPost]
        [Authorize]
        [Route("/chargerJoursCaisse/Report")]
        public async Task<List<ReportDTO>> GenerateReport(string dateOuverture, string dateOuvertureTo, int etablissementId)
        {
            GetCurrentUserIdFromClaim();
            DateTime? startDate = null;
            DateTime? endDate = null;
            DateTime startDateOut ;
            DateTime endDateOut ;

            if (!string.IsNullOrEmpty(dateOuverture))
            {
              
                bool success = DateTime.TryParseExact(dateOuverture, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out startDateOut);

                if (success)
                {
                    startDate = startDateOut;

                }
                else {
                    startDate = DateTime.ParseExact(dateOuverture, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                }

            }

            if (!string.IsNullOrEmpty(dateOuvertureTo))
            {
                bool success = DateTime.TryParseExact(dateOuvertureTo, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out endDateOut);

                if (success)
                {
                    endDate = endDateOut;
                }
                else
                {
                    endDate = DateTime.ParseExact(dateOuvertureTo, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }

            }

            return await _joursCaisseService.GenerateReport(startDate, endDate, etablissementId,currentUserId);
        }
        [HttpPost]
        [Route("/chargerJoursCaisse/ReportDetailsBillet")]
        public async Task<List<ReportBilletDTO>> GenerateReportDetailsBillet([FromBody] ReportBilletRequestDTO reportBilletsDTO)
        {
            DateTime? startDate = null;
            DateTime? endDate = null;
            DateTime startDateOut ;
            DateTime endDateOut ;

            if (!string.IsNullOrEmpty(reportBilletsDTO.StartDate))
            {
              
                bool success = DateTime.TryParseExact(reportBilletsDTO.StartDate, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out startDateOut);

                if (success)
                {
                    startDate = startDateOut;

                }
                else {
                    startDate = DateTime.ParseExact(reportBilletsDTO.StartDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                }

            }

            if (!string.IsNullOrEmpty(reportBilletsDTO.EndDate))
            {
                bool success = DateTime.TryParseExact(reportBilletsDTO.EndDate, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out endDateOut);

                if (success)
                {
                    endDate = endDateOut;
                }
                else
                {
                    endDate = DateTime.ParseExact(reportBilletsDTO.EndDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }

            }

            return await _joursCaisseService.GenerateReportDetailsBillet(startDate, endDate, reportBilletsDTO);
        }
    }
}
