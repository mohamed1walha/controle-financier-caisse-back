﻿using DTO;
using DDD.Domain.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace DDD.Services.Api.Controllers
{
    [Authorize]
    [ApiController]
    public class StatusController : GenericController
    {
        private readonly IStatusService<StatusDTO> _StatusService;

        public StatusController(
             IStatusService<StatusDTO> StatusService)
        {
            _StatusService = StatusService;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/Status/GetAll")]
        public async Task<List<StatusDTO>> GetAll()
        {
            List<StatusDTO> all = new List<StatusDTO>();
            all = await _StatusService.GetAll();
            all = all.OrderBy(status => status.Statuslibelle).ToList();
            return all;
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("/Status/Create")]
        public async Task<StatusDTO> Create(StatusDTO statusDTO)
        {
            return await _StatusService.Create(statusDTO);
        }
        [HttpPost]
        [AllowAnonymous]
        [Route("/Status/checkStatus")]  
        public async Task<StatusDTO> checkStatusJoursCaisse(int id)
        {
            GetCurrentUserIdFromClaim();
            return await _StatusService.checkStatus(id,currentUserId);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/Status/GetById")]
        public async Task<StatusDTO> GetById(int id)
        {
            return await _StatusService.GetById(id);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/Status/GetByCode")]
        public async Task<StatusDTO> GetByCode(string code)
        {
            return await _StatusService.GetByCode(code);
        }

        [HttpPut]
        [AllowAnonymous]
        [Route("/Status/Update")]
        public async Task<StatusDTO> Update(StatusDTO statusDTO)
        {
            return await _StatusService.Update(statusDTO);
        }

        [HttpDelete]
        [AllowAnonymous]
        [Route("/Status/Delete")]
        public async Task<int> Delete(int id)
        {
            return await _StatusService.Delete(id);
        }

    }
}
