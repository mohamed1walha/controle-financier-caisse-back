﻿using DTO;
using DDD.Domain.Interfaces;
using Entity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DDD.Services.Api.Controllers
{
    [Authorize]
    [ApiController]
    public class BanqueController : GenericController
    {
        private readonly IBanqueService<BanqueDTO> _BanqueService;

        public BanqueController(
             IBanqueService<BanqueDTO> BanqueService)
        {
            _BanqueService = BanqueService;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/Banque/GetAll")]
        public async Task<List<BanqueDTO>> GetAll()
        {
            return await _BanqueService.GetAll();
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("/Banque/Create")]
        public async Task<BanqueDTO> Create(BanqueDTO BanqueDTO)
        {
            return await _BanqueService.Create(BanqueDTO);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/Banque/GetById")]
        public async Task<BanqueDTO> GetById(int id)
        {
            return await _BanqueService.GetById(id);
        }

        [HttpPut]
        [AllowAnonymous]
        [Route("/Banque/Update")]
        public async Task<BanqueDTO> Update(BanqueDTO BanqueDTO)
        {
            return await _BanqueService.Update(BanqueDTO);
        }

        [HttpDelete]
        [AllowAnonymous]
        [Route("/Banque/Delete")]
        public async Task<int> Delete(int id)
        {
            return await _BanqueService.Delete(id);
        }

    }
}
