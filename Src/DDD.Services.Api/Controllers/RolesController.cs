﻿using DTO;
using DDD.Domain.Interfaces;
using Entity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DDD.Services.Api.Controllers
{
    [Authorize]
    [ApiController]
    public class RolesController : GenericController
    {
        private readonly IRolesService<RolesDTO> _rolesService;

        public RolesController(
             IRolesService<RolesDTO> rolesService)
        {
            _rolesService = rolesService;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/Roles/GetAll")]
        public async Task<List<RolesDTO>> GetAll()
        {
            return await _rolesService.GetAll();
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("/Roles/Create")]
        public async Task<RolesDTO> Create(RolesDTO roles)
        {
            return await _rolesService.Create(roles);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/Roles/GetById")]
        public async Task<RolesDTO> GetById(int id)
        {
            return await _rolesService.GetById(id);
        }

        [HttpPut]
        [AllowAnonymous]
        [Route("/Roles/Update")]
        public async Task<RolesDTO> Update(RolesDTO roles)
        {
            return await _rolesService.Update(roles);
        }

        [HttpDelete]
        [AllowAnonymous]
        [Route("/Roles/Delete")]
        public async Task<int> Delete(int id)
        {
            return await _rolesService.Delete(id);
        }

    }
}
