﻿using DTO;
using DDD.Domain.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DDD.Services.Api.Controllers
{
    [Authorize]
    [ApiController]
    public class EmetteurController : GenericController
    {
        private readonly IEmetteurService<EmetteurDTO> _EmetteurService;

        public EmetteurController(
             IEmetteurService<EmetteurDTO> EmetteurService)
        {
            _EmetteurService = EmetteurService;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/Emetteur/GetAll")]
        public async Task<List<EmetteurDTO>> GetAll()
        {
            return await _EmetteurService.GetAll();
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("/Emetteur/Create")]
        public async Task<EmetteurDTO> Create(EmetteurDTO emetteurDTO)
        {
            return await _EmetteurService.Create(emetteurDTO);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("/Emetteur/GetById")]
        public async Task<EmetteurDTO> GetById(int id)
        {
            return await _EmetteurService.GetById(id);
        }

        [HttpPut]
        [AllowAnonymous]
        [Route("/Emetteur/Update")]
        public async Task<EmetteurDTO> Update(EmetteurDTO emetteurDTO)
        {
            return await _EmetteurService.Update(emetteurDTO);
        }

        [HttpDelete]
        [AllowAnonymous]
        [Route("/Emetteur/Delete")]
        public async Task<int> Delete(int id)
        {
            return await _EmetteurService.Delete(id);
        }

    }
}
