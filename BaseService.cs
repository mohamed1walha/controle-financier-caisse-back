﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using NHibernate.Cache;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace ERP.Common.Classes.Services
{
    public abstract class BaseService<TEntityDTO, TEntity, DataBaseContext> : IBaseService<TEntityDTO>
        where TEntityDTO : class
        where TEntity : class
        where DataBaseContext : DbContext
    {
        protected ConcreteGenericBaseRepository<TEntity, DataBaseContext> genericBaseRepository;
        protected IMemoryCache _cache;
        protected void SetDbContext(DbContext dbContext = null)
        {
            if (dbContext != null)
                genericBaseRepository.SetDbContext(dbContext);
        }
        protected BaseService(DataBaseContext context)
        {
            genericBaseRepository = new ConcreteGenericBaseRepository<TEntity, DataBaseContext>(context);
        }

        protected BaseService(DataBaseContext context, IMemoryCache cache)
        {
            genericBaseRepository = new ConcreteGenericBaseRepository<TEntity, DataBaseContext>(context);
            _cache = cache;
        }

        public async Task<TEntityDTO> Create(TEntityDTO dto, DbContext dbContext = null)
        {
            SetDbContext(dbContext);
            var entity = dto.MapTo<TEntity>();
            entity = await genericBaseRepository.Create(entity);
            return entity.MapTo<TEntityDTO>();
        }



        public async Task<List<TEntityDTO>> CreateList(List<TEntityDTO> dto, DbContext dbContext = null)
        {
            SetDbContext(dbContext);
            var entity = dto.MapTo<List<TEntity>>();
            foreach (TEntity e in entity)
            {
                await genericBaseRepository.Create(e);
            }

            return entity.MapTo<List<TEntityDTO>>();
        }

        public async Task<int> Delete(object id, DbContext dbContext = null)
        {
            SetDbContext(dbContext);
            return await genericBaseRepository.Delete(id);

        }

        public async Task<int> ExecuteQuery(string queryString, DbContext dbContext = null)
        {
            SetDbContext(dbContext);
            return await genericBaseRepository.ExecuteQuery(queryString);
        }

        public async Task<TEntityDTO> Edit(TEntityDTO dto, object id, DbContext dbContext = null)
        {
            SetDbContext(dbContext);
            var entity = await genericBaseRepository.GetById(id);

            entity = AutoMapper.Mapper.Map(dto, entity);

            entity = await genericBaseRepository.Edit(entity);

            return entity.MapTo<TEntityDTO>();
        }



        public async Task<DataSourceResult> GetAll(DataSourceRequest requestModel, DbContext dbContext = null)
        {
            SetDbContext(dbContext);
            var intermidiateResult = await genericBaseRepository.GetAll(requestModel);

            return await ManageGroupings<TEntityDTO>(intermidiateResult, requestModel, false);
        }


        public async Task<DataSourceResult> ManageGroupings<T>(DataSourceResult unGroupeData, DataSourceRequest requestModel, bool isSqlRequest = false, DbContext dbContext = null)
        {
            SetDbContext(dbContext);
            //apply the cast
            unGroupeData.Data = unGroupeData.Data.MapTo<List<T>>();
            //only one page
            if (!isSqlRequest || ((requestModel.Page - 1) * (requestModel.PageSize)) > unGroupeData.Total)
                requestModel.Page = 1;

            var lastResult = await unGroupeData.Data.ToDataSourceResultAsync(requestModel);
            if (!isSqlRequest)
            {
                lastResult.Total = unGroupeData.Total;
                lastResult.AggregateResults = unGroupeData.AggregateResults;
                lastResult.Errors = unGroupeData.Errors;
            }

            return lastResult;
        }


        public async Task<DataSourceResult> GetAll<ResultType>(DataSourceRequest requestModel, DbContext dbContext = null)
        {
            SetDbContext(dbContext);
            var intermidiateResult = await genericBaseRepository.GetAll(requestModel);
            return await ManageGroupings<ResultType>(intermidiateResult, requestModel, false);
        }

        public async Task<TEntityDTO> GetById(object id, DbContext dbContext = null)
        {
            SetDbContext(dbContext);
            var entity = await genericBaseRepository.GetById(id);
            return entity.MapTo<TEntityDTO>();
        }


        public async Task<int> DeleteMany(List<object> idsToDelete, DbContext dbContext = null)
        {
            SetDbContext(dbContext);
            var DeletedCount = await genericBaseRepository.DeleteMany(idsToDelete.Cast<object>().ToList());
            return DeletedCount;
        }



        public async Task<List<TEntityDTO>> GetAll(DbContext dbContext = null)
        {
            SetDbContext(dbContext);
            var result = await genericBaseRepository.GetAll();
            return result.MapTo<List<TEntityDTO>>();
        }

        public SaveImageDTO SaveImage(string photo, string folder)
        {
            string GeneratedName = Guid.NewGuid().ToString(); // will generate an unique name
            string extension = photo["data:image/".Length..photo.IndexOf(";base64")]; //getting extension of image eg: png
            string convert = photo.Replace("data:image/" + extension + ";base64,", String.Empty); //strip out header from base 64 image

            byte[] Convert_bytes = Convert.FromBase64String(convert); // convert base64 to byte

            Image image;

            using (MemoryStream ms = new MemoryStream(Convert_bytes)) //convert byte to image
            {
                image = Image.FromStream(ms);
            }

            using (var bitmap = new Bitmap(image)) // save image in folder 
            {
                switch (extension.ToLower())
                {
                    case "jpeg": bitmap.Save(folder + "/" + GeneratedName + "." + ImageFormat.Jpeg); break;

                    case "png": bitmap.Save(folder + "/" + GeneratedName + "." + ImageFormat.Png); break;

                }
            }

            var saveImage = new SaveImageDTO
            {
                ImageUrl = GeneratedName + "." + extension
            };

            return saveImage;


        }

        public async Task<List<TEntityDTO>> ReadDataFromQueryAsync(string queryString, DbContext dbContext = null)
        {
            SetDbContext(dbContext);
            var res = await genericBaseRepository.ReadDataFromQueryAsync<TEntity>(queryString);
            return res.MapTo<List<TEntityDTO>>();
        }

        #region EntityWithTranslation
        public async Task<TEntityDTO> CreateWithTranslate(TEntityDTO dto, TranslateCreatorEntityDTO translateCreatorEntityDto, string language, DbContext dbContext = null)
        {
            SetDbContext(dbContext);
            var entity = dto.MapTo<TEntity>();
            entity = await genericBaseRepository.Create(entity);
            //prepare translator
            GenericTranslationEntity genericTranslationEntity = new GenericTranslationEntity();
            string idColumnValue = typeof(TEntity).GetProperty(translateCreatorEntityDto.IdColumnName).GetValue(entity).ToString();
            genericTranslationEntity.FieldCode = idColumnValue;
            genericTranslationEntity.LanguageCode = language;
            genericTranslationEntity = await PrepareTranslator<TEntity>(entity, translateCreatorEntityDto, genericTranslationEntity, dbContext);
            int i = await InsertTranslationDataFromQueryAsync(translateCreatorEntityDto.TranslationTableName, genericTranslationEntity);
            return entity.MapTo<TEntityDTO>();
        }
        public async Task<TEntityDTO> EditWithTranslation(TEntityDTO dto, TranslateCreatorEntityDTO translateCreatorEntityDto, object id, string language, DbContext dbContext = null)
        {

            Dictionary<string, string> dict = new Dictionary<string, string>();
            SetDbContext(dbContext);
            var entity = await genericBaseRepository.GetById(id);

            foreach (var field in translateCreatorEntityDto.TranslatedColumnNames)
            {
                dict.Add(field, typeof(TEntity).GetProperty(field).GetValue(entity).ToString());
            }

            var entityMap = AutoMapper.Mapper.Map(dto, entity);
            foreach (var d in dict)
            {
                PropertyInfo nameFields = typeof(TEntity).GetProperty(d.Key);
                nameFields.SetValue(entityMap, d.Value);
            }
            entityMap = await genericBaseRepository.Edit(entityMap);


            entity = await genericBaseRepository.GetById(id);
            var entityTranslator = AutoMapper.Mapper.Map(dto, entity);
            //prepare translator 
            GenericTranslationEntity genericTranslationEntity = new GenericTranslationEntity();
            GenericTranslationEntity genericTranslationEntityToInsertOrEdit = new GenericTranslationEntity();
            string idColumnValue = typeof(TEntity).GetProperty(translateCreatorEntityDto.IdColumnName).GetValue(entityTranslator).ToString();
            //verify if translator is created 
            genericTranslationEntity = await ReadTranslationDataFromQueryAsync(translateCreatorEntityDto.TranslationTableName, idColumnValue, CultureInfo.CurrentCulture.Name);

            genericTranslationEntityToInsertOrEdit.FieldCode = idColumnValue;
            genericTranslationEntityToInsertOrEdit.LanguageCode = language;
            genericTranslationEntityToInsertOrEdit = await PrepareTranslator<TEntity>(entityTranslator, translateCreatorEntityDto, genericTranslationEntityToInsertOrEdit, dbContext);
            if (genericTranslationEntity == null)
            {
                // insert translator table
                int i = await InsertTranslationDataFromQueryAsync(translateCreatorEntityDto.TranslationTableName, genericTranslationEntityToInsertOrEdit, dbContext);
            }
            else
            {
                //update translator table    
                genericTranslationEntityToInsertOrEdit.Id = genericTranslationEntity.Id;
                int j = await UpdateTranslationDataFromQueryAsync(translateCreatorEntityDto.TranslationTableName, genericTranslationEntityToInsertOrEdit);
            }
            return entityMap.MapTo<TEntityDTO>();
        }

        public async Task<List<TEntityDTO>> GetAllWithTranslation(TranslateEntityDTO translateEntityDTO, DbContext dbContext = null)
        {
            SetDbContext(dbContext);
            var intermidiateResult = await genericBaseRepository.GetAll();
            List<TEntityDTO> entityListDTO = intermidiateResult.MapTo<List<TEntityDTO>>();
            var translatedResult = await Translate(entityListDTO, translateEntityDTO, dbContext);
            return translatedResult.MapTo<List<TEntityDTO>>();
        }
        public async Task<DataSourceResult> GetAllWithTranslation(DataSourceRequest requestModel, TranslateEntityDTO translateEntityDTO, DbContext dContext = null)
        {
            SetDbContext(dContext);
            var intermidiateResult = await genericBaseRepository.GetAll(requestModel, dContext);
            List<TEntityDTO> entityListDTO = intermidiateResult.Data.MapTo<List<TEntityDTO>>();
            var translatedResult = await Translate(entityListDTO, translateEntityDTO, dContext);
            intermidiateResult.Data = translatedResult;
            return await ManageGroupings<TEntityDTO>(intermidiateResult, requestModel, false);
        }

        public async Task<TEntityDTO> GetByIdWithTranslation(int id, TranslateEntityDTO translateEntityDTO, DbContext dbContext = null)
        {
            SetDbContext(dbContext);
            var intermidiateResult = await genericBaseRepository.GetById(id);
            TEntityDTO entityDTO = intermidiateResult.MapTo<TEntityDTO>();
            var translatedResult = await TranslateById(entityDTO, translateEntityDTO, dbContext);
            return translatedResult.MapTo<TEntityDTO>();
        }
        private async Task<List<GenericTranslationEntity>> ReadTranslationDataFromQueryAsync(string tableName, string language, DbContext dbContext = null)
        {
            SetDbContext(dbContext);
            string query = "select * from " + tableName + " where LanguageCode =  '" + language + "'";
            var res = await genericBaseRepository.ReadDataFromQueryAsync<GenericTranslationEntity>(query);
            return res;
        }
        private async Task<GenericTranslationEntity> GetByFieldCodeTranslationDataFromQueryAsync(string tableName, string language, string fieldCode, DbContext dbContext = null)
        {
            SetDbContext(dbContext);
            string query = "select * from " + tableName + " where LanguageCode =  '" + language + "' and FieldCode ='" + fieldCode + "'";
            var res = await genericBaseRepository.ReadDataFromQueryAsync<GenericTranslationEntity>(query);
            return res.FirstOrDefault();
        }
        private async Task<GenericTranslationEntity> ReadTranslationDataFromQueryAsync(string tableName, string fieldCode, string language, DbContext dbContext = null)
        {
            SetDbContext(dbContext);
            string query = "select * from " + tableName + " where LanguageCode =  '" + language + "' and FieldCode ='" + fieldCode + "'";
            var res = await genericBaseRepository.ReadDataFromQueryAsync<GenericTranslationEntity>(query);
            return res.FirstOrDefault();
        }
        public async Task<int> InsertTranslationDataFromQueryAsync(string tableName, GenericTranslationEntityDTO genericTranslation, DbContext dbContext = null)
        {
            SetDbContext(dbContext);
            string query = "INSERT INTO " + tableName + "([FieldCode],[LanguageCode], [FieldTranslation1], [FieldTranslation2], [FieldTranslation3], [FieldTranslation4])VALUES ('" + genericTranslation.FieldCode + "','" + genericTranslation.LanguageCode + "','" + genericTranslation.FieldTranslation1 + "','" + genericTranslation.FieldTranslation2 + "','" + genericTranslation.FieldTranslation3 + "','" + genericTranslation.FieldTranslation4 + "') ";
            int i = await genericBaseRepository.ExecuteQuery(query);
            return i;
        }
        public async Task<int> UpdateTranslationDataFromQueryAsync(string tableName, GenericTranslationEntity genericTranslation, DbContext dbContext = null)
        {
            SetDbContext(dbContext);
            string query = "UPDATE " + tableName + " SET [FieldTranslation1] = '" + genericTranslation.FieldTranslation1 + "' ,[FieldTranslation2] ='" + genericTranslation.FieldTranslation2 + "' ,[FieldTranslation3] = '" + genericTranslation.FieldTranslation3 + "' ,[FieldTranslation4] = '" + genericTranslation.FieldTranslation4 + "' WHERE Id =" + genericTranslation.Id;
            int i = await genericBaseRepository.ExecuteQuery(query);
            return i;
        }

        public async Task<List<T>> Translate<T>(List<T> source, TranslateEntityDTO translateEntityDTO, DbContext dContext)
        {
            // All Translate Table Lines
            List<GenericTranslationEntity> translationTable = await ReadTranslationDataFromQueryAsync(translateEntityDTO.TranslationTableName, translateEntityDTO.LanguageName, dContext);
            foreach (T SourceItem in source)
            {
                int Index = 0;
                string idColumnValue = typeof(T).GetProperty(translateEntityDTO.IdColumnName).GetValue(SourceItem).ToString();
                GenericTranslationEntity TranslationLine = translationTable.Where(e => e.FieldCode.ToString() == idColumnValue).FirstOrDefault();
                if (TranslationLine != null && TranslationLine.Id != 0)
                {
                    foreach (string TranslatedColumn in translateEntityDTO.TranslatedColumnNames)
                    {
                        Index++;
                        PropertyInfo SourceItemFields = typeof(T).GetProperty(TranslatedColumn);
                        if (SourceItemFields != null)
                        {
                            switch (Index)
                            {
                                case 1:
                                    if (!string.IsNullOrEmpty(TranslationLine.FieldTranslation1.ToString()))
                                        SourceItemFields.SetValue(SourceItem, TranslationLine.FieldTranslation1);
                                    break;
                                case 2:
                                    if (!string.IsNullOrEmpty(TranslationLine.FieldTranslation2.ToString()))
                                        SourceItemFields.SetValue(SourceItem, TranslationLine.FieldTranslation2);
                                    break;
                                case 3:
                                    if (!string.IsNullOrEmpty(TranslationLine.FieldTranslation3.ToString()))
                                        SourceItemFields.SetValue(SourceItem, TranslationLine.FieldTranslation3);
                                    break;
                                case 4:
                                    if (!string.IsNullOrEmpty(TranslationLine.FieldTranslation4.ToString()))
                                        SourceItemFields.SetValue(SourceItem, TranslationLine.FieldTranslation4);
                                    break;
                            }
                        }
                    }
                }
            }
            return source;
        }

        public async Task<T> TranslateById<T>(T source, TranslateEntityDTO translateEntityDTO, DbContext dContext)
        {
            int Index = 0;
            string idColumnValue = typeof(T).GetProperty(translateEntityDTO.IdColumnName).GetValue(source).ToString();
            GenericTranslationEntity TranslationLine = await GetByFieldCodeTranslationDataFromQueryAsync(translateEntityDTO.TranslationTableName, translateEntityDTO.LanguageName, idColumnValue, dContext);
            if (TranslationLine != null && TranslationLine.Id != 0)
            {
                foreach (string TranslatedColumn in translateEntityDTO.TranslatedColumnNames)
                {
                    Index++;
                    PropertyInfo SourceItemFields = typeof(T).GetProperty(TranslatedColumn);
                    if (SourceItemFields != null)
                    {
                        switch (Index)
                        {
                            case 1:
                                if (!string.IsNullOrEmpty(TranslationLine.FieldTranslation1.ToString()))
                                    SourceItemFields.SetValue(source, TranslationLine.FieldTranslation1);
                                break;
                            case 2:
                                if (!string.IsNullOrEmpty(TranslationLine.FieldTranslation2.ToString()))
                                    SourceItemFields.SetValue(source, TranslationLine.FieldTranslation2);
                                break;
                            case 3:
                                if (!string.IsNullOrEmpty(TranslationLine.FieldTranslation3.ToString()))
                                    SourceItemFields.SetValue(source, TranslationLine.FieldTranslation3);
                                break;
                            case 4:
                                if (!string.IsNullOrEmpty(TranslationLine.FieldTranslation4.ToString()))
                                    SourceItemFields.SetValue(source, TranslationLine.FieldTranslation4);
                                break;
                        }
                    }
                }
            }

            return source;
        }

        public async Task<GenericTranslationEntity> PrepareTranslator<T>(T source, TranslateCreatorEntityDTO translateCreatorEntityDTO, GenericTranslationEntity genricTranslationEntity, DbContext dContext = null)
        {
            return await Task.Run(() =>
            {
                int Index = 0;
                foreach (string TranslatedColumn in translateCreatorEntityDTO.TranslatedColumnNames)
                {
                    Index++;
                    PropertyInfo SourceItemFields = typeof(T).GetProperty(TranslatedColumn);
                    if (SourceItemFields != null)
                    {
                        switch (Index)
                        {
                            case 1:
                                genricTranslationEntity.FieldTranslation1 = SourceItemFields.GetValue(source);
                                break;
                            case 2:
                                genricTranslationEntity.FieldTranslation2 = SourceItemFields.GetValue(source);
                                break;
                            case 3:
                                genricTranslationEntity.FieldTranslation3 = SourceItemFields.GetValue(source);
                                break;
                            case 4:
                                genricTranslationEntity.FieldTranslation4 = SourceItemFields.GetValue(source);
                                break;
                        }
                    }
                }
                return genricTranslationEntity;
            });

        }

        public async Task<TEntityDTO> GetByIdFromCach(string key, object id, DbContext dbContext = null)
        {
            if (_cache == null)
                //not initialized
                return await GetById(id);

            Task<TEntityDTO> result;
            if (!_cache.TryGetValue(key, out result))
                await _cache.Set(key, GetById(id), CacheOptions.GetMemoryCachEntryOptions());
            result = _cache.Get(key) as Task<TEntityDTO>;

            return await result;
        }

        public async Task<DataSourceResult> GetAllFromCacheKendo(string key, DataSourceRequest requestModel, DbContext dbContext = null)
        {
            if (_cache == null)
                //not initialized
                return await GetAll(requestModel);

            Task<DataSourceResult> result;
            if (!_cache.TryGetValue(key, out result))
                await _cache.Set(key, GetAll(requestModel), CacheOptions.GetMemoryCachEntryOptions());
            result = _cache.Get(key) as Task<DataSourceResult>;

            return await result;
        }

        public async Task<List<TEntityDTO>> GetAllFromCache(string key, DbContext dbContext = null)
        {
            if (_cache == null)
                //not initialized
                return await GetAll(dbContext);

            Task<List<TEntityDTO>> result;

            if (!_cache.TryGetValue(key, out result))
                await _cache.Set(key, GetAll(dbContext), CacheOptions.GetMemoryCachEntryOptions());
            result = _cache.Get(key) as Task<List<TEntityDTO>>;

            return await result;
        }





        //Task<GenericTranslationEntity> IBaseService<TEntityDTO>.GetByFieldCodeTranslationDataFromQueryAsync(string tableName, string language, string fieldCode, DbContext dbContext)
        //{
        //    throw new NotImplementedException();
        //}
        //public async Task<int> DeleteWithTranslation(object id, string tableName, string language, DbContext dbContext = null)
        //{
        //    int deletedId = 0;
        //    SetDbContext(dbContext);
        //    deletedId = await genericBaseRepository.Delete(id);
        //    await DeleteTranslationDataFromQueryAsync(tableName, (string)id, language);
        //    return deletedId;

        //}  
        //public async Task<int> DeleteTranslationDataFromQueryAsync(string tableName, string FieldCode, string  language,  DbContext dbContext = null)
        //{
        //    SetDbContext(dbContext);
        //    string query = "delete from " + tableName + " where FieldCode = '" +FieldCode+ "'  and LanguageCode = '" + language+"'";
        //    int i = await genericBaseRepository.ExecuteQuery(query);
        //    return i;
        //}
        #endregion
    }
}
