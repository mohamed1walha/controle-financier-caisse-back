﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ERP.Common.Classes.CommonDTOs;
using ERP.Common.Classes.EFCore;
using ERP.Common.Classes.Services;
using ERP.Common.Classes.SqlReqBuilder;
using Kendo.Mvc.UI;
using Microsoft.EntityFrameworkCore;

namespace ERP.Common.Interfaces.Services
{

    public interface IGenericServiceWithTabFilterOptions : IGenericRequestExecuterService
    {
        public Task<DataSourceResult> ExecuteRequest<T>([DataSourceRequest] DataSourceRequest requestModel, string tabName, List<SqlFilterParam> externalParams);
        public Task<DataSourceResult> ExecuteRequestParams<T>([DataSourceRequest] DataSourceRequest requestModel, string tabName, List<SqlRequestParamDTO> paramList);

        public void SetTabOptions(Dictionary<string, List<SqlRequestParamDTO>> options);
        public void SetSqlRequest(string SqlRequest);
    }
    public interface IBaseService<TEntityDTO>
    where TEntityDTO : class
    {
        Task<TEntityDTO> GetById(object id, DbContext dbContext = null);
        Task<TEntityDTO> GetByIdFromCach(string key, object id, DbContext dbContext = null);
        Task<DataSourceResult> GetAll(DataSourceRequest requestModel, DbContext dbContext = null);

        Task<DataSourceResult> GetAllFromCacheKendo(string key, DataSourceRequest requestModel, DbContext dbContext = null);

        //Task<DataSourceResult> GetAllWithExternalParams(DataSourceRequest requestModel, List<SqlFilterParam> externalParams);
        Task<DataSourceResult> GetAll<ResultType>(DataSourceRequest requestModel, DbContext dbContext = null);
        Task<List<TEntityDTO>> GetAllFromCache(string key, DbContext dbContext = null);
        Task<List<TEntityDTO>> GetAll(DbContext dbContext = null);
        Task<int> Delete(object id, DbContext dbContext = null);
        Task<TEntityDTO> Edit(TEntityDTO dto, object id, DbContext dbContext = null);
        Task<TEntityDTO> Create(TEntityDTO dto, DbContext dbContext = null);
        Task<List<TEntityDTO>> CreateList(List<TEntityDTO> dto, DbContext dbContext = null);
        Task<int> DeleteMany(List<object> idsToDelete, DbContext dbContext = null);
        public Task<DataSourceResult> ManageGroupings<T>(DataSourceResult unGroupeData, DataSourceRequest requestModel, bool isSqlRequest, DbContext dbContext = null);
        public Task<int> ExecuteQuery(string queryString, DbContext dbContext = null);
        Task<List<TEntityDTO>> ReadDataFromQueryAsync(string queryString, DbContext dbContext = null);

        Task<DataSourceResult> GetAllWithTranslation(DataSourceRequest requestModel, TranslateEntityDTO translateEntityDTO, DbContext dContext = null);
        Task<List<TEntityDTO>> GetAllWithTranslation(TranslateEntityDTO translateEntityDTO, DbContext dbContext = null);
        Task<List<T>> Translate<T>(List<T> source, TranslateEntityDTO translateEntityDTO, DbContext dContext);
        Task<GenericTranslationEntity> PrepareTranslator<T>(T source, TranslateCreatorEntityDTO translateCreatorEntityDTO, GenericTranslationEntity genricTranslationEntity, DbContext dContext = null);

        Task<int> InsertTranslationDataFromQueryAsync(string tableName, GenericTranslationEntityDTO genericTranslation, DbContext dbContext = null);

        Task<int> UpdateTranslationDataFromQueryAsync(string tableName, GenericTranslationEntity genericTranslation, DbContext dbContext = null);
        Task<TEntityDTO> CreateWithTranslate(TEntityDTO dto, TranslateCreatorEntityDTO translateCreatorEntityDto, string language, DbContext dbContext = null);


        Task<TEntityDTO> EditWithTranslation(TEntityDTO dto, TranslateCreatorEntityDTO translateCreatorEntityDto, object id, string language, DbContext dbContext = null);

        //Task<int> DeleteTranslationDataFromQueryAsync(string tableName, string FieldCode, string language, DbContext dbContext = null);
        //Task<int> DeleteWithTranslation(object id, string tableName, string language, DbContext dbContext = null);
        Task<TEntityDTO> GetByIdWithTranslation(int id, TranslateEntityDTO translateEntityDTO, DbContext dbContext = null);

        Task<T> TranslateById<T>(T source, TranslateEntityDTO translateEntityDTO, DbContext dContext);
        //Task<GenericTranslationEntity> GetByFieldCodeTranslationDataFromQueryAsync(string tableName, string language, string fieldCode, DbContext dbContext = null);

        SaveImageDTO SaveImage(string photo, string folder);
    }
}
