﻿using System;

namespace DTO
{
    public class RolesDTO
    {
        public int Id { get; set; }
        public string Label { get; set; }
        public string Description { get; set; }
    }
}
