﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class CoffreDTO
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Libelle { get; set; }
        public decimal? Montant { get; set; }
        public decimal? MontantConvertie { get; set; }
        public decimal? MontantAlimenterExcep { get; set; }
        public string ModesPaieLibelle { get; set; }

        //public virtual ICollection<CategorieModePaieCoffreDTO> CategorieModepaieCoffre { get; set; }
        //public virtual ICollection<CoffrealimentationDTO> Coffrealimentation { get; set; }
        //public virtual ICollection<CoffresortieDTO> Coffresortie { get; set; }
        //public virtual ICollection<PiedecheDTO> Piedeche { get; set; }
        public List<int> ModePaie { get; set; }

    }
}
