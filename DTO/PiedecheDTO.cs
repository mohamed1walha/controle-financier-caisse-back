﻿using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class PiedecheDTO
    {
        public int Id { get; set; }
        public decimal? Montant { get; set; }
        public DateTime? DateCreation { get; set; }
        public string Source { get; set; }
        public decimal? MontantCompte { get; set; }
        public decimal? MontantConvertie { get; set; }
        public decimal? MontantAlimenter { get; set; }
        public decimal? EspeceVerse { get; set; }
        public decimal? EspeceCash { get; set; }
        public decimal? MontantTotal { get; set; }
        public decimal? Ecart { get; set; }
        public string Commentaire { get; set; }
        public decimal? Depence { get; set; }
        public int? StatusComptage { get; set; }
        public int? ModepaieId { get; set; }
        public int? JourcaisseId { get; set; }
        public int? MotifId { get; set; }
        public int? CoffreId { get; set; }
                public string AgentComptage { get; set; }
        public DateTime? Datevalidation { get; set; }



        public virtual EtablissementDTO Etablissement { get; set; }
        public virtual Coffre Coffre { get; set; }
        public virtual ModepaieDTO Modepaie { get; set; }
        public virtual MotifDTO Motif { get; set; }
        public virtual StatusDTO Status { get; set; }
        public  JoursCaiseDTO Jourcaisse { get; set; }
        public string GpeModepaie { get; set; }

        public List<BilletDetailComptageDTO> billetDetailComptageDTO { get; set; }

    }
}
