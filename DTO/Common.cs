using System;
using System.Collections.Generic;


namespace DTO
{
    public class Filters : Dictionary<string, string>
    {
    }

    public class PagedResults
    {
        public int Page { get; set; }
        public int TotalPages { get; set; }
        public int TotalRecords { get; set; }
        public bool HasPreviousPage => Page > 1;
        public bool HasNextPage => Page < TotalPages;
        public object Records { get; set; }

        public PagedResults()
        {
        }

        public PagedResults(object items, int count, int pageIndex, int pageSize)
        {
            Page = pageIndex;
            TotalPages = (int)Math.Ceiling(count / (double)pageSize);
            Records = items;
            TotalRecords = count;
        }

    }

}
