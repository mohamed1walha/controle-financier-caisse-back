﻿using System;

namespace DTO
{
    public class CaissonDTO
    {
        public int Id { get; set; }
        public string EtCaisson { get; set; }
        public string EtLibelle { get; set; }
        public string EtEtablissement { get; set; }
        public string EtVendeur { get; set; }
        public DateTime? EtLastaction { get; set; }
        public string EtLastcaisse { get; set; }
        public DateTime? EtDateintegr { get; set; }
        public string EtCreateur { get; set; }
        public string EtUtilisateur { get; set; }
        public DateTime? EtDatecreation { get; set; }
        public DateTime? EtDatemodif { get; set; }
    }
}
