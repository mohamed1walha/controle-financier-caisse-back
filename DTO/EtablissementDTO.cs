﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class EtablissementDTO
    {
        public int Id { get; set; }
        public string EtEtablissement { get; set; }
        public string EtDevise { get; set; }
        public string EtLibelle { get; set; }
        public string EtAdresse1 { get; set; }
        public string EtAdresse2 { get; set; }
        public string EtAdresse3 { get; set; }
        public string EtCodepostal { get; set; }
        public string EtVille { get; set; }
        public string EtPays { get; set; }
        public string EtLangue { get; set; }
        public string EtTelephone { get; set; }
    }
}
