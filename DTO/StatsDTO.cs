﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class StatsDTO
    {
        public int TotJoursCaisses { get; set; }
        public int TotEnvEnAttente { get; set; }
        public int TotEnvReceptionne { get; set; }
        public int TotEnvCompte { get; set; }

    }
}
