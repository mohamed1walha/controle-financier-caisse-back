﻿#nullable disable
using Entity;
using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;

namespace DTO
{
    public class GenerationComptableEnteteDTO
    {
        public ParamsComptableDTO ENTETE1 { get; set; }
        public ParamsComptableDTO ENTETE2 { get; set; }
        public ParamsComptableDTO ENTETE3 { get; set; }
        public ParamsComptableDTO ENTETE4 { get; set; }

            public GenerationComptableEnteteDTO()
            {
                ENTETE1 = new ParamsComptableDTO(41, CreerChaineEspaces(41));
                ENTETE2 = new ParamsComptableDTO(8, CreerChaineEspaces(8));
                ENTETE3 = new ParamsComptableDTO(74, CreerChaineEspaces(74));
                ENTETE4 = new ParamsComptableDTO(70, CreerChaineEspaces(70));
            }

        static string CreerChaineEspaces(int nbrEspaces)
        {
            return new string(' ', nbrEspaces);

        }

    }

    public class GenerationComptableDTO
    {
        public ParamsComptableDTO JOURNAL { get; set; }
        public ParamsComptableDTO DATECOMPTABLE { get; set; }
        public ParamsComptableDTO TYPE_PIECE { get; set; }
        public ParamsComptableDTO GENERAL { get; set; }
        public ParamsComptableDTO TYPE_CPTE { get; set; }
        public ParamsComptableDTO UXILIAIRE_OU_SECTION { get; set; }
        public ParamsComptableDTO REFINTERNE { get; set; }
        public ParamsComptableDTO LIBELLE { get; set; }
       
        public ParamsComptableDTO MODEPAIE { get; set; }
        
        public ParamsComptableDTO ECHEANCE { get; set; }
        public ParamsComptableDTO SENS { get; set; }
        public ParamsComptableDTO MONTANT1 { get; set; }
        public ParamsComptableDTO TYPE_ECRITURE { get; set; }
        public ParamsComptableDTO NUMERO { get; set; }


        public ParamsComptableDTO DEVISE { get; set; }
        public ParamsComptableDTO TAUXDEV { get; set; }
        public ParamsComptableDTO CODEMONTANT { get; set; }
        public ParamsComptableDTO MONTANT2 { get; set; }
        public ParamsComptableDTO MONTANT3 { get; set; }
        public ParamsComptableDTO ETABLISSEMENT { get; set; }
        public ParamsComptableDTO AXE { get; set; }
        public ParamsComptableDTO NUMECHE { get; set; }
        public ParamsComptableDTO REFEXTERNE { get; set; }

        public ParamsComptableDTO DATEREFEXTERNE { get; set; }
        public ParamsComptableDTO DATECREATION { get; set; }
        public ParamsComptableDTO SOCIETE { get; set; }
        public ParamsComptableDTO AFFAIRE { get; set; }
        public ParamsComptableDTO DATETAUXDEV { get; set; }
        public ParamsComptableDTO ECRANOUVEAU { get; set; }
        public ParamsComptableDTO QTE1 { get; set; }
        public ParamsComptableDTO QTE2 { get; set; }
        public ParamsComptableDTO QUALIFQTE1 { get; set; }

        public ParamsComptableDTO QUALIFQTE2 { get; set; }
        public ParamsComptableDTO REFLIBRE { get; set; }
        public ParamsComptableDTO TVAENCAISSEMENT { get; set; }
        public ParamsComptableDTO REGIMETVA { get; set; }
        public ParamsComptableDTO TVA { get; set; }
        public ParamsComptableDTO TPF { get; set; }
        public ParamsComptableDTO CONTREPARTIEGEN { get; set; }
        public ParamsComptableDTO CONTREPARTIEAUX { get; set; }
        public ParamsComptableDTO REFPOINTAGE { get; set; }


        public ParamsComptableDTO SOUSPLAN1 { get; set; }


        public GenerationComptableDTO()
        {
            JOURNAL = new ParamsComptableDTO(3, CreerChaineEspaces(3));
            DATECOMPTABLE = new ParamsComptableDTO(8, CreerChaineEspaces(8));
            TYPE_PIECE = new ParamsComptableDTO(2, CreerChaineEspaces(2));
            GENERAL = new ParamsComptableDTO(7, CreerChaineEspaces(17));
            TYPE_CPTE = new ParamsComptableDTO(1, CreerChaineEspaces(1));
            UXILIAIRE_OU_SECTION = new ParamsComptableDTO(17, CreerChaineEspaces(17));
            REFINTERNE = new ParamsComptableDTO(35, CreerChaineEspaces(35));
            LIBELLE = new ParamsComptableDTO(35, CreerChaineEspaces(35));
            MODEPAIE = new ParamsComptableDTO(3, CreerChaineEspaces(3));
            ECHEANCE = new ParamsComptableDTO(8, CreerChaineEspaces(8));
            SENS = new ParamsComptableDTO(1, CreerChaineEspaces(1));
            MONTANT1 = new ParamsComptableDTO(20, CreerChaineEspaces(20));
            TYPE_ECRITURE = new ParamsComptableDTO(1, CreerChaineEspaces(1));
            NUMERO = new ParamsComptableDTO(8, CreerChaineEspaces(8));
            DEVISE = new ParamsComptableDTO(3, CreerChaineEspaces(3));
            TAUXDEV = new ParamsComptableDTO( 10, CreerChaineEspaces(10));
            CODEMONTANT = new ParamsComptableDTO(3, CreerChaineEspaces(3));
            MONTANT2 = new ParamsComptableDTO(20, CreerChaineEspaces(20));
            MONTANT3 = new ParamsComptableDTO(20, CreerChaineEspaces(20));
            ETABLISSEMENT = new ParamsComptableDTO(3, CreerChaineEspaces(3));
            AXE = new ParamsComptableDTO(2, CreerChaineEspaces(2));
            NUMECHE = new ParamsComptableDTO(2, CreerChaineEspaces(2));
            REFEXTERNE = new ParamsComptableDTO(35, CreerChaineEspaces(35));
            DATEREFEXTERNE = new ParamsComptableDTO(8, CreerChaineEspaces(8));
            DATECREATION = new ParamsComptableDTO(8, CreerChaineEspaces(8));
            SOCIETE = new ParamsComptableDTO(3, CreerChaineEspaces(3));
            AFFAIRE = new ParamsComptableDTO(17, CreerChaineEspaces(17));
            DATETAUXDEV = new ParamsComptableDTO( 8, CreerChaineEspaces(8));
            ECRANOUVEAU = new ParamsComptableDTO(3, CreerChaineEspaces(3));
            QTE1 = new ParamsComptableDTO(20, CreerChaineEspaces(20));
            QTE2 = new ParamsComptableDTO(20, CreerChaineEspaces(20));
            QUALIFQTE1 = new ParamsComptableDTO(3, CreerChaineEspaces(3));
            QUALIFQTE2 = new ParamsComptableDTO(3, CreerChaineEspaces(3));
            REFLIBRE = new ParamsComptableDTO(35, CreerChaineEspaces(35));
            TVAENCAISSEMENT = new ParamsComptableDTO( 1, CreerChaineEspaces(1));
            REGIMETVA = new ParamsComptableDTO(3, CreerChaineEspaces(3));
            TVA = new ParamsComptableDTO(3, CreerChaineEspaces(3));
            TPF = new ParamsComptableDTO(3, CreerChaineEspaces(3));
            CONTREPARTIEGEN = new ParamsComptableDTO(17, CreerChaineEspaces(17));
            CONTREPARTIEAUX = new ParamsComptableDTO(17, CreerChaineEspaces(17));
            REFPOINTAGE = new ParamsComptableDTO(1, CreerChaineEspaces(1));
            SOUSPLAN1 = new ParamsComptableDTO(17, CreerChaineEspaces(17));
        }

        static string CreerChaineEspaces(int nbrEspaces)
        {
            return new string(' ', nbrEspaces);

        }
    }

    public class ParamsComptableDTO
    {
        public int LENGTH { get; set; }
        public string VALUE { get; set; }

        public ParamsComptableDTO( int LENGTH, string VALUE)
        {
            this.LENGTH = LENGTH;
            this.VALUE = VALUE;
        }
    }
}