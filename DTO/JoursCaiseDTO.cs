using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class JoursCaiseDTO
    {      
        public int Id { get; set; }
        public int GjcNumzcaisse { get; set; }
        public string Code { get; set; }

        public string GjcCaisse { get; set; }
        public string GjcHeureouv { get; set; }
        public DateTime? GjcDateferme { get; set; }
        public string GjcHeureferme { get; set; }
        public string GjcUserferme { get; set; }
        public string GjcEtat { get; set; }
        public DateTime GjcDateouv { get; set; }
        public string GjcCaisselabel { get; set; }
        public string GjcEtablissementcode { get; set; }
        public string GjcEtablissementlabel { get; set; }
        public decimal? MontantCaisse { get; set; }
        public int? GjcEtablissementId { get; set; }
        public DateTime? ReceptionDate { get; set; }
        public DateTime? ValidationDateDebut { get; set; }
        public DateTime? ValidationDateFin { get; set; }

        public int? EmetteurId { get; set; }
        public int? CahuffeurId { get; set; }
        public int? StatusReception { get; set; }
        public DateTime? DateComptage { get; set; }
        public string AgentComptage { get; set; }
        public string Commentaire { get; set; }
        public int? StatusComtage { get; set; }
        public DateTime? DateCreate { get; set; }
        public string Comments { get; set; }
        public int? MotifId { get; set; }
        public bool? IsManuel { get; set; }
        public int? RefExterne { get; set; }


        public bool? IsEcart { get; set; }

        public virtual EtablissementDTO GjcEtablissement { get; set; }
        public virtual StatusDTO statusReceptionObj { get; set; }
        public virtual StatusDTO statusComptageObj { get; set; }
        public virtual ChauffeurDTO Chauffeur { get; set; }
        public virtual EmetteurDTO Emetteur { get; set; }


    }
}
