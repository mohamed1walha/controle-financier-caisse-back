﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class CategorieModePaieCoffreDTO
    {
        public int Id { get; set; }
        public int? ModePaieId { get; set; }
        public int? CoffreId { get; set; }
    }
}
