using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class ModepaieDTO
    {
        public int Id { get; set; }
        public string MpModepaie { get; set; }
        public string MpLibelle { get; set; }
        public string LibelleAppFinance { get; set; }
        public DateTime? DateModification { get; set; }
        public DateTime? DateCreation { get; set; }
        public bool? Actif { get; set; }
        public int? DeviseId { get; set; }
        public string DeviseLabel { get; set; }
        public bool? ShowCalculette { get; set; }
        public bool? IsCash { get; set; }


    }
}
