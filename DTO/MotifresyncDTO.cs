﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class MotifresyncDTO
    {
        public int Id { get; set; }
        public string MotifReSyncDescription { get; set; }
    }
}
