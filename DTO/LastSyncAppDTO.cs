﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class LastSyncAppDTO
    {
        public int Id { get; set; }
        public DateTime? Lastsync { get; set; }
    }
}
