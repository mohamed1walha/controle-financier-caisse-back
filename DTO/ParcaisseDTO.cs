﻿using System;

namespace DTO
{
    public class ParcaisseDTO
    {
        public int Id { get; set; }
        public string GpkEtablissement { get; set; }
        public string GpkCaisse { get; set; }
        public string GpkLibelle { get; set; }
    }
}
