﻿using System.Collections.Generic;
using System.Data;

namespace DTO
{
    public class OperationResult
    {
        public bool ErrorOccured { get; set; }
        public string Message { get; set; }

        public object Data { get; set; }

        public OperationResult()
        {
            Message = string.Empty;
            ErrorOccured = false;
            Data = string.Empty;
        }
        public static OperationResult of(bool isSuccess, string message = null, object data = null)
        {
            return new OperationResult(!isSuccess, message, data);
        }

        public OperationResult(bool errorOccured, string message = null, object data = null)
        {
            Message = message;
            ErrorOccured = errorOccured;
            Data = data;
        }

        public class CommandDTO
        {
            public List<CommandParameters> parameters { get; set; }
            public CommandType Type { get; set; }
            public string Text { get; set; }
        }
        public class CommandParameters
        {
            public string ParamName { get; set; }
            public string ParamValue { get; set; }
            public SqlDbType OutPutParamType { get; set; }
            public bool OutputParam { get; set; }
        }


    }
    public class GenericOperationResult
    {
        public bool SuccessResult { get; set; }
        public string Message { get; set; }
        public string Value1 { get; set; }
        public string Value2 { get; set; }
        public GenericOperationResult(bool successResult, string message, string value1, string value2)
        {
            Message = message;
            SuccessResult = successResult;
            Value1 = value1;
            Value2 = value2;
        }
    }
}