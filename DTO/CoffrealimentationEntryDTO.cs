﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class CoffrealimentationEntryDTO
    {
        public int Id { get; set; }
        public DateTime? AlimentationDate { get; set; }
        public int? CoffreId { get; set; }
        public decimal? Montantcompter { get; set; }
        public decimal? MontantAlimenter { get; set; }
        public decimal? MontantConvertie { get; set; }
        public string Username { get; set; }
        public string Motif { get; set; }

        public virtual List<int> PiedecheIds { get; set; }
    }
}
