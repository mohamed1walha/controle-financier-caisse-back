using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class ReportDTO
    {      
        public string Etablissement { get; set; }
        public string Caisse { get; set; }
        public string ReferenceZ { get; set; }
        public string DateOuverture { get; set; }
        public string HeureOuverture { get; set; }
        public string ModePaiement { get; set; }
        public decimal MontantTotal { get; set; }
        public decimal MontantCompte { get; set; }
        public decimal MontantVerse { get; set; }
        public decimal Depense { get; set; }
        public string Designation { get; set; }
        public decimal Ecart { get; set; }
        public string StatutCode { get; set; }
        public string Statut { get; set; }
        public string StatutEN { get; set; }
    }
}
