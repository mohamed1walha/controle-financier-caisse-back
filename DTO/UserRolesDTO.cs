﻿using System;

namespace DTO
{
    public class UserRolesDTO
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int RoleId { get; set; }
    }
}
