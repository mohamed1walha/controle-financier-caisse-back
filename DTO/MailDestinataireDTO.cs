﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
#nullable disable
using System;
using System.Collections.Generic;

namespace DTO
{
    public class MailDestinataireDTO
    {
        public int Id { get; set; }
        public string Libelle { get; set; }
        public string AdresseMail { get; set; }
    }
}