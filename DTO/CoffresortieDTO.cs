﻿using System;
using System.Collections.Generic;

namespace DTO
{
    public class CoffresortieDTO
    {
        public int Id { get; set; }
        public decimal? Montant { get; set; }
        public decimal? MontantConvertie { get; set; }
        public DateTime? SortieDate { get; set; }
        public string Commentaire { get; set; }
        public bool? Justificatif { get; set; }
        public byte[] PieceJustificatif { get; set; }
        public int? BanqueId { get; set; }
        public string BanqueLibelle { get; set; }
        public int? CoffreId { get; set; }
        public string CoffreLibelle { get; set; }
        public int? TypeFraisId { get; set; }
        public string TypeFraisLibelle { get; set; }
        public short? TypeSortie { get; set; }
        public string TypeSortieLibelle { get; set; }
        public string Username { get; set; }
        public string SiteConcernee { get; set; }
        public string ServiceConcernee { get; set; }
        public string JustificatifName { get; set; }
        public string Extention { get; set; }
        public int? DeviseId { get; set; }
        public virtual CoffreDTO Coffre { get; set; }

        public List<BilletDetailSortieDTO> billetDetailSortieDTOs { get; set; }



    }
}