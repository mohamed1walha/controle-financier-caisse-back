﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
#nullable disable
using System;
using System.Collections.Generic;

namespace DTO
{
    public class ReportBilletRequestDTO
    {
        public string? StartDate { get; set; }
        public string? EndDate { get; set; }
        public List<string?> EtablissmentIds { get; set; }
        public List<string?> CaisseIds { get; set; }
        

    }
}