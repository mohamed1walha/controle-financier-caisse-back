﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public  class CoffrealimentationDTO
    {
        public int Id { get; set; }
        public DateTime? AlimentationDate { get; set; }
        public int? CoffreId { get; set; }
        public decimal? Montantalimenter { get; set; }
        public decimal? MontantConvertie { get; set; }
        public string Username { get; set; }
        public int? PiedecheId { get; set; }
        public string CoffreLibelle { get; set; }
        public string Motif { get; set; }
        public int? ModePaieId { get; set; }

        public List<BilletDetailAliExcepDTO> billetDetailAliExcepDTOs { get; set; }

        //public virtual CoffreDTO Coffre { get; set; }
    }
}