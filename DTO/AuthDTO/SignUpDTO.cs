﻿

using AU.DTO.AuthDTO;
using DTO;

namespace AU.DTO
{
    public class SignUpDTO : LoginDTO
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string ConfirmPassword { get; set; }
    }
}