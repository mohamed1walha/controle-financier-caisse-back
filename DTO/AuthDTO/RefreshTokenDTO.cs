﻿using DTO;

namespace AU.DTO.AuthDTO
{
    public class RefreshTokenDTO
    {
        public Token Token { get; set; }
    }
}
