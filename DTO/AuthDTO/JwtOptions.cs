﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Text;

namespace AU.API
{
    public class JwtOptions
    {
        public string Issuer { get; set; }
        public string Audience { get; set; }
        public string Subject { get; set; }
        public byte[] AccessSecret { get; set; }
        public byte[] RefreshSecret { get; set; }
        public DateTime IssuedAt => DateTime.UtcNow;

        private static int AcessValidForMinutes = 3600;
        private static int RefreshValidForMinutes = 43200;
        public TimeSpan AccessValidFor { get; set; } = TimeSpan.FromMinutes(AcessValidForMinutes);
        public TimeSpan RefreshValidFor { get; set; } = TimeSpan.FromMinutes(RefreshValidForMinutes);
        
        public DateTime NotBefore => DateTime.UtcNow;
        public DateTime AccessExpiration => IssuedAt.AddMinutes(AcessValidForMinutes);
        public DateTime RefreshExpiration => IssuedAt.AddMinutes(RefreshValidForMinutes);
        public SigningCredentials AccessSigningCredentials { get; set; }
        public SigningCredentials RefreshSigningCredentials { get; set; }
    }
}
