﻿
namespace DTO
{
    public class GridStateDTO
    {
        public int Page { get; set; }
        public int PageSize { get; set; }
        public string SortField { get; set; }
        public string SortDirection { get; set; }
        public string SearchTerm { get; set; }
        public object Filters { get; set; }
    }
}