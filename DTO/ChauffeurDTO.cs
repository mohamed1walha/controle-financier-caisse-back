﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class ChauffeurDTO
    {
        public int Id { get; set; }
        public string ChauffeurLibelle { get; set; }
    }
}
