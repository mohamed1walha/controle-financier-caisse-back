﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DeviseDTO
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Label { get; set; }
        public decimal? TauxChange { get; set; }
    }
}
