﻿using System;

namespace DTO
{
    public class ChangePasswordDTO
    {
        public string OldPassword { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
    }
}
