﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
#nullable disable
using System;
using System.Collections.Generic;

namespace Entity
{
    public partial class CompteDepense
    {
        public CompteDepense()
        {
            EntetePrelevement = new HashSet<EntetePrelevement>();
        }

        public int Id { get; set; }
        public string Libelle { get; set; }
        public int? EtablissementId { get; set; }
        public string CompteCaisseG { get; set; }
        public string JOURNAL { get; set; }
        public decimal? Solde { get; set; }
        public decimal? MontantLimite { get; set; }

        public virtual Etablissement Etablissement { get; set; }
        public virtual ICollection<EntetePrelevement> EntetePrelevement { get; set; }
    }
}