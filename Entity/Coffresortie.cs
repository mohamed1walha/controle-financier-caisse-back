﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
#nullable disable
using System;
using System.Collections.Generic;

namespace Entity
{
    public partial class Coffresortie
    {
        public int Id { get; set; }
        public decimal? Montant { get; set; }
        public DateTime? SortieDate { get; set; }
        public string Commentaire { get; set; }
        public bool? Justificatif { get; set; }
        public byte[] PieceJustificatif { get; set; }
        public int? BanqueId { get; set; }
        public int? CoffreId { get; set; }
        public int? TypeFraisId { get; set; }
        public short? TypeSortie { get; set; }
        public string Username { get; set; }
        public string SiteConcernee { get; set; }
        public string ServiceConcernee { get; set; }
        public string JustificatifName { get; set; }
        public string Extention { get; set; }
        public int? ModePaieId { get; set; }
        public int? DeviseId { get; set; }

        public virtual Banque Banque { get; set; }
        public virtual Coffre Coffre { get; set; }
        public virtual Devise Devise { get; set; }
        public virtual Typefrais TypeFrais { get; set; }
    }
}