﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
#nullable disable
using System;
using System.Collections.Generic;

namespace Entity
{
    public partial class Jourscaisse
    {
        public Jourscaisse()
        {
            Piedeche = new HashSet<Piedeche>();
        }

        public int Id { get; set; }
        public int GjcNumzcaisse { get; set; }
        public string GjcCaisse { get; set; }
        public string GjcHeureouv { get; set; }
        public DateTime GjcDateferme { get; set; }
        public string GjcHeureferme { get; set; }
        public string GjcUserferme { get; set; }
        public string GjcEtat { get; set; }
        public DateTime GjcDateouv { get; set; }
        public string GjcCaisselabel { get; set; }
        public string GjcEtablissementlabel { get; set; }
        public decimal? MontantCaisse { get; set; }
        public int? GjcEtablissementId { get; set; }
        public DateTime? ReceptionDate { get; set; }
        public DateTime? ValidationDateDebut { get; set; }
        public int? EmetteurId { get; set; }
        public int? CahuffeurId { get; set; }
        public int? StatusReception { get; set; }
        public DateTime? DateComptage { get; set; }
        public string AgentComptage { get; set; }
        public string Commentaire { get; set; }
        public int? StatusComtage { get; set; }
        public DateTime? DateCreate { get; set; }
        public DateTime? ValidationDateFin { get; set; }
        public int? MotifId { get; set; }
        public string Comments { get; set; }
        public bool? IsManuel { get; set; }
        public int? RefExterne { get; set; }
        public bool? IsEcart { get; set; }

        public virtual Chauffeur Cahuffeur { get; set; }
        public virtual Emetteur Emetteur { get; set; }
        public virtual Etablissement GjcEtablissement { get; set; }
        public virtual ICollection<Piedeche> Piedeche { get; set; }
    }
}