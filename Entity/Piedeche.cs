﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
#nullable disable
using System;
using System.Collections.Generic;

namespace Entity
{
    public partial class Piedeche
    {
        public int Id { get; set; }
        public decimal? Montant { get; set; }
        public DateTime? DateCreation { get; set; }
        public string Source { get; set; }
        public decimal? MontantCompte { get; set; }
        public decimal? EspeceVerse { get; set; }
        public decimal? EspeceCash { get; set; }
        public decimal? MontantTotal { get; set; }
        public decimal? Ecart { get; set; }
        public string Commentaire { get; set; }
        public decimal? Depence { get; set; }
        public int? StatusComptage { get; set; }
        public int? ModepaieId { get; set; }
        public int? JourcaisseId { get; set; }
        public int? MotifId { get; set; }
        public int? CoffreId { get; set; }
        public string AgentComptage { get; set; }
        public decimal? MontantAlimenter { get; set; }
        public DateTime? Datevalidation { get; set; }

        public virtual Coffre Coffre { get; set; }
        public virtual Jourscaisse Jourcaisse { get; set; }
        public virtual Modepaie Modepaie { get; set; }
        public virtual Motif Motif { get; set; }
    }
}