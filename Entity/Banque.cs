﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
#nullable disable
using System;
using System.Collections.Generic;

namespace Entity
{
    public partial class Banque
    {
        public Banque()
        {
            Coffresortie = new HashSet<Coffresortie>();
        }

        public int Id { get; set; }
        public string Libelle { get; set; }

        public virtual ICollection<Coffresortie> Coffresortie { get; set; }
    }
}